<html>
   <head></head>
   <body>
      <p style="margin-top:10px">
         <span style="color:#4485B8;margin-top:10px;margin-bottom:10px;">Hi ${approover_name}</span><br/>
      <div style="margin-bottom:10px;">
          <#if gender == 'male'>
           Mr. 
           <#else>
           Miss. 
           </#if> <span style="color:green"><b>${emp_name}</b></span> has been created a new Expense type for payment advice. Details are given below 
        <table border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;border:none;margin-top:10px;">
               <tbody>
                     <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:108.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Name<u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black"> ${sub_category_name}<u></u><u></u></span></p>
                        </td>
                     </tr>
                          <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:108.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Expense<u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black"> ${category_name}<u></u><u></u></span></p>
                        </td>
                     </tr>                 
                  </tbody>
               </table>
               <div>
               </p>
               
                <p>
               <a style="color:green" href="${subCategoryApproveUrl}/${approval_status}/${id}/${empId}/subCategory">Approve</a>
               &nbsp;<a style="color:red" href="${subCategoryApproveUrl}/${reject_status}/${id}/${empId}/subCategory">Reject</a>
               </p>
                
   </body>
</html>