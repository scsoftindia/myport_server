<html>
   <head></head>
   <body>
      <p style="margin-top:10px">
         <span>Hi ${emp_name}</span><br/><br/>
      <div style="margin-bottom:10px;">
        <span ><b>${approover_name}</b></span> has ${status} the below payment advice <#if aprrovalStatusId == 7 || aprrovalStatusId == 8>${forwarded_by}</#if> <#if isApproved == true && (aprrovalStatusId == 2 || aprrovalStatusId == 8)>and it has been forwarded to Finance Department</#if> . Details are given below :
        <table border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;border:none;margin-top:10px;">
               <tbody>
                 <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:150.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Payment Advice Created on <u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black">-  <#if (created_on)??>${created_on?datetime}</#if><u></u><u></u></span></p>
                        </td>
                     </tr> 
                     <#if isApproved == true> 
                 <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:150.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">   Payment Advice Approved by <u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black"> - ${first_approover_name}<u></u><u></u></span></p>
                        </td>
                     </tr> 
                 <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:150.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">      Date of Approval  <u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black"> - ${approved_date?datetime}<u></u><u></u></span></p>
                        </td>
                     </tr>
                     </#if>
                     <#if isApproved == false> 
                 <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:150.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">   Payment Advice Rejected by <u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black"> - ${approover_name}<u></u><u></u></span></p>
                        </td>
                     </tr> 
                 <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:150.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">      Date of Rejection  <u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black"> - ${rejected_date?datetime}<u></u><u></u></span></p>
                        </td>
                     </tr>
                     </#if>  
                          <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:150.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Expense<u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black"> - ${expense_name}<u></u><u></u></span></p>
                        </td>
                     </tr> 
                 <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:150;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Description <u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black"> - <#if (description)??> &nbsp;${description}</#if><u></u><u></u></span></p>
                        </td>
                     </tr>
                     <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:150.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Expense Type<u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black"> - ${expense_type_name}<u></u><u></u></span></p>
                        </td>
                     </tr>     
                       <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:150.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Vendor<u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black"> - ${vendor_name}<u></u><u></u></span></p>
                        </td>
                     </tr>  
                     <#if (bill_date)??>     
                      <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:150.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Bill Date<u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black"> - <#if (bill_date)??>&nbsp;${bill_date?date}</#if><u></u><u></u></span></p>
                        </td>
                     </tr>
                    </#if>   
                      <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:150.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Bill Number <u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black">- <#if (bill_number)??> &nbsp;${bill_number}</#if><u></u><u></u></span></p>
                        </td>
                     </tr>  
                      <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:150.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Bill Amount <u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black">-  &nbsp;${bill_amount}<u></u><u></u></span></p>
                        </td>
                     </tr> 
                         <#if (cheque_number)??>
                       <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:150.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Cheque Number <u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black">-  &nbsp;${cheque_number}<u></u><u></u></span></p>
                        </td>
                     </tr>
                     </#if> 
                     <#if (currency_type)??>
                       <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:150.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Currency Type <u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black">-  &nbsp;${currency_type}<u></u><u></u></span></p>
                        </td>
                     </tr>
                     </#if>        
                  </tbody>
               </table>
               <div>
               </p>
               <p>
                  <#if isApprovalToFinance>
               <a style="color:green" href="${adviceApproveUrl}/${PROCESSED_BY_FINANCE}/${empId}/${paymentAdviceId}/advice">Approve</a>
               <a style="color:red" href="${adviceApproveUrl}/${REJECTED_BY_FINANCE}/${empId}/${paymentAdviceId}/advice">Reject</a>
               </#if>
               </p>
                
   </body>
</html>