<html>
   <head></head>
   <body>
      <p style="margin-top:10px">
         <span style="color:#4485B8;margin-top:10px;margin-bottom:10px;">Hi ${supevisor_name}</span><br/>
      <div style="margin-bottom:10px;">
          <#if gender == 'male'>
           Mr. 
           <#else>
           Miss. 
           </#if> <span style="color:green"><b>${emp_name}</b></span> has been requested for a leave. Details are given below 
      <table border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;border:none;margin-top:10px;">
      <tbody>
         <tr>
            <td width="527" valign="top" style="width:395.6pt;padding:0in 5.4pt 0in 5.4pt">
               <h5 style="color:grey">Leave Details</h5>
               <table border="0" cellspacing="0" cellpadding="0" style="margin-left:1.85pt;border-  collapse:collapse">
                  <tbody>
                     <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:108.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Leave Type<u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black"> ${leave_type}<u></u><u></u></span></p>
                        </td>
                     </tr>
                          <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:108.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">No of Days<u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black"> ${no_of_days}<u></u><u></u></span></p>
                        </td>
                     </tr>
                     <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:108.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">From Date<u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black"> ${from_date?date}<u></u><u></u></span></p>
                        </td>
                     </tr>
                     <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:108.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">To Date<u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black"> ${to_date?date}<u></u><u></u></span></p>
                        </td>
                     </tr>
                     <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:108.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Reason<u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black"> <#if reason??> ${reason}</#if><u></u><u></u></span></p>
                        </td>
                     </tr>
                       <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:108.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Leaves Remaining<u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black"> ${leaves_remaining}<u></u><u></u></span></p>
                        </td>
                     </tr>
                        <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:108.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Leaves Taken<u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black"> ${leaves_taken}<u></u><u></u></span></p>
                        </td>
                     </tr>
                  </tbody>
              </table>
                  </td>
                  <td width="527" valign="top" style="width:395.6pt;padding:0in 5.4pt 0in 5.4pt">
                     <h5 style="color:grey">Employee Details</h5>
                     <table border="0" cellspacing="0" cellpadding="0" style="margin-left:1.85pt;border-  collapse:collapse">
                  <tbody>
                     <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:108.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Name<u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black"> ${emp_name}<u></u><u></u></span></p>
                        </td>
                     </tr>
                     <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:108.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Emp ID<u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black"> ${emp_id?long?c}<u></u><u></u></span></p>
                        </td>
                     </tr>
                     <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:108.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Vertical<u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black"> ${vertical_name}<u></u><u></u></span></p>
                        </td>
                     </tr>
                     <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:108.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Designation<u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black"> ${designation}<u></u><u></u></span></p>
                        </td>
                     </tr>
                     <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:108.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Location<u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black"> ${jobLocation}<u></u><u></u></span></p>
                        </td>
                     </tr>
                  </tbody>
              </table>
                  </td>
                  </tr>
                  </tbody>
               </table>
               <div>
               </p>
               <p>
               <a style="color:green" href="${leaveApproveUrl}">Approve</a>&nbsp;<a style="color:red" href="${leaveRejectUrl}">Reject</a>             
               </p>
   </body>
</html>