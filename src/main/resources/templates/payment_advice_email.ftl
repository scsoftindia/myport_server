<html>
   <head></head>
   <body>
      <p style="margin-top:10px">
         <span >Hi ${approover_name},</span><br/><br/>
      <span ><b>${emp_name}</b></span> has raised a new Payment advice. Details are given below:
        <table border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;border:none;margin-top:10px;">
               <tbody>
               <#if isForwarded == true>
                          <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:150.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Payment Advice Created on <u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black">-  <#if (created_on)??>&nbsp;${created_on?datetime}</#if><u></u><u></u></span></p>
                        </td>
                        </tr>
                          <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:150.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Payment Advice Forwarded on <u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black">-  <#if (forwarded_on)??>&nbsp; ${forwarded_on?datetime}</#if><u></u><u></u></span></p>
                        </td>
                     </tr>
                      </#if>  
                          <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:150.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Expense<u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black"> - &nbsp; ${expense_name}<u></u><u></u></span></p>
                        </td>
                     </tr> 
                 <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:150;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Description <u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black"> - <#if (description)??> &nbsp;${description}</#if><u></u><u></u></span></p>
                        </td>
                     </tr> 
                     <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:150.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Expense Type<u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black"> -  &nbsp;${expense_type_name}<u></u><u></u></span></p>
                        </td>
                     </tr>     
                       <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:150.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Vendor<u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black">-   &nbsp;${vendor_name}<u></u><u></u></span></p>
                        </td>
                     </tr> 
                     <#if (bill_date)??>     
                      <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:150.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Bill Date<u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black"> - <#if (bill_date)??>&nbsp;${bill_date?date}</#if><u></u><u></u></span></p>
                        </td>
                     </tr>
                    </#if>   
                      <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:150.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Bill Number <u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black">- <#if (bill_number)??> &nbsp;${bill_number}</#if><u></u><u></u></span></p>
                        </td>
                     </tr>  
                      <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:150.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Bill Amount <u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black">-  &nbsp;${bill_amount}<u></u><u></u></span></p>
                        </td>
                     </tr>
                     <#if (cheque_number)??>
                       <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:150.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Cheque Number <u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black">-  &nbsp;${cheque_number}<u></u><u></u></span></p>
                        </td>
                     </tr>
                     </#if> 
                     <#if (currency_type)??>
                       <tr style="height:24.25pt">
                        <td width="145" valign="top" style="width:150.95pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p   align="right" style="text-align:left"><b><span style="font-size:9.0pt;font-family:Arial,sans-serif;">Currency Type <u></u><u></u></span></b>
                           </p>
                        </td>
                        <td width="295" valign="top" style="width:221.3pt;padding:0in 5.4pt 0in 5.4pt;height:24.25pt">
                           <p><span style="font-size:9.0pt;font-family:Arial,sans-serif;color:black">-  &nbsp;${currency_type}<u></u><u></u></span></p>
                        </td>
                     </tr>
                     </#if>                                        
                  </tbody>
               </table>
               <div>
               </p>
               <br/>
               <p>Kindly approve the payment advice. </p>
               
               <p>
               <#if isNewAdvice>
               <a style="color:green" href="${adviceApproveUrl}/${APRROVED}/${empId}/${paymentAdviceId}/advice">Approve</a>
               <a style="color:red" href="${adviceApproveUrl}/${REJECT}/${empId}/${paymentAdviceId}/advice">Reject</a>
               <a style="color:blue" href="${adviceApproveUrl}/${APPRROVE_AND_FORWARD}/${empId}/${paymentAdviceId}/advice">Approve And Forward</a>
               </#if>
               <#if !isNewAdvice && isForwarded>
               <a style="color:green" href="${adviceApproveUrl}/${FORWARD_APRROVED}/${empId}/${paymentAdviceId}/advice">Approve</a>
               <a style="color:red" href="${adviceApproveUrl}/${FORWARD_REJECT}/${empId}/${paymentAdviceId}/advice">Reject</a>
               </#if>
               
               </p>
              
                
               
   </body>
</html>