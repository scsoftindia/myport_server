package com.scsoft.myport.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.scsoft.myport.domain.Vendors;
@Repository("vendorsRepository")
public interface VendorsRepository extends JpaRepository<Vendors, Integer> {
	
	@Query("select d from Vendors d where d.payadviceCategories.id = ?1 and d.status = true order by d.vendor_name ASC ")
	List<Vendors> getAllVendorsByCategoryAndSubCategory(Integer catId);
	
	@Query("select new com.scsoft.myport.domain.Vendors(d.id,d.vendor_name,d.vendor_address,d.gst,d.contact_person,d.phone_number,d.email_id,d.website,d.bank_name,"
	+ "d.account_no,d.ifsc_code,d.status,d.createdDate,d.lastModifiedDate,d.payadviceCategories,d.payadviceSubCategories"
	+ ",e.emp_fname,e.emp_mname,e.emp_lname,e.id,f.emp_fname,f.emp_mname,f.emp_lname,f.id) "
	+ "from Vendors d join EmpPersonal e on d.createdBy.id = e.id join EmpPersonal f on d.lastModifiedBy.id = f.id ")
Page<Vendors> getAllVendors(Pageable pageable);
	
	@Query("select new com.scsoft.myport.domain.Vendors(d.id,d.vendor_name,d.vendor_address,d.gst,d.contact_person,d.phone_number,d.email_id,d.website,d.bank_name,"
	+ "d.account_no,d.ifsc_code,d.status,d.createdDate,d.lastModifiedDate,d.payadviceCategories,d.payadviceSubCategories"
	+ ",e.emp_fname,e.emp_mname,e.emp_lname,e.id,f.emp_fname,f.emp_mname,f.emp_lname,f.id) "
	+ "from Vendors d join EmpPersonal e on d.createdBy.id = e.id join EmpPersonal f on d.lastModifiedBy.id = f.id where d.status = false and d.is_rejected = false")
Page<Vendors> getAllPendingVendors(Pageable pageable);
	
	@Query("select new com.scsoft.myport.domain.Vendors(d.id,d.vendor_name,d.vendor_address,d.gst,d.contact_person,d.phone_number,d.email_id,d.website,d.bank_name,"
	+ "d.account_no,d.ifsc_code,d.status,d.createdDate,d.lastModifiedDate,d.payadviceCategories,d.payadviceSubCategories"
	+ ",e.emp_fname,e.emp_mname,e.emp_lname,e.id,f.emp_fname,f.emp_mname,f.emp_lname,f.id) "
	+ "from Vendors d join EmpPersonal e on d.createdBy.id = e.id join EmpPersonal f on d.lastModifiedBy.id = f.id where d.payadviceCategories.id =?1 ")
	Page<Vendors> getAllVendorsByCategory(Integer catId,Pageable pageable);
	@Query("select new com.scsoft.myport.domain.Vendors(d.id,d.vendor_name,d.vendor_address,d.gst,d.contact_person,d.phone_number,d.email_id,d.website,d.bank_name,"
	+ "d.account_no,d.ifsc_code,d.status,d.createdDate,d.lastModifiedDate,d.payadviceCategories,d.payadviceSubCategories"
	+ ",e.emp_fname,e.emp_mname,e.emp_lname,e.id,f.emp_fname,f.emp_mname,f.emp_lname,f.id) "
	+ "from Vendors d join EmpPersonal e on d.createdBy.id = e.id join EmpPersonal f on d.lastModifiedBy.id = f.id where d.payadviceCategories.id =?1 and d.status = false and d.is_rejected = false ")
	Page<Vendors> getAllPendingVendorsByCategory(Integer catId,Pageable pageable);
	@Query("select count(d) from Vendors d where d.status = false and d.is_rejected = false  ")
	Integer approvalCount();
	
	
}
