package com.scsoft.myport.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.scsoft.myport.domain.ApprovalStatusType;

@Repository("approvalStatusType")
public interface ApprovalStatusTypeRepository extends JpaRepository<ApprovalStatusType, Integer> {
	ApprovalStatusType findByName(String name);
}