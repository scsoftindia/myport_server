package com.scsoft.myport.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.scsoft.myport.domain.CurrencyType;
@Repository("currencyTypeRepository")
public interface CurrencyTypeRepository extends JpaRepository<CurrencyType, Integer> {

}
