package com.scsoft.myport.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.scsoft.myport.domain.FcmDetail;
@Repository("fcmDetailRepository")
public interface FcmDetailRepository extends JpaRepository<FcmDetail, Integer> {
	
	@Query("select case when count(d) > 0 then 'true' else 'false' end from FcmDetail d where  d.empEmployment.id = ?1 ")
	Boolean isExists(Integer empId);
	
	FcmDetail findByEmpEmploymentId(Integer empId);
	FcmDetail findByEmpEmploymentIdAndIsAvailable(Integer empId,Boolean isAvailable);
	List<FcmDetail> findByIsAvailable(Boolean isAvailable);
}
