package com.scsoft.myport.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.scsoft.myport.domain.BestPerformer;

@Repository("bestPerformerRepository")
public interface BestPerformerRepository extends JpaRepository<BestPerformer, Integer> {
    @Query("select d from BestPerformer d where d.fYear = ?1 and d.quarter = ?2  ")
	List<BestPerformer> getBestPerformerList(String year,String quarter);
    
    List<BestPerformer> findTop3ByOrderByEnteredOnDescQuarterDesc();

}
