package com.scsoft.myport.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.scsoft.myport.domain.LeaveAvailability;

@Repository("leaveAvailabilityRepository")
public interface LeaveAvailabilityRepository extends JpaRepository<LeaveAvailability, Integer> {
	@Query("select d from LeaveAvailability d where d.emp_id = ?1 and d.year = ?2 and d.leaveTypes.id != 6 ")
	List<LeaveAvailability> getLeaveAvailabilityByEmployId(Integer id,Integer year);
	@Query("select d from LeaveAvailability d where d.emp_id = ?1 and d.leaveTypes.id = ?2 and d.year = ?3 ")
	LeaveAvailability getLeaveAvailability(Integer empId,Integer leaveTypeId,Integer year);
}
