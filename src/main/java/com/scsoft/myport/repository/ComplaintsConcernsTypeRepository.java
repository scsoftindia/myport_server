package com.scsoft.myport.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.scsoft.myport.domain.ComplaintsConcernsType;
@Repository("complaintsConcernsTypeRepository")
public interface ComplaintsConcernsTypeRepository extends JpaRepository<ComplaintsConcernsType, Integer> {

}
