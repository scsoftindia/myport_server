package com.scsoft.myport.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.scsoft.myport.domain.PaymentModes;
@Repository("paymentModesRepository")
public interface PaymentModesRepository extends JpaRepository<PaymentModes, Integer> {

}
