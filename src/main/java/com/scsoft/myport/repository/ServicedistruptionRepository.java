package com.scsoft.myport.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.scsoft.myport.domain.SalarySlipRequestsYear;
import com.scsoft.myport.domain.Servicedistruption;

@Repository("servicedistruptionRepository")
public interface ServicedistruptionRepository extends JpaRepository<Servicedistruption, Integer> {
	@Query("select count(sd) from Servicedistruption sd where sd.status not in ('Close') ")
	Integer getOpenSdnCount();

}
