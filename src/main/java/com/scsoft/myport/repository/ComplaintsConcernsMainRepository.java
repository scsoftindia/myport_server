package com.scsoft.myport.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;
import com.scsoft.myport.domain.ComplaintsConcernsMain;
import com.scsoft.myport.service.DTO.ComplaintDTO;
@Repository("complaintsConcernsMainRepository")
public interface ComplaintsConcernsMainRepository extends JpaRepository<ComplaintsConcernsMain, Integer> {
	@Query("select new com.scsoft.myport.service.DTO.ComplaintDTO(d.id,d.emp_id,p.id,p.cc_name,d.cc_subject,d.cc_remark,"
			+ "d.cc_doc,d.cc_resolved,d.cc_resolve_remark,d.cc_resolved_on,d.cc_resolved_by,c.emp_fname,c.emp_mname,c.emp_lname) from ComplaintsConcernsMain d join ComplaintsConcernsType p on d.cc_id = p.id left join EmpPersonal c on d.cc_resolved_by = c.id where d.emp_id = ?1")
	List<ComplaintDTO> getComplaintsByEmployee(Integer empId);
	
	@Query("select new com.scsoft.myport.service.DTO.ComplaintDTO(d.id,d.emp_id,p.id,p.cc_name,d.cc_subject,d.cc_remark,"
			+ "d.cc_doc,d.cc_resolved,d.cc_resolve_remark,d.cc_resolved_on,d.cc_resolved_by,c.emp_fname,c.emp_mname,c.emp_lname,e.emp_fname,e.emp_mname,e.emp_lname,e.emp_avatar) from ComplaintsConcernsMain d join ComplaintsConcernsType p on d.cc_id = p.id join EmpPersonal e on d.emp_id = e.id  left join EmpPersonal c on d.cc_resolved_by = c.id where d.cc_resolved ='Pending'")
	List<ComplaintDTO> getPendingComplaints();
}
