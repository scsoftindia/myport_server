package com.scsoft.myport.repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.scsoft.myport.domain.ApplyWfh;
import com.scsoft.myport.service.DTO.WfhDTO;

@Repository("applyWfhRepository")
public interface ApplyWfhRepository extends JpaRepository<ApplyWfh, Integer> {
	
//	@Query("select SUM(d.noOfDays) from ApplyWfh d where d.empPersonal.id = ?1 and EXTRACT(YEAR FROM d.from_date) = ?2 and month(d.from_date) = ?3")
//	BigDecimal getNoOfWfhByMonth(Integer empId,Integer year,Integer month);
	
	@Query("select new com.scsoft.myport.service.DTO.WfhDTO(d as applyWfh,e as empEmployment) from ApplyWfh d join EmpEmployment e on d.empPersonal.id = e.id "
			+ "where (e.emp_supervisor = ?1 or d.add_sup_id = ?1) and(d.application_status = 2 or d.application_status = 11) and (e.emp_job_status != 'resigned' and e.emp_job_status != 'terminated' and e.emp_job_status != 'absconding' ) ")
	List<WfhDTO> getPendingWfhRequests(Integer empId);
	
	List<ApplyWfh> findByEmpPersonalId(Integer empId);
	
	@Query("select count(d) from ApplyWfh d join EmpEmployment e on d.empPersonal.id = e.id where (d.application_status = 2 or d.application_status = 11) and (e.emp_supervisor = ?1 or d.add_sup_id = ?1) and (e.emp_job_status != 'resigned' and e.emp_job_status != 'terminated' and e.emp_job_status != 'absconding' )")
	Integer getWfhRequestCount(Integer supevisorId);
	
	@Query("select sum(d.no_of_days) from ApplyWfh d where d.empPersonal.id = ?1 and (d.application_status = 9 or d.application_status = 2 or d.application_status = 13) and (month(d.from_date) = ?2 or  month(d.to_date) = ?3) and (year(d.from_date) = ?4 or  year(d.to_date) = ?5)  ")
	BigDecimal getCurrentMonthWorkFromHomeCount(Integer empId,Integer fromMonth,Integer toMonth,Integer fromYear,Integer toYear );
	
	@Query("select case when count(d) > 0 then 'true' else 'false' end from ApplyWfh d where d.empPersonal.id = ?1 and (d.application_status = 9 or d.application_status = 2 or d.application_status = 13) and ((d.from_date between ?2 and ?3) or (d.to_date between ?2 and ?3) )")
    Boolean isAlreadyAppliedForTheDate(Integer empId,Date fromDate,Date toDate);
}
