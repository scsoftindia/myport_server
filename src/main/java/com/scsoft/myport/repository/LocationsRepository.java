package com.scsoft.myport.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.scsoft.myport.domain.Locations;

@Repository
public interface LocationsRepository extends JpaRepository<Locations, Integer> {

}
