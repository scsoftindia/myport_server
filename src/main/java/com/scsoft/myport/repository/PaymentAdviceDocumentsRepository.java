package com.scsoft.myport.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.scsoft.myport.domain.PaymentAdviceDocuments;
@Repository("paymentAdviceDocumentsRepository")
public interface PaymentAdviceDocumentsRepository extends JpaRepository<PaymentAdviceDocuments, Integer> {

}
