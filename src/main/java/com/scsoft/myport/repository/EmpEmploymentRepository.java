package com.scsoft.myport.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.scsoft.myport.domain.Designation;
import com.scsoft.myport.domain.EmpEmployment;
import com.scsoft.myport.domain.EmpPersonal;
import com.scsoft.myport.domain.Vertical;

@Repository("empEmploymentRepository")
public interface EmpEmploymentRepository extends JpaRepository<EmpEmployment, Integer> {

	@Query("select new com.scsoft.myport.domain.EmpEmployment(p.id,p.probation_extension_reason,p.ad_username,p.emp_doj,p.emp_email,"
			+ "p.emp_phone,p.emp_phone_ext,p.emp_fax,p.vertical,p.designation,p.shiftTypes,p.emp_ctc,p.emp_total_exp_yr,p.emp_total_exp_mn,"
			+ "p.emp_rel_exp_yr,p.emp_rel_exp_mn,p.emp_job_status,p.user_role,p.emp_supervisor,p.status_flag,"
			+ "p.leave_status,p.is_wfh,p.probation_extended_upto,(select d from EmpPersonal d where d.id = p.emp_supervisor) as superwiser,"
			+ "(select e from EmpPersonal e where e.id = p.id and e.emp_fname != null) as empPersonal,p.locations)  "
			+ "from EmpEmployment p where ( p.emp_job_status = 'trainee' or p.emp_job_status = 'active' or p.emp_job_status = 'probation' or p.emp_job_status = 'notice')")
	List<EmpEmployment> getEmployeeList();
	@Query("select new com.scsoft.myport.domain.EmpEmployment(p.id,p.probation_extension_reason,p.ad_username,p.emp_doj,p.emp_email,"
			+ "p.emp_phone,p.emp_phone_ext,p.emp_fax,p.vertical,p.designation,p.shiftTypes,p.emp_ctc,p.emp_total_exp_yr,p.emp_total_exp_mn,"
			+ "p.emp_rel_exp_yr,p.emp_rel_exp_mn,p.emp_job_status,p.user_role,p.emp_supervisor,p.status_flag,"
			+ "p.leave_status,p.is_wfh,p.probation_extended_upto,(select d from EmpPersonal d where d.id = p.emp_supervisor) as superwiser,"
			+ "(select e from EmpPersonal e where e.id = p.id and e.emp_fname != null) as empPersonal,p.locations)  "
			+ "from EmpEmployment p")
	List<EmpEmployment> getAllEmployeeList();

	@Query("select new com.scsoft.myport.domain.EmpEmployment(p.id,p.probation_extension_reason,p.ad_username,p.emp_doj,p.emp_email,"
			+ "p.emp_phone,p.emp_phone_ext,p.emp_fax,p.vertical,p.designation,p.shiftTypes,p.emp_ctc,p.emp_total_exp_yr,p.emp_total_exp_mn,"
			+ "p.emp_rel_exp_yr,p.emp_rel_exp_mn,p.emp_job_status,p.user_role,p.emp_supervisor,p.status_flag,"
			+ "p.leave_status,p.is_wfh,p.probation_extended_upto,(select d from EmpPersonal d where d.id = p.emp_supervisor) as superwiser,"
			+ "(select e from EmpPersonal e where e.id = p.id) as empPersonal,p.locations)  "
			+ "from EmpEmployment p where p.ad_username = ?1  ")
	EmpEmployment getEmployeeByUserNameAndPassword(String username);

	@Query("select new com.scsoft.myport.domain.EmpEmployment(p.id,p.probation_extension_reason,p.ad_username,p.emp_doj,p.emp_email,"
			+ "p.emp_phone,p.emp_phone_ext,p.emp_fax,p.vertical,p.designation,p.shiftTypes,p.emp_ctc,p.emp_total_exp_yr,p.emp_total_exp_mn,"
			+ "p.emp_rel_exp_yr,p.emp_rel_exp_mn,p.emp_job_status,p.user_role,p.emp_supervisor,p.status_flag,"
			+ "p.leave_status,p.is_wfh,p.probation_extended_upto,(select d from EmpPersonal d where d.id = p.emp_supervisor) as superwiser,"
			+ "(select e from EmpPersonal e where e.id = p.id) as empPersonal,p.locations)"
			+ "from EmpEmployment p where p.id = ?1  ")
	EmpEmployment getEmployeeById(Integer id);

	@Query("select d from EmpPersonal d join EmpEmployment p on p.emp_supervisor = d.id where p.id = ?1  ")
	EmpPersonal getSupervisor(Integer id);

	@Query("select d.designation from EmpEmployment d where d.id =?1 ")
	Designation getEmployeeDesignation(Integer empId);

	@Query("select d.vertical from EmpEmployment d where d.id =?1 ")
	Vertical getEmployeeVertical(Integer empId);

	@Query("select d from EmpPersonal d join EmpEmployment p on p.id = d.id where p.designation.id = 1 or p.designation.id = 2  ")
	List<EmpPersonal> getAprooverList();
	
	@Query("select d from EmpEmployment d join EmpPersonal p on d.id = p.id where p.emp_fname=?1 and (d.emp_job_status = 'trainee' or d.emp_job_status = 'active' or d.emp_job_status = 'probation' or d.emp_job_status = 'notice')")
	EmpEmployment selectEmployeDetailByFirstName(String firstName);
	
	@Modifying
    @Query("UPDATE EmpEmployment c SET c.emp_phone = ?2 WHERE c.id = ?1")
    int updatePhoneNumber(Integer companyId, String phoneNumer);
	
	@Query("select case when count(d)> 0 then 'true' else 'false' end from EmpEmployment d where d.emp_supervisor =?1 ")
	Boolean isSupervisor(Integer empId);
	@Query("select d from EmpEmployment d where d.designation.id = 68 and d.vertical.id = 550001000 and d.user_role = 'LEAD' and (d.emp_job_status = 'trainee' or d.emp_job_status = 'active' or d.emp_job_status = 'probation' or d.emp_job_status = 'notice')")
	EmpEmployment getFinanceLead();
	@Query("select d from EmpEmployment d where d.designation.id = 11 and d.vertical.id = 550001000 and d.user_role = 'HR' and (d.emp_job_status = 'trainee' or d.emp_job_status = 'active' or d.emp_job_status = 'probation' or d.emp_job_status = 'notice')")
	EmpEmployment getHrManager();
	
	
//	@Query("select d from EmpEmployment d where d.designation.id = 31 and d.vertical.id = 550001000 and d.user_role = 'HR' and (d.emp_job_status = 'trainee' or d.emp_job_status = 'active' or d.emp_job_status = 'probation' or d.emp_job_status = 'notice')")
//	EmpEmployment getHrAssosiate();
	
	@Query("select d from EmpEmployment d where d.designation.id = 31 and d.vertical.id = 550001000 and d.user_role = 'HR' and (d.emp_job_status = 'trainee' or d.emp_job_status = 'active' or d.emp_job_status = 'probation' or d.emp_job_status = 'notice')")
	List<EmpEmployment> getHrAssosiateList();
	
	@Query("select d from EmpEmployment d where d.vertical.id = 550001000 and d.user_role = 'HR' and (d.emp_job_status = 'trainee' or d.emp_job_status = 'active' or d.emp_job_status = 'probation' or d.emp_job_status = 'notice')")
	List<EmpEmployment> getHrManagers();
	@Query("select d from EmpEmployment d where d.vertical.id = 550001000 and d.user_role = 'ADMIN' and (d.emp_job_status = 'trainee' or d.emp_job_status = 'active' or d.emp_job_status = 'probation' or d.emp_job_status = 'notice')")
	List<EmpEmployment> getAdmins();
	@Query("select case when d.emp_job_status = 'trainee' then 'true' when d.emp_job_status = 'probation' then 'true' when d.emp_job_status = 'active' then 'true' when d.emp_job_status = 'notice' then 'true' else 'false' end from EmpEmployment d where d.id = ?1")
	Boolean isActiveEmployee(Integer empId);

}
