package com.scsoft.myport.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.scsoft.myport.domain.ApplyLeave;
import com.scsoft.myport.service.DTO.EmployessOnLeaveDTO;

@Repository("applyLeaveRepository")
public interface ApplyLeaveRepository extends JpaRepository<ApplyLeave, Integer> {
	@Query("select d from ApplyLeave d  where (d.fromDate =?1 or d.toDate = ?1 or (?1 between d.fromDate and d.toDate)) and (d.applicationStatus = 9 or d.hrApproval = 'Approved')")
	List<ApplyLeave> getEmployessOnLeave(Date leaveDate);
	

	@Query("select d from ApplyLeave d  where d.empPersonal.id =?1 order by d.appliedDate desc")
	List<ApplyLeave> getLeaveHistoryByEmployee(Integer empId);
	
	@Query("select d from ApplyLeave d join EmpEmployment e on d.empPersonal.id = e.id  where  (d.applicationStatus = 2 or d.applicationStatus = 11) and (d.addSupId = ?1 or e.emp_supervisor = ?1 ) and (e.emp_job_status != 'resigned' and e.emp_job_status != 'terminated' and e.emp_job_status != 'absconding' )")
	List<ApplyLeave> getAppliedLeaves(Integer id);
	
	@Query("select d from ApplyLeave d join EmpEmployment e on d.empPersonal.id = e.id  where (d.addSupId = ?1 or e.emp_supervisor = ?1 ) and (e.emp_job_status != 'resigned' and e.emp_job_status != 'terminated' and e.emp_job_status != 'absconding' )")
	Page<ApplyLeave> getEmployeeHistoryBySupevisor(Integer empId,Pageable pageable);
	
	@Query("select count(d) from ApplyLeave d join EmpEmployment e on d.empPersonal.id = e.id where (d.applicationStatus = 2 or d.applicationStatus = 11) and (e.emp_supervisor = ?1 or d.addSupId = ?1) and (e.emp_job_status != 'resigned' and e.emp_job_status != 'terminated' and e.emp_job_status != 'absconding' )")
	Integer getLeaveRequestCount(Integer supevisorId);
	
	@Query("select case when count(d) > 0 then 'true' else 'false' end from ApplyLeave d where d.empPersonal.id = ?1 and (d.applicationStatus = 9 or d.applicationStatus = 2 or d.applicationStatus = 13) and ((d.fromDate between ?2 and ?3) or (d.toDate between ?2 and ?3) )")
	Boolean isAlreadyApplied(Integer empId,Date fromDate,Date toDate);

}
