package com.scsoft.myport.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.scsoft.myport.domain.PaymentAdvice;
@Repository("paymentAdviceRepository")
public interface PaymentAdviceRepository extends JpaRepository<PaymentAdvice, Integer> {

	@Query("select d from PaymentAdvice d where (d.payadviceCategories.id = ?1 or ?1 = null or ?1 = 0)" 
						+ "and (d.payadviceSubCategories.id = ?2 or  ?2 = null or ?2 = 0)" 
						+ "and (d.vendors.id = ?3 or ?3 = null or ?3 = 0) and (EXTRACT(YEAR FROM d.bill_date) = ?4 or ?4 = null or ?4 = 0)" 
						+ "and  (month(d.bill_date) = ?5 or ?5 = null or ?5 = 0) "
						+ "and  ( d.payment_cheque_number = ?6  or  ?6 = null or  ?6 = 'null')")
	Page<PaymentAdvice> getPaymentAdviceList(Pageable pageable,Integer categoryId,Integer subCategoryId,Integer vendorId,Integer year,Integer month,String checkNumber);
	
	@Query("select d from PaymentAdvice d where (d.aprrovalStatus.id = 1 or d.aprrovalStatus.id = 6 ) and d.is_admin_rejected = false and (d.payadviceCategories.id = ?1 or ?1 = null or ?1 = 0) "
			+ "and (d.payadviceSubCategories.id = ?2 or  ?2 = null or ?2 = 0)"
			+ "and (d.vendors.id = ?3 or ?3 = null or ?3 = 0) and (EXTRACT(YEAR FROM d.bill_date) = ?4 or ?4 = null or ?4 = 0)"
			+ "and  (month(d.bill_date) = ?5 or ?5 = null or ?5 = 0)"
			+ "and  ( d.payment_cheque_number = ?6  or  ?6 = null or  ?6 = 'null' ) ")
	Page<PaymentAdvice> getPaymentAdviceListForAdminApproval(Pageable pageable,Integer categoryId,Integer subCategoryId,Integer vendorId,Integer year,Integer month,String checkNumber);
	@Query("select d from PaymentAdvice d where (d.aprrovalStatus.id = 2 or d.aprrovalStatus.id = 8)  and d.is_finance_rejected = false and (d.payadviceCategories.id = ?1 or ?1 = null or ?1 = 0)" 
					+ "and (d.payadviceSubCategories.id = ?2 or  ?2 = null or ?2 = 0)" 
						+ "and (d.vendors.id = ?3 or ?3 = null or ?3 = 0) and (EXTRACT(YEAR FROM d.bill_date) = ?4 or ?4 = null or ?4 = 0)"
						+ "and  (month(d.bill_date) = ?5 or ?5 = null or ?5 = 0) "
						+ "and  ( d.payment_cheque_number = ?6  or  ?6 = null or  ?6 = 'null')  ")
	Page<PaymentAdvice> getPaymentAdviceListForFinanceApproval(Pageable pageable,Integer categoryId,Integer subCategoryId,Integer vendorId,Integer year,Integer month,String checkNumber);
	@Query("select count(d) from PaymentAdvice d where (d.aprrovalStatus.id = 1 or d.aprrovalStatus.id = 6)  and d.is_admin_rejected = false ")
	Integer getAdminApprovalCount();
	@Query("select count(d) from PaymentAdvice d where  d.aprrovalStatus.id = 6 and d.is_admin_rejected = false and d.primary_aproover_emp_Id = ?1 ")
	Integer getAdminApprovalForwardedCount(Integer empId);
	@Query("select count(d) from PaymentAdvice d where d.aprrovalStatus.id = 6 and d.primary_aproover_emp_Id = ?1 ")
	Integer getAdminForwardApprovalCount(Integer empId);
	@Query("select count(d) from PaymentAdvice d where (d.aprrovalStatus.id = 2 or d.aprrovalStatus.id = 8) and d.is_finance_rejected = false ")
	Integer getFinanceApprovalCount();
	
	@Query("select distinct EXTRACT(YEAR FROM d.bill_date) from PaymentAdvice d ")
	List<Integer> getYearList();
	
	@Query("select  d.payment_cheque_number from PaymentAdvice d where d.payment_cheque_number IS NOT NULL")
	List<String> checkNumberList();
	
	@Query("select case when count(d) > 0 then 'true' else 'false' end from PaymentAdvice d where d.id = ?1")
	Boolean isExists(Integer id);
	
}
