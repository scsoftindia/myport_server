package com.scsoft.myport.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.scsoft.myport.domain.SalarySlipRequestsYear;
@Repository("salarySlipRequestsYearRepository")
public interface SalarySlipRequestsYearRepository extends JpaRepository<SalarySlipRequestsYear, Integer> {

}
