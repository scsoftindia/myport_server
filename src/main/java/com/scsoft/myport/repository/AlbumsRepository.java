package com.scsoft.myport.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.scsoft.myport.domain.Albums;
@Repository("albumsRepository")
public interface AlbumsRepository extends JpaRepository<Albums, Integer> {
	
	@Query("select d from Albums d where d.id != 1 and d.id != 3")
	List<Albums> findAll();

}
