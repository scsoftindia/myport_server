package com.scsoft.myport.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.scsoft.myport.domain.PayadviceCategories;
@Repository("payadviceCategoriesRepository")
public interface PayadviceCategoriesRepository extends JpaRepository<PayadviceCategories, Integer> {
	
	@Query("select d from PayadviceCategories d where d.is_approved = true")
	List<PayadviceCategories> getActiveCategories();
	@Query("select new com.scsoft.myport.domain.PayadviceCategories(d.id,d.cat_name,d.cat_description,d.is_approved,e.emp_fname,e.emp_mname,e.emp_lname,e.id,f.emp_fname,f.emp_mname,f.emp_lname,f.id,d.createdDate,d.lastModifiedDate) from PayadviceCategories d join  EmpPersonal e on d.createdBy.id = e.id join  EmpPersonal f on d.lastModifiedBy.id = f.id")
	Page<PayadviceCategories> getCategories(Pageable pageable);
	@Query("select new com.scsoft.myport.domain.PayadviceCategories(d.id,d.cat_name,d.cat_description,d.is_approved,e.emp_fname,e.emp_mname,e.emp_lname,e.id,f.emp_fname,f.emp_mname,f.emp_lname,f.id,d.createdDate,d.lastModifiedDate) from PayadviceCategories d join  EmpPersonal e on d.createdBy.id = e.id join  EmpPersonal f on d.lastModifiedBy.id = f.id where d.is_approved = false and d.is_rejected = false")
	Page<PayadviceCategories> getPendingCategories(Pageable pageable);
	@Query("select count(d) from PayadviceCategories d where d.is_approved = false and d.is_rejected = false  ")
	Integer approvalCount();

}
