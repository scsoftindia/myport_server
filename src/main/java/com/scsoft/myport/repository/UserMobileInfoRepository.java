package com.scsoft.myport.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.scsoft.myport.domain.UserMobileInfo;
@Repository
public interface UserMobileInfoRepository extends JpaRepository<UserMobileInfo, Integer> {

 UserMobileInfo findByEmpEmploymentIdAndMacAdress(Integer empId,String macadress);
}
