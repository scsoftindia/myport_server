package com.scsoft.myport.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.scsoft.myport.domain.Announcements;

@Repository("announcementsRepository")
public interface AnnouncementsRepository extends JpaRepository<Announcements, Integer> {
	@Query("select d from Announcements d where d.startDate <=CURRENT_DATE and d.announcementDate >= CURRENT_DATE order by d.announcementDate desc  ")
	List<Announcements> findAllAnnouncements();
}
