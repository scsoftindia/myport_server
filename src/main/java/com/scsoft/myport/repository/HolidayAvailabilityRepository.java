package com.scsoft.myport.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.scsoft.myport.domain.HolidayAvailability;
@Repository
public interface HolidayAvailabilityRepository extends JpaRepository<HolidayAvailability, Integer> {
    @Query("select d from HolidayAvailability d where d.emp_id = ?1 and d.year = ?2 ")
	HolidayAvailability getUserHoliDayAvailability(Integer userID,Integer year);
}
