package com.scsoft.myport.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.scsoft.myport.domain.EmpPersonal;
import com.scsoft.myport.service.DTO.AnniversaryDTO;
import com.scsoft.myport.service.DTO.BirthDaysDTO;

@Repository("empPersonalRepository")
public interface EmpPersonalRepository extends JpaRepository<EmpPersonal, Integer> {
	
	@Query(value = "select new com.scsoft.myport.service.DTO.BirthDaysDTO(d.id,d.emp_fname,d.emp_mname,d.emp_lname,d.emp_avatar,d.emp_dob,p.designation) from EmpPersonal d join EmpEmployment p  on d.id = p.id  where d.emp_dob != null and ((extract (month from d.emp_dob) = ?1 and extract (day from d.emp_dob) >= ?3 ) or (extract (month from d.emp_dob) = ?2 and extract (day from d.emp_dob) <= ?4)) and (p.emp_job_status = 'trainee' or p.emp_job_status = 'active' or p.emp_job_status = 'probation' or p.emp_job_status = 'notice' ) order by extract (month from d.emp_dob) asc  ")
	List<BirthDaysDTO> fetchBirthDays(int currentMonth,int toMonth,int currentDay,int toDay);
	
	@Query("select distinct d from EmpPersonal d join EmpEmployment e on d.id = e.emp_supervisor where ( e.emp_job_status = 'trainee' or e.emp_job_status = 'active' or e.emp_job_status = 'probation' or e.emp_job_status = 'notice') ")
	List<EmpPersonal> getSupevisorList();
	
	@Query("select new com.scsoft.myport.service.DTO.AnniversaryDTO(d.id,d.emp_fname,d.emp_mname,d.emp_lname,d.emp_avatar,p.emp_doj,p.designation) from  EmpPersonal d join EmpEmployment p  on d.id = p.id where p.emp_doj != null and ((extract (month from p.emp_doj) = ?1 and extract (day from p.emp_doj) >= ?3 and extract (day from p.emp_doj) < ?4 ) or (extract (month from p.emp_doj) = ?2 and extract (day from p.emp_doj) <= ?4 and extract (day from p.emp_doj) > ?3)) and (p.emp_job_status = 'trainee' or p.emp_job_status = 'active' or p.emp_job_status = 'probation' or p.emp_job_status = 'notice' ) order by extract (month from p.emp_doj) asc   ")
	List<AnniversaryDTO> fetchAnniversaries(int currentMonth,int toMonth,int currentDay,int toDay);
}
