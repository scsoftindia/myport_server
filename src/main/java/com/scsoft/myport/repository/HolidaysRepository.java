package com.scsoft.myport.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.scsoft.myport.domain.Holidays;
import com.scsoft.myport.service.DTO.UserHoliDayDTO;

@Repository
public interface HolidaysRepository extends JpaRepository<Holidays, Integer> {
    @Query("select d from Holidays d where d.holiday_year = ?1 and (d.holidayLocation = ?2 or d.holidayLocation = 0) ")
	List<Holidays> findHoliDaysByYear(Integer year,Integer holidayLocation);
    
    //Boolean isExists(Integer empId,Integer holidayId);
	

}
