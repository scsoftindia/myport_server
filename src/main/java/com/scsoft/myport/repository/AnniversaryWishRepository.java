package com.scsoft.myport.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.scsoft.myport.domain.AnniversaryWish;
@Repository
public interface AnniversaryWishRepository extends JpaRepository<AnniversaryWish, Integer> {

}
