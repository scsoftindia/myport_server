package com.scsoft.myport.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.scsoft.myport.domain.ApplyHoliday;
import com.scsoft.myport.service.DTO.HolidayHistroryByEmpDTO;

@Repository
public interface ApplyHolidayRepository extends JpaRepository<ApplyHoliday, Integer> {
	@Query("select d from ApplyHoliday d where d.emp_id = ?1 and d.holidayId = ?2 and (d.status = 2 or d.status = 9 or d.status = 10 or d.status = 11 or d.status = 12 or d.status = 13 or d.status = 20 or d.status = 21 or d.status = 22 ) order by d.id DESC")
	Page<ApplyHoliday> getAppliedHoliDay(Integer empId, Integer holiDayId,Pageable pageable);
	@Query("select case when count(d) > 0 then 'true' else 'false' end from ApplyHoliday d where d.emp_id = ?1 and d.holidayId = ?2 and (d.status = 2 or d.status = 9  or  d.status = 13  or d.status = 11 ) ")
	Boolean isApplied(Integer empId, Integer holiDayId);
	
	@Query("select d from ApplyHoliday d where d.emp_id = ?1")
	List<ApplyHoliday> getHolidayList(Integer empId);
	
	@Query("select d from ApplyHoliday d join EmpEmployment e on d.emp_id = e.id join Holidays h on d.holidayId = h.id  where (d.addSupId = ?1 or e.emp_supervisor = ?1) and h.holiday_year = ?2 and (d.status = 2 or d.status = 11 or d.status = 20) ")
	List<ApplyHoliday> getHolidayRequests(Integer empId,Integer year);
	
	@Query("select count(d) from ApplyHoliday d join EmpEmployment e on d.emp_id = e.id  where (d.status = 2 or d.status = 11 or d.status = 20) and  (e.emp_supervisor = ?1 or d.addSupId = ?1) and (e.emp_job_status != 'resigned' and e.emp_job_status != 'terminated' and e.emp_job_status != 'absconding' )")
	Integer getHolidayRequestCount(Integer supevisorId);
	
	@Query("select count(d) from ApplyHoliday d join Holidays h on d.holidayId = h.id where d.emp_id = ?1 and (d.status = 2 or d.status = 9 or d.status = 11  or d.status = 13  or d.status = 20 or d.status = 22 ) and h.holiday_year = ?2 ")
	Integer holidaysTaken(Integer empId,Integer year);
	
	@Query("select new com.scsoft.myport.service.DTO.HolidayHistroryByEmpDTO(d,e,h) from ApplyHoliday d join EmpPersonal e on d.emp_id = e.id join EmpEmployment emp on  d.emp_id = emp.id join Holidays h on d.holidayId = h.id  where (d.addSupId = ?1 or emp.emp_supervisor = ?1)")
	Page<HolidayHistroryByEmpDTO> getHolidayHistoryBySupervisor(Integer empId,Pageable pageable);
	
}
