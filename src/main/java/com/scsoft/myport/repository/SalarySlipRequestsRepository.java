package com.scsoft.myport.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.scsoft.myport.domain.SalarySlipRequests;
import com.scsoft.myport.rest.response.SalarySlipDetails;

@Repository("salarySlipRequestsRepository")
public interface SalarySlipRequestsRepository extends JpaRepository<SalarySlipRequests, Integer> {
	@Query("select new com.scsoft.myport.rest.response.SalarySlipDetails(sr.request_reason,sr.request_year,sr.request_months,sr.requested_date,sr.request_status,sr.addressed_on,sr.addressee_note) "
			+ "from SalarySlipRequests sr where sr.requested_by = ?1 order by sr.requested_date desc")
	List<SalarySlipDetails> getSalarySlipDetailsByEmployee(Integer empId);
}
