package com.scsoft.myport.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.scsoft.myport.domain.BirthdayWishes;

@Repository("birthdayWishesRepository")
public interface BirthdayWishesRepository extends JpaRepository<BirthdayWishes, Integer> {

	@Query("select case when count(bw) > 0  then 'true' else 'false' end from BirthdayWishes bw where bw.by_emp = ?1 and bw.for_emp = ?2 and  bw.bday_year = ?3 ")
	Boolean isBirthdayWishSentAlready(Integer byEmp,Integer toEmp,Integer year);
}
