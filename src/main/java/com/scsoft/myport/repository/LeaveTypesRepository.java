package com.scsoft.myport.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.scsoft.myport.domain.LeaveTypes;
@Repository("leaveTypesRepository")
public interface LeaveTypesRepository extends JpaRepository<LeaveTypes, Integer> {

}
