package com.scsoft.myport.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.scsoft.myport.domain.PayadviceSubCategories;
@Repository("payadviceSubCategoriesRepository")
public interface PayadviceSubCategoriesRepository extends JpaRepository<PayadviceSubCategories, Integer> {
	
	@Query("select d from PayadviceSubCategories d where d.payadviceCategories.id =?1 and d.is_approved = true order by d.pay_subcategory_name ASC ")
	List<PayadviceSubCategories> getActivePayadviceSubCategoriesByCategory(Integer catId);
    
	@Query("select new com.scsoft.myport.domain.PayadviceSubCategories(d.id,d.payadviceCategories,d.pay_subcategory_name,d.pay_subcategory_description,d.createdDate,d.lastModifiedDate,d.is_approved,e.emp_fname,e.emp_mname,e.emp_lname,e.id,f.emp_fname,f.emp_mname,f.emp_lname,f.id,d.payadviceCategories.cat_name,d.payadviceCategories.id) from PayadviceSubCategories d join EmpPersonal e on d.createdBy.id = e.id join EmpPersonal f on d.lastModifiedBy.id = f.id   where d.payadviceCategories.id = ?1")
	Page<PayadviceSubCategories> getAllPayadviceSubCategoriesByCategory(Integer catId,Pageable pageable);
	
	@Query("select new com.scsoft.myport.domain.PayadviceSubCategories(d.id,d.payadviceCategories,d.pay_subcategory_name,d.pay_subcategory_description,d.createdDate,d.lastModifiedDate,d.is_approved,e.emp_fname,e.emp_mname,e.emp_lname,e.id,f.emp_fname,f.emp_mname,f.emp_lname,f.id,d.payadviceCategories.cat_name,d.payadviceCategories.id) from PayadviceSubCategories d join EmpPersonal e on d.createdBy.id = e.id join EmpPersonal f on d.lastModifiedBy.id = f.id   where d.payadviceCategories.id = ?1 and d.is_approved = false and d.is_rejected = false")
	Page<PayadviceSubCategories> getAllPendingPayadviceSubCategoriesByCategory(Integer catId,Pageable pageable);

	@Query("select new com.scsoft.myport.domain.PayadviceSubCategories(d.id,d.payadviceCategories,d.pay_subcategory_name,d.pay_subcategory_description,d.createdDate,d.lastModifiedDate,d.is_approved,e.emp_fname,e.emp_mname,e.emp_lname,e.id,f.emp_fname,f.emp_mname,f.emp_lname,f.id,d.payadviceCategories.cat_name,d.payadviceCategories.id) from PayadviceSubCategories d join EmpPersonal e on d.createdBy.id = e.id join EmpPersonal f on d.lastModifiedBy.id = f.id")
	Page<PayadviceSubCategories> getAllPayadviceSubCategories(Pageable pageable);
	
	@Query("select new com.scsoft.myport.domain.PayadviceSubCategories(d.id,d.payadviceCategories,d.pay_subcategory_name,d.pay_subcategory_description,d.createdDate,d.lastModifiedDate,d.is_approved,e.emp_fname,e.emp_mname,e.emp_lname,e.id,f.emp_fname,f.emp_mname,f.emp_lname,f.id,d.payadviceCategories.cat_name,d.payadviceCategories.id) from PayadviceSubCategories d join EmpPersonal e on d.createdBy.id = e.id join EmpPersonal f on d.lastModifiedBy.id = f.id where d.is_approved = false and d.is_rejected = false")
	Page<PayadviceSubCategories> getAllPendingPayadviceSubCategories(Pageable pageable);
	@Query("select count(d) from PayadviceSubCategories d where d.is_approved = false and d.is_rejected = false  ")
	Integer approvalCount();
	
	
}
