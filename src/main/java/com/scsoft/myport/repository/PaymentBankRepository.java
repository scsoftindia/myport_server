package com.scsoft.myport.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.scsoft.myport.domain.PaymentBank;
@Repository
public interface PaymentBankRepository extends JpaRepository<PaymentBank,Integer> {

}
