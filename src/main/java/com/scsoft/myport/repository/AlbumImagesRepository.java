package com.scsoft.myport.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.scsoft.myport.domain.AlbumImages;
@Repository("albumImagesRepository")
public interface AlbumImagesRepository extends JpaRepository<AlbumImages, Integer> {
	
	List<AlbumImages> findByAlbumId(Integer albumId);
	
	Page<AlbumImages> findByAlbumId(Integer albumId,Pageable pageable);

}
