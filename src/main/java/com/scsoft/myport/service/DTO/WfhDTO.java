package com.scsoft.myport.service.DTO;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.scsoft.myport.domain.ApplyWfh;
import com.scsoft.myport.domain.EmpEmployment;

public class WfhDTO {
	private Integer id;
	private String fromDate;
	private String toDate;
	private BigDecimal noOfDays;
	private String reason;
	private Boolean isCancellationRequest;
	private String cancellationReson;
	private String empName;
	private String empFname;
	private String empMname;
	private String empLname;
	private String empVertical;
	private String empDesignation;
	private String empAvathar;
	private Integer empId;
	private Integer application_status;
	private Date applied_date;
	
	private static transient SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
	
	public WfhDTO(ApplyWfh applyWfh,EmpEmployment empEmployment) {
		this.id = applyWfh.getId();
		this.fromDate = sdf.format(applyWfh.getFrom_date());
		this.toDate = sdf.format(applyWfh.getTo_date());
		this.noOfDays = applyWfh.getNo_of_days();
		this.reason = applyWfh.getWfh_reason();
		this.cancellationReson = applyWfh.getCancel_reason();
		this.empFname = applyWfh.getEmpPersonal().getEmp_fname();
		this.empMname = applyWfh.getEmpPersonal().getEmp_mname();
		this.empLname = applyWfh.getEmpPersonal().getEmp_lname();
		this.empVertical = empEmployment.getVertical().getVertical();
		this.empDesignation = empEmployment.getDesignation().getDesignation();
		this.empAvathar = applyWfh.getEmpPersonal().getEmp_avatar();
		this.empId = applyWfh.getEmpPersonal().getId();
		this.application_status = applyWfh.getApplication_status();
		this.applied_date = applyWfh.getApplied_date();
	}
	public WfhDTO() {
		
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public BigDecimal getNoOfDays() {
		return noOfDays;
	}
	public void setNoOfDays(BigDecimal noOfDays) {
		this.noOfDays = noOfDays;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Boolean getIsCancellationRequest() {
		if(this.application_status != null) {
			if(this.application_status.equals(11)) {
				this.isCancellationRequest = true;
			}else {
				this.isCancellationRequest = false;
			}
		}else {
			this.isCancellationRequest = false;
		}
		return isCancellationRequest;
	}
	public void setIsCancellationRequest(Boolean isCancellationRequest) {
		this.isCancellationRequest = isCancellationRequest;
	}
	public String getCancellationReson() {
		
		return cancellationReson;
	}
	public void setCancellationReson(String cancellationReson) {
		this.cancellationReson = cancellationReson;
	}
	
	public String getEmpFname() {
		return empFname;
	}
	public void setEmpFname(String empFname) {
		this.empFname = empFname;
	}
	public String getEmpMname() {
		return empMname;
	}
	public void setEmpMname(String empMname) {
		this.empMname = empMname;
	}
	public String getEmpLname() {
		return empLname;
	}
	public void setEmpLname(String empLname) {
		this.empLname = empLname;
	}
	public String getEmpName() {
		String name = this.empFname+ " "+this.empMname+" "+this.empLname;
		this.empName = name;
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getEmpVertical() {
		return empVertical;
	}
	public void setEmpVertical(String empVertical) {
		this.empVertical = empVertical;
	}
	public String getEmpDesignation() {
		return empDesignation;
	}
	public void setEmpDesignation(String empDesignation) {
		this.empDesignation = empDesignation;
	}
	public String getEmpAvathar() {
		return empAvathar;
	}
	public void setEmpAvathar(String empAvathar) {
		this.empAvathar = empAvathar;
	}
	public Integer getEmpId() {
		return empId;
	}
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	public Integer getApplication_status() {
		return application_status;
	}
	public void setApplication_status(Integer application_status) {
		this.application_status = application_status;
	}
	public Date getApplied_date() {
		return applied_date;
	}
	public void setApplied_date(Date applied_date) {
		this.applied_date = applied_date;
	}
	
	
	
	
	
	
	
	
	
	
	

}
