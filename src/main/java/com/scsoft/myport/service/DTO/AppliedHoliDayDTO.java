package com.scsoft.myport.service.DTO;

import java.math.BigDecimal;
import java.util.Date;

import com.scsoft.myport.domain.Designation;
import com.scsoft.myport.domain.Vertical;

public class AppliedHoliDayDTO {
	private Integer id;
	private String emp_fname;
	private String emp_mname;
	private String emp_lname;
	private String emp_avathar;
	private Integer empId;
	private Designation designation;
	private Vertical vertical;
	private Date date;
	private String holiDayName;
	private Integer holiDayId;
	private BigDecimal holiDaysTaken;
	private BigDecimal holiDayAvailability;
	private Boolean isCancellationRequest;
	private String cancellationReason;
	private String description;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmp_fname() {
		return emp_fname;
	}

	public void setEmp_fname(String emp_fname) {
		this.emp_fname = emp_fname;
	}

	public String getEmp_mname() {
		return emp_mname;
	}

	public void setEmp_mname(String emp_mname) {
		this.emp_mname = emp_mname;
	}

	public String getEmp_lname() {
		return emp_lname;
	}

	public void setEmp_lname(String emp_lname) {
		this.emp_lname = emp_lname;
	}

	
	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	
	

	public String getEmp_avathar() {
		return emp_avathar;
	}

	public void setEmp_avathar(String emp_avathar) {
		this.emp_avathar = emp_avathar;
	}

	public Designation getDesignation() {
		return designation;
	}

	public void setDesignation(Designation designation) {
		this.designation = designation;
	}

	public Vertical getVertical() {
		return vertical;
	}

	public void setVertical(Vertical vertical) {
		this.vertical = vertical;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getHoliDayName() {
		return holiDayName;
	}

	public void setHoliDayName(String holiDayName) {
		this.holiDayName = holiDayName;
	}

	public Integer getHoliDayId() {
		return holiDayId;
	}

	public void setHoliDayId(Integer holiDayId) {
		this.holiDayId = holiDayId;
	}

	public BigDecimal getHoliDaysTaken() {
		return holiDaysTaken;
	}

	public void setHoliDaysTaken(BigDecimal holiDaysTaken) {
		this.holiDaysTaken = holiDaysTaken;
	}

	public BigDecimal getHoliDayAvailability() {
		return holiDayAvailability;
	}

	public void setHoliDayAvailability(BigDecimal holiDayAvailability) {
		this.holiDayAvailability = holiDayAvailability;
	}

	public Boolean getIsCancellationRequest() {
		if(this.isCancellationRequest == null) {
			this.isCancellationRequest = false;
		}
		return isCancellationRequest;
	}

	public void setIsCancellationRequest(Boolean isCancellationRequest) {
		this.isCancellationRequest = isCancellationRequest;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCancellationReason() {
		return cancellationReason;
	}

	public void setCancellationReason(String cancellationReason) {
		this.cancellationReason = cancellationReason;
	}
	
	
	

}
