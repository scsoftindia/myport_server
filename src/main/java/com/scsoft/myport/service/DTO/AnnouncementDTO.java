package com.scsoft.myport.service.DTO;

import java.util.Date;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.scsoft.myport.service.util.DateDeserializer;
import com.scsoft.myport.service.util.DateSerializer;

public class AnnouncementDTO {

	private Integer id;
	private String announcementHeading;
	private String announcement;
	private String announcementBy;
	private Date startDate;
	private Date announcementDate;
	private String announcementFile;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAnnouncementHeading() {
		return announcementHeading;
	}
	public void setAnnouncementHeading(String announcementHeading) {
		this.announcementHeading = announcementHeading;
	}
	public String getAnnouncement() {
		return announcement;
	}
	public void setAnnouncement(String announcement) {
		this.announcement = announcement;
	}
	public String getAnnouncementBy() {
		return announcementBy;
	}
	public void setAnnouncementBy(String announcementBy) {
		this.announcementBy = announcementBy;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getAnnouncementDate() {
		return announcementDate;
	}
	public void setAnnouncementDate(Date announcementDate) {
		this.announcementDate = announcementDate;
	}
	public String getAnnouncementFile() {
		return announcementFile;
	}
	public void setAnnouncementFile(String announcementFile) {
		this.announcementFile = announcementFile;
	}
	
	
}
