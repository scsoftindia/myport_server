package com.scsoft.myport.service.DTO;

public class BirthDayWishDTO {
	private String birthDayWishBy;
	private String message;
	public String getBirthDayWishBy() {
		return birthDayWishBy;
	}
	public void setBirthDayWishBy(String birthDayWishBy) {
		this.birthDayWishBy = birthDayWishBy;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	

}
