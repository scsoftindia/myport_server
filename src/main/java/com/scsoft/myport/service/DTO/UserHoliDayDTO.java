package com.scsoft.myport.service.DTO;

import java.util.Date;

import com.scsoft.myport.domain.Holidays;

public class UserHoliDayDTO {
	private Holidays holidays;
//	private Boolean applied;
//	private Boolean isApproved;
//	private Boolean isHolidayTaken;
//	private Boolean isHoliDayOver;
	private Boolean isApplicable;
	private String userHolidayStatus;


	public Holidays getHolidays() {
		return holidays;
	}

	public void setHolidays(Holidays holidays) {
		this.holidays = holidays;
	}

	
	public Boolean getIsApplicable() {
		return isApplicable;
	}

	public void setIsApplicable(Boolean isApplicable) {
		this.isApplicable = isApplicable;
	}

	public String getUserHolidayStatus() {
		return userHolidayStatus;
	}

	public void setUserHolidayStatus(String userHolidayStatus) {
		this.userHolidayStatus = userHolidayStatus;
	}
	
	
	
	

}
