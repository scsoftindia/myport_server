package com.scsoft.myport.service.DTO;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.scsoft.myport.domain.Designation;
import com.scsoft.myport.domain.ShiftTypes;
import com.scsoft.myport.domain.Vertical;
import com.scsoft.myport.service.util.DateDeserializer;
import com.scsoft.myport.service.util.DateSerializer;

public class EmployeeDTO {

	private Integer id;
	private String probation_extension_reason;
	private String ad_username;
	private Date emp_doj;
	private String emp_email;
	private String emp_phone;
	private String emp_phone_ext;
	private String emp_personal_phone;
	private String emp_fax;
	private Vertical vertical;
	private Designation designation;
	private ShiftTypes shiftTypes;
	private String emp_total_exp_yr;
	private String emp_total_exp_mn;
	private String emp_rel_exp_yr;
	private String emp_rel_exp_mn;
	private String emp_job_status;
	private Date last_working_day;
	private String user_role;
	private Date probation_extended_upto;
	private String emp_fname;
	private String emp_mname;
	private String emp_lname;
	private Date emp_dob;
	private String emp_gender;
	private String emp_bloodgroup;
	private String emp_fathername;
	private String emp_marital_status;
	private Date emp_anniversary;
	private String emp_temp_addr;
	private String emp_perm_addr;
	private String emp_avatar;
	private Integer supervispr_id;
	private String supervispr_fname;
	private String supervispr_mname;
	private String supervispr_lname;
	private Boolean isSupervisor;
	private String is_wfh;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getProbation_extension_reason() {
		return probation_extension_reason;
	}
	public void setProbation_extension_reason(String probation_extension_reason) {
		this.probation_extension_reason = probation_extension_reason;
	}
	public String getAd_username() {
		return ad_username;
	}
	public void setAd_username(String ad_username) {
		this.ad_username = ad_username;
	}
	public Date getEmp_doj() {
		return emp_doj;
	}
	public void setEmp_doj(Date emp_doj) {
		this.emp_doj = emp_doj;
	}
	public String getEmp_email() {
		return emp_email;
	}
	public void setEmp_email(String emp_email) {
		this.emp_email = emp_email;
	}
	public String getEmp_phone() {
		return emp_phone;
	}
	public void setEmp_phone(String emp_phone) {
		this.emp_phone = emp_phone;
	}
	public String getEmp_phone_ext() {
		return emp_phone_ext;
	}
	public void setEmp_phone_ext(String emp_phone_ext) {
		this.emp_phone_ext = emp_phone_ext;
	}
	public String getEmp_fax() {
		return emp_fax;
	}
	public void setEmp_fax(String emp_fax) {
		this.emp_fax = emp_fax;
	}
	public Vertical getVertical() {
		return vertical;
	}
	public void setVertical(Vertical vertical) {
		this.vertical = vertical;
	}
	public Designation getDesignation() {
		return designation;
	}
	public void setDesignation(Designation designation) {
		this.designation = designation;
	}
	public ShiftTypes getShiftTypes() {
		return shiftTypes;
	}
	public void setShiftTypes(ShiftTypes shiftTypes) {
		this.shiftTypes = shiftTypes;
	}
	public String getEmp_total_exp_yr() {
		return emp_total_exp_yr;
	}
	public void setEmp_total_exp_yr(String emp_total_exp_yr) {
		this.emp_total_exp_yr = emp_total_exp_yr;
	}
	public String getEmp_total_exp_mn() {
		return emp_total_exp_mn;
	}
	public void setEmp_total_exp_mn(String emp_total_exp_mn) {
		this.emp_total_exp_mn = emp_total_exp_mn;
	}
	public String getEmp_rel_exp_yr() {
		return emp_rel_exp_yr;
	}
	public void setEmp_rel_exp_yr(String emp_rel_exp_yr) {
		this.emp_rel_exp_yr = emp_rel_exp_yr;
	}
	public String getEmp_rel_exp_mn() {
		return emp_rel_exp_mn;
	}
	public void setEmp_rel_exp_mn(String emp_rel_exp_mn) {
		this.emp_rel_exp_mn = emp_rel_exp_mn;
	}
	public String getEmp_job_status() {
		return emp_job_status;
	}
	public void setEmp_job_status(String emp_job_status) {
		this.emp_job_status = emp_job_status;
	}
	public Date getLast_working_day() {
		return last_working_day;
	}
	public void setLast_working_day(Date last_working_day) {
		this.last_working_day = last_working_day;
	}
	public String getUser_role() {
		return user_role;
	}
	public void setUser_role(String user_role) {
		this.user_role = user_role;
	}
	public Date getProbation_extended_upto() {
		return probation_extended_upto;
	}
	public void setProbation_extended_upto(Date probation_extended_upto) {
		this.probation_extended_upto = probation_extended_upto;
	}
	public String getEmp_fname() {
		return emp_fname;
	}
	public void setEmp_fname(String emp_fname) {
		this.emp_fname = emp_fname;
	}
	public String getEmp_mname() {
		return emp_mname;
	}
	public void setEmp_mname(String emp_mname) {
		this.emp_mname = emp_mname;
	}
	public String getEmp_lname() {
		return emp_lname;
	}
	public void setEmp_lname(String emp_lname) {
		this.emp_lname = emp_lname;
	}
	public Date getEmp_dob() {
		return emp_dob;
	}
	public void setEmp_dob(Date emp_dob) {
		this.emp_dob = emp_dob;
	}
	public String getEmp_gender() {
		return emp_gender;
	}
	public void setEmp_gender(String emp_gender) {
		this.emp_gender = emp_gender;
	}
	public String getEmp_bloodgroup() {
		return emp_bloodgroup;
	}
	public void setEmp_bloodgroup(String emp_bloodgroup) {
		this.emp_bloodgroup = emp_bloodgroup;
	}
	public String getEmp_fathername() {
		return emp_fathername;
	}
	public void setEmp_fathername(String emp_fathername) {
		this.emp_fathername = emp_fathername;
	}
	public String getEmp_marital_status() {
		return emp_marital_status;
	}
	public void setEmp_marital_status(String emp_marital_status) {
		this.emp_marital_status = emp_marital_status;
	}
	public Date getEmp_anniversary() {
		return emp_anniversary;
	}
	public void setEmp_anniversary(Date emp_anniversary) {
		this.emp_anniversary = emp_anniversary;
	}
	public String getEmp_temp_addr() {
		return emp_temp_addr;
	}
	public void setEmp_temp_addr(String emp_temp_addr) {
		this.emp_temp_addr = emp_temp_addr;
	}
	public String getEmp_perm_addr() {
		return emp_perm_addr;
	}
	public void setEmp_perm_addr(String emp_perm_addr) {
		this.emp_perm_addr = emp_perm_addr;
	}
	public String getEmp_avatar() {
		return emp_avatar;
	}
	public void setEmp_avatar(String emp_avatar) {
		this.emp_avatar = emp_avatar;
	}
	public Integer getSupervispr_id() {
		return supervispr_id;
	}
	public void setSupervispr_id(Integer supervispr_id) {
		this.supervispr_id = supervispr_id;
	}
	public String getSupervispr_fname() {
		return supervispr_fname;
	}
	public void setSupervispr_fname(String supervispr_fname) {
		this.supervispr_fname = supervispr_fname;
	}
	public String getSupervispr_mname() {
		return supervispr_mname;
	}
	public void setSupervispr_mname(String supervispr_mname) {
		this.supervispr_mname = supervispr_mname;
	}
	public String getSupervispr_lname() {
		return supervispr_lname;
	}
	public void setSupervispr_lname(String supervispr_lname) {
		this.supervispr_lname = supervispr_lname;
	}
	public String getEmp_personal_phone() {
		return emp_personal_phone;
	}
	public void setEmp_personal_phone(String emp_personal_phone) {
		this.emp_personal_phone = emp_personal_phone;
	}
	public Boolean getIsSupervisor() {
		return isSupervisor;
	}
	public void setIsSupervisor(Boolean isSupervisor) {
		this.isSupervisor = isSupervisor;
	}
	public String getIs_wfh() {
		return is_wfh;
	}
	public void setIs_wfh(String is_wfh) {
		this.is_wfh = is_wfh;
	}
	
	
	
	
	

}
