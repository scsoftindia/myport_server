package com.scsoft.myport.service.DTO;

import javax.persistence.Column;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class BestPerformerDTO {
	private Integer id;
	private String fname;
	private String mname;
	private String lname;
	private String fYear;
	private String quarter;
	private String avathar;
	private Integer empId;
	@JsonIgnore
	private String achieve1;
	@JsonIgnore
	private String achieve2;
	@JsonIgnore
	private String achieve3;
	@JsonIgnore
	private String achieve4;
	@JsonIgnore
	private String achieve5;
	@JsonIgnore
	private String app1;
	@JsonIgnore
	private String app1By;
	@JsonIgnore
	private String app2;
	@JsonIgnore
	private String app2By;
	@JsonIgnore
	private String app3;
	@JsonIgnore
	private String app3By;
	@JsonIgnore
	private String app4;
	@JsonIgnore
	private String app4By;
	@JsonIgnore
	private String app5;
	@JsonIgnore
	private String app5By;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getMname() {
		return mname;
	}

	public void setMname(String mname) {
		this.mname = mname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getfYear() {
		return fYear;
	}

	public void setfYear(String fYear) {
		this.fYear = fYear;
	}

	public String getQuarter() {
		return quarter;
	}

	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}

	
	public String getAvathar() {
		return avathar;
	}

	public void setAvathar(String avathar) {
		this.avathar = avathar;
	}

	public String getAchieve1() {
		return achieve1;
	}

	public void setAchieve1(String achieve1) {
		this.achieve1 = achieve1;
	}

	public String getAchieve2() {
		return achieve2;
	}

	public void setAchieve2(String achieve2) {
		this.achieve2 = achieve2;
	}

	public String getAchieve3() {
		return achieve3;
	}

	public void setAchieve3(String achieve3) {
		this.achieve3 = achieve3;
	}

	public String getAchieve4() {
		return achieve4;
	}

	public void setAchieve4(String achieve4) {
		this.achieve4 = achieve4;
	}

	public String getAchieve5() {
		return achieve5;
	}

	public void setAchieve5(String achieve5) {
		this.achieve5 = achieve5;
	}

	public String getApp1() {
		return app1;
	}

	public void setApp1(String app1) {
		this.app1 = app1;
	}

	public String getApp1By() {
		return app1By;
	}

	public void setApp1By(String app1By) {
		this.app1By = app1By;
	}

	public String getApp2() {
		return app2;
	}

	public void setApp2(String app2) {
		this.app2 = app2;
	}

	public String getApp2By() {
		return app2By;
	}

	public void setApp2By(String app2By) {
		this.app2By = app2By;
	}

	public String getApp3() {
		return app3;
	}

	public void setApp3(String app3) {
		this.app3 = app3;
	}

	public String getApp3By() {
		return app3By;
	}

	public void setApp3By(String app3By) {
		this.app3By = app3By;
	}

	public String getApp4() {
		return app4;
	}

	public void setApp4(String app4) {
		this.app4 = app4;
	}

	public String getApp4By() {
		return app4By;
	}

	public void setApp4By(String app4By) {
		this.app4By = app4By;
	}

	public String getApp5() {
		return app5;
	}

	public void setApp5(String app5) {
		this.app5 = app5;
	}

	public String getApp5By() {
		return app5By;
	}

	public void setApp5By(String app5By) {
		this.app5By = app5By;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	
	

}
