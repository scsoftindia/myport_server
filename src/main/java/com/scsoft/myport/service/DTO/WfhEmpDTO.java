package com.scsoft.myport.service.DTO;

import java.math.BigDecimal;
import java.util.Date;



import com.scsoft.myport.domain.EmpPersonal;

public class WfhEmpDTO {
	private Integer id;
	private Date applied_date;
	private Date from_date;
	private Date to_date;
	private BigDecimal no_of_days;
	private String wfh_reason;
	private Integer application_status;
	private String cancel_approval;
	private String cancel_approval_note;
	private String addsup_approval;
	private String addsup_approval_note;
	private Date addsup_approval_date;
	private Date cancel_date;
	private String sup_approval;
	private String sup_approval_note;
	private Date sup_approval_date;
	private Boolean isWithdrwable;
	private String currentStatus;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Date getApplied_date() {
		return applied_date;
	}
	public void setApplied_date(Date applied_date) {
		this.applied_date = applied_date;
	}
	public Date getFrom_date() {
		return from_date;
	}
	public void setFrom_date(Date from_date) {
		this.from_date = from_date;
	}
	public Date getTo_date() {
		return to_date;
	}
	public void setTo_date(Date to_date) {
		this.to_date = to_date;
	}
	public BigDecimal getNo_of_days() {
		return no_of_days;
	}
	public void setNo_of_days(BigDecimal no_of_days) {
		this.no_of_days = no_of_days;
	}
	public String getWfh_reason() {
		return wfh_reason;
	}
	public void setWfh_reason(String wfh_reason) {
		this.wfh_reason = wfh_reason;
	}
	public Integer getApplication_status() {
		return application_status;
	}
	public void setApplication_status(Integer application_status) {
		this.application_status = application_status;
	}
	public String getCancel_approval() {
		return cancel_approval;
	}
	public void setCancel_approval(String cancel_approval) {
		this.cancel_approval = cancel_approval;
	}
	public String getCancel_approval_note() {
		return cancel_approval_note;
	}
	public void setCancel_approval_note(String cancel_approval_note) {
		this.cancel_approval_note = cancel_approval_note;
	}
	public String getAddsup_approval() {
		return addsup_approval;
	}
	public void setAddsup_approval(String addsup_approval) {
		this.addsup_approval = addsup_approval;
	}
	public String getAddsup_approval_note() {
		return addsup_approval_note;
	}
	public void setAddsup_approval_note(String addsup_approval_note) {
		this.addsup_approval_note = addsup_approval_note;
	}
	public Date getAddsup_approval_date() {
		return addsup_approval_date;
	}
	public void setAddsup_approval_date(Date addsup_approval_date) {
		this.addsup_approval_date = addsup_approval_date;
	}
	public Date getCancel_date() {
		return cancel_date;
	}
	public void setCancel_date(Date cancel_date) {
		this.cancel_date = cancel_date;
	}
	public String getSup_approval() {
		return sup_approval;
	}
	public void setSup_approval(String sup_approval) {
		this.sup_approval = sup_approval;
	}
	public String getSup_approval_note() {
		return sup_approval_note;
	}
	public void setSup_approval_note(String sup_approval_note) {
		this.sup_approval_note = sup_approval_note;
	}
	public Date getSup_approval_date() {
		return sup_approval_date;
	}
	public void setSup_approval_date(Date sup_approval_date) {
		this.sup_approval_date = sup_approval_date;
	}
	public Boolean getIsWithdrwable() {
		if(this.application_status.equals(2) || this.application_status.equals(9)) {
			this.isWithdrwable = true;
		}else {
			this.isWithdrwable = false;
		}
		return isWithdrwable;
	}
	public void setIsWithdrwable(Boolean isWithdrwable) {
		this.isWithdrwable = isWithdrwable;
	}
	public String getCurrentStatus() {
		switch (this.application_status) {
		case 2:
			this.currentStatus = " Approval Pending";
			break;
		case 9:
			this.currentStatus = " Approved";
			break;
		case 10:
			this.currentStatus = " Rejected";
			break;
		case 11:
			this.currentStatus = "Cancellation Pending";
			break;
		case 12:
			this.currentStatus = "Cancellation Approved";
			break;
		case 13:
			this.currentStatus = "Cancellation Rejected";
			break;

		default:
			this.currentStatus = "Approval Pending";
			break;
		}
		return currentStatus;
	}
	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}
	
	
}
