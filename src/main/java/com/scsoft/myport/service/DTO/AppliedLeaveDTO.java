package com.scsoft.myport.service.DTO;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.scsoft.myport.domain.Designation;
import com.scsoft.myport.domain.LeaveTypes;
import com.scsoft.myport.domain.Vertical;

public class AppliedLeaveDTO {
	private Integer id;
	private String emp_fname;
	private String emp_mname;
	private String emp_lname;
	private String emp_avatar;
	private Integer empId;
	private Designation designation;
	private Vertical vertical;
	private Date appliedDate;
	private Date fromDate;
	private Date toDate;
	private String leaveReason;
	private BigDecimal noOfDays;
	private BigDecimal totalLeaves;
	private BigDecimal leavesTaken;
	private BigDecimal leaveAvailability;
	private LeaveTypes leaveTypes;
	private String attachedDoc;
	private String status;
	private Boolean isLeaveCancelRequest;
	private String leaveCancelReason;
	private Date cancelDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEmp_fname() {
		return emp_fname;
	}

	public void setEmp_fname(String emp_fname) {
		this.emp_fname = emp_fname;
	}

	public String getEmp_mname() {
		return emp_mname;
	}

	public void setEmp_mname(String emp_mname) {
		this.emp_mname = emp_mname;
	}

	public String getEmp_lname() {
		return emp_lname;
	}

	public void setEmp_lname(String emp_lname) {
		this.emp_lname = emp_lname;
	}

	public String getEmp_avatar() {
		return emp_avatar;
	}

	public void setEmp_avatar(String emp_avatar) {
		this.emp_avatar = emp_avatar;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public Designation getDesignation() {
		return designation;
	}

	public void setDesignation(Designation designation) {
		this.designation = designation;
	}

	public Vertical getVertical() {
		return vertical;
	}

	public void setVertical(Vertical vertical) {
		this.vertical = vertical;
	}

	public Date getAppliedDate() {
		return appliedDate;
	}

	public void setAppliedDate(Date appliedDate) {
		this.appliedDate = appliedDate;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getLeaveReason() {
		return leaveReason;
	}

	public void setLeaveReason(String leaveReason) {
		this.leaveReason = leaveReason;
	}

	public BigDecimal getNoOfDays() {
		return noOfDays;
	}

	public void setNoOfDays(BigDecimal noOfDays) {
		this.noOfDays = noOfDays;
	}

	public BigDecimal getTotalLeaves() {
		return totalLeaves;
	}

	public void setTotalLeaves(BigDecimal totalLeaves) {
		this.totalLeaves = totalLeaves;
	}

	public BigDecimal getLeavesTaken() {
		return leavesTaken;
	}

	public void setLeavesTaken(BigDecimal leavesTaken) {
		this.leavesTaken = leavesTaken;
	}

	public BigDecimal getLeaveAvailability() {
		return leaveAvailability;
	}

	public void setLeaveAvailability(BigDecimal leaveAvailability) {
		this.leaveAvailability = leaveAvailability;
	}

	public LeaveTypes getLeaveTypes() {
		return leaveTypes;
	}

	public void setLeaveTypes(LeaveTypes leaveTypes) {
		this.leaveTypes = leaveTypes;
	}
	

	public String getAttachedDoc() {
		return attachedDoc;
	}

	public void setAttachedDoc(String attachedDoc) {
		this.attachedDoc = attachedDoc;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Boolean getIsLeaveCancelRequest() {
		return isLeaveCancelRequest;
	}

	public void setIsLeaveCancelRequest(Boolean isLeaveCancelRequest) {
		this.isLeaveCancelRequest = isLeaveCancelRequest;
	}

	public String getLeaveCancelReason() {
		return leaveCancelReason;
	}

	public void setLeaveCancelReason(String leaveCancelReason) {
		this.leaveCancelReason = leaveCancelReason;
	}

	public Date getCancelDate() {
		return cancelDate;
	}

	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}
	
	
	
	
	
	
	

}
