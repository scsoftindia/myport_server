package com.scsoft.myport.service.DTO;

public class ApprovalCountDTO {
	private Integer vendorCount;
	private Integer categoryCount;
	private Integer subCategoryCount;
	private Integer adviceAdminApprovalCount;
	private Integer adviceFincanceApprovalCount;

	public Integer getVendorCount() {
		return vendorCount;
	}

	public void setVendorCount(Integer vendorCount) {
		this.vendorCount = vendorCount;
	}

	public Integer getCategoryCount() {
		return categoryCount;
	}

	public void setCategoryCount(Integer categoryCount) {
		this.categoryCount = categoryCount;
	}

	public Integer getSubCategoryCount() {
		return subCategoryCount;
	}

	public void setSubCategoryCount(Integer subCategoryCount) {
		this.subCategoryCount = subCategoryCount;
	}

	public Integer getAdviceAdminApprovalCount() {
		return adviceAdminApprovalCount;
	}

	public void setAdviceAdminApprovalCount(Integer adviceAdminApprovalCount) {
		this.adviceAdminApprovalCount = adviceAdminApprovalCount;
	}

	public Integer getAdviceFincanceApprovalCount() {
		return adviceFincanceApprovalCount;
	}

	public void setAdviceFincanceApprovalCount(Integer adviceFincanceApprovalCount) {
		this.adviceFincanceApprovalCount = adviceFincanceApprovalCount;
	}
	

}
