package com.scsoft.myport.service.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LeaveAvailabilityDTO {
	@JsonProperty("leave_type")
	private String leaveType;
	@JsonProperty("leaves_remaining")
	private Double leavesRemaining;
	@JsonProperty("leaves_taken")
	private Double leavesTaken;
	public String getLeaveType() {
		return leaveType;
	}
	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}
	public Double getLeavesRemaining() {
		return leavesRemaining;
	}
	public void setLeavesRemaining(Double leavesRemaining) {
		this.leavesRemaining = leavesRemaining;
	}
	public Double getLeavesTaken() {
		return leavesTaken;
	}
	public void setLeavesTaken(Double leavesTaken) {
		this.leavesTaken = leavesTaken;
	}
	

}
