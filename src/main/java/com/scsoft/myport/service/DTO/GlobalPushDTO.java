package com.scsoft.myport.service.DTO;

public class GlobalPushDTO {
	private String title;
	private String body;
	private String avathar;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getAvathar() {
		return avathar;
	}
	public void setAvathar(String avathar) {
		this.avathar = avathar;
	}
	
	

}
