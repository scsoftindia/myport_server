package com.scsoft.myport.service.DTO;

import java.util.Date;

import com.scsoft.myport.domain.ApplyHoliday;
import com.scsoft.myport.domain.EmpPersonal;
import com.scsoft.myport.domain.Holidays;

import net.logstash.logback.encoder.org.apache.commons.lang.StringUtils;

public class HolidayHistroryByEmpDTO {
	private Integer id;
	private Integer empId;
	private String empName;
	private Integer holidayId;
	private String holidayName;
	private Date holidayDate;
	private String status;
	private Date appliedDate;
	private Date approvedDate;
	private String supervisorNote;
	private Boolean isHolidayCancellation;
	private String cancellationReason;
	private Date holidayCancelledDate;

	public HolidayHistroryByEmpDTO() {
		super();
	}

	public HolidayHistroryByEmpDTO(ApplyHoliday applyHoliday, EmpPersonal empPersonal, Holidays holidays) {
		this.isHolidayCancellation = false;
		this.id = applyHoliday.getId();
		this.empId = applyHoliday.getEmp_id();
		this.holidayId = applyHoliday.getHolidayId();
		this.appliedDate = applyHoliday.getAppliedOn();
		this.approvedDate = applyHoliday.getReviewedDate();
		this.supervisorNote = applyHoliday.getStatusNote();
		this.holidayCancelledDate = applyHoliday.getCancelDate();
		this.cancellationReason = applyHoliday.getCancelReason();
		this.holidayName = holidays.getHoliday_name();
		this.holidayDate = holidays.getHoliday_date();
		StringBuilder nameBuilder = new StringBuilder();
		nameBuilder.append(empPersonal.getEmp_fname());
		if (StringUtils.isNotEmpty(empPersonal.getEmp_mname())) {
			nameBuilder.append(" ");
			nameBuilder.append(empPersonal.getEmp_mname());
		}
		nameBuilder.append(empPersonal.getEmp_lname());
		this.empName = nameBuilder.toString();
		Integer status = applyHoliday.getStatus();
		switch (status) {
		case 2:
			this.status = "Waiting for approval";

			break;
		case 9:
			this.status = "Approved";

			break;
		case 10:
			this.status = "Rejected";

			break;
		case 11:
			this.status = "Cancellation approval waiting";
			this.isHolidayCancellation = true;

			break;
		case 12:
			this.status = "Cancellation approved";
			this.isHolidayCancellation = true;

			break;
		case 13:
			this.status = "Cancellation rejected";
			this.isHolidayCancellation = true;

			break;
		case 20:
			this.status = "Cancellation approval waiting";
			this.isHolidayCancellation = true;

			break;
		case 21:
			this.status = "Cancellation approved";
			this.isHolidayCancellation = true;

			break;
		case 22:
			this.status = "Cancellation rejected";
			this.isHolidayCancellation = true;

			break;

		default:
			break;
		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public Integer getHolidayId() {
		return holidayId;
	}

	public void setHolidayId(Integer holidayId) {
		this.holidayId = holidayId;
	}

	public String getHolidayName() {
		return holidayName;
	}

	public void setHolidayName(String holidayName) {
		this.holidayName = holidayName;
	}

	public Date getHolidayDate() {
		return holidayDate;
	}

	public void setHolidayDate(Date holidayDate) {
		this.holidayDate = holidayDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getAppliedDate() {
		return appliedDate;
	}

	public void setAppliedDate(Date appliedDate) {
		this.appliedDate = appliedDate;
	}

	public Date getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}

	public String getSupervisorNote() {
		return supervisorNote;
	}

	public void setSupervisorNote(String supervisorNote) {
		this.supervisorNote = supervisorNote;
	}

	public Boolean getIsHolidayCancellation() {
		return isHolidayCancellation;
	}

	public void setIsHolidayCancellation(Boolean isHolidayCancellation) {
		this.isHolidayCancellation = isHolidayCancellation;
	}

	public String getCancellationReason() {
		return cancellationReason;
	}

	public void setCancellationReason(String cancellationReason) {
		this.cancellationReason = cancellationReason;
	}

	public Date getHolidayCancelledDate() {
		return holidayCancelledDate;
	}

	public void setHolidayCancelledDate(Date holidayCancelledDate) {
		this.holidayCancelledDate = holidayCancelledDate;
	}

	

}
