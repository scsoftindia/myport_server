package com.scsoft.myport.service.DTO;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.scsoft.myport.domain.LeaveTypes;

public class LeaveDTO {
	private Integer id;
	private String emp_fname;
	private String emp_mname;
	private String emp_lname;
	private String emp_avatar;
	private Integer empId;
	private LeaveTypes leaveTypes;
	private LeaveTypes halfDayTypeLeaveTypes;
	private Date appliedDate;
	private Date fromDate;
	private Date toDate;
	private BigDecimal noOfDays;
	private BigDecimal leaveTypeRemaining;
	private String leaveReason;
	private String acceptLop;
	private String supApproval;
	private Date supApprovalDate;
	private Date cancelDate;
	private String cancelReason;
	private String cancel_approval;
	private Date cancelApprovalDate;
	private Integer applicationStatus;
	private Integer cancelApprovalBy;
	private String addsup_approval;
	private Boolean isLeaveCancelled;
	private String currentStatus;
	private String rejectionNote;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LeaveTypes getLeaveTypes() {
		return leaveTypes;
	}

	public void setLeaveTypes(LeaveTypes leaveTypes) {
		this.leaveTypes = leaveTypes;
	}

	public LeaveTypes getHalfDayTypeLeaveTypes() {
		return halfDayTypeLeaveTypes;
	}

	public void setHalfDayTypeLeaveTypes(LeaveTypes halfDayTypeLeaveTypes) {
		this.halfDayTypeLeaveTypes = halfDayTypeLeaveTypes;
	}

	public Date getAppliedDate() {
		return appliedDate;
	}

	public void setAppliedDate(Date appliedDate) {
		this.appliedDate = appliedDate;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public BigDecimal getNoOfDays() {
		return noOfDays;
	}

	public void setNoOfDays(BigDecimal noOfDays) {
		this.noOfDays = noOfDays;
	}

	public BigDecimal getLeaveTypeRemaining() {
		return leaveTypeRemaining;
	}

	public void setLeaveTypeRemaining(BigDecimal leaveTypeRemaining) {
		this.leaveTypeRemaining = leaveTypeRemaining;
	}

	public String getLeaveReason() {
		return leaveReason;
	}

	public void setLeaveReason(String leaveReason) {
		this.leaveReason = leaveReason;
	}

	public String getAcceptLop() {
		return acceptLop;
	}

	public void setAcceptLop(String acceptLop) {
		this.acceptLop = acceptLop;
	}

	public String getSupApproval() {
		return supApproval;
	}

	public void setSupApproval(String supApproval) {
		this.supApproval = supApproval;
	}

	public Date getSupApprovalDate() {
		return supApprovalDate;
	}

	public void setSupApprovalDate(Date supApprovalDate) {
		this.supApprovalDate = supApprovalDate;
	}

	public Date getCancelDate() {
		return cancelDate;
	}

	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}

	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public String getCancel_approval() {
		return cancel_approval;
	}

	public void setCancel_approval(String cancel_approval) {
		this.cancel_approval = cancel_approval;
	}

	public Date getCancelApprovalDate() {
		return cancelApprovalDate;
	}

	public void setCancelApprovalDate(Date cancelApprovalDate) {
		this.cancelApprovalDate = cancelApprovalDate;
	}

	public Integer getApplicationStatus() {
		return applicationStatus;
	}

	public void setApplicationStatus(Integer applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	public Integer getCancelApprovalBy() {
		return cancelApprovalBy;
	}

	public void setCancelApprovalBy(Integer cancelApprovalBy) {
		this.cancelApprovalBy = cancelApprovalBy;
	}

	public String getEmp_fname() {
		return emp_fname;
	}

	public void setEmp_fname(String emp_fname) {
		this.emp_fname = emp_fname;
	}

	public String getEmp_mname() {
		return emp_mname;
	}

	public void setEmp_mname(String emp_mname) {
		this.emp_mname = emp_mname;
	}

	public String getEmp_lname() {
		return emp_lname;
	}

	public void setEmp_lname(String emp_lname) {
		this.emp_lname = emp_lname;
	}


	public String getEmp_avatar() {
		return emp_avatar;
	}

	public void setEmp_avatar(String emp_avatar) {
		this.emp_avatar = emp_avatar;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getAddsup_approval() {
		return addsup_approval;
	}

	public void setAddsup_approval(String addsup_approval) {
		this.addsup_approval = addsup_approval;
	}

	public Boolean getIsLeaveCancelled() {
		if(this.isLeaveCancelled == null) {
			this.isLeaveCancelled = false;
		}
		return isLeaveCancelled;
	}

	public void setIsLeaveCancelled(Boolean isLeaveCancelled) {
		this.isLeaveCancelled = isLeaveCancelled;
	}

	public String getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}

	public String getRejectionNote() {
		return rejectionNote;
	}

	public void setRejectionNote(String rejectionNote) {
		this.rejectionNote = rejectionNote;
	}


	
	
	

	
	
	
	
	

}
