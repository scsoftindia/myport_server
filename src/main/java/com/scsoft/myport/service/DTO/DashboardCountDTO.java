package com.scsoft.myport.service.DTO;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DashboardCountDTO {

	@JsonProperty("leaves_availability")
	List<LeaveAvailabilityDTO> leaveAvailabilityDTOs;
	@JsonProperty("holidays_remaining")
	private Integer holidaysRemaining;
	@JsonProperty("holidays_taken")
	private Integer holidaysTaken;
	@JsonProperty("holidays_request_pending")
	private Integer holidaysRequestPending;
	@JsonProperty("leave_request_pending")
	private Integer leaveRequestPending;
	@JsonProperty("payment_advice_pending")
	private Integer paymentAdvicePending;
	@JsonProperty("wfh_pending")
	private Integer wfhPending;
	@JsonProperty("sdn_count")
	private Integer sdn_count;
	@JsonProperty("casual_leave_count")
	private Double casualLeaveCount;

	public List<LeaveAvailabilityDTO> getLeaveAvailabilityDTOs() {
		return leaveAvailabilityDTOs;
	}

	public void setLeaveAvailabilityDTOs(List<LeaveAvailabilityDTO> leaveAvailabilityDTOs) {
		this.leaveAvailabilityDTOs = leaveAvailabilityDTOs;
	}

	public Integer getHolidaysRemaining() {
		return holidaysRemaining;
	}

	public void setHolidaysRemaining(Integer holidaysRemaining) {
		this.holidaysRemaining = holidaysRemaining;
	}

	public Integer getHolidaysTaken() {
		return holidaysTaken;
	}

	public void setHolidaysTaken(Integer holidaysTaken) {
		this.holidaysTaken = holidaysTaken;
	}

	public Integer getHolidaysRequestPending() {
		return holidaysRequestPending;
	}

	public void setHolidaysRequestPending(Integer holidaysRequestPending) {
		this.holidaysRequestPending = holidaysRequestPending;
	}

	public Integer getLeaveRequestPending() {
		return leaveRequestPending;
	}

	public void setLeaveRequestPending(Integer leaveRequestPending) {
		this.leaveRequestPending = leaveRequestPending;
	}

	public Integer getPaymentAdvicePending() {
		return paymentAdvicePending;
	}

	public void setPaymentAdvicePending(Integer paymentAdvicePending) {
		this.paymentAdvicePending = paymentAdvicePending;
	}

	public Integer getWfhPending() {
		return wfhPending;
	}

	public void setWfhPending(Integer wfhPending) {
		this.wfhPending = wfhPending;
	}

	public Integer getSdn_count() {
		return sdn_count;
	}

	public void setSdn_count(Integer sdn_count) {
		this.sdn_count = sdn_count;
	}

	public Double getCasualLeaveCount() {
		if (casualLeaveCount == null) {
			casualLeaveCount = 0d;
		}
		return casualLeaveCount;
	}

	public void setCasualLeaveCount(Double casualLeaveCount) {
		this.casualLeaveCount = casualLeaveCount;
	}

}
