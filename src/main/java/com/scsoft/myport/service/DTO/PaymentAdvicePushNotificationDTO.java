package com.scsoft.myport.service.DTO;

import java.util.Date;

public class PaymentAdvicePushNotificationDTO {
	
	private Integer id;
	private String category_name;
	private String sub_category_name;
	private String vendor_name;
	private double bill_amount;
	private Date bill_period_from;
	private Date bill_peroid_to;
	private String currency;
	private String paymentMode;
	private Date applied_on;
	private String applied_emp_name;
	private Integer applied_emp_Id;
	private String applied_emp_avathar;
	private Boolean isPrimaryApprover;
	private String description;
	private Boolean isForwardedRequest;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCategory_name() {
		return category_name;
	}
	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}
	public String getSub_category_name() {
		return sub_category_name;
	}
	public void setSub_category_name(String sub_category_name) {
		this.sub_category_name = sub_category_name;
	}
	public String getVendor_name() {
		return vendor_name;
	}
	public void setVendor_name(String vendor_name) {
		this.vendor_name = vendor_name;
	}
	public double getBill_amount() {
		return bill_amount;
	}
	public void setBill_amount(double bill_amount) {
		this.bill_amount = bill_amount;
	}
	public Date getBill_period_from() {
		return bill_period_from;
	}
	public void setBill_period_from(Date bill_period_from) {
		this.bill_period_from = bill_period_from;
	}
	public Date getBill_peroid_to() {
		return bill_peroid_to;
	}
	public void setBill_peroid_to(Date bill_peroid_to) {
		this.bill_peroid_to = bill_peroid_to;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public Date getApplied_on() {
		return applied_on;
	}
	public void setApplied_on(Date applied_on) {
		this.applied_on = applied_on;
	}
	public String getApplied_emp_name() {
		return applied_emp_name;
	}
	public void setApplied_emp_name(String applied_emp_name) {
		this.applied_emp_name = applied_emp_name;
	}
	public Integer getApplied_emp_Id() {
		return applied_emp_Id;
	}
	public void setApplied_emp_Id(Integer applied_emp_Id) {
		this.applied_emp_Id = applied_emp_Id;
	}
	public Boolean getIsPrimaryApprover() {
		if(this.isPrimaryApprover == null) {
			this.isPrimaryApprover = false;
		}
		return isPrimaryApprover;
	}
	public void setIsPrimaryApprover(Boolean isPrimaryApprover) {
		this.isPrimaryApprover = isPrimaryApprover;
	}
	public String getApplied_emp_avathar() {
		return applied_emp_avathar;
	}
	public void setApplied_emp_avathar(String applied_emp_avathar) {
		this.applied_emp_avathar = applied_emp_avathar;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Boolean getIsForwardedRequest() {
		if(this.isForwardedRequest == null) {
			this.isForwardedRequest = false;
		}
		return isForwardedRequest;
	}
	public void setIsForwardedRequest(Boolean isForwardedRequest) {
		this.isForwardedRequest = isForwardedRequest;
	}
	
	
	
	
	
	

}
