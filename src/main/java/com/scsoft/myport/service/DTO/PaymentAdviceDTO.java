package com.scsoft.myport.service.DTO;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.scsoft.myport.domain.ApprovalStatusType;
import com.scsoft.myport.domain.CurrencyType;
import com.scsoft.myport.domain.PayadviceCategories;
import com.scsoft.myport.domain.PayadviceSubCategories;
import com.scsoft.myport.domain.PaymentAdviceDocuments;
import com.scsoft.myport.domain.PaymentBank;
import com.scsoft.myport.domain.PaymentModes;
import com.scsoft.myport.domain.Vendors;
import com.scsoft.myport.service.util.DateSerializer;
import com.scsoft.myport.domain.PaymentAdvice.PaymentRemark;
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PaymentAdviceDTO {

	private Integer id;
	private PayadviceCategories payadviceCategories;
	private String category_name;
	private Integer category_id;
	private PayadviceSubCategories payadviceSubCategories;
	private String sub_category_name;
	private Integer sub_category_id;
	private Vendors vendors;
	private String vendor_name;
	private Integer vendor_id;
	private String description;
	private Date bill_date;
	private Date due_date;
	private String bill_number;
	private double bill_amount;
	private String amount;
	private Date bill_period_from;
	private Date bill_peroid_to;
	private CurrencyType currencyType;
	private String currency_type_name;
	private Integer currency_type_id;
	private PaymentModes paymentModes;
	private String payment_mode_name;
	private Integer payment_mode_id;
//	@JsonSerialize(using=DateSerializer.class)
	private DateTime payment_release_time;
//	@JsonSerialize(using=DateSerializer.class)
	private DateTime payment_release_reject_time;
	private String release_note;
	private String release_reject_note;
	private String payment_cash_billno;
	private String payment_cheque_number;
	private Date payment_cheque_date;
	private Date payment_online_date;
	@JsonSerialize(using=DateSerializer.class)
	private DateTime applied_on;
	private ApprovalStatusType aprrovalStatus;
	private String aprrovalStatus_name;
	@JsonSerialize(using=DateSerializer.class)
	private DateTime approved_date;
	private String approval_comment;
	private String reject_reson;
	@JsonSerialize(using=DateSerializer.class)
	private DateTime reject_date;
	private PaymentRemark paymentRemark;
	private List<PaymentAdviceDocuments> paymentAdviceDocumentsList;
	private String applied_emp_name;
	private Integer applied_emp_Id;
	private String payment_release_emp_name;
	private Integer payment_release_emp_Id;
	private String reject_emp_name;
	private Integer reject_emp_Id;
	private String aprooved_emp_name;
	private Integer aprooved_emp_Id;
	private String primary_aproover_emp_name;
	private Integer primary_aproover_emp_Id;
	private Integer secondary_aproover_emp_id;
	private String secondary_aproover_name;
	private PaymentBank paymentBank;
	private String currentStatus;
	private AprooverDTO aprooverDTO;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public PayadviceCategories getPayadviceCategories() {
		return payadviceCategories;
	}

	public void setPayadviceCategories(PayadviceCategories payadviceCategories) {
		this.payadviceCategories = payadviceCategories;
	}

	public PayadviceSubCategories getPayadviceSubCategories() {
		return payadviceSubCategories;
	}

	public void setPayadviceSubCategories(PayadviceSubCategories payadviceSubCategories) {
		this.payadviceSubCategories = payadviceSubCategories;
	}

	public Vendors getVendors() {
		return vendors;
	}

	public void setVendors(Vendors vendors) {
		this.vendors = vendors;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getBill_date() {
		return bill_date;
	}

	public void setBill_date(Date bill_date) {
		this.bill_date = bill_date;
	}

	public String getBill_number() {
		return bill_number;
	}

	public void setBill_number(String bill_number) {
		this.bill_number = bill_number;
	}

	public double getBill_amount() {
		return bill_amount;
	}

	public void setBill_amount(double bill_amount) {
		this.bill_amount = bill_amount;
	}

	public Date getBill_period_from() {
		return bill_period_from;
	}

	public void setBill_period_from(Date bill_period_from) {
		this.bill_period_from = bill_period_from;
	}

	public Date getBill_peroid_to() {
		return bill_peroid_to;
	}

	public void setBill_peroid_to(Date bill_peroid_to) {
		this.bill_peroid_to = bill_peroid_to;
	}

	public CurrencyType getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(CurrencyType currencyType) {
		this.currencyType = currencyType;
	}

	public PaymentModes getPaymentModes() {
		return paymentModes;
	}

	public void setPaymentModes(PaymentModes paymentModes) {
		this.paymentModes = paymentModes;
	}

	public Date getPayment_release_time() {
		if (this.payment_release_time != null) {
			DateTime ldt = this.payment_release_time;
			// ZonedDateTime zdt = ldt.atZone(ZoneId.systemDefault());
			// Date output = Date.from(zdt.toInstant());
			Date output = ldt.toDate();
			return output;
		}
		return null;
	}

	public void setPayment_release_time(DateTime payment_release_time) {
		this.payment_release_time = payment_release_time;
	}

	public Date getPayment_release_reject_time() {
		if (this.payment_release_reject_time != null) {
			DateTime ldt = this.payment_release_reject_time;
			// ZonedDateTime zdt = ldt.atZone(ZoneId.systemDefault());
			// Date output = Date.from(zdt.toInstant());
			Date output = ldt.toDate();
			return output;
		}
		return null;
	}

	public void setPayment_release_reject_time(DateTime payment_release_reject_time) {
		this.payment_release_reject_time = payment_release_reject_time;
	}

	public String getRelease_note() {
		return release_note;
	}

	public void setRelease_note(String release_note) {
		this.release_note = release_note;
	}

	public String getRelease_reject_note() {
		return release_reject_note;
	}

	public void setRelease_reject_note(String release_reject_note) {
		this.release_reject_note = release_reject_note;
	}

	public String getPayment_cash_billno() {
		return payment_cash_billno;
	}

	public void setPayment_cash_billno(String payment_cash_billno) {
		this.payment_cash_billno = payment_cash_billno;
	}

	public String getPayment_cheque_number() {
		return payment_cheque_number;
	}

	public void setPayment_cheque_number(String payment_cheque_number) {
		this.payment_cheque_number = payment_cheque_number;
	}

	public Date getPayment_cheque_date() {
		return payment_cheque_date;
	}

	public void setPayment_cheque_date(Date payment_cheque_date) {
		this.payment_cheque_date = payment_cheque_date;
	}

	public Date getPayment_online_date() {
		return payment_online_date;
	}

	public void setPayment_online_date(Date payment_online_date) {
		this.payment_online_date = payment_online_date;
	}

	public DateTime getApplied_on() {
		return applied_on;
	}

	public void setApplied_on(DateTime applied_on) {
		this.applied_on = applied_on;
	}

	public ApprovalStatusType getAprrovalStatus() {
		return aprrovalStatus;
	}

	public void setAprrovalStatus(ApprovalStatusType aprrovalStatus) {
		this.aprrovalStatus = aprrovalStatus;
	}

	public DateTime getApproved_date() {
		return approved_date;
	}

	public void setApproved_date(DateTime approved_date) {
		this.approved_date = approved_date;
	}

	public String getApproval_comment() {
		return approval_comment;
	}

	public void setApproval_comment(String approval_comment) {
		this.approval_comment = approval_comment;
	}

	public String getReject_reson() {
		return reject_reson;
	}

	public void setReject_reson(String reject_reson) {
		this.reject_reson = reject_reson;
	}

	public DateTime getReject_date() {
		return reject_date;
	}

	public void setReject_date(DateTime reject_date) {
		this.reject_date = reject_date;
	}

	public PaymentRemark getPaymentRemark() {
		return paymentRemark;
	}

	public void setPaymentRemark(PaymentRemark paymentRemark) {
		this.paymentRemark = paymentRemark;
	}

	public List<PaymentAdviceDocuments> getPaymentAdviceDocumentsList() {
		return paymentAdviceDocumentsList;
	}

	public void setPaymentAdviceDocumentsList(List<PaymentAdviceDocuments> paymentAdviceDocumentsList) {
		this.paymentAdviceDocumentsList = paymentAdviceDocumentsList;
	}

	public String getApplied_emp_name() {
		return applied_emp_name;
	}

	public void setApplied_emp_name(String applied_emp_name) {
		this.applied_emp_name = applied_emp_name;
	}

	public Integer getApplied_emp_Id() {
		return applied_emp_Id;
	}

	public void setApplied_emp_Id(Integer applied_emp_Id) {
		this.applied_emp_Id = applied_emp_Id;
	}

	public String getPayment_release_emp_name() {
		return payment_release_emp_name;
	}

	public void setPayment_release_emp_name(String payment_release_emp_name) {
		this.payment_release_emp_name = payment_release_emp_name;
	}

	public Integer getPayment_release_emp_Id() {
		return payment_release_emp_Id;
	}

	public void setPayment_release_emp_Id(Integer payment_release_emp_Id) {
		this.payment_release_emp_Id = payment_release_emp_Id;
	}

	public String getReject_emp_name() {
		return reject_emp_name;
	}

	public void setReject_emp_name(String reject_emp_name) {
		this.reject_emp_name = reject_emp_name;
	}

	public Integer getReject_emp_Id() {
		return reject_emp_Id;
	}

	public void setReject_emp_Id(Integer reject_emp_Id) {
		this.reject_emp_Id = reject_emp_Id;
	}

	public String getAprooved_emp_name() {
		return aprooved_emp_name;
	}

	public void setAprooved_emp_name(String aprooved_emp_name) {
		this.aprooved_emp_name = aprooved_emp_name;
	}

	public Integer getAprooved_emp_Id() {
		return aprooved_emp_Id;
	}

	public void setAprooved_emp_Id(Integer aprooved_emp_Id) {
		this.aprooved_emp_Id = aprooved_emp_Id;
	}

	public String getPrimary_aproover_emp_name() {
		return primary_aproover_emp_name;
	}

	public void setPrimary_aproover_emp_name(String primary_aproover_emp_name) {
		this.primary_aproover_emp_name = primary_aproover_emp_name;
	}

	public Integer getPrimary_aproover_emp_Id() {
		return primary_aproover_emp_Id;
	}

	public void setPrimary_aproover_emp_Id(Integer primary_aproover_emp_Id) {
		this.primary_aproover_emp_Id = primary_aproover_emp_Id;
	}

	public String getCategory_name() {
		return category_name;
	}

	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}

	public Integer getCategory_id() {
		return category_id;
	}

	public void setCategory_id(Integer category_id) {
		this.category_id = category_id;
	}

	public String getSub_category_name() {
		return sub_category_name;
	}

	public void setSub_category_name(String sub_category_name) {
		this.sub_category_name = sub_category_name;
	}

	public Integer getSub_category_id() {
		return sub_category_id;
	}

	public void setSub_category_id(Integer sub_category_id) {
		this.sub_category_id = sub_category_id;
	}

	public String getVendor_name() {
		return vendor_name;
	}

	public void setVendor_name(String vendor_name) {
		this.vendor_name = vendor_name;
	}

	public Integer getVendor_id() {
		return vendor_id;
	}

	public void setVendor_id(Integer vendor_id) {
		this.vendor_id = vendor_id;
	}

	public String getCurrency_type_name() {
		return currency_type_name;
	}

	public void setCurrency_type_name(String currency_type_name) {
		this.currency_type_name = currency_type_name;
	}

	public Integer getCurrency_type_id() {
		return currency_type_id;
	}

	public void setCurrency_type_id(Integer currency_type_id) {
		this.currency_type_id = currency_type_id;
	}

	public String getPayment_mode_name() {
		return payment_mode_name;
	}

	public void setPayment_mode_name(String payment_mode_name) {
		this.payment_mode_name = payment_mode_name;
	}

	public Integer getPayment_mode_id() {
		return payment_mode_id;
	}

	public void setPayment_mode_id(Integer payment_mode_id) {
		this.payment_mode_id = payment_mode_id;
	}

	public String getAprrovalStatus_name() {
		return aprrovalStatus_name;
	}

	public void setAprrovalStatus_name(String aprrovalStatus_name) {
		this.aprrovalStatus_name = aprrovalStatus_name;
	}

	public Integer getSecondary_aproover_emp_id() {
		return secondary_aproover_emp_id;
	}

	public void setSecondary_aproover_emp_id(Integer secondary_aproover_emp_id) {
		this.secondary_aproover_emp_id = secondary_aproover_emp_id;
	}

	public String getSecondary_aproover_name() {
		return secondary_aproover_name;
	}

	public void setSecondary_aproover_name(String secondary_aproover_name) {
		this.secondary_aproover_name = secondary_aproover_name;
	}

	public PaymentBank getPaymentBank() {
		return paymentBank;
	}

	public void setPaymentBank(PaymentBank paymentBank) {
		this.paymentBank = paymentBank;
	}

	public String getCurrentStatus() {

		if (this.aprrovalStatus.getId() == 1) {
			this.currentStatus = "Waiting";
		} else if (this.aprrovalStatus.getId() == 2) {
			if (this.aprooved_emp_name != null) {
				String[] nameArray = this.aprooved_emp_name.split("\\s+");
				this.currentStatus = "Approved by " + nameArray[0];
			} else {
				this.currentStatus = "Approved";
			}

		} else if (this.aprrovalStatus.getId() == 3) {
			if (this.reject_emp_name != null) {
				String[] nameArray = this.reject_emp_name.split("\\s+");
				this.currentStatus = "Rejected by " + nameArray[0];
			} else {
				this.currentStatus = "Rejected";
			}
		} else if (this.aprrovalStatus.getId() == 4) {
			this.currentStatus = "Payment processed by finance";
		} else if (this.aprrovalStatus.getId() == 5) {
			this.currentStatus = "Payment rejected by finance";
		} else if (this.aprrovalStatus.getId() == 6) {
			if (this.primary_aproover_emp_name != null && this.secondary_aproover_name != null) {
				String[] primaryNameArray = this.primary_aproover_emp_name.split("\\s+");
				String[] secondaryNameArray = this.secondary_aproover_name.split("\\s+");
				StringBuilder stringBuilder = new StringBuilder("Approved by ");
				stringBuilder.append(primaryNameArray[0]).append(" and ").append("Forwarded to ")
						.append(secondaryNameArray[0]);
				this.currentStatus = stringBuilder.toString();
			} else {
				this.currentStatus = "Forwarded to secondary approver";
			}

		} else if (this.aprrovalStatus.getId() == 7) {
			if (this.secondary_aproover_name != null) {
				String[] secondaryNameArray = this.secondary_aproover_name.split("\\s+");
				this.currentStatus = "Payment rejected by " + secondaryNameArray[0];
			} else {
				this.currentStatus = "Payment rejected";
			}

		} else if (this.aprrovalStatus.getId() == 8) {
			if (this.secondary_aproover_name != null) {
				String[] secondaryNameArray = this.secondary_aproover_name.split("\\s+");
				this.currentStatus = "Payment approved by " + secondaryNameArray[0];
			} else {
				this.currentStatus = "Payment approved";
			}
		}
		return currentStatus;
	}

	public String getAmount() {
		if (this.currencyType != null) {
			if (this.currencyType.getId() == 1) {
				this.amount = String.valueOf(this.bill_amount) + " $";
			} else if (this.currencyType.getId() == 2) {
				this.amount = String.valueOf(this.bill_amount) + " ₹";
			} else {
				this.amount = String.valueOf(this.bill_amount);
			}
		}
		return amount;
	}

	public Date getDue_date() {
		return due_date;
	}

	public void setDue_date(Date due_date) {
		this.due_date = due_date;
	}

	public AprooverDTO getAprooverDTO() {
		return aprooverDTO;
	}

	public void setAprooverDTO(AprooverDTO aprooverDTO) {
		this.aprooverDTO = aprooverDTO;
	}
	
	

}