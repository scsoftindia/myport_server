package com.scsoft.myport.service.DTO;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.scsoft.myport.domain.ApplyLeave;
import com.scsoft.myport.domain.EmpPersonal;
import com.scsoft.myport.domain.LeaveTypes;

public class LeaveHistoryByEmpDTO {
	private Integer id;
	private Integer empId;
	private String empName;
	private LeaveTypes leaveTypes;
	private BigDecimal noOfDays;
	private Date appliedDate;
	private Date fromDate;
	private Date todate;
	private String leaveReason;
	private Date leaveCancelledDate;
	private String status;
	private Date approvedDate;
	private String supervisorComment;
	private String leaveCancellationReason;
	private Boolean isLeaveCancellation;

	public LeaveHistoryByEmpDTO() {
	}

	public LeaveHistoryByEmpDTO(ApplyLeave applyLeave) {
		this.id = applyLeave.getId();
		EmpPersonal empPersonal = applyLeave.getEmpPersonal();
		this.empId = empPersonal.getId();
		StringBuilder nameBuilder = new StringBuilder();
		nameBuilder.append(empPersonal.getEmp_fname());
		if (StringUtils.isNotEmpty(empPersonal.getEmp_mname())) {
			nameBuilder.append(" ");
			nameBuilder.append(empPersonal.getEmp_mname());
		}
		nameBuilder.append(" ");
		nameBuilder.append(empPersonal.getEmp_lname());
		this.empName = nameBuilder.toString();
		if (applyLeave.getLeaveTypes().getId() == 6) {
			this.leaveTypes = applyLeave.getHalfDayTypeLeaveTypes();
		} else {
			this.leaveTypes = applyLeave.getLeaveTypes();
		}
		this.noOfDays = applyLeave.getNoOfDays();
		this.appliedDate = applyLeave.getAppliedDate();
		this.leaveReason = applyLeave.getLeaveReason();
		this.fromDate = applyLeave.getFromDate();
		this.todate = applyLeave.getToDate();
		this.isLeaveCancellation = false;
		Integer leaveStatus = applyLeave.getApplicationStatus();
		switch (leaveStatus) {
		case 2:
			this.status = "Waiting";

			break;
		case 9:
			this.status = "Approved";
			this.approvedDate = applyLeave.getSupApprovalDate();
			if (this.approvedDate == null) {
				this.approvedDate = applyLeave.getAddsupApprovalDate();
			}

			if (StringUtils.isNotEmpty(applyLeave.getSupApprovalNote())) {
				this.supervisorComment = applyLeave.getSupApprovalNote();
			} else if (StringUtils.isNotEmpty(applyLeave.getAddsupApprovalNote())) {
				this.supervisorComment = applyLeave.getAddsupApprovalNote();
			}
			break;
		case 10:
			this.status = "Rejected";
			this.approvedDate = applyLeave.getSupApprovalDate();
			if (this.approvedDate == null) {
				this.approvedDate = applyLeave.getAddsupApprovalDate();
			}

			if (StringUtils.isNotEmpty(applyLeave.getSupApprovalNote())) {
				this.supervisorComment = applyLeave.getSupApprovalNote();
			} else if (StringUtils.isNotEmpty(applyLeave.getAddsupApprovalNote())) {
				this.supervisorComment = applyLeave.getAddsupApprovalNote();
			}
			break;
		case 11:
			this.status = "Leave cancellation waiting for approval";
			this.isLeaveCancellation = true;
			this.leaveCancelledDate = applyLeave.getCancelDate();
			this.leaveCancellationReason = applyLeave.getCancelReason();
			break;
		case 12:
			this.status = "Leave cancellation approved";
			this.isLeaveCancellation = true;
			this.leaveCancelledDate = applyLeave.getCancelDate();
			this.leaveCancellationReason = applyLeave.getCancelReason();
			this.approvedDate = applyLeave.getCancelApprovalDate();
			this.supervisorComment = applyLeave.getCancelApprovalNote();
			break;
		case 13:
			this.status = "Leave cancellation rejected";
			this.isLeaveCancellation = true;
			this.leaveCancelledDate = applyLeave.getCancelDate();
			this.leaveCancellationReason = applyLeave.getCancelReason();
			this.approvedDate = applyLeave.getCancelApprovalDate();
			this.supervisorComment = applyLeave.getCancelApprovalNote();
			break;

		default:
			break;
		}

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public LeaveTypes getLeaveTypes() {
		return leaveTypes;
	}

	public void setLeaveTypes(LeaveTypes leaveTypes) {
		this.leaveTypes = leaveTypes;
	}

	public BigDecimal getNoOfDays() {
		return noOfDays;
	}

	public void setNoOfDays(BigDecimal noOfDays) {
		this.noOfDays = noOfDays;
	}

	public Date getAppliedDate() {
		return appliedDate;
	}

	public void setAppliedDate(Date appliedDate) {
		this.appliedDate = appliedDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getApprovedDate() {
		return approvedDate;
	}

	public void setApprovedDate(Date approvedDate) {
		this.approvedDate = approvedDate;
	}

	public String getSupervisorComment() {
		return supervisorComment;
	}

	public void setSupervisorComment(String supervisorComment) {
		this.supervisorComment = supervisorComment;
	}

	public Date getLeaveCancelledDate() {
		return leaveCancelledDate;
	}

	public void setLeaveCancelledDate(Date leaveCancelledDate) {
		this.leaveCancelledDate = leaveCancelledDate;
	}

	public String getLeaveCancellationReason() {
		return leaveCancellationReason;
	}

	public void setLeaveCancellationReason(String leaveCancellationReason) {
		this.leaveCancellationReason = leaveCancellationReason;
	}

	public Boolean getIsLeaveCancellation() {
		return isLeaveCancellation;
	}

	public void setIsLeaveCancellation(Boolean isLeaveCancellation) {
		this.isLeaveCancellation = isLeaveCancellation;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getTodate() {
		return todate;
	}

	public void setTodate(Date todate) {
		this.todate = todate;
	}

	public String getLeaveReason() {
		return leaveReason;
	}

	public void setLeaveReason(String leaveReason) {
		this.leaveReason = leaveReason;
	}

}
