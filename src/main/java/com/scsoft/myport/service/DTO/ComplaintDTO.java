package com.scsoft.myport.service.DTO;

import java.util.Date;

public class ComplaintDTO {
	private Integer id;
	private Integer emp_id;
	private String emp_fname;
	private String emp_mname;
	private String emp_lname;
	private String emp_avathar;
	private Integer cc_id;
	private String complaintType;
	private String cc_subject;
	private String cc_remark;
	private String cc_doc;
	private String cc_resolved;
	private String cc_resolve_remark;
	private Date cc_resolved_on;
	private Integer cc_resolved_by;
	private String resolved_fname;
	private String resolved_mname;
	private String resolved_lname;
	
	public ComplaintDTO() {
		
	}

	public ComplaintDTO(Integer id, Integer emp_id, Integer cc_id, String complaintType, String cc_subject,
			String cc_remark, String cc_doc, String cc_resolved, String cc_resolve_remark, Date cc_resolved_on,
			Integer cc_resolved_by, String resolved_fname, String resolved_mname, String resolved_lname,String emp_fname,String emp_mname,String emp_lname,String emp_avathar) {
		this.id = id;
		this.emp_id = emp_id;
		this.cc_id = cc_id;
		this.complaintType = complaintType;
		this.cc_subject = cc_subject;
		this.cc_remark = cc_remark;
		this.cc_doc = cc_doc;
		this.cc_resolved = cc_resolved;
		this.cc_resolve_remark = cc_resolve_remark;
		this.cc_resolved_on = cc_resolved_on;
		this.cc_resolved_by = cc_resolved_by;
		this.resolved_fname = resolved_fname;
		this.resolved_mname = resolved_mname;
		this.resolved_lname = resolved_lname;
		this.emp_fname = emp_fname;
		this.emp_mname = emp_mname;
		this.emp_lname = emp_lname;
		this.emp_avathar = emp_avathar;
	}
	public ComplaintDTO(Integer id, Integer emp_id, Integer cc_id, String complaintType, String cc_subject,
			String cc_remark, String cc_doc, String cc_resolved, String cc_resolve_remark, Date cc_resolved_on,
			Integer cc_resolved_by, String resolved_fname, String resolved_mname, String resolved_lname) {
		this.id = id;
		this.emp_id = emp_id;
		this.cc_id = cc_id;
		this.complaintType = complaintType;
		this.cc_subject = cc_subject;
		this.cc_remark = cc_remark;
		this.cc_doc = cc_doc;
		this.cc_resolved = cc_resolved;
		this.cc_resolve_remark = cc_resolve_remark;
		this.cc_resolved_on = cc_resolved_on;
		this.cc_resolved_by = cc_resolved_by;
		this.resolved_fname = resolved_fname;
		this.resolved_mname = resolved_mname;
		this.resolved_lname = resolved_lname;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getEmp_id() {
		return emp_id;
	}
	public void setEmp_id(Integer emp_id) {
		this.emp_id = emp_id;
	}
	public Integer getCc_id() {
		return cc_id;
	}
	public void setCc_id(Integer cc_id) {
		this.cc_id = cc_id;
	}
	public String getComplaintType() {
		return complaintType;
	}
	public void setComplaintType(String complaintType) {
		this.complaintType = complaintType;
	}
	public String getCc_subject() {
		return cc_subject;
	}
	public void setCc_subject(String cc_subject) {
		this.cc_subject = cc_subject;
	}
	public String getCc_remark() {
		return cc_remark;
	}
	public void setCc_remark(String cc_remark) {
		this.cc_remark = cc_remark;
	}
	public String getCc_doc() {
		return cc_doc;
	}
	public void setCc_doc(String cc_doc) {
		this.cc_doc = cc_doc;
	}
	public String getCc_resolved() {
		return cc_resolved;
	}
	public void setCc_resolved(String cc_resolved) {
		this.cc_resolved = cc_resolved;
	}
	public String getCc_resolve_remark() {
		return cc_resolve_remark;
	}
	public void setCc_resolve_remark(String cc_resolve_remark) {
		this.cc_resolve_remark = cc_resolve_remark;
	}
	public Date getCc_resolved_on() {
		return cc_resolved_on;
	}
	public void setCc_resolved_on(Date cc_resolved_on) {
		this.cc_resolved_on = cc_resolved_on;
	}
	public Integer getCc_resolved_by() {
		return cc_resolved_by;
	}
	public void setCc_resolved_by(Integer cc_resolved_by) {
		this.cc_resolved_by = cc_resolved_by;
	}
	public String getResolved_fname() {
		return resolved_fname;
	}
	public void setResolved_fname(String resolved_fname) {
		this.resolved_fname = resolved_fname;
	}
	public String getResolved_mname() {
		return resolved_mname;
	}
	public void setResolved_mname(String resolved_mname) {
		this.resolved_mname = resolved_mname;
	}
	public String getResolved_lname() {
		return resolved_lname;
	}
	public void setResolved_lname(String resolved_lname) {
		this.resolved_lname = resolved_lname;
	}

	public String getEmp_fname() {
		return emp_fname;
	}

	public void setEmp_fname(String emp_fname) {
		this.emp_fname = emp_fname;
	}

	public String getEmp_mname() {
		return emp_mname;
	}

	public void setEmp_mname(String emp_mname) {
		this.emp_mname = emp_mname;
	}

	public String getEmp_lname() {
		return emp_lname;
	}

	public void setEmp_lname(String emp_lname) {
		this.emp_lname = emp_lname;
	}

	public String getEmp_avathar() {
		return emp_avathar;
	}

	public void setEmp_avathar(String emp_avathar) {
		this.emp_avathar = emp_avathar;
	}
	
	
	
	
	
}
