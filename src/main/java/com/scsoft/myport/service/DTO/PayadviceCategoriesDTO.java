package com.scsoft.myport.service.DTO;

import java.util.Date;

import javax.persistence.Transient;

public class PayadviceCategoriesDTO {
	private Integer id;
	private String cat_name;
	private String cat_description;
	private Boolean is_approved;
	private Boolean is_rejected;
	private String cretaed_emp_fname;
	private String cretaed_emp_mname;
	private String cretaed_emp_lname;
	private String createdEmp_full_name;
	private Integer cretaed_emp_id;
	private String modified_emp_fname;
	private String modified_emp_mname;
	private String modified_emp_lname;
	private String modified_full_name;
	private Integer modified_emp_id;
	private Date createdDate;
	private Date lastModifiedDate; 

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCat_name() {
		return cat_name;
	}

	public void setCat_name(String cat_name) {
		this.cat_name = cat_name;
	}

	public String getCat_description() {
		return cat_description;
	}

	public void setCat_description(String cat_description) {
		this.cat_description = cat_description;
	}

	public Boolean getIs_approved() {
		return is_approved;
	}

	public void setIs_approved(Boolean is_approved) {
		this.is_approved = is_approved;
	}

	public String getCretaed_emp_fname() {
		return cretaed_emp_fname;
	}

	public void setCretaed_emp_fname(String cretaed_emp_fname) {
		this.cretaed_emp_fname = cretaed_emp_fname;
	}

	public String getCretaed_emp_mname() {
		return cretaed_emp_mname;
	}

	public void setCretaed_emp_mname(String cretaed_emp_mname) {
		this.cretaed_emp_mname = cretaed_emp_mname;
	}

	public String getCretaed_emp_lname() {
		return cretaed_emp_lname;
	}

	public void setCretaed_emp_lname(String cretaed_emp_lname) {
		this.cretaed_emp_lname = cretaed_emp_lname;
	}

	public Integer getCretaed_emp_id() {

		return cretaed_emp_id;
	}

	public void setCretaed_emp_id(Integer cretaed_emp_id) {
		this.cretaed_emp_id = cretaed_emp_id;
	}

	public String getCreatedEmp_full_name() {
		StringBuilder name = new StringBuilder();
		name.append(this.cretaed_emp_fname).append(" ");
		name.append(this.cretaed_emp_mname).append(" ");
		name.append(this.cretaed_emp_lname);
		this.createdEmp_full_name = name.toString();
		return createdEmp_full_name;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getModified_emp_fname() {
		return modified_emp_fname;
	}

	public void setModified_emp_fname(String modified_emp_fname) {
		this.modified_emp_fname = modified_emp_fname;
	}

	public String getModified_emp_mname() {
		return modified_emp_mname;
	}

	public void setModified_emp_mname(String modified_emp_mname) {
		this.modified_emp_mname = modified_emp_mname;
	}

	public String getModified_emp_lname() {
		return modified_emp_lname;
	}

	public void setModified_emp_lname(String modified_emp_lname) {
		this.modified_emp_lname = modified_emp_lname;
	}

	public String getModified_full_name() {
		StringBuilder name = new StringBuilder();
		name.append(this.modified_emp_fname).append(" ");
		name.append(this.modified_emp_mname).append(" ");
		name.append(this.modified_emp_lname);
		this.modified_full_name = name.toString();
		return modified_full_name;
	}

	public Integer getModified_emp_id() {
		return modified_emp_id;
	}

	public void setModified_emp_id(Integer modified_emp_id) {
		this.modified_emp_id = modified_emp_id;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public Boolean getIs_rejected() {
		if(this.is_rejected == null) {
			this.is_rejected = false;
		}
		return is_rejected;
	}

	public void setIs_rejected(Boolean is_rejected) {
		this.is_rejected = is_rejected;
	}
	
	
	

	

}
