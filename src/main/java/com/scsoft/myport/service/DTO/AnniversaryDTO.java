package com.scsoft.myport.service.DTO;

import java.util.Date;

import com.scsoft.myport.domain.Designation;

public class AnniversaryDTO {
	private Integer empId;
	private String fname;
	private String mname;
	private String lname;
	private String imageUrl;
	private Date date;
	private Integer noOfYear;
	private Designation designation;
	
	

	public AnniversaryDTO(Integer empId, String fname, String mname, String lname, String imageUrl, Date date,
			 Designation designation) {
		this.empId = empId;
		this.fname = fname;
		this.mname = mname;
		this.lname = lname;
		this.imageUrl = imageUrl;
		this.date = date;
		this.designation = designation;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getMname() {
		return mname;
	}

	public void setMname(String mname) {
		this.mname = mname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getNoOfYear() {
		return noOfYear;
	}

	public void setNoOfYear(Integer noOfYear) {
		this.noOfYear = noOfYear;
	}

	public Designation getDesignation() {
		return designation;
	}

	public void setDesignation(Designation designation) {
		this.designation = designation;
	}

}
