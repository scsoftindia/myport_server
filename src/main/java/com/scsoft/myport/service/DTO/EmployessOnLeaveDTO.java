package com.scsoft.myport.service.DTO;

import java.util.Date;

public class EmployessOnLeaveDTO {
	private Integer empId;
	private String fname;
	private String mname;
	private String lname;
	private String leaveType;
	private String halfDayType;
	private String designation;
	private Date date;

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getMname() {
		return mname;
	}

	public void setMname(String mname) {
		this.mname = mname;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}

	public String getHalfDayType() {
		return halfDayType;
	}

	public void setHalfDayType(String halfDayType) {
		this.halfDayType = halfDayType;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public Date getDate() {
		if (this.date == null) {
			this.date = new Date();
		}
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

//	public EmployessOnLeaveDTO(Integer empId, String fname, String mname, String lname, String leaveType,
//			String halfDayType) {
//		this.empId = empId;
//		this.fname = fname;
//		this.mname = mname;
//		this.lname = lname;
//		this.leaveType = leaveType;
//		this.halfDayType = halfDayType;
////		this.designation = designation;
//	}

}
