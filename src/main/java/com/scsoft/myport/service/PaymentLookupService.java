package com.scsoft.myport.service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.scsoft.myport.domain.CurrencyType;
import com.scsoft.myport.domain.EmpEmployment;
import com.scsoft.myport.domain.EmpPersonal;
import com.scsoft.myport.domain.FcmDetail;
import com.scsoft.myport.domain.PayadviceCategories;
import com.scsoft.myport.domain.PayadviceSubCategories;
import com.scsoft.myport.domain.PaymentBank;
import com.scsoft.myport.domain.PaymentModes;
import com.scsoft.myport.domain.Vendors;
import com.scsoft.myport.repository.CurrencyTypeRepository;
import com.scsoft.myport.repository.EmpEmploymentRepository;
import com.scsoft.myport.repository.EmpPersonalRepository;
import com.scsoft.myport.repository.FcmDetailRepository;
import com.scsoft.myport.repository.PayadviceCategoriesRepository;
import com.scsoft.myport.repository.PayadviceSubCategoriesRepository;
import com.scsoft.myport.repository.PaymentAdviceRepository;
import com.scsoft.myport.repository.PaymentBankRepository;
import com.scsoft.myport.repository.PaymentModesRepository;
import com.scsoft.myport.repository.VendorsRepository;
import com.scsoft.myport.rest.request.FcmDetailRequest;
import com.scsoft.myport.service.DTO.ApprovalCountDTO;
import com.scsoft.myport.service.DTO.AprooverDTO;
import com.scsoft.myport.service.DTO.PayadviceCategoriesDTO;
import com.scsoft.myport.service.DTO.PaymentBankDTO;
import com.scsoft.myport.service.mapper.EmployeePersonalMapper;
import com.scsoft.myport.service.mapper.PayadviceCategoriesMapper;
import com.scsoft.myport.service.mapper.PaymentBankMapper;

@Service("paymentLookupService")
public class PaymentLookupService {
	private final Logger logger = LoggerFactory.getLogger(PaymentLookupService.class);
	@Autowired
	private PaymentModesRepository paymentModesRepository;
	@Autowired
	private PayadviceCategoriesRepository payadviceCategoriesRepository;
	@Autowired
	private PayadviceSubCategoriesRepository payadviceSubCategoriesRepository;
	@Autowired
	private VendorsRepository vendorsRepository;
	@Autowired
	private CurrencyTypeRepository currencyTypeRepository;
	@Autowired
	private PayadviceCategoriesMapper payadviceCategoriesMapper;
	@Autowired
	private EmpPersonalRepository empPersonalRepository;
	@Autowired
	private EmployeePersonalMapper employeePersonalMapper;
	@Autowired
	private FcmDetailRepository fcmDetailRepository;
	@Autowired
	private EmpEmploymentRepository empEmploymentRepository;
	@Autowired
	private PaymentAdviceRepository paymentAdviceRepository;
	@Autowired
	private PaymentBankRepository paymentBankRepository;
	@Autowired
	private PaymentBankMapper paymentBankMapper;

	public List<PaymentModes> getAllPayments() {
		logger.debug("Request to fetch all payments from DB");
		return paymentModesRepository.findAll();
	}

	public List<PayadviceCategories> getAllCategories() {
		logger.debug("Retrieving all active Categories from DB ");
		return payadviceCategoriesRepository.getActiveCategories();
	}
	public PayadviceCategories getCategoryById(Integer id) {
		logger.debug("Retrieving Category by Id");
		return this.payadviceCategoriesRepository.findOne(id);
	}
	public PayadviceSubCategories getSubCategoryById(Integer id) {
		logger.debug("Retrieving Sub Category by Id");
		return this.payadviceSubCategoriesRepository.findOne(id);
	}
	public Vendors getVendorsById(Integer id) {
		logger.debug("Retrieving Vendors by Id");
		return this.vendorsRepository.findOne(id);
	}

	public List<PayadviceSubCategories> getAllSubCategoriesByCategories(Integer categoryId) {
		logger.debug("Retrieving all Actibe Sub categories from Db by Category Id");
		return payadviceSubCategoriesRepository.getActivePayadviceSubCategoriesByCategory(categoryId);
	}

	public Page<PayadviceSubCategories> getAllSubCategories(Integer categoryId, Pageable pageable, Boolean isApproval) {
		logger.debug("Retrieving all Actibe Sub categories from Db");
		if (!isApproval) {
			if (categoryId == null || categoryId == 0) {
				return payadviceSubCategoriesRepository.getAllPayadviceSubCategories(pageable);
			} else {
				return payadviceSubCategoriesRepository.getAllPayadviceSubCategoriesByCategory(categoryId, pageable);
			}
		} else {
			if (categoryId == null || categoryId == 0) {
				return payadviceSubCategoriesRepository.getAllPendingPayadviceSubCategories(pageable);
			} else {
				return payadviceSubCategoriesRepository.getAllPendingPayadviceSubCategoriesByCategory(categoryId,pageable);
			}
		}
	}

	public Page<Vendors> getAllVendors(Integer categoryId, Pageable pageable,Boolean isApproval) {
		logger.debug("Retrieving all Actibe Sub categories from Db");
		if (!isApproval) {
		if (categoryId == null || categoryId == 0) {
			return this.vendorsRepository.getAllVendors(pageable);
		} else {
			return this.vendorsRepository.getAllVendorsByCategory(categoryId, pageable);
		}
		}else {
			if (categoryId == null || categoryId == 0) {
				return this.vendorsRepository.getAllPendingVendors(pageable);
			} else {
				return this.vendorsRepository.getAllPendingVendorsByCategory(categoryId, pageable);
			}
		}
	}

	public List<Vendors> getAllVendors(Integer catId) {
		logger.debug("Retrieving all the Vendors from DB");
		return vendorsRepository.getAllVendorsByCategoryAndSubCategory(catId);
	}

	public List<CurrencyType> getAllCurrencyTypes() {
		logger.debug("Fetching all currency types from DB");
		return currencyTypeRepository.findAll();
	}

	public Page<PayadviceCategoriesDTO> getCategories(Pageable pageable, Boolean isApproval) {
		logger.debug("Fetching acategoris page from DB");
		Page<PayadviceCategories> payadviceCategories = null;
		if (!isApproval) {
			payadviceCategories = this.payadviceCategoriesRepository.getCategories(pageable);
		} else {
			payadviceCategories = this.payadviceCategoriesRepository.getPendingCategories(pageable);
		}
		return payadviceCategories
				.map(item -> payadviceCategoriesMapper.payadviceCategoriesToPayadviceCategoriesDTO(item));
	}

	public List<AprooverDTO> getApproverList() {
		logger.debug("Retrieving Aproover Details from DB");
		List<EmpPersonal> empPersonals = this.empEmploymentRepository.getAprooverList();
		List<AprooverDTO>  aprooverDTOs = empPersonals.stream().map(item -> this.employeePersonalMapper.empPersonalToAprooverDTO(item))
				.collect(Collectors.toList());
		aprooverDTOs.sort(Comparator.comparing(AprooverDTO::getFname));
		return aprooverDTOs;

	}

	public String savePushRequestDetails(FcmDetailRequest fcmDetailRequest) {
		logger.debug("saving fcm details into the DB");
		FcmDetail fcmDetail;
		String result;
		EmpEmployment empEmployment = this.empEmploymentRepository.findOne(fcmDetailRequest.getEmp_id());
		Boolean isExists = this.fcmDetailRepository.isExists(fcmDetailRequest.getEmp_id());
		if (isExists) {
			fcmDetail = this.fcmDetailRepository.findByEmpEmploymentId(fcmDetailRequest.getEmp_id());
			fcmDetail.setFcm_id(fcmDetailRequest.getFcm_id());
			fcmDetail.setIsAvailable(true);
			result = "updated";
		} else {
			fcmDetail = new FcmDetail();
			fcmDetail.setFcm_id(fcmDetailRequest.getFcm_id());
			fcmDetail.setEmpEmployment(empEmployment);
			fcmDetail.setIsAvailable(true);
			result = "activated";
		}
		this.fcmDetailRepository.saveAndFlush(fcmDetail);
		return result;

	}
	public void saveFcmDetail(FcmDetail fcmDetail) {
		logger.debug("Saving Fcm Details intoi DB");
		this.fcmDetailRepository.saveAndFlush(fcmDetail);
		
		
	}
	public String deActivatePushRequest(Integer empId) {
		logger.debug("disabling fcm details inside the DB");
		FcmDetail fcmDetail;
		String result;
		Boolean isExists = this.fcmDetailRepository.isExists(empId);
		if(!isExists) {
			result = "error"; 
		}else {
			fcmDetail = this.fcmDetailRepository.findByEmpEmploymentId(empId);
			fcmDetail.setIsAvailable(false);
			this.fcmDetailRepository.saveAndFlush(fcmDetail);
			result = "succes"; 
		}
		return result;
	}

	public FcmDetail getFcmDetailByEmpId(Integer empId) {
		logger.debug("retrieving Fcm detail from DB by emp Id");
		return this.fcmDetailRepository.findByEmpEmploymentId(empId);
	}
	public FcmDetail getEmployeeFcmDetail(Integer empId) {
		logger.debug("retrieving Fcm detail from DB by emp Id");
		return this.fcmDetailRepository.findByEmpEmploymentIdAndIsAvailable(empId,true);
	}
	public List<FcmDetail> getAllActiveEmployyeFcmDetail() {
		logger.debug("retrieving  all Fcm detail from DB except id ");
		return this.fcmDetailRepository.findByIsAvailable(true);
	}
	
	public ApprovalCountDTO getApprovalCounts(Integer empId) {
		logger.debug("Fetching category,sub category,vendor and advice count from DB");
		ApprovalCountDTO approvalCountDTO = new ApprovalCountDTO();
		Integer categoryCount = payadviceCategoriesRepository.approvalCount();
		approvalCountDTO.setCategoryCount(categoryCount);
		Integer subCategoryCount = payadviceSubCategoriesRepository.approvalCount();
		approvalCountDTO.setSubCategoryCount(subCategoryCount);
		Integer vendorCount = vendorsRepository.approvalCount();
		approvalCountDTO.setVendorCount(vendorCount);
		Integer adminApprovalCount = paymentAdviceRepository.getAdminApprovalCount();
		adminApprovalCount -= paymentAdviceRepository.getAdminForwardApprovalCount(empId);
		approvalCountDTO.setAdviceAdminApprovalCount(adminApprovalCount);
		Integer financeAdviceApprovalCount = paymentAdviceRepository.getFinanceApprovalCount();
		approvalCountDTO.setAdviceFincanceApprovalCount(financeAdviceApprovalCount);
		return approvalCountDTO;
	}
	
	public PaymentBankDTO savePaymentBank(PaymentBankDTO paymentBankDTO) {
		logger.debug("Saving payment bank details into the db");
		PaymentBank paymentBank = this.paymentBankMapper.paymentBankDTOToPaymentBank(paymentBankDTO);
		paymentBank = this.paymentBankRepository.saveAndFlush(paymentBank);
		paymentBankDTO.setId(paymentBank.getId());
		return paymentBankDTO;
	}
	
	public List<PaymentBankDTO> getAllPaymentBanks(){
		logger.debug("Retrieving all the payment bank details from db");
		List<PaymentBank> paymentBanks = this.paymentBankRepository.findAll();
		return paymentBanks.stream()
				.map(item -> this.paymentBankMapper.paymentBankToPaymentBankDTO(item))
				.collect(Collectors.toList());
	}

}
