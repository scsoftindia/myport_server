package com.scsoft.myport.service.comparator;

import java.util.Comparator;
import java.util.Date;

import org.joda.time.LocalDateTime;

import com.scsoft.myport.service.DTO.BirthDaysDTO;

public class BirthDayComparator implements Comparator<BirthDaysDTO> {

	@Override
	public int compare(BirthDaysDTO o1, BirthDaysDTO o2) {
		LocalDateTime date1 = new LocalDateTime(o1.getDate());
		LocalDateTime date2 = new LocalDateTime(o2.getDate());
		if(date1.isBefore(date2)) {
			return -1;
		}else if(date1.equals(date2)) {
			return 0;
		}

		return 1;
	}

}
