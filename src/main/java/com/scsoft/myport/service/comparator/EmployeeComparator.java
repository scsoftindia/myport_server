package com.scsoft.myport.service.comparator;

import java.util.Comparator;

import com.scsoft.myport.service.DTO.EmployeeDTO;

public class EmployeeComparator implements Comparator<EmployeeDTO> {

	@Override
	public int compare(EmployeeDTO o1, EmployeeDTO o2) {
	Integer empId1 = o1.getId();
	Integer empId2 = o2.getId();
	if(empId1 < empId2) {
		return -1;
	}else if(empId1 == empId2) {
		return 0;
	}

	return 1;
	}

}
