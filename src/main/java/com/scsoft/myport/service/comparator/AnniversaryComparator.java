package com.scsoft.myport.service.comparator;

import java.util.Comparator;

import org.joda.time.LocalDateTime;

import com.scsoft.myport.service.DTO.AnniversaryDTO;

public class AnniversaryComparator implements Comparator<AnniversaryDTO> {

	@Override
	public int compare(AnniversaryDTO o1, AnniversaryDTO o2) {
		LocalDateTime date1 = new LocalDateTime(o1.getDate());
		LocalDateTime date2 = new LocalDateTime(o2.getDate());
		if(date1.isBefore(date2)) {
			return -1;
		}else if(date1.equals(date2)) {
			return 0;
		}

		return 1;
	}

}
