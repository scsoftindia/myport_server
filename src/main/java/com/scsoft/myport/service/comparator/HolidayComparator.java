package com.scsoft.myport.service.comparator;

import java.util.Comparator;

import org.joda.time.LocalDateTime;

import com.scsoft.myport.service.DTO.UserHoliDayDTO;

public class HolidayComparator implements Comparator<UserHoliDayDTO> {

	@Override
	public int compare(UserHoliDayDTO o1, UserHoliDayDTO o2) {
		LocalDateTime date1 = new LocalDateTime(o1.getHolidays().getHoliday_date());
		LocalDateTime date2 = new LocalDateTime(o2.getHolidays().getHoliday_date());
		if(date1.isBefore(date2)) {
			return -1;
		}else if(date1.equals(date2)) {
			return 0;
		}

		return 1;
	}

}
