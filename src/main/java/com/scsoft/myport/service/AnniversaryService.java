package com.scsoft.myport.service;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.scsoft.myport.domain.AnniversaryWish;
import com.scsoft.myport.domain.EmpEmployment;
import com.scsoft.myport.domain.EmpPersonal;
import com.scsoft.myport.domain.FcmDetail;
import com.scsoft.myport.error.InvalidRequestException;
import com.scsoft.myport.error.NotFoundException;
import com.scsoft.myport.repository.AnniversaryWishRepository;
import com.scsoft.myport.repository.EmpEmploymentRepository;
import com.scsoft.myport.repository.EmpPersonalRepository;
import com.scsoft.myport.repository.FcmDetailRepository;
import com.scsoft.myport.rest.request.AniversaryWishRequest;
import com.scsoft.myport.service.DTO.AnniversaryDTO;
import com.scsoft.myport.service.DTO.GlobalPushDTO;
import com.scsoft.myport.service.comparator.AnniversaryComparator;
import com.scsoft.myport.service.comparator.BirthDayComparator;
import com.scsoft.myport.service.mapper.AniversaryWishMapper;

@Service
public class AnniversaryService {
	private final Logger logger = LoggerFactory.getLogger(AnniversaryService.class);
	@Autowired
	private EmpPersonalRepository empPersonalRepository;
	@Autowired
	private EmpEmploymentRepository empEmploymentRepository;
	@Autowired
	private AnniversaryWishRepository anniversaryWishRepository;
	@Autowired
	private AniversaryWishMapper aniversaryWishMapper;
	@Autowired
	private EmailService emailService;
	@Autowired
	private FcmDetailRepository fcmDetailRepository;
	@Autowired
	private FcmService fcmService;

	public List<AnniversaryDTO> getUpcomingAnniversaries() {
		logger.debug("Retrieving upcoming anniversaries from DB");
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);
		Integer currentYear = dateTime.getYear();
		Integer start = dateTime.getMonthOfYear();
		Integer startDay = dateTime.getDayOfMonth();
		DateTime toTimeStamp = dateTime.plusDays(3);
		Integer toYear = toTimeStamp.getYear();
		Integer end = toTimeStamp.getMonthOfYear();
		Integer endDay = toTimeStamp.getDayOfMonth();
		List<AnniversaryDTO> anniversaryList = this.empPersonalRepository.fetchAnniversaries(start, end, startDay,
				endDay);
		anniversaryList.stream().forEach(item -> {
			Date doj = item.getDate();
			DateTime itemTimeStamp = new DateTime(item.getDate());
			Integer noOfYears = DateTime.now().getYear() - itemTimeStamp.getYear();
			item.setNoOfYear(noOfYears);
			if (itemTimeStamp.getMonthOfYear() == start) {
				DateTime birthDayDate = new DateTime(currentYear, itemTimeStamp.getMonthOfYear(),
						itemTimeStamp.getDayOfMonth(), 0, 0);

				item.setDate(birthDayDate.toDate());
			} else if (itemTimeStamp.getMonthOfYear() == end && currentYear != toYear) {
				DateTime birthDayDate = new DateTime(toYear, itemTimeStamp.getMonthOfYear(),
						itemTimeStamp.getDayOfMonth(), 0, 0);
				item.setDate(birthDayDate.toDate());
			}
		});
		Collections.sort(anniversaryList, new AnniversaryComparator());
		return anniversaryList;

	}

	public AnniversaryWish saveAnniversaryWish(AniversaryWishRequest aniversaryWishRequest)
			throws NotFoundException, InvalidRequestException {
		logger.debug("Saving Anniversary wish request into DB");
		AnniversaryWish anniversaryWish = this.aniversaryWishMapper
				.aniversaryWishRequestToAnniversaryWish(aniversaryWishRequest);
		if (anniversaryWish.getFor_emp() == null || anniversaryWish.getFor_emp().equals(0)) {
			throw new NotFoundException("error", "User ID Not Found");
		}
		if (anniversaryWish.getBy_emp() == null || anniversaryWish.getBy_emp().equals(0)) {
			throw new NotFoundException("error", "User ID Not Found");
		}
		EmpEmployment empEmployment = this.empEmploymentRepository.findOne(anniversaryWish.getFor_emp());
		if (empEmployment == null) {
			throw new NotFoundException("error", "Invalid User");
		}
		EmpEmployment byEmpEmployment = this.empEmploymentRepository.findOne(anniversaryWish.getBy_emp());
		if (byEmpEmployment == null) {
			throw new NotFoundException("error", "Invalid User");
		}
		if (anniversaryWish.getAnniversary_year() == null || anniversaryWish.getAnniversary_year().equals(0)) {
			throw new InvalidRequestException("error", "Year value not found");
		}
		if (anniversaryWish.getWish() == null || anniversaryWish.getWish().isEmpty()) {
			throw new InvalidRequestException("error", "Message not found");
		}
		anniversaryWish.setApproved_status(true);
		anniversaryWish.setApproved_by(0);
		this.anniversaryWishRepository.saveAndFlush(anniversaryWish);
		this.sendAnniversaryWishEmail(anniversaryWish);
		this.sendAnniversaryWishPushNotification(anniversaryWish);
		return anniversaryWish;
	}

	private void sendAnniversaryWishEmail(AnniversaryWish anniversaryWish) {
		logger.debug("Ending email for anniversry wish");
		try {
			EmpPersonal wishByPersonal = this.empPersonalRepository.findOne(anniversaryWish.getBy_emp());
			EmpEmployment wishByEmployment = this.empEmploymentRepository.findOne(wishByPersonal.getId());
			EmpPersonal wishToPersonal = this.empPersonalRepository.findOne(anniversaryWish.getFor_emp());
			EmpEmployment wishToEmployment = this.empEmploymentRepository.findOne(wishToPersonal.getId());
			StringBuilder nameBuilder = new StringBuilder();
			nameBuilder.append(wishByPersonal.getEmp_fname()).append(" ");
			nameBuilder.append(wishByPersonal.getEmp_mname()).append(" ");
			nameBuilder.append(wishByPersonal.getEmp_lname());
			String wishByName = nameBuilder.toString();
			String emp_name = wishToPersonal.getEmp_fname();
			String wish = anniversaryWish.getWish();
			String subject = wishByName + " congratulated you on your work Anniversary";
			String email = wishToEmployment.getEmp_email();
			String emailFrom = wishByEmployment.getEmp_email();
			Map<String, Object> emailModel = new HashMap();
			emailModel.put("emp_name", emp_name);
			emailModel.put("wish", wish);
			emailModel.put("subject", subject);
			emailModel.put("email", email);
			emailModel.put("emailFrom", emailFrom);
			this.emailService.sendAnniversaryWishEmail(emailModel);
		} catch (Exception e) {
			logger.error("Eror occured while sending  anniversary wish email", e);
		}

	}

	private void sendAnniversaryWishPushNotification(AnniversaryWish anniversaryWish) {
		logger.debug("Ending notification for anniversry wish");
		ObjectMapper mapper = new ObjectMapper();
		try {
			GlobalPushDTO globalPushDTO = new GlobalPushDTO();
			if (anniversaryWish != null && anniversaryWish.getWish() != null) {
				FcmDetail fcmDetail = this.fcmDetailRepository
						.findByEmpEmploymentIdAndIsAvailable(anniversaryWish.getFor_emp(), true);
				EmpPersonal wishByPersonal = this.empPersonalRepository.findOne(anniversaryWish.getBy_emp());
				if (fcmDetail != null && wishByPersonal != null) {

					StringBuilder nameBuilder = new StringBuilder();
					nameBuilder.append(wishByPersonal.getEmp_fname()).append(" ");
					nameBuilder.append(wishByPersonal.getEmp_mname()).append(" ");
					nameBuilder.append(wishByPersonal.getEmp_lname());
					String wishByName = nameBuilder.toString();
					String title = wishByName + " congratulated you on your work Anniversary";
					globalPushDTO.setBody(anniversaryWish.getWish());
					globalPushDTO.setTitle(title);
					globalPushDTO.setAvathar(wishByPersonal.getEmp_avatar());
					this.fcmService.sendMessage(fcmDetail.getFcm_id(), null, mapper.writeValueAsString(globalPushDTO),
							"others");

				}
			}
		} catch (Exception e) {
			logger.error("Error occured while sending push notification for birthday wish", e);
		}

	}

}
