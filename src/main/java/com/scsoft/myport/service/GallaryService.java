package com.scsoft.myport.service;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.scsoft.myport.domain.AlbumImages;
import com.scsoft.myport.domain.Albums;
import com.scsoft.myport.repository.AlbumImagesRepository;
import com.scsoft.myport.repository.AlbumsRepository;

@Service("gallaryService")
public class GallaryService {
	Logger logger = LoggerFactory.getLogger(GallaryService.class);
	@Autowired
	private AlbumImagesRepository albumImagesRepository;
	@Value("${gallary.thumb.url}")
	private String gallaryBaseUrl;
	
	@Autowired
	private AlbumsRepository albumsRepository;

	public List<AlbumImages> getAlbumImages(Integer albumId) {
		logger.debug("Retrieving album images from db by album  id");
		String albumUrl = this.gallaryBaseUrl+albumId.toString()+"/";
		List<AlbumImages> albumImages = this.albumImagesRepository.findByAlbumId(albumId);
		albumImages.stream().forEach(item->{
			item.setImage_name(albumUrl+item.getImage_name());
		});
		return albumImages;

	}
	public List<Albums> getAllAlbums(){
		logger.debug("fetching all Albums from db");
		List<Albums> albums = this.albumsRepository.findAll();
		albums.stream().forEach(item ->{
			Pageable topThree = new PageRequest(0, 3);
			Page<AlbumImages> albumImages = this.albumImagesRepository.findByAlbumId(item.getId(),topThree);
			String albumUrl = this.gallaryBaseUrl+item.getId().toString()+"/";
			albumImages.getContent().stream().forEach(image ->{
				image.setImage_name(albumUrl+image.getImage_name());
			});
			List<String> images = albumImages.getContent().stream()
				    .map(AlbumImages::getImage_name)
					.collect(Collectors.toList());
			item.setAlbumImages(images);			
		});
		
		return albums;
	}

}
