package com.scsoft.myport.service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.scsoft.myport.domain.ComplaintsConcernsMain;
import com.scsoft.myport.domain.ComplaintsConcernsType;
import com.scsoft.myport.domain.EmpEmployment;
import com.scsoft.myport.domain.EmpPersonal;
import com.scsoft.myport.domain.FcmDetail;
import com.scsoft.myport.error.ApprovalNotPossibleException;
import com.scsoft.myport.error.InvalidRequestException;
import com.scsoft.myport.error.NotFoundException;
import com.scsoft.myport.repository.ComplaintsConcernsMainRepository;
import com.scsoft.myport.repository.ComplaintsConcernsTypeRepository;
import com.scsoft.myport.repository.EmpEmploymentRepository;
import com.scsoft.myport.repository.EmpPersonalRepository;
import com.scsoft.myport.rest.request.ComplaintRequest;
import com.scsoft.myport.rest.request.ComplaintStatusUpdateRequest;
import com.scsoft.myport.service.DTO.ComplaintDTO;
import com.scsoft.myport.service.DTO.GlobalPushDTO;

@Service
public class CompliantsService {
	@Autowired
	private ComplaintsConcernsTypeRepository complaintsConcernsTypeRepository;
	@Autowired
	private ComplaintsConcernsMainRepository complaintsConcernsMainRepository;
	private final Logger logger = LoggerFactory.getLogger(CompliantsService.class);
	@Value("${complaints.file.path}")
	private String filePath;
	@Autowired
	private EmailService emailService;
	@Autowired
	private EmpPersonalRepository empPersonalRepository;
	@Autowired
	private EmpEmploymentRepository empEmploymentRepository;
	@Autowired
	private PaymentLookupService paymentLookupService;
	@Autowired
	private FcmService fcmService;

	// Save compliant
	public ComplaintsConcernsMain saveComplaint(ComplaintRequest complaintRequest, MultipartFile file)
			throws IOException, NotFoundException, InvalidRequestException {
		logger.debug("Saving complaints into DB");
		if(complaintRequest.getEmp_id() == null || complaintRequest.getEmp_id().equals(0)) {
			throw new NotFoundException("error", "Invalid User ID");
		}
		if(complaintRequest.getCc_id() == null || complaintRequest.getCc_id().equals(0)) {
			throw new NotFoundException("error", "Invalid Complaint type");
		}
		ComplaintsConcernsMain complaintsConcernsMain = new ComplaintsConcernsMain();
		complaintsConcernsMain.setEmp_id(complaintRequest.getEmp_id());
		complaintsConcernsMain.setCc_id(complaintRequest.getCc_id());
		complaintsConcernsMain.setCc_subject(complaintRequest.getCc_subject());
		complaintsConcernsMain.setCc_remark(complaintRequest.getCc_remark());
		complaintsConcernsMain.setCc_resolved("Pending");
		complaintsConcernsMain.setCc_forward(0);
		// Saving file if anything found
		if (file != null) {
			Long time = new Date().getTime();
			String fileName = complaintRequest.getEmp_id() + "-complaint-" + time;
			String[] nameArray = file.getOriginalFilename().split("\\.");
			fileName = fileName + "." + file.getOriginalFilename().split("\\.")[nameArray.length -1];
			file.transferTo(new File(filePath  + fileName));
			complaintsConcernsMain.setCc_doc(fileName);

		}
		complaintsConcernsMainRepository.saveAndFlush(complaintsConcernsMain);
		EmpPersonal empPersonal = empPersonalRepository.findOne(complaintRequest.getEmp_id());
		EmpEmployment empEmployment = empEmploymentRepository.findOne(complaintRequest.getEmp_id());
		if(empPersonal != null && empEmployment != null) {
		String emailFrom = empEmployment.getEmp_email();
		String empName = empPersonal.getEmp_fname() + " " + empPersonal.getEmp_mname() + " "
				+ empPersonal.getEmp_lname();
		String subject = complaintsConcernsMain.getCc_subject();
		subject = empName + " Raised a compliant - " + subject;
		String body = complaintsConcernsMain.getCc_remark();
		this.sendComplaintEmail(subject, emailFrom, body);
		this.sendComplaintPushNotification(complaintsConcernsMain);
		}
		return complaintsConcernsMain;
	}

	// Saving compliant status
	public ComplaintsConcernsMain updateComplaintStatus(ComplaintStatusUpdateRequest complaintStatusUpdateRequest) throws ApprovalNotPossibleException, NotFoundException, InvalidRequestException {
		logger.debug("Updating complaints status into DB");
		String subject;
		String body;
		if(complaintStatusUpdateRequest.getComplaintId() == null || complaintStatusUpdateRequest.getComplaintId().equals(0)) {
			throw new NotFoundException("user","Id is not valid");
		}
		if(complaintStatusUpdateRequest.getEmpId() == null || complaintStatusUpdateRequest.getEmpId().equals(0)) {
			throw new NotFoundException("user","Approver Id is not valid");
		}
		if(complaintStatusUpdateRequest.getStatus() == null) {
			throw new InvalidRequestException("error", "Status not specified in the request");
		}
		EmpEmployment approverEmployment = this.empEmploymentRepository
				.findOne(complaintStatusUpdateRequest.getEmpId());
		if(approverEmployment == null) {
			throw new NotFoundException("user","Approver Id is not valid");
		}
		String email = approverEmployment.getEmp_email();
		String emailTo;
		String empName;
		Integer complaintId = complaintStatusUpdateRequest.getComplaintId();
		ComplaintsConcernsMain complaintsConcernsMain = complaintsConcernsMainRepository.getOne(complaintId);
		if(complaintsConcernsMain == null) {
			throw new NotFoundException("complaint","Invalid id");
		}
		if(!complaintsConcernsMain.getCc_resolved().equals("Pending")) {
			throw new ApprovalNotPossibleException("complaint", "The Complaint/Concern is already handled by another person", "success", complaintsConcernsMain.getCc_resolved());
		}
		complaintsConcernsMain.setCc_resolved(complaintStatusUpdateRequest.getStatus().toString());
		complaintsConcernsMain.setCc_resolve_remark(complaintStatusUpdateRequest.getComment());
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);
		complaintsConcernsMain.setCc_resolved_on(dateTime.toDate());
		complaintsConcernsMain.setCc_resolved_by(complaintStatusUpdateRequest.getEmpId());
		complaintsConcernsMainRepository.saveAndFlush(complaintsConcernsMain);
		EmpEmployment empEmployment = this.empEmploymentRepository.findOne(complaintsConcernsMain.getEmp_id());
		if(empEmployment != null) {
		emailTo = empEmployment.getEmp_email();
		EmpPersonal empPersonal = this.empPersonalRepository.findOne(complaintsConcernsMain.getEmp_id());
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(empPersonal.getEmp_fname()).append(" ").append(empPersonal.getEmp_mname()).append(" ")
				.append(empPersonal.getEmp_lname());
		empName = stringBuilder.toString();
		
		if ("Yes".equals(complaintStatusUpdateRequest.getStatus().toString())) {
			subject = complaintsConcernsMain.getCc_subject() + " Resolved";
			body = " Complaint/concern regarding " + complaintsConcernsMain.getCc_remark() + " has been resolved," + "\n"
					+ "Comment : " + complaintsConcernsMain.getCc_resolve_remark();

		} else if ("No".equals(complaintStatusUpdateRequest.getStatus().toString())) {
			subject = complaintsConcernsMain.getCc_subject() + " Cancelled";
			body = " Complaint/concern regarding " + complaintsConcernsMain.getCc_remark() + " Cancelled," + "\n"
					+ "Comment : " + complaintsConcernsMain.getCc_resolve_remark();
		} else {
			subject = complaintsConcernsMain.getCc_subject() + " Forwarded";
			body = " Complaint/concern regarding " + complaintsConcernsMain.getCc_remark() + " has been forwarded,"
					+ "\n" + "Comment : " + complaintsConcernsMain.getCc_resolve_remark();
		}
		
		// Sending email's to employee and admin team
		this.sendComplaintResolveEmail(empName, subject, email, emailTo, body);
		List<EmpEmployment> empList = this.empEmploymentRepository.getHrManagers();
		List<EmpEmployment> adminList = this.empEmploymentRepository.getAdmins();
		empList.addAll(adminList);
		for (EmpEmployment approver : empList) {
			EmpPersonal approverPersonal = this.empPersonalRepository.findOne(approver.getId());
			emailTo = approver.getEmp_email();
			StringBuilder nameBuilder = new StringBuilder();
			nameBuilder.append(approverPersonal.getEmp_fname()).append(" ").append(approverPersonal.getEmp_mname())
					.append(" ").append(approverPersonal.getEmp_lname());
			empName = nameBuilder.toString();
			this.sendComplaintResolveEmail(empName, subject, email, emailTo, body);
		}
		// Sending push notification
		this.sendComplaintResolvePushNotification(subject, body, complaintsConcernsMain.getCc_resolved_by(),
				complaintsConcernsMain.getEmp_id());
		}
		return complaintsConcernsMain;
	}

	// Retrieving complaints type from db
	public List<ComplaintsConcernsType> getAllComplaintsConcernsType() {
		logger.debug("Retieving all ComplaintsConcernsTypes from DB");
		return complaintsConcernsTypeRepository.findAll();
	}

	// Retrieving complaints list from db
	public List<ComplaintDTO> getEmployeeComplaintsList(Integer empId) {
		logger.debug("Fetching employee complaints from DB");
		List<ComplaintDTO> complaintsList = complaintsConcernsMainRepository.getComplaintsByEmployee(empId);
		return complaintsList;
	}

	// Retrieving pending complaints list from db
	public List<ComplaintDTO> getPendingComplaints() {
		logger.debug("Fetching employee complaints from DB");
		List<ComplaintDTO> complaintsList = complaintsConcernsMainRepository.getPendingComplaints();
		return complaintsList;
	}

	// Sending email
//	@Async
	private void sendComplaintEmail(String subject, String emailFrom, String body) {
		try {
			logger.debug("Sending complaint email");
			List<EmpEmployment> empList = this.empEmploymentRepository.getHrManagers();
			List<EmpEmployment> adminList = this.empEmploymentRepository.getAdmins();
			empList.addAll(adminList);
			Map<String, Object> emailModel = new HashedMap();
			emailModel.put("compliant", body);
			emailModel.put("emailFrom", emailFrom);
			emailModel.put("subject", subject);
			for (EmpEmployment approver : empList) {
				EmpPersonal approverPersonal = this.empPersonalRepository.findOne(approver.getId());
				emailModel.put("emp_name", approverPersonal.getEmp_fname());
				emailModel.put("email", approver.getEmp_email());
				this.emailService.sendComplaintEmail(emailModel);
			}
		} catch (Exception e) {
			logger.error("Error occured while sending email",e);
		}
	}

	// Sending complaint resolve email
//	@Async
	private void sendComplaintResolveEmail(String empName, String subject, String emailFrom, String emailTo,
			String body) {
		logger.debug("sending complaint resolve email");
		try {
			Map<String, Object> emailModel = new HashedMap();
			emailModel.put("message", body);
			emailModel.put("emp_name", empName);
			emailModel.put("email", emailTo);
			emailModel.put("emailFrom", emailFrom);
			emailModel.put("subject", subject);
			this.emailService.sendComplaintResolveEmail(emailModel);
		} catch (Exception e) {
			logger.error("Error occured while sending email",e);
		}

	}

	//@Async
	private void sendComplaintPushNotification(ComplaintsConcernsMain complaintRequest) {
		logger.debug("Request to send complaint and concern push notification");
		ObjectMapper mapper = new ObjectMapper();
		GlobalPushDTO globalPushDTO = new GlobalPushDTO();
		try {
			String title = complaintRequest.getCc_subject() + " (Complaint)";
			String body = complaintRequest.getCc_remark();
			EmpPersonal empPersonal = this.empPersonalRepository.findOne(complaintRequest.getEmp_id());
			String avathar = empPersonal.getEmp_avatar();
			globalPushDTO.setAvathar(avathar);
			globalPushDTO.setBody(body);
			globalPushDTO.setTitle(title);
			List<EmpEmployment> empList = this.empEmploymentRepository.getHrManagers();
			List<EmpEmployment> adminList = this.empEmploymentRepository.getAdmins();
			empList.addAll(adminList);
			List<String> fcmIdList = new ArrayList();
			for (EmpEmployment approver : empList) {
				FcmDetail fcmDetail = paymentLookupService.getEmployeeFcmDetail(approver.getId());
				if (fcmDetail != null) {
					fcmIdList.add(fcmDetail.getFcm_id());
				}
			}
			this.fcmService.sendMessage(null,fcmIdList, mapper.writeValueAsString(globalPushDTO),
					"others");
		} catch (Exception e) {
			logger.error("Error cooured during sending push notification",e);
		}
	}

	//@Async
	private void sendComplaintResolvePushNotification(String title, String body, Integer resolvedBy, Integer empId) {
		logger.debug("Request to send complaint and concern resolve push notification");
		ObjectMapper mapper = new ObjectMapper();
		GlobalPushDTO globalPushDTO = new GlobalPushDTO();
		try {
			EmpPersonal empPersonal = this.empPersonalRepository.findOne(resolvedBy);
			String avathar = empPersonal.getEmp_avatar();
			globalPushDTO.setAvathar(avathar);
			globalPushDTO.setTitle(title);
			globalPushDTO.setBody(body);
			FcmDetail fcmDetail = paymentLookupService.getEmployeeFcmDetail(empId);
			List<String> fcmIdList = new ArrayList();
			if (fcmDetail != null) {
				fcmIdList.add(fcmDetail.getFcm_id());
			}
			List<EmpEmployment> empList = this.empEmploymentRepository.getHrManagers();
			List<EmpEmployment> adminList = this.empEmploymentRepository.getAdmins();
			empList.addAll(adminList);
			for (EmpEmployment approver : empList) {
				FcmDetail approverfcmDetail = paymentLookupService.getEmployeeFcmDetail(approver.getId());
				if (approverfcmDetail != null) {
					fcmIdList.add(approverfcmDetail.getFcm_id());
				}
			}		
			this.fcmService.sendMessage(null,fcmIdList, mapper.writeValueAsString(globalPushDTO), "others");
		} catch (Exception e) {
			logger.error("Error occured while complaint resolve push notification",e);
		}
	}

}
