package com.scsoft.myport.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.scsoft.myport.repository.CcfRepository;
import com.scsoft.myport.service.mapper.CcfMapper;

@Service("ccfService")
public class CcfService {
	private final Logger logger = LoggerFactory.getLogger(CcfService.class);
	@Autowired
	private CcfRepository ccfRepository;
	@Autowired
	private CcfMapper ccfMapper;

}
