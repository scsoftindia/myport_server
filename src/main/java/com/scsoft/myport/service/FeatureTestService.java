package com.scsoft.myport.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.scsoft.myport.service.DTO.GlobalPushDTO;

@Service
public class FeatureTestService {
	@Autowired
	private FcmService fcmService;
	@Autowired
	private EmailService emailService;
	@Value("${myport.test.email}")
	private String testEmailAdress;
	

	public String testPush(String fcmId) {
		try {
			GlobalPushDTO globalPushDTO = new GlobalPushDTO();
			globalPushDTO.setTitle("Test by devlopmentTeam");
			globalPushDTO.setBody("Push notification test");
			globalPushDTO.setAvathar("10225JishnuM S.jpg");
			ObjectMapper mapper = new ObjectMapper();
			this.fcmService.sendMessage(fcmId, null, mapper.writeValueAsString(globalPushDTO), "others");
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
		return "success";
	}

	public String testEmail() {
		Map<String, Object> emailModel = new HashMap();
		try {
			emailModel.put("subject", "Test Email");
			emailModel.put("email", testEmailAdress);
			emailModel.put("emailFrom", "employee.communication@scsoft.in");
			this.emailService.sendTestEmail(emailModel);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";
	}


}
