package com.scsoft.myport.service;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.scsoft.myport.domain.BestPerformer;
import com.scsoft.myport.repository.BestPerformerRepository;
import com.scsoft.myport.service.DTO.BestPerformerDTO;
import com.scsoft.myport.service.mapper.BestPerformerMapper;

@Service
public class BestPerformerService {
	private final Logger logger = LoggerFactory.getLogger(BestPerformerService.class);
	@Autowired
	private BestPerformerRepository bestPerformerRepository;
	@Autowired
	BestPerformerMapper bestPerformerMapper;

	// Retrieving best performer list.
	public List<BestPerformerDTO> getBestPerformers() {
		logger.debug("Retrieving BestPerformer from DB");
		List<BestPerformer> bestPerformerList = new ArrayList();
		bestPerformerList = bestPerformerRepository.findTop3ByOrderByEnteredOnDescQuarterDesc();
		List<BestPerformerDTO> bestPerformerDTOs = new ArrayList();
		bestPerformerList.stream().forEach(item -> {
			BestPerformerDTO bestPerformerDTO = bestPerformerMapper.bestPerformerToBestPerformerDTO(item);
			if (bestPerformerDTO.getQuarter().equals("1")) {
				bestPerformerDTO.setQuarter("First Quarter");
			} else if (bestPerformerDTO.getQuarter().equals("2")) {
				bestPerformerDTO.setQuarter("Second Quarter");
			} else if (bestPerformerDTO.getQuarter().equals("3")) {
				bestPerformerDTO.setQuarter("Third Quarter");
			} else if (bestPerformerDTO.getQuarter().equals("4")) {
				bestPerformerDTO.setQuarter("Fourth Quarter");
			}
			bestPerformerDTOs.add(bestPerformerDTO);
		});
		return bestPerformerDTOs;
	}

}
