package com.scsoft.myport.service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.mail.MessagingException;
import javax.servlet.ServletContext;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.scsoft.myport.domain.ApplyHoliday;
import com.scsoft.myport.domain.ApplyLeave;
import com.scsoft.myport.domain.Designation;
import com.scsoft.myport.domain.EmpEmployment;
import com.scsoft.myport.domain.EmpPersonal;
import com.scsoft.myport.domain.FcmDetail;
import com.scsoft.myport.domain.HolidayAvailability;
import com.scsoft.myport.domain.Holidays;
import com.scsoft.myport.domain.LeaveAvailability;
import com.scsoft.myport.domain.LeaveTypes;
import com.scsoft.myport.domain.Vertical;
import com.scsoft.myport.error.ApprovalNotPossibleException;
import com.scsoft.myport.error.NotFoundException;
import com.scsoft.myport.error.OperationNotPossibleException;
import com.scsoft.myport.repository.ApplyHolidayRepository;
import com.scsoft.myport.repository.ApplyLeaveRepository;
import com.scsoft.myport.repository.EmpEmploymentRepository;
import com.scsoft.myport.repository.EmpPersonalRepository;
import com.scsoft.myport.repository.HolidayAvailabilityRepository;
import com.scsoft.myport.repository.HolidaysRepository;
import com.scsoft.myport.repository.LeaveAvailabilityRepository;
import com.scsoft.myport.repository.LeaveTypesRepository;
import com.scsoft.myport.rest.request.ApplyHoliDayApproveRequest;
import com.scsoft.myport.rest.request.ApplyLeaveApproovalRequest;
import com.scsoft.myport.rest.request.ApplyLeaveRequest;
import com.scsoft.myport.rest.request.HoliDayRequest;
import com.scsoft.myport.rest.response.UserHoliDayResponse;
import com.scsoft.myport.service.DTO.AppliedHoliDayDTO;
import com.scsoft.myport.service.DTO.AppliedLeaveDTO;
import com.scsoft.myport.service.DTO.GlobalPushDTO;
import com.scsoft.myport.service.DTO.HolidayHistroryByEmpDTO;
import com.scsoft.myport.service.DTO.LeaveDTO;
import com.scsoft.myport.service.DTO.LeaveHistoryByEmpDTO;
import com.scsoft.myport.service.DTO.UserHoliDayDTO;
import com.scsoft.myport.service.comparator.HolidayComparator;
import com.scsoft.myport.service.mapper.ApplyLeaveMapper;
import com.scsoft.myport.service.mapper.EmployeePersonalMapper;
import com.scsoft.myport.service.util.AppConstants;
//This Service handles all the requests related with leaves and holidays.

@Service("holidaysService")
public class HolidaysService {
	@Autowired
	private HolidaysRepository holidaysRepository;
	@Autowired
	private HolidayAvailabilityRepository holidayAvailabilityRepository;
	@Autowired
	private ApplyHolidayRepository applyHolidayRepository;
	@Autowired
	private LeaveAvailabilityRepository leaveAvailabilityRepository;
	@Autowired
	private EmpEmploymentRepository empEmploymentRepository;
	@Autowired
	private EmpPersonalRepository empPersonalRepository;
	@Autowired
	private EmployeePersonalMapper employeePersonalMapper;
	@Autowired
	private ApplyLeaveRepository applyLeaveRepository;
	@Autowired
	private ApplyLeaveMapper applyLeaveMapper;
	@Autowired
	private LeaveTypesRepository leaveTypesRepository;
	@Value("${leave.file.path}")
	private String filePath;
	@Value("${myport.leave.approve.base-url}")
	private String leaveApproveUrl;
	@Value("${myport.holiday.approve.base-url}")
	private String holidayApproveUrl;
	@Autowired
	private EmailService emailService;
	@Autowired
	private FcmService fcmService;
	@Autowired
	private PaymentLookupService paymentLookupService;

	private final Logger logger = LoggerFactory.getLogger(HolidaysService.class);

	// Returns all the holiday and leave details.
	public UserHoliDayResponse getUserHoliDayDetails(Integer userId) {
		logger.debug("FetchingUser HoliDay information from DB");
		// used to display the holiday log to the user:holidaysTaken
		Integer holidaysTaken = 0;
		// Current year
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);
		LocalDate currentDate = LocalDate.now(indianZone);
		Integer year = dateTime.getYear();
		DateTime lastHoliday = new DateTime().withDate(year, 12, 25).withZone(indianZone);

		EmpEmployment empEmployment = empEmploymentRepository.findOne(userId);

		HolidayAvailability holidayAvailability = holidayAvailabilityRepository.getUserHoliDayAvailability(userId,
				year);
		// Remaining holidays from DB.
		BigDecimal hlidaysRemaining = holidayAvailability.getHolidays_remaining();
		// Number of Holidays listed by the organization for the current year
		List<Holidays> holidayList = holidaysRepository.findHoliDaysByYear(year, empEmployment.getLocations().getId());
		if (dateTime.isAfter(lastHoliday) && holidayList != null) {
			holidayList.addAll(holidaysRepository.findHoliDaysByYear(year + 1, empEmployment.getLocations().getId()));
		}

		// Used to Indicate whether a user can apply a holiday||applied||over etc.
		List<UserHoliDayDTO> userHoliDayDTOs = new ArrayList();
		holidayList.stream().forEach(item -> {
			UserHoliDayDTO userHoliDayDTO = new UserHoliDayDTO();
			userHoliDayDTO.setHolidays(item);
			userHoliDayDTO.setIsApplicable(false);
			LocalDate holiday_date = new LocalDate(item.getHoliday_date(), indianZone);
			Integer holidayStatus = 0;
			// Checking whether the user has already applied the holiday
			// Chance for duplicates, So the latest row is fetched
			Page<ApplyHoliday> appliedHolidayPage = applyHolidayRepository.getAppliedHoliDay(userId, item.getId(),
					new PageRequest(0, 1));
			ApplyHoliday applyHoliday = null;
			if (appliedHolidayPage != null && !CollectionUtils.isEmpty(appliedHolidayPage.getContent())) {
				applyHoliday = appliedHolidayPage.getContent().get(0);
				holidayStatus = applyHoliday.getStatus();
			}

			switch (holidayStatus) {
			case 2:
				userHoliDayDTO.setUserHolidayStatus("Approval Pending");
				userHoliDayDTO.setIsApplicable(false);
				break;
			case 9:
				userHoliDayDTO.setIsApplicable(false);
				userHoliDayDTO.setUserHolidayStatus(AppConstants.APPROVED);
				break;
			case 10:
				userHoliDayDTO.setUserHolidayStatus(AppConstants.REJECTED);
				if (!holiday_date.isBefore(currentDate) && hlidaysRemaining.compareTo(BigDecimal.ZERO) > 0) {
					userHoliDayDTO.setIsApplicable(true);
				}
				break;
			case 11:
				userHoliDayDTO.setIsApplicable(false);
				userHoliDayDTO.setUserHolidayStatus("Cancellation Approval Pending");
				break;
			case 12:
				userHoliDayDTO.setUserHolidayStatus("Cancellation Approved");
				if (!holiday_date.isBefore(currentDate) && hlidaysRemaining.compareTo(BigDecimal.ZERO) > 0) {
					userHoliDayDTO.setIsApplicable(true);
				}
				break;
			case 13:
				userHoliDayDTO.setIsApplicable(false);
				userHoliDayDTO.setUserHolidayStatus("Cancellation Rejected");
				break;
			case 20:
				userHoliDayDTO.setIsApplicable(false);
				userHoliDayDTO.setUserHolidayStatus("Withdrawal Approval Pending");
				break;
			case 21:
				if (!holiday_date.isBefore(currentDate) && hlidaysRemaining.compareTo(BigDecimal.ZERO) > 0) {
					userHoliDayDTO.setIsApplicable(true);
				}
				userHoliDayDTO.setUserHolidayStatus("Withdrawal Approved");
				break;
			case 22:
				userHoliDayDTO.setIsApplicable(false);
				userHoliDayDTO.setUserHolidayStatus("Withdrawal Rejected");
				break;

			default:
				if (holiday_date.isBefore(currentDate)) {
					// Checking is holiday date over.
					userHoliDayDTO.setUserHolidayStatus("Over");
					userHoliDayDTO.setIsApplicable(false);
				} else {
					// Checking is user has reached the maximum limit for taking holidays for an
					// year.
					if (hlidaysRemaining.compareTo(BigDecimal.ZERO) > 0) {
						userHoliDayDTO.setIsApplicable(true);
					} else {
						userHoliDayDTO.setIsApplicable(false);
						userHoliDayDTO.setUserHolidayStatus("Holiday Limit reached");
					}

				}
				break;
			}
			userHoliDayDTOs.add(userHoliDayDTO);

		});
		Collections.sort(userHoliDayDTOs, new HolidayComparator());

		UserHoliDayResponse userHoliDayResponse = new UserHoliDayResponse();
		// Availability of different types of leaves for the year.
		List<LeaveAvailability> leaveAvailability = leaveAvailabilityRepository.getLeaveAvailabilityByEmployId(userId,
				year);
		// Supervisor of the employee.
		EmpPersonal supervisor = empEmploymentRepository.getSupervisor(userId);
		// For Listing employees who are also responsible in the role as a supervisor.
		List<EmpPersonal> supervisorList = empPersonalRepository.getSupevisorList();
		// Converting the supervisor object into a user friendly format
		List<SupervisorDTO> supervisorDTOs = new ArrayList<>();
		supervisorList.stream().forEach(item -> {
			supervisorDTOs.add(employeePersonalMapper.empPersonalToSupervisorDTO(item));
		});
		// The history of leaves taken by the employee.
		List<ApplyLeave> applyLeaves = applyLeaveRepository.getLeaveHistoryByEmployee(userId);
		List<LeaveDTO> leaveDTOs = new ArrayList();
		applyLeaves.stream().forEach(item -> {
			LeaveDTO leaveDTO = applyLeaveMapper.applyLeaveToLeaveDTO(item);
			Integer applicationStatus = item.getApplicationStatus();
			// the applicationStatus value = 11||12||13 indicates the user has cancelled the
			// leave request and he is waiting for got the approval
			if (applicationStatus == 11 || applicationStatus == 12 || applicationStatus == 13
					|| applicationStatus == 10) {
				leaveDTO.setIsLeaveCancelled(true);
			}
			// finding the current status of the leave request.
			if (applicationStatus == 2) {
				leaveDTO.setCurrentStatus("Leave Request send");
			} else if (applicationStatus == 9) {
				leaveDTO.setCurrentStatus("Leave Request approved");
			} else if (applicationStatus == 10) {
				leaveDTO.setCurrentStatus("Leave Request rejected");
				leaveDTO.setRejectionNote(item.getSupApprovalNote());
			} else if (applicationStatus == 11) {
				leaveDTO.setCurrentStatus("Leave cancellation pending");
			} else if (applicationStatus == 12) {
				leaveDTO.setCurrentStatus("Leave cancellation approved");
			} else if (applicationStatus == 13) {
				leaveDTO.setCurrentStatus("Leave cancellation rejected");
				leaveDTO.setRejectionNote(item.getCancelApprovalNote());
			}
			leaveDTOs.add(leaveDTO);
		});
		// @TODO
		leaveDTOs.stream()
				.filter((item) -> Objects.isNull(item.getSupApproval()) && Objects.isNull(item.getAddsup_approval()))
				.forEach((item) -> {
					item.setSupApproval("Pending");
				});
		// List leave types availed for the user.
		List<LeaveTypes> leaveTypes = leaveTypesRepository.findAll();
		List<ApplyHoliday> applyHolidays = applyHolidayRepository.getHolidayList(userId);
		List<HolidayDTO> holidayDTOs = new ArrayList();

		for (ApplyHoliday item : applyHolidays) {
			HolidayDTO holidayDTO = new HolidayDTO();
			holidayDTO.setHoliDayId(item.getHolidayId());
			holidayDTO.setId(item.getId());
			// Finding holiday request status.
			if (item.getStatus().equals(2)) {
				holidayDTO.setIsWithdrawable(true);
				holidayDTO.setStatus("Pending");
			} else if (item.getStatus().equals(9)) {
				holidayDTO.setStatus(AppConstants.APPROVED);
				holidayDTO.setIsWithdrawable(true);
			} else if (item.getStatus().equals(10)) {
				holidayDTO.setIsWithdrawable(false);
				holidayDTO.setIsApplicable(false);
				holidayDTO.setStatus(AppConstants.REJECTED);
				holidayDTO.setCancelReason(item.getStatusNote());
			} else if (item.getStatus().equals(11)) {
				holidayDTO.setIsWithdrawable(false);
				holidayDTO.setIsApplicable(false);
				holidayDTO.setStatus("Cancellation request sent");
			} else if (item.getStatus().equals(12)) {
				holidayDTO.setIsWithdrawable(false);
				holidayDTO.setIsApplicable(true);
				holidayDTO.setStatus("Cancellation request Approved");
			} else if (item.getStatus().equals(13)) {
				holidayDTO.setIsWithdrawable(false);
				holidayDTO.setStatus("Cancellation request Rejected");
				holidayDTO.setCancelReason(item.getStatusNote());
			} else if (item.getStatus().equals(20)) {
				holidayDTO.setIsWithdrawable(false);
				holidayDTO.setIsApplicable(false);
				holidayDTO.setStatus("Withdrawal request sent");
			} else if (item.getStatus().equals(21)) {
				holidayDTO.setIsWithdrawable(false);
				holidayDTO.setIsApplicable(false);
				holidayDTO.setStatus("Withdrawal request Approved");
			} else if (item.getStatus().equals(22)) {
				holidayDTO.setIsWithdrawable(false);
				holidayDTO.setIsApplicable(false);
				holidayDTO.setStatus("Withdrawal request Rejected");
				holidayDTO.setCancelReason(item.getStatusNote());
			}
			Holidays holidays = holidaysRepository.findOne(item.getHolidayId());
			holidayDTO.setHoliDayDate(holidays.getHoliday_date());
			// LocalDateTime holiDayDate = new LocalDateTime(holidays.getHoliday_date());
			// if (holiDayDate.getYear() == LocalDateTime.now().getYear()) {
			// if (item.getStatus() != 10 && item.getStatus() != 12) {
			// holidaysTaken = holidaysTaken + 1;
			// }
			//
			// }
			holidayDTO.setHolidayName(holidaysRepository.findOne(item.getHolidayId()).getHoliday_name());
			if (item.getAddSupId() == null || item.getAddSupId() == supervisor.getId()) {
				String supervisorName = supervisor.getEmp_fname() + " " + supervisor.getEmp_mname() + " "
						+ supervisor.getEmp_lname();
				holidayDTO.setSupevisorName(supervisorName);
			} else if (item.getAddSupId() != null) {
				EmpPersonal adSupervisor = empEmploymentRepository.getSupervisor(item.getAddSupId());
				String supervisorName = supervisor.getEmp_fname() + " " + supervisor.getEmp_mname() + " "
						+ supervisor.getEmp_lname();
				holidayDTO.setSupevisorName(supervisorName);
			}
			holidayDTOs.add(holidayDTO);
		}
		// The number of holidays taken by a user this year.
		holidaysTaken = this.applyHolidayRepository.holidaysTaken(userId, dateTime.getYear());
		holidayAvailability.setHolidays_taken(holidaysTaken);
		userHoliDayResponse.setHolidayAvailability(holidayAvailability);
		if (hlidaysRemaining != null) {
			userHoliDayResponse.setHolidaysRemaining(hlidaysRemaining.intValue());
		}
		userHoliDayResponse.setLeaveAvailability(leaveAvailability);
		userHoliDayResponse.setUserHoliDayDTOs(userHoliDayDTOs);
		userHoliDayResponse.setSupervisorId(supervisor.getId());
		userHoliDayResponse.setSupervisorName(
				supervisor.getEmp_fname() + " " + supervisor.getEmp_mname() + " " + supervisor.getEmp_lname());
		userHoliDayResponse.setSupervisors(supervisorDTOs);
		userHoliDayResponse.setLeaveHistory(leaveDTOs);
		userHoliDayResponse.setLeaveTypes(leaveTypes);
		userHoliDayResponse.setHolidayHistory(holidayDTOs);
		return userHoliDayResponse;
	}

	// method to save multiple holiday requests.
	public String saveHolidayRequest(List<HoliDayRequest> holiDayRequestList) throws OperationNotPossibleException {
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);
		Integer year = dateTime.getYear();
		if (!CollectionUtils.isEmpty(holiDayRequestList) && holiDayRequestList.size() > 10) {
			throw new OperationNotPossibleException("limitreached",
					"The Number of Holidays for a year is restricted to 10");
		}
		HolidayAvailability holidayAvailability = holidayAvailabilityRepository
				.getUserHoliDayAvailability(holiDayRequestList.get(0).getEmpId(), year);
		if (holidayAvailability != null && !CollectionUtils.isEmpty(holiDayRequestList)) {
			BigDecimal hlidaysRemaining = holidayAvailability.getHolidays_remaining();
			if (hlidaysRemaining != null && hlidaysRemaining.intValue() < holiDayRequestList.size()
					&& !holiDayRequestList.get(0).getIsCancelHoliday()) {
				throw new OperationNotPossibleException("limitreached",
						"The Number of Holidays for a year is restricted to 10");

			}
		}
		// Return value;
		String result = null;
		// Current year
		// User holiday availability details for the current year.

		// Processing each request
		for (HoliDayRequest holiDayRequest : holiDayRequestList) {
			ApplyHoliday applyHoliday;
			if (holiDayRequest.getId() != null && holiDayRequest.getId() != 0) {
				applyHoliday = applyHolidayRepository.findOne(holiDayRequest.getId());
			} else {
				applyHoliday = new ApplyHoliday();
			}
			// End of Applyholiday object initialization either old object or new object
			// will available
			// Checking whether it is new holiday request.
			if ((holiDayRequest.getId() == null || holiDayRequest.getId() == 0) && applyHoliday.getId() == null
					&& !holiDayRequest.getIsCancelHoliday()) {
				// New holiday request status will be 2
				applyHoliday.setStatus(2);
				applyHoliday.setAppliedOn(dateTime.toDate());
				applyHoliday.setHolidayId(holiDayRequest.getHolidayId());
				applyHoliday.setEmp_id(holiDayRequest.getEmpId());
				applyHoliday.setHolidayId(holiDayRequest.getHolidayId());
				applyHoliday.setAddSupId(holiDayRequest.getSuperVisorId());
				// Holiday availability should be checked here
				if (holidayAvailability != null) {
					holidayAvailability.setHolidays_remaining(
							holidayAvailability.getHolidays_remaining().subtract(new BigDecimal(1)));
					holidayAvailabilityRepository.saveAndFlush(holidayAvailability);
					double holiDayRemainingDoubleValue = Double
							.parseDouble(holidayAvailability.getHolidays_remaining().toString());
					int remainingDays = (int) holiDayRemainingDoubleValue;
					applyHoliday.setHolidaysRemaining(remainingDays);
				}
				result = AppConstants.SUCCES;
			} else {
				if (applyHoliday.getStatus() == 2) {
					// Here the holiday request is still fresh/or the supervisor/additional
					// supervisor didn't handled the request
					// Status needs to be set as 11
					// Holiday Availability should be increased
					applyHoliday.setStatus(11);
					applyHoliday.setCancelDate(dateTime.toDate());
					applyHoliday.setCancelReason(holiDayRequest.getCancelReaseon());
					result = AppConstants.SUCCES;

				} else if (applyHoliday.getStatus() == 9) {
					// Here the holiday request is not fresh/or the supervisor/additional
					// supervisor handled the request
					// Status needs to be set as 20
					// Holiday Availability should be increased
					applyHoliday.setStatus(20);
					applyHoliday.setCancelDate(dateTime.toDate());
					applyHoliday.setCancelReason(holiDayRequest.getCancelReaseon());
					result = AppConstants.SUCCES;
				}
			}
			this.applyHolidayRepository.saveAndFlush(applyHoliday);
			this.sendHolidayRequestEmail(applyHoliday);
			this.sendHolidayPushNotification(applyHoliday);
		}

		return result;
	}

	// Method to save leave request for employee.
	public Map<String, String> applyLeaveForEmp(ApplyLeaveRequest applyLeaveRequest, MultipartFile file)
			throws IOException, NotFoundException, OperationNotPossibleException {
		logger.debug("Saving Leeave request to db");
		Integer empId = applyLeaveRequest.getEmpId();
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);
		EmpPersonal empPersonal = empPersonalRepository.findOne(empId);
		if (empPersonal == null) {
			throw new NotFoundException("user", "Invalid user");
		}
		// This field will become true if the user tries to delete the leave request
		// after supervisor approval
		Boolean isLeaveCancel = false;
		// This filed will become true if the user tries to cancel the leave request
		// without supervisor approval.
		Boolean isLeaveDelete = false;
		// Return value.
		Map<String, String> response = new HashMap();
		ApplyLeave applyLeave = new ApplyLeave();
		// checking and processing new leave request
		if (applyLeaveRequest.getId() == null) {
			//Check whether employee already applied for the leave
			LocalDateTime fromTime = LocalDateTime.fromDateFields(applyLeaveRequest.getFomDate()).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0);
			LocalDateTime toTime = LocalDateTime.fromDateFields(applyLeaveRequest.getToDate()).withHourOfDay(0).withMinuteOfHour(0).withSecondOfMinute(0);
			Boolean isAlreadyApplied = applyLeaveRepository.isAlreadyApplied(empId, fromTime.toDate(), toTime.toDate());
			if(isAlreadyApplied == true) {
				throw new OperationNotPossibleException("error", "Leave already applied for the date");
			}
			applyLeave.setAppliedDate(dateTime.toDate());
			applyLeave.setAcceptLop(applyLeaveRequest.getAcceptLop());
			applyLeave.setAddSupId(applyLeaveRequest.getAddSupId());
				applyLeave.setFromDate(new DateTime(applyLeaveRequest.getFomDate()).withZone(indianZone).toDate());
				applyLeave.setToDate(new DateTime(applyLeaveRequest.getToDate()).withZone(indianZone).toDate());
			applyLeave.setNoOfDays(applyLeaveRequest.getNoOfDays());
			applyLeave.setLeaveReason(applyLeaveRequest.getReason());
			// For leave request the leave type id will be 6 in db.
			// So the number of days will be .5 so we need to extract it from the filed
			// halfDayTypeLeaveTypes count
			if (applyLeaveRequest.getIsHalfDay()) {
				LeaveTypes halDayLeave = this.leaveTypesRepository.findOne(6);
				applyLeave.setLeaveTypes(halDayLeave);
				applyLeave.setHalfDayTypeLeaveTypes(applyLeaveRequest.getLeaveTypes());
			} else {
				applyLeave.setLeaveTypes(applyLeaveRequest.getLeaveTypes());
			}
			applyLeave.setEmpPersonal(empPersonal);
			// If the leave request has an attached document.
			if (file != null) {
				Long time = new Date().getTime();
				String fileName = applyLeaveRequest.getEmpId() + "-applyleave-" + time;
				String[] nameArray = file.getOriginalFilename().split("\\.");
				fileName = fileName + "." + file.getOriginalFilename().split("\\.")[nameArray.length - 1];
				byte[] bytes = file.getBytes();
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(new File(filePath + fileName)));
				stream.write(bytes);
				stream.close();
				applyLeave.setAttachedDoc(fileName);
			}
			// Fetching the current leave availability details of the user for the applied
			// leave type.
			LeaveAvailability leaveAvailability;

			leaveAvailability = leaveAvailabilityRepository.getLeaveAvailability(empId,
					applyLeaveRequest.getLeaveTypes().getId(), dateTime.getYear());
			// Subtracting the number of days.
			if (leaveAvailability != null) {
				// BigDecimal leaveRemainingBeforeApply =
				// leaveAvailability.getLeaves_remaining();
				BigDecimal leaveRemaining = leaveAvailability.getLeaves_remaining()
						.subtract(applyLeaveRequest.getNoOfDays());
				leaveAvailability.setLeaves_remaining(leaveRemaining);

				// BigDecimal leavesTaken = leaveRemainingBeforeApply.subtract(leaveRemaining);
				if (leaveAvailability.getLeaves_used() != null) {
					leaveAvailability
							.setLeaves_used(leaveAvailability.getLeaves_used().add(applyLeaveRequest.getNoOfDays()));
				} else {
					leaveAvailability.setLeaves_used(applyLeaveRequest.getNoOfDays());
				}
				leaveAvailabilityRepository.saveAndFlush(leaveAvailability);
				applyLeave.setLeaveTypeRemaining(leaveAvailability.getLeaves_remaining());
				applyLeave.setApplicationStatus(2);
			} else {
				throw new NotFoundException("user", "User leave details not found");
			}

		} else if (applyLeaveRequest.getId() != null && applyLeaveRequest.getIsLeaveCancel()) {
			ApplyLeave cancelApplyLeave = applyLeaveRepository.findOne(applyLeaveRequest.getId());
			if (cancelApplyLeave == null) {
				response.put(AppConstants.STATUS, AppConstants.ERROR);
				response.put("message", "Leave request was cacelled by employee itself");
				return response;
			}
			ApplyLeave capplyLeaveEmailCopy = cancelApplyLeave;
			LeaveAvailability leaveAvailability = null;
			// While taking leave availability we will not consider half day leave types.
			if (cancelApplyLeave.getLeaveTypes() != null) {

				leaveAvailability = leaveAvailabilityRepository.getLeaveAvailability(applyLeaveRequest.getEmpId(),
						cancelApplyLeave.getLeaveTypes().getId(), dateTime.getYear());
			}
			if (cancelApplyLeave.getLeaveTypes().getId() == 6 && cancelApplyLeave.getHalfDayTypeLeaveTypes() != null) {
				// While taking leave availability we will not consider half day leave types.

				leaveAvailability = leaveAvailabilityRepository.getLeaveAvailability(applyLeaveRequest.getEmpId(),
						cancelApplyLeave.getHalfDayTypeLeaveTypes().getId(), dateTime.getYear());
			}
			// This leave request will be deleted from db since the supervisor is not
			// approved it yet
			if (cancelApplyLeave.getApplicationStatus() == 2) {
				isLeaveDelete = true;
				// BigDecimal leaveRemainingBeforeCancell =
				// leaveAvailability.getLeaves_remaining();
				// Incrementing the leave remaining count
				leaveAvailability.setLeaves_remaining(
						leaveAvailability.getLeaves_remaining().add(cancelApplyLeave.getNoOfDays()));

				// Decrementing the leave used count.
				if (leaveAvailability.getLeaves_used() != null) {
					leaveAvailability.setLeaves_used(
							leaveAvailability.getLeaves_used().subtract(cancelApplyLeave.getNoOfDays()));
				}
				// else {
				// // TODO
				// }
				leaveAvailabilityRepository.saveAndFlush(leaveAvailability);
				applyLeaveRepository.delete(cancelApplyLeave);
				ApplyLeave deletedApplyLeave = applyLeaveRepository.findOne(applyLeaveRequest.getId());
				if (deletedApplyLeave == null) {
					response.put(AppConstants.STATUS, AppConstants.SUCCES);
					response.put("message", "Leave cacelled successfully ");
				} else {
					response.put(AppConstants.STATUS, AppConstants.ERROR);
					response.put("message", "Leave cancelling failed ");
				}
			} else if (cancelApplyLeave.getApplicationStatus() == 9) {
				// Leave cancellation request will be send.
				cancelApplyLeave.setApplicationStatus(11);
				cancelApplyLeave.setCancelReason(applyLeaveRequest.getCancelReason());
				cancelApplyLeave.setCancelDate(dateTime.toDate());
				applyLeaveRepository.saveAndFlush(cancelApplyLeave);
				// While taking leave availability we will not consider half day leave types.
				if (cancelApplyLeave.getLeaveTypes() != null) {
					leaveAvailability = leaveAvailabilityRepository.getLeaveAvailability(applyLeaveRequest.getEmpId(),
							cancelApplyLeave.getLeaveTypes().getId(), dateTime.getYear());
				}
				if (cancelApplyLeave.getLeaveTypes().getId() == 6
						&& cancelApplyLeave.getHalfDayTypeLeaveTypes() != null) {
					// While taking leave availability we will not consider half day leave types.
					leaveAvailability = leaveAvailabilityRepository.getLeaveAvailability(applyLeaveRequest.getEmpId(),
							cancelApplyLeave.getHalfDayTypeLeaveTypes().getId(), dateTime.getYear());
				}

				// The leave availability should be handled by the approver during approval time

				/*
				 * // Incrementing the leave remaining count
				 * leaveAvailability.setLeaves_remaining(
				 * leaveAvailability.getLeaves_remaining().add(cancelApplyLeave.getNoOfDays()));
				 * // Decrementing the leave used count. if (leaveAvailability.getLeaves_used()
				 * != null) { leaveAvailability.setLeaves_used(
				 * leaveAvailability.getLeaves_used().subtract(cancelApplyLeave.getNoOfDays()));
				 * } // else { // // TODO // }
				 * leaveAvailabilityRepository.saveAndFlush(leaveAvailability);
				 */
				capplyLeaveEmailCopy = cancelApplyLeave;
				isLeaveCancel = true;
				isLeaveDelete = false;
				response.put(AppConstants.STATUS, AppConstants.SUCCES);
				response.put("message", "Leave cacellation request send successfully ");
			}
			if (!isLeaveDelete) {
				// Sending push notification
				this.sendLeavePushNotification(cancelApplyLeave);
			}
			// Sending email
			this.sendApplyLeaveEmail(capplyLeaveEmailCopy, isLeaveDelete, isLeaveCancel);
			return response;
		}
		// If the leave request is cancel leave request then this segment will not be
		// executed
		applyLeaveRepository.saveAndFlush(applyLeave);
		this.sendApplyLeaveEmail(applyLeave, isLeaveDelete, isLeaveCancel);
		this.sendLeavePushNotification(applyLeave);
		if (applyLeave.getId() != null) {
			response.put(AppConstants.STATUS, AppConstants.SUCCES);
			response.put("message", "Leave applied succesfully");
		} else {
			response.put(AppConstants.STATUS, AppConstants.ERROR);
			response.put("message", "Leave apply failed ");
		}
		return response;

	}

	// Method to approve single leave request
	public String aprroveLeave(ApplyLeaveApproovalRequest applyLeaveApproovalRequest)
			throws ApprovalNotPossibleException, NotFoundException {
		logger.debug("Request save leave approoval/cancelation into DB");
		String returnValue = null;
		// This filed represent whether approval is related with the leave cancellation.
		Boolean isLeaveCancel = false;
		// This filed represent whether approval is related with the fresh leave
		// request.
		Boolean isReject = false;
		ApplyLeave applyLeave = applyLeaveRepository.findOne(applyLeaveApproovalRequest.getLeaveRequestId());
		if (applyLeave == null) {
			throw new NotFoundException("leave", "The leave request is already cancelled by the employee");
		}
		// Checking whether additional supervisor and supervisor conflicts not exists
		Boolean isOperationInvalid = false;
		if (applyLeave.getApplicationStatus() != 2 && applyLeave.getApplicationStatus() != 11) {
			isOperationInvalid = true;
		}
		// If no conflicts
		if (!isOperationInvalid) {
			if (applyLeave.getApplicationStatus() == 11) {
				isLeaveCancel = true;
			}
			if (applyLeaveApproovalRequest.getIsReject() || applyLeaveApproovalRequest.getIsRejectCancelLeave()) {
				isReject = true;
			}

			Integer empId = applyLeave.getEmpPersonal().getId();
			if (applyLeaveApproovalRequest.getIsReject()) {
				returnValue = AppConstants.REJECTED;
				applyLeave.setSupApproval(AppConstants.REJECTED);
				DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
				DateTime dateTime = new DateTime().withZone(indianZone);
				applyLeave.setSupApprovalDate(dateTime.toDate());
				applyLeave.setApplicationStatus(10);
				applyLeave.setSupApprovalNote(applyLeaveApproovalRequest.getCancelReason());
				LeaveAvailability leaveAvailability;
				if (applyLeave.getLeaveTypes() != null && applyLeave.getLeaveTypes().getId() == 6) {
					leaveAvailability = leaveAvailabilityRepository.getLeaveAvailability(empId,
							applyLeave.getHalfDayTypeLeaveTypes().getId(), dateTime.getYear());
				} else {
					leaveAvailability = leaveAvailabilityRepository.getLeaveAvailability(empId,
							applyLeave.getLeaveTypes().getId(), dateTime.getYear());
				}
				// BigDecimal leaveRemainingBeforeReject =
				// leaveAvailability.getLeaves_remaining();
				leaveAvailability
						.setLeaves_remaining(leaveAvailability.getLeaves_remaining().add(applyLeave.getNoOfDays()));
				// BigDecimal leavesTaken =
				// leaveAvailability.getLeaves_remaining().subtract(leaveRemainingBeforeReject);
				// leaveAvailability.setLeaves_used(leavesTaken);
				if (leaveAvailability.getLeaves_used() != null) {
					leaveAvailability
							.setLeaves_used(leaveAvailability.getLeaves_used().subtract(applyLeave.getNoOfDays()));
				}
				// else {
				// //Todo
				// }
				leaveAvailabilityRepository.saveAndFlush(leaveAvailability);
			} else if (applyLeaveApproovalRequest.getIsApproveCancelLeave()) {
				applyLeave.setApplicationStatus(12);
				applyLeave.setCancelApprovalNote(applyLeaveApproovalRequest.getCanCelLeaveComment());
				applyLeave.setCancelApprovalBy(applyLeaveApproovalRequest.getApproverId());

				// Incrementing the leave remaining count
				DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
				DateTime dateTime = new DateTime().withZone(indianZone);
				LeaveAvailability leaveAvailability;
				if (applyLeave.getLeaveTypes() != null && applyLeave.getLeaveTypes().getId() == 6) {
					leaveAvailability = leaveAvailabilityRepository.getLeaveAvailability(empId,
							applyLeave.getHalfDayTypeLeaveTypes().getId(), dateTime.getYear());
				} else {
					leaveAvailability = leaveAvailabilityRepository.getLeaveAvailability(empId,
							applyLeave.getLeaveTypes().getId(), dateTime.getYear());
				}
				leaveAvailability
						.setLeaves_remaining(leaveAvailability.getLeaves_remaining().add(applyLeave.getNoOfDays()));
				// Decrementing the leave used count.
				if (leaveAvailability.getLeaves_used() != null) {
					leaveAvailability
							.setLeaves_used(leaveAvailability.getLeaves_used().subtract(applyLeave.getNoOfDays()));
				}
				// else {
				// // TODO
				// }
				leaveAvailabilityRepository.saveAndFlush(leaveAvailability);

				// LeaveAvailability leaveAvailability;
				// if (applyLeave.getLeaveTypes() != null && applyLeave.getLeaveTypes().getId()
				// == 6) {
				// leaveAvailability = leaveAvailabilityRepository.getLeaveAvailability(empId,
				// applyLeave.getHalfDayTypeLeaveTypes().getId(),
				// LocalDateTime.now().getYear());
				// } else {
				// leaveAvailability = leaveAvailabilityRepository.getLeaveAvailability(empId,
				// applyLeave.getLeaveTypes().getId(), LocalDateTime.now().getYear());
				// }
				// leaveAvailability
				// .setLeaves_remaining(leaveAvailability.getLeaves_remaining().add(applyLeave.getNoOfDays()));
				// if (leaveAvailability.getLeaves_used() != null) {
				// leaveAvailability
				// .setLeaves_used(leaveAvailability.getLeaves_used().subtract(applyLeave.getNoOfDays()));
				// }
				// leaveAvailabilityRepository.saveAndFlush(leaveAvailability);
				applyLeave.setCancelApprovalDate(dateTime.toDate());
				returnValue = AppConstants.APPROVED;

			} else if (applyLeaveApproovalRequest.getIsRejectCancelLeave()) {
				applyLeave.setApplicationStatus(13);
				applyLeave.setCancelApprovalNote(applyLeaveApproovalRequest.getCanCelLeaveComment());
				applyLeave.setCancelApprovalBy(applyLeaveApproovalRequest.getApproverId());
				DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
				DateTime dateTime = new DateTime().withZone(indianZone);
				applyLeave.setCancelApprovalDate(dateTime.toDate());
				returnValue = AppConstants.REJECTED;
				/*
				 * LeaveAvailability leaveAvailability; if (applyLeave.getLeaveTypes() != null
				 * && applyLeave.getLeaveTypes().getId() == 6) { leaveAvailability =
				 * leaveAvailabilityRepository.getLeaveAvailability(empId,
				 * applyLeave.getHalfDayTypeLeaveTypes().getId(), dateTime.getYear()); } else {
				 * leaveAvailability = leaveAvailabilityRepository.getLeaveAvailability(empId,
				 * applyLeave.getLeaveTypes().getId(), dateTime.getYear()); } // BigDecimal
				 * leaveRemainingBeforeReject = // leaveAvailability.getLeaves_remaining();
				 * leaveAvailability.setLeaves_remaining(
				 * leaveAvailability.getLeaves_remaining().subtract(applyLeave.getNoOfDays()));
				 * // BigDecimal leavesTaken = //
				 * leaveRemainingBeforeReject.subtract(leaveAvailability.getLeaves_remaining());
				 * // leaveAvailability.setLeaves_used(leavesTaken); if
				 * (leaveAvailability.getLeaves_used() != null) {
				 * leaveAvailability.setLeaves_used(leaveAvailability.getLeaves_used().add(
				 * applyLeave.getNoOfDays())); } // else { // //TODO // }
				 * leaveAvailabilityRepository.saveAndFlush(leaveAvailability);
				 */

			} else {
				returnValue = AppConstants.APPROVED;
				applyLeave.setSupApproval(AppConstants.APPROVED);
				applyLeave.setApplicationStatus(9);
				DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
				DateTime dateTime = new DateTime().withZone(indianZone);
				applyLeave.setSupApprovalDate(dateTime.toDate());
				applyLeave.setAddsupApprovalNote(applyLeaveApproovalRequest.getCancelReason());

			}
			applyLeaveRepository.saveAndFlush(applyLeave);
			this.sendLeaveApproveEmail(applyLeave, isLeaveCancel, isReject, applyLeaveApproovalRequest.getApproverId());
			this.sendLeaveApprovalPushNotification(applyLeave, applyLeaveApproovalRequest.getApproverId());
		} else {
			String currentStatus = null;
			Integer applicationStatus = applyLeave.getApplicationStatus();
			if (applicationStatus == 9 || applicationStatus == 12) {
				currentStatus = AppConstants.APPROVED;
			} else if (applicationStatus == 10 || applicationStatus == 13) {
				currentStatus = AppConstants.REJECTED;
			}
			throw new ApprovalNotPossibleException("leave", "Operation already handled by another person",
					"AppConstants.SUCCES", currentStatus);
		}
		return returnValue;
	}

	// Method to approve multiple leave
	public String aprroveMltipleLeave(List<ApplyLeaveApproovalRequest> applyLeaveApproovalRequests)
			throws ApprovalNotPossibleException, NotFoundException {
		logger.debug("Request save multiple leave approoval/cancelation into DB");
		String result = null;
		String status;
		// Here at a time only single operation is possible for all the leaves requests
		// ie either approval or rejection.
		if (applyLeaveApproovalRequests.get(0).getIsReject()
				|| applyLeaveApproovalRequests.get(0).getIsRejectCancelLeave()) {
			status = AppConstants.REJECTED;
		} else {
			status = AppConstants.APPROVED;
		}
		// Each request is iterated and called the single leave aproove emthod
		for (ApplyLeaveApproovalRequest applyLeaveApproovalRequest : applyLeaveApproovalRequests) {
			result = this.aprroveLeave(applyLeaveApproovalRequest);
			if (!status.equals(result) && !"Operation already done".equals(result)) {
				return null;
			}
		}

		return result;
	}

	// Method to approve holiday
	public String aprroveHoliDay(ApplyHoliDayApproveRequest applyHoliDayApproveRequest)
			throws ApprovalNotPossibleException, NotFoundException {
		logger.debug("Saving HoliDay approval status to db");
		if (applyHoliDayApproveRequest.getHolidayId() == null || applyHoliDayApproveRequest.getHolidayId().equals(0)) {
			throw new NotFoundException("error", "ID not found");
		}
		String returnValue = null;
		Integer approverId = applyHoliDayApproveRequest.getApproverId();
		// Fetching object to be updated
		ApplyHoliday applyHoliday = applyHolidayRepository.findOne(applyHoliDayApproveRequest.getHolidayId());
		Integer currentStatus = applyHoliday.getStatus();
		// Checking whether additional supervisor and supervisor conflicts not exists
		if (currentStatus != 2 && currentStatus != 11 && currentStatus != 20) {
			String status;
			if (currentStatus == 9 || currentStatus == 12 || currentStatus == 21) {
				status = AppConstants.APPROVED;
			} else {
				status = AppConstants.REJECTED;
			}
			throw new ApprovalNotPossibleException("holiday", "Operation already handled by another person",
					AppConstants.SUCCES, status);
		}
		// Current year
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);
		Integer year = dateTime.getYear();
		Integer empId = applyHoliday.getEmp_id();
		HolidayAvailability holidayAvailability = holidayAvailabilityRepository.getUserHoliDayAvailability(empId, year);
		if (applyHoliDayApproveRequest.getIsReject() && currentStatus == 2) {
			returnValue = AppConstants.REJECTED;
			// Status should be 10,rejection date,rejected by and reason should be
			// specified.
			applyHoliday.setStatus(10);
			applyHoliday.setStatusNote(applyHoliDayApproveRequest.getCancelReason());

			applyHoliday.setReviewedDate(dateTime.toDate());
			applyHoliday.setReviewedBy(approverId);
			holidayAvailability
					.setHolidays_remaining(holidayAvailability.getHolidays_remaining().add(new BigDecimal(1)));
			holidayAvailabilityRepository.saveAndFlush(holidayAvailability);
		} else if (applyHoliDayApproveRequest.getIsApproveCancelHoliday()
				&& (currentStatus == 11 || currentStatus == 20)) {
			/*
			 * The supervisor approving the leave cancellation request Status shuold be
			 * 12/21 review date and reviewed by should be set Holiday availability should
			 * be increased because the holiday is cancelled
			 */
			if (currentStatus == 11) {
				applyHoliday.setStatus(12);
			} else if (currentStatus == 20) {
				applyHoliday.setStatus(21);
				applyHoliday.setStatusNote("approved");
			}
			returnValue = AppConstants.APPROVED;
			applyHoliday.setReviewedDate(dateTime.toDate());
			applyHoliday.setReviewedBy(approverId);

			// Adding holiday remaining on cancellation approval
			holidayAvailability
					.setHolidays_remaining(holidayAvailability.getHolidays_remaining().add(new BigDecimal(1)));
			holidayAvailabilityRepository.saveAndFlush(holidayAvailability);

		} else if (applyHoliDayApproveRequest.getIsRejectCancelLHoliday()
				&& (currentStatus == 11 || currentStatus == 20)) {
			/*
			 * The supervisor approving the leave cancellation request Status should be
			 * 13/22 review date and reviewed by should be set No change in Holiday
			 * availability
			 */
			returnValue = AppConstants.REJECTED;
			if (currentStatus == 11) {
				applyHoliday.setStatus(13);
			} else if (currentStatus == 20) {
				applyHoliday.setStatus(22);
			}
			applyHoliday.setReviewedDate(dateTime.toDate());
			applyHoliday.setReviewedBy(approverId);
			applyHoliday.setStatusNote(applyHoliDayApproveRequest.getCancelReason());
			/*
			 * Here holiday availability is credit with employee action but if supervisor
			 * cancelled the he/she need to accept the holiday request
			 */

		} else if (currentStatus == 2) {
			/*
			 * The supervisor approving the leave cancellation request Status shuold be 9
			 * review date and reviewed by should be set No change in holiday availability
			 */
			returnValue = AppConstants.APPROVED;
			applyHoliday.setStatus(9);

			applyHoliday.setReviewedDate(dateTime.toDate());
			applyHoliday.setReviewedBy(approverId);
			applyHoliday.setStatusNote("approved");
		}
		applyHolidayRepository.saveAndFlush(applyHoliday);
		this.sendHolidayApproveEmail(applyHoliday, approverId);
		this.sendHolidayApprovalPushNotification(applyHoliday, approverId);

		return returnValue;
	}

	// Method to approve multiple holiday
	public String aprroveMultipleHoliDay(List<ApplyHoliDayApproveRequest> applyHoliDayApproveRequests)
			throws ApprovalNotPossibleException, NotFoundException {
		logger.debug("Saving multiple HoliDay approval status to db");
		String result = null;
		String status;
		if (applyHoliDayApproveRequests.get(0).getIsReject()
				|| applyHoliDayApproveRequests.get(0).getIsRejectCancelLHoliday()) {
			status = AppConstants.REJECTED;
		} else {
			status = AppConstants.APPROVED;
		}
		// Each request is iterated and called the single holiday approval method
		for (ApplyHoliDayApproveRequest applyHoliDayApproveRequest : applyHoliDayApproveRequests) {
			result = this.aprroveHoliDay(applyHoliDayApproveRequest);
			if (!status.equals(result) && !"Operation already done".equals(result)) {
				return null;
			}
		}
		return status;
	}

	public List<LeaveDTO> getEmployessOnLeave() {
		logger.debug("Retrieving Employess on leave from DB");
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);
		Date currentDate = dateTime.toDate();

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate = null;
		try {
			startDate = format.parse(format.format(currentDate));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<LeaveDTO> leaveDTOs = new ArrayList();
		List<ApplyLeave> empLeaveList = applyLeaveRepository.getEmployessOnLeave(startDate);
		Set<Integer> empSet = new HashSet();
		empLeaveList.stream().forEach(item -> {
			if (!empSet.contains(item.getEmpPersonal().getId())) {
				leaveDTOs.add(applyLeaveMapper.applyLeaveToLeaveDTO(item));
				empSet.add(item.getEmpPersonal().getId());
			}
		});

		return leaveDTOs;
	}

	// Method to get availability of different types of leaves.
	public List<LeaveAvailability> getEmployeeLeaveAvailability(Integer userId) {
		logger.debug("retireving leave details from db");
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);
		List<LeaveAvailability> leaveAvailabilities = leaveAvailabilityRepository.getLeaveAvailabilityByEmployId(userId,
				dateTime.getYear());
		return leaveAvailabilities;
	}

	// Method to get leave request list in approver side.
	public List<AppliedLeaveDTO> getLeaveRequests(Integer supId) {
		logger.debug("Request to retrieve all leave requests from DB");
		List<ApplyLeave> appliedLeaves = applyLeaveRepository.getAppliedLeaves(supId);
		List<AppliedLeaveDTO> appliedLeaveDTOs = new ArrayList();
		appliedLeaves.stream().forEach(item -> {
			AppliedLeaveDTO appliedLeaveDTO = new AppliedLeaveDTO();
			appliedLeaveDTO.setId(item.getId());
			appliedLeaveDTO.setAppliedDate(item.getAppliedDate());
			appliedLeaveDTO.setFromDate(item.getFromDate());
			appliedLeaveDTO.setToDate(item.getToDate());
			appliedLeaveDTO.setNoOfDays(item.getNoOfDays());
			appliedLeaveDTO.setEmp_avatar(item.getEmpPersonal().getEmp_avatar());
			appliedLeaveDTO.setEmp_fname(item.getEmpPersonal().getEmp_fname());
			appliedLeaveDTO.setEmp_lname(item.getEmpPersonal().getEmp_lname());
			appliedLeaveDTO.setEmp_mname(item.getEmpPersonal().getEmp_mname());
			appliedLeaveDTO.setEmpId(item.getEmpPersonal().getId());
			appliedLeaveDTO.setLeaveReason(item.getLeaveReason());
			if (item.getAttachedDoc() != null && !item.getAttachedDoc().isEmpty()
					&& item.getAttachedDoc().length() > 2) {
				appliedLeaveDTO.setAttachedDoc(item.getAttachedDoc());
			} else {
				appliedLeaveDTO.setAttachedDoc(null);
			}
			appliedLeaveDTO.setCancelDate(item.getCancelDate());
			Designation designation = empEmploymentRepository.getEmployeeDesignation(item.getEmpPersonal().getId());
			Vertical vertical = empEmploymentRepository.getEmployeeVertical(item.getEmpPersonal().getId());
			appliedLeaveDTO.setDesignation(designation);
			appliedLeaveDTO.setVertical(vertical);
			Integer empId = item.getEmpPersonal().getId();
			Integer leaveTypeId = null;
			if (item.getLeaveTypes() != null && item.getLeaveTypes().getId() != 6) {
				leaveTypeId = item.getLeaveTypes().getId();
				appliedLeaveDTO.setLeaveTypes(item.getLeaveTypes());
			} else {
				leaveTypeId = item.getHalfDayTypeLeaveTypes().getId();
				appliedLeaveDTO.setLeaveTypes(item.getHalfDayTypeLeaveTypes());
			}
			DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
			DateTime dateTime = new DateTime().withZone(indianZone);
			LeaveAvailability leaveAvailability = leaveAvailabilityRepository.getLeaveAvailability(empId, leaveTypeId,
					dateTime.getYear());
			appliedLeaveDTO.setLeaveAvailability(leaveAvailability.getLeaves_remaining());
			appliedLeaveDTO.setLeavesTaken(leaveAvailability.getLeaves_used());
			if (leaveAvailability.getLeaves_remaining() != null && leaveAvailability.getLeaves_used() != null) {
				appliedLeaveDTO.setTotalLeaves(
						leaveAvailability.getLeaves_remaining().add(leaveAvailability.getLeaves_used()));
			}
			if (item.getApplicationStatus() == 2) {
				appliedLeaveDTO.setStatus("New Leave request");
			} else if (item.getApplicationStatus() == 11) {
				appliedLeaveDTO.setStatus("Leave cancel request");
				appliedLeaveDTO.setLeaveCancelReason(item.getCancelReason());
				appliedLeaveDTO.setIsLeaveCancelRequest(true);
			}

			appliedLeaveDTOs.add(appliedLeaveDTO);
		});
		return appliedLeaveDTOs;
	}

	public Page<LeaveHistoryByEmpDTO> getEmployeeLeaveHisotryBySupevisor(Integer empId, Pageable pageable) {

		Page<ApplyLeave> applyLeaveHisotry = this.applyLeaveRepository.getEmployeeHistoryBySupevisor(empId, pageable);
		Page<LeaveHistoryByEmpDTO> leaveHistory = applyLeaveHisotry
				.map(applyLeave -> new LeaveHistoryByEmpDTO(applyLeave));
		return leaveHistory;

	}

	public Page<HolidayHistroryByEmpDTO> getEmpHolidayHistoryBySupervisor(Integer empId, Pageable pageable) {

		return this.applyHolidayRepository.getHolidayHistoryBySupervisor(empId, pageable);
	}

	// Method to get holiday request list in approver side.
	public List<AppliedHoliDayDTO> getAppliedHoliDays(Integer empId) {
		logger.debug("Request to fetch applied holidays from DB");
		List<AppliedHoliDayDTO> appliedHoliDayDTOs = new ArrayList();
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);
		Integer year = dateTime.getYear();
		List<ApplyHoliday> applyHolidays = applyHolidayRepository.getHolidayRequests(empId, year);
		applyHolidays.stream().forEach(item -> {
			AppliedHoliDayDTO appliedHoliDayDTO = new AppliedHoliDayDTO();
			appliedHoliDayDTO.setId(item.getId());
			EmpEmployment empEmployment = empEmploymentRepository.getEmployeeById(item.getEmp_id());
			appliedHoliDayDTO.setDesignation(empEmployment.getDesignation());
			appliedHoliDayDTO.setVertical(empEmployment.getVertical());
			appliedHoliDayDTO.setEmpId(empEmployment.getId());
			appliedHoliDayDTO.setEmp_fname(empEmployment.getEmpPersonal().getEmp_fname());
			appliedHoliDayDTO.setEmp_mname(empEmployment.getEmpPersonal().getEmp_mname());
			appliedHoliDayDTO.setEmp_lname(empEmployment.getEmpPersonal().getEmp_lname());
			appliedHoliDayDTO.setEmp_avathar(empEmployment.getEmpPersonal().getEmp_avatar());
			Holidays holidays = holidaysRepository.findOne(item.getHolidayId());
			appliedHoliDayDTO.setDate(holidays.getHoliday_date());
			appliedHoliDayDTO.setHoliDayId(holidays.getId());
			appliedHoliDayDTO.setHoliDayName(holidays.getHoliday_name());
			HolidayAvailability holidayAvailability = holidayAvailabilityRepository
					.getUserHoliDayAvailability(empEmployment.getId(), year);
			Integer holidaysTaken = this.applyHolidayRepository.holidaysTaken(empEmployment.getId(), year);// Holidays
																											// taken for
																											// the year
			appliedHoliDayDTO.setHoliDaysTaken(new BigDecimal(holidaysTaken));
			appliedHoliDayDTO.setHoliDayAvailability(holidayAvailability.getHolidays_remaining());
			if (item.getStatus() == 2) {
				appliedHoliDayDTO.setIsCancellationRequest(false);
				appliedHoliDayDTO.setCancellationReason(item.getCancelReason());
				appliedHoliDayDTO.setDescription("Holiday request");
			} else if (item.getStatus() == 11) {
				appliedHoliDayDTO.setIsCancellationRequest(true);
				appliedHoliDayDTO.setCancellationReason(item.getCancelReason());
				appliedHoliDayDTO.setDescription("Holiday Cancellation request");
			} else if (item.getStatus() == 20) {
				appliedHoliDayDTO.setIsCancellationRequest(true);
				appliedHoliDayDTO.setCancellationReason(item.getCancelReason());
				appliedHoliDayDTO.setDescription("Holiday Withdrawal request");
			}
			appliedHoliDayDTOs.add(appliedHoliDayDTO);

		});
		return appliedHoliDayDTOs;
	}

	// Method to send push notification of a new leave request to approver
	//
	// @Async
	private void sendLeavePushNotification(ApplyLeave applyLeave) {
		logger.debug("Sending push notification regarding leave");
		ObjectMapper mapper = new ObjectMapper();
		AppliedLeaveDTO appliedLeaveDTO = new AppliedLeaveDTO();
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);
		try {
			appliedLeaveDTO.setId(applyLeave.getId());
			appliedLeaveDTO.setAppliedDate(applyLeave.getAppliedDate());
			appliedLeaveDTO.setFromDate(applyLeave.getFromDate());
			appliedLeaveDTO.setToDate(applyLeave.getToDate());
			appliedLeaveDTO.setNoOfDays(applyLeave.getNoOfDays());
			appliedLeaveDTO.setEmp_avatar(applyLeave.getEmpPersonal().getEmp_avatar());
			appliedLeaveDTO.setEmp_fname(applyLeave.getEmpPersonal().getEmp_fname());
			appliedLeaveDTO.setEmp_lname(applyLeave.getEmpPersonal().getEmp_lname());
			appliedLeaveDTO.setEmp_mname(applyLeave.getEmpPersonal().getEmp_mname());
			appliedLeaveDTO.setEmpId(applyLeave.getEmpPersonal().getId());
			appliedLeaveDTO.setLeaveReason(applyLeave.getLeaveReason());
			appliedLeaveDTO.setAttachedDoc(applyLeave.getAttachedDoc());
			Designation designation = empEmploymentRepository
					.getEmployeeDesignation(applyLeave.getEmpPersonal().getId());
			Vertical vertical = empEmploymentRepository.getEmployeeVertical(applyLeave.getEmpPersonal().getId());
			appliedLeaveDTO.setDesignation(designation);
			appliedLeaveDTO.setVertical(vertical);
			Integer empId = applyLeave.getEmpPersonal().getId();
			Integer leaveTypeId = null;
			// Half day leave request is is not seen alone,we need to mention the real type
			// like compassionate,sick etc.
			if (applyLeave.getLeaveTypes() != null && applyLeave.getLeaveTypes().getId() != 6) {
				leaveTypeId = applyLeave.getLeaveTypes().getId();
				appliedLeaveDTO.setLeaveTypes(applyLeave.getLeaveTypes());
			} else {
				leaveTypeId = applyLeave.getHalfDayTypeLeaveTypes().getId();
				appliedLeaveDTO.setLeaveTypes(applyLeave.getHalfDayTypeLeaveTypes());
			}
			LeaveAvailability leaveAvailability = leaveAvailabilityRepository.getLeaveAvailability(empId, leaveTypeId,
					dateTime.getYear());
			appliedLeaveDTO.setLeaveAvailability(leaveAvailability.getLeaves_remaining());
			appliedLeaveDTO.setLeavesTaken(leaveAvailability.getLeaves_used());
			if (leaveAvailability.getLeaves_remaining() != null && leaveAvailability.getLeaves_used() != null) {
				appliedLeaveDTO.setTotalLeaves(
						leaveAvailability.getLeaves_remaining().add(leaveAvailability.getLeaves_used()));
			}
			// The request can be either a fresh leave request or cancellation of an already
			// approved leave.
			if (applyLeave.getApplicationStatus() == 2) {
				appliedLeaveDTO.setStatus("New Leave request");
			} else if (applyLeave.getApplicationStatus() == 11) {
				appliedLeaveDTO.setStatus("Leave cancel request");
				appliedLeaveDTO.setIsLeaveCancelRequest(true);
				appliedLeaveDTO.setLeaveCancelReason(applyLeave.getCancelReason());
			}
			EmpPersonal supervisor = empEmploymentRepository.getSupervisor(applyLeave.getEmpPersonal().getId());
			Integer supeVisorId = supervisor.getId();
			Integer additionalSupeVisorId = applyLeave.getAddSupId();

			FcmDetail fcmDetail = paymentLookupService.getEmployeeFcmDetail(supeVisorId);
			List<String> fcmIdList = new ArrayList();
			if (fcmDetail != null) {
				fcmIdList.add(fcmDetail.getFcm_id());
			}
			// Sending push notification if additional supervisor other than real supervisor
			// is mentioned in the request.
			if (additionalSupeVisorId != null && additionalSupeVisorId != 0
					&& !supeVisorId.equals(additionalSupeVisorId)) {

				fcmDetail = paymentLookupService.getEmployeeFcmDetail(additionalSupeVisorId);
				if (fcmDetail != null) {
					fcmIdList.add(fcmDetail.getFcm_id());
				}
			}
			if (!CollectionUtils.isEmpty(fcmIdList)) {
				this.fcmService.sendMessage(null, fcmIdList, mapper.writeValueAsString(appliedLeaveDTO), "apply_leave");
			}
		} catch (JsonProcessingException e) {
			logger.error("Error occured during creating push notification for Leave request", e);
		} catch (Exception e) {
			logger.error("Error occured during sending push notification for Leave  request", e);
		}

	}

	// Sending supervisor approval push notification to employee
	// @Async
	private void sendLeaveApprovalPushNotification(ApplyLeave applyLeave, Integer approverId) {
		logger.debug("Sending push notification regarding leave Approval");
		ObjectMapper mapper = new ObjectMapper();
		try {
			EmpPersonal supervisor = this.empPersonalRepository.findOne(approverId);
			String leaveType = null;
			if (applyLeave.getLeaveTypes().getId() == 6) {
				leaveType = applyLeave.getHalfDayTypeLeaveTypes().getLeave_type_name() + "(Half Day Request)";
			} else {
				leaveType = applyLeave.getLeaveTypes().getLeave_type_name();
			}
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(supervisor.getEmp_fname()).append(" ").append(supervisor.getEmp_mname()).append(" ")
					.append(supervisor.getEmp_lname());
			String supervisorName = stringBuilder.toString();
			GlobalPushDTO globalPushDTO = new GlobalPushDTO();
			String tittle = null;
			String body = null;
			String avathar = supervisor.getEmp_avatar();
			Integer applicationStatus = applyLeave.getApplicationStatus();
			if (applicationStatus == 9) {
				tittle = leaveType + " Request approved ";
				body = leaveType + "  request has been Approved by " + supervisorName;
			} else if (applicationStatus == 10) {
				if (applyLeave.getSupApprovalNote() != null) {
					tittle = leaveType + " Request rejected";
					body = leaveType + "  request has been Rejected by " + supervisorName + ".\n" + "Reason : "
							+ applyLeave.getSupApprovalNote();
				} else if (applyLeave.getAddsupApprovalNote() != null) {
					tittle = leaveType + " Request rejected ";
					body = leaveType + "  request has been Rejected by " + supervisorName + ".\n" + "Reason : "
							+ applyLeave.getAddsupApprovalNote();
				} else {
					tittle = leaveType + " Request rejected";
					body = leaveType + "  request has been Rejected by " + supervisorName + ".\n";
				}
			} else if (applicationStatus == 12) {
				tittle = leaveType + " Request cancellation approved";
				body = leaveType + "  request cancellation approved  by " + supervisorName;
			} else if (applicationStatus == 13) {
				tittle = leaveType + " Request cancellation rejected";
				body = leaveType + "  request cancellation rejected  by " + supervisorName + ".\n" + "Reason : "
						+ applyLeave.getCancelApprovalNote();
			}
			if (tittle != null && body != null) {
				globalPushDTO.setTitle(tittle);
				globalPushDTO.setBody(body);
				globalPushDTO.setAvathar(avathar);

				FcmDetail fcmDetail = paymentLookupService.getEmployeeFcmDetail(applyLeave.getEmpPersonal().getId());
				if (fcmDetail != null) {
					fcmService.sendMessage(fcmDetail.getFcm_id(), null, mapper.writeValueAsString(globalPushDTO),
							"others");
				}
			}

		} catch (JsonProcessingException e) {
			logger.error("Error occured during creating push notification for Leave Approval", e);
		} catch (Exception e) {
			logger.error("Error occured during sending push notification for Leave  Approval", e);
		}
	}

	// Holiday Push Notification
	// @Async
	private void sendHolidayPushNotification(ApplyHoliday applyHoliday) {
		logger.debug("Sending push notification regarding Holiday request");
		ObjectMapper mapper = new ObjectMapper();
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);
		Integer year = dateTime.getYear();
		AppliedHoliDayDTO appliedHoliDayDTO = new AppliedHoliDayDTO();
		try {
			appliedHoliDayDTO.setId(applyHoliday.getId());
			EmpEmployment empEmployment = empEmploymentRepository.getEmployeeById(applyHoliday.getEmp_id());
			appliedHoliDayDTO.setDesignation(empEmployment.getDesignation());
			appliedHoliDayDTO.setVertical(empEmployment.getVertical());
			appliedHoliDayDTO.setEmpId(empEmployment.getId());
			appliedHoliDayDTO.setEmp_fname(empEmployment.getEmpPersonal().getEmp_fname());
			appliedHoliDayDTO.setEmp_mname(empEmployment.getEmpPersonal().getEmp_mname());
			appliedHoliDayDTO.setEmp_lname(empEmployment.getEmpPersonal().getEmp_lname());
			appliedHoliDayDTO.setEmp_avathar(empEmployment.getEmpPersonal().getEmp_avatar());
			Holidays holidays = holidaysRepository.findOne(applyHoliday.getHolidayId());
			appliedHoliDayDTO.setDate(holidays.getHoliday_date());
			appliedHoliDayDTO.setHoliDayId(holidays.getId());
			appliedHoliDayDTO.setHoliDayName(holidays.getHoliday_name());
			HolidayAvailability holidayAvailability = holidayAvailabilityRepository
					.getUserHoliDayAvailability(empEmployment.getId(), year);
			Integer holidaysTaken = this.applyHolidayRepository.holidaysTaken(applyHoliday.getEmp_id(), year);// Holidays
																												// //
																												// year
			appliedHoliDayDTO.setHoliDaysTaken(new BigDecimal(holidaysTaken));
			appliedHoliDayDTO.setHoliDayAvailability(holidayAvailability.getHolidays_remaining());
			if (applyHoliday.getStatus() == 11) {
				appliedHoliDayDTO.setIsCancellationRequest(true);
				appliedHoliDayDTO.setCancellationReason(applyHoliday.getCancelReason());
				appliedHoliDayDTO.setDescription("Holiday Cancellation request");
			}
			if (applyHoliday.getStatus() == 20) {
				appliedHoliDayDTO.setIsCancellationRequest(true);
				appliedHoliDayDTO.setCancellationReason(applyHoliday.getCancelReason());
				appliedHoliDayDTO.setDescription("Holiday withdrawal request");
			} else {
				appliedHoliDayDTO.setDescription("New Holiday request");
			}
			EmpPersonal supervisor = empEmploymentRepository.getSupervisor(applyHoliday.getEmp_id());
			Integer supeVisorId = supervisor.getId();
			Integer additionalSupeVisorId = applyHoliday.getAddSupId();
			List<String> fcmIdList = new ArrayList();
			FcmDetail fcmDetail = paymentLookupService.getEmployeeFcmDetail(supeVisorId);
			if (fcmDetail != null) {
				fcmIdList.add(fcmDetail.getFcm_id());
			}
			// If additional supervisor is mentioned in the request
			if (additionalSupeVisorId != null && additionalSupeVisorId != 0
					&& !supeVisorId.equals(additionalSupeVisorId)) {
				fcmDetail = paymentLookupService.getEmployeeFcmDetail(additionalSupeVisorId);
				if (fcmDetail != null) {
					fcmIdList.add(fcmDetail.getFcm_id());
				}
			}
			if (!CollectionUtils.isEmpty(fcmIdList)) {
				this.fcmService.sendMessage(null, fcmIdList, mapper.writeValueAsString(appliedHoliDayDTO),
						"apply_holiday");
			}
		} catch (Exception e) {
			logger.error("Error occured during sending push notification for Holiday  request", e);
		}
	}

	// Method for sending push notification regarding holiday approval.
	// @Async
	private void sendHolidayApprovalPushNotification(ApplyHoliday applyHoliday, Integer approverId) {
		logger.debug("Sending push notification regarding Holiday approval");
		try {
			ObjectMapper mapper = new ObjectMapper();
			EmpPersonal supervisor = this.empPersonalRepository.findOne(approverId);
			Holidays holidays = this.holidaysRepository.findOne(applyHoliday.getHolidayId());
			String holidayName = holidays.getHoliday_name();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(supervisor.getEmp_fname()).append(" ").append(supervisor.getEmp_mname()).append(" ")
					.append(supervisor.getEmp_lname());
			String supervisorName = stringBuilder.toString();
			GlobalPushDTO globalPushDTO = new GlobalPushDTO();
			String tittle = null;
			String body = null;
			String avathar = supervisor.getEmp_avatar();
			if (applyHoliday.getStatus().equals(9)) {
				tittle = holidayName + " Holiday Request Approved";
				body = holidayName + " Holiday request has been Approved by " + supervisorName;
			} else if (applyHoliday.getStatus().equals(10)) {
				tittle = holidayName + " Holiday Request Rejected";
				body = holidayName + " Holiday request has been Rejected by " + supervisorName + ".\n"
						+ applyHoliday.getStatusNote();
			} else if (applyHoliday.getStatus().equals(12)) {
				tittle = holidayName + " Holiday Cancel Withdrawal request Approved";
				body = holidayName + " Holiday Cancel request has been Approved by " + supervisorName;
			} else if (applyHoliday.getStatus().equals(13)) {
				tittle = holidayName + " Holiday Cancel request Rejected";
				body = holidayName + " Holiday Cancel request has been Rejected by " + supervisorName + ".\n"
						+ applyHoliday.getStatusNote();
			} else if (applyHoliday.getStatus().equals(21)) {
				tittle = holidayName + " Holiday  Withdrawal request Approved";
				body = holidayName + " Holiday Withdrawal request has been Approved by " + supervisorName;
			} else if (applyHoliday.getStatus().equals(22)) {
				tittle = holidayName + " Holiday Withdrawal request Rejected";
				body = holidayName + " Holiday Withdrawal request has been Rejected by " + supervisorName + ".\n"
						+ applyHoliday.getStatusNote();
			}
			if (tittle != null && body != null) {
				globalPushDTO.setTitle(tittle);
				globalPushDTO.setBody(body);
				globalPushDTO.setAvathar(avathar);
				FcmDetail fcmDetail = paymentLookupService.getEmployeeFcmDetail(applyHoliday.getEmp_id());
				if (fcmDetail != null) {
					fcmService.sendMessage(fcmDetail.getFcm_id(), null, mapper.writeValueAsString(globalPushDTO),
							"others");

				}
			}
		} catch (JsonProcessingException e) {
			logger.error("Error occured during creating push notification for Leave Approval", e);
		} catch (Exception e) {
			logger.error("Error occured during sending push notification for Leave  Approval", e);
		}
	}

	// Sending email for holiday request
	// @Async
	private void sendHolidayRequestEmail(ApplyHoliday applyHoliday) {
		logger.debug("Sending Holiday Request email");
		String subject = null;
		Boolean isNew = false;
		Boolean isCancelWithoutApproval = false;
		Boolean isCancelApprovedHoliday = false;
		String jobLocation;
		String message = null;
		Integer status = applyHoliday.getStatus();
		Integer empId = applyHoliday.getEmp_id();
		Integer addtionalSupervisorId = applyHoliday.getAddSupId();
		EmpPersonal empPersonal = this.empPersonalRepository.findOne(empId);
		EmpEmployment empEmployment = this.empEmploymentRepository.findOne(empId);
		jobLocation = empEmployment.getLocations().getName();
		// Setting supervisor and additional supervisor details required in the email
		EmpPersonal supervisor = this.empEmploymentRepository.getSupervisor(empId);
		Integer supervisorId = supervisor.getId();
		EmpEmployment supervisorEmpEmployment = this.empEmploymentRepository.findOne(supervisorId);
		String supervisorName = supervisor.getEmp_fname();
		String supervisorEmail = supervisorEmpEmployment.getEmp_email();
		String addtionalSupervisorName = null;
		String addtionalSupervisorEmail = null;
		/*
		 * Need to send email for additional supervisor too if the user selected a
		 * person - other than supervisor while applying holiday request.
		 */
		if (addtionalSupervisorId != null && addtionalSupervisorId != 0
				&& !addtionalSupervisorId.equals(supervisorId)) {
			EmpPersonal additionalSupervisor = this.empPersonalRepository.findOne(addtionalSupervisorId);
			EmpEmployment additionalSupervisorEmployment = this.empEmploymentRepository.findOne(addtionalSupervisorId);
			addtionalSupervisorEmail = additionalSupervisorEmployment.getEmp_email();
			addtionalSupervisorName = additionalSupervisor.getEmp_fname();
		}
		// Setting employee related details required in the email
		StringBuilder empNameBuilder = new StringBuilder();
		empNameBuilder.append(empPersonal.getEmp_fname()).append(" ").append(empPersonal.getEmp_mname()).append(" ")
				.append(empPersonal.getEmp_lname());
		String empName = empNameBuilder.toString();
		String gender = empPersonal.getEmp_gender();
		String verticalName = empEmployment.getVertical().getVertical();
		String designation = empEmployment.getDesignation().getDesignation();
		String emailFrom = empEmployment.getEmp_email();
		// Deciding which email should be sent to the supervisor
		if (status == 2) {
			isNew = true;
			subject = " Holiday Request";
			message = "has been requested for Holiday";
		} else if (status == 11) {
			isCancelWithoutApproval = true;
			subject = "Holiday Cancel Request";
			String applied_date = applyHoliday.getAppliedOn().toString().substring(0, 10);
			message = "wants to Cancel the holiday applied on " + applied_date;
		} else if (status == 20) {
			isCancelApprovedHoliday = true;
			subject = "Holiday Cancel Request";
			String applied_date = applyHoliday.getAppliedOn().toString().substring(0, 10);
			message = "wants to Cancel the holiday applied on " + applied_date;
		}
		// Setting holiday Related informations in the email
		Integer holidayId = applyHoliday.getHolidayId();
		Holidays holiday = this.holidaysRepository.findOne(holidayId);
		String holidayName = holiday.getHoliday_name();
		Date appliedDate = applyHoliday.getAppliedOn();// Applied date
		Date reviewdDate = applyHoliday.getReviewedDate();// Approved date
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);
		Integer year = dateTime.getYear();// Current year
		/*
		 * Holiday availability for the year
		 */
		HolidayAvailability holidayAvailability = holidayAvailabilityRepository.getUserHoliDayAvailability(empId, year);
		Integer holidaysRemaining = holidayAvailability.getHolidays_remaining().intValueExact();// Remaining days
		Integer holidaysTaken = this.applyHolidayRepository.holidaysTaken(empId, year);// Holidays taken for the year
		// Packaging values into the email;
		Map<String, Object> emailModel = new HashMap();
		emailModel.put("supevisorName", supervisorName);
		emailModel.put("empName", empName);
		emailModel.put("holidayName", holidayName);
		emailModel.put("holidaysRemaining", holidaysRemaining);
		emailModel.put("holidaysTaken", holidaysTaken);
		emailModel.put("empId", empId);
		emailModel.put("verticalName", verticalName);
		emailModel.put("designation", designation);
		emailModel.put("gender", gender);
		emailModel.put("emailFrom", emailFrom);
		emailModel.put("subject", subject);
		emailModel.put("appliedDate", appliedDate);
		emailModel.put("email", supervisorEmail);
		emailModel.put("supevisor_name", supervisorName);
		emailModel.put("isNew", isNew);
		emailModel.put("isCancelWithoutApproval", isCancelWithoutApproval);
		emailModel.put("isCancelApprovedHoliday", isCancelApprovedHoliday);
		emailModel.put("message", message);
		emailModel.put("jobLocation", jobLocation);
		Integer approvalStatus = 1;
		Integer rejectStatus = 2;
		String holidayApprovalUrl = this.holidayApproveUrl + applyHoliday.getId() + "/" + empId + "/" + status + "/"
				+ approvalStatus;
		String holidayRejectUrl = this.holidayApproveUrl + applyHoliday.getId() + "/" + empId + "/" + status + "/"
				+ rejectStatus;
		emailModel.put("holidayApprovalUrl", holidayApprovalUrl);
		emailModel.put("holidayRejectUrl", holidayRejectUrl);
		if (isNew) {
			// Mail should be sent to supervisor
			try {
				this.emailService.sendHolidayRequestEmail(emailModel);
			} catch (MessagingException e) {
				logger.error("Error occured while sending email to supervisor", e);
			}
			// after sending first email
			if (addtionalSupervisorEmail != null && addtionalSupervisorName != null) {
				emailModel.remove("email");
				emailModel.remove("supevisor_name");
				emailModel.put("email", addtionalSupervisorEmail);
				emailModel.put("supevisor_name", addtionalSupervisorName);
				try {
					this.emailService.sendHolidayRequestEmail(emailModel);
				} catch (MessagingException e) {
					logger.error("Error occured while sending email to addtional supervisor", e);
				}
			}
		} else if (isCancelWithoutApproval) {
			// Mail should be sent to supervisor
			try {
				this.emailService.sendHolidayRequestEmail(emailModel);
			} catch (MessagingException e) {
				logger.error("Error occured while sending email to supervisor", e);
			}
			// after sending first email
			if (addtionalSupervisorEmail != null && addtionalSupervisorName != null) {
				emailModel.remove("email");
				emailModel.remove("supevisor_name");
				emailModel.put("email", addtionalSupervisorEmail);
				emailModel.put("supevisor_name", addtionalSupervisorName);
				try {
					this.emailService.sendHolidayRequestEmail(emailModel);
				} catch (MessagingException e) {
					logger.error("Error occured while sending email to addtional supervisor", e);
				}
			}

		} else if (isCancelApprovedHoliday) {
			emailModel.put("reviewDate", reviewdDate);
			// Mail should be sent to supervisor
			try {
				this.emailService.sendHolidayRequestEmail(emailModel);
			} catch (MessagingException e) {
				logger.error("Error occured while sending email to supervisor", e);
			}
			// after sending first email
			if (addtionalSupervisorEmail != null && addtionalSupervisorName != null) {
				emailModel.remove("email");
				emailModel.remove("supevisor_name");
				emailModel.put("email", addtionalSupervisorEmail);
				emailModel.put("supevisor_name", addtionalSupervisorName);
				try {
					this.emailService.sendHolidayRequestEmail(emailModel);
				} catch (MessagingException e) {
					logger.error("Error occured while sending email to supervisor", e);
				}
			}

		}
	}

	private void sendHolidayApproveEmail(ApplyHoliday applyHoliday, Integer approverId) {
		logger.debug("Sending email to employee about holiday Approval");
		try {
			Integer empId = applyHoliday.getEmp_id();
			EmpEmployment empEmployment = this.empEmploymentRepository.findOne(empId);
			EmpPersonal empPersonal = this.empPersonalRepository.findOne(empId);
			EmpEmployment supevisorEmployment = this.empEmploymentRepository.findOne(approverId);
			EmpPersonal supervisorPersonal = this.empPersonalRepository.findOne(approverId);
			StringBuilder supervisorNameBuilder = new StringBuilder();
			supervisorNameBuilder.append(supervisorPersonal.getEmp_fname()).append(" ")
					.append(supervisorPersonal.getEmp_mname()).append(" ").append(supervisorPersonal.getEmp_lname());
			String supevisorName = supervisorNameBuilder.toString();
			String emailFrom = supevisorEmployment.getEmp_email();
			String empName = empPersonal.getEmp_fname();
			String emailTo = empEmployment.getEmp_email();
			String requestType = null;
			Integer status = applyHoliday.getStatus();
			String currentStatus = null;
			if (status == 9 || status == 10) {
				requestType = "Request";
			} else if (status == 12 || status == 13) {
				requestType = "Cancel Request";
			} else if (status == 21 || status == 22) {
				requestType = "withdrawal Request";
			}
			Boolean isReasonExists = false;
			String reason = applyHoliday.getStatusNote();
			if (reason != null && reason != "approved") {
				isReasonExists = true;
			}
			String subject = null;
			if (status == 9) {
				subject = "Holiday Request Approved";
				currentStatus = AppConstants.APPROVED;
			} else if (status == 10) {
				subject = "Holiday Request Rejected";
				currentStatus = AppConstants.REJECTED;
			} else if (status == 12) {
				subject = "Holiday Cancel Request Approved";
				currentStatus = AppConstants.APPROVED;
			} else if (status == 13) {
				subject = "Holiday Cancel Request Rejected";
				currentStatus = AppConstants.REJECTED;
			} else if (status == 21) {
				subject = "Holiday Withdrawal Request Approved";
				currentStatus = AppConstants.APPROVED;
			} else if (status == 22) {
				subject = "Holiday Withdrawal Request Rejected";
				currentStatus = AppConstants.REJECTED;
			}
			Date appliedDate = applyHoliday.getAppliedOn();
			Holidays holiday = this.holidaysRepository.findOne(applyHoliday.getHolidayId());
			String holidayName = holiday.getHoliday_name();
			Map<String, Object> emailModel = new HashMap();
			emailModel.put("supevisor_name", supevisorName);
			emailModel.put("emp_name", empName);
			emailModel.put("holiday_name", holidayName);
			emailModel.put("subject", subject);
			emailModel.put("applied_on", appliedDate);
			emailModel.put(AppConstants.STATUS, currentStatus);
			emailModel.put("email", emailTo);
			emailModel.put("emailFrom", emailFrom);
			emailModel.put("reason", reason);
			emailModel.put("isReasonExists", isReasonExists);
			emailModel.put("requestType", requestType);
			this.emailService.sendHolidayApprovalEmail(emailModel);
		} catch (Exception e) {
			logger.error("Error occured while sending email", e);
		}
	}

	// Holiday Approval email

	// Leave request email
	private void sendApplyLeaveEmail(ApplyLeave applyLeave, Boolean isLeaveDelete, Boolean isCancelRequest) {
		logger.debug("Sending leave request email to supervisor");
		try {
			String supevisor_name;
			String emp_name;
			String leave_type;
			Double no_of_days;
			Date from_date;
			Date to_date;
			String reason;
			Double leaves_remaining;
			Double leaves_taken;
			Integer emp_id;
			String vertical_name;
			String designation;
			String gender;
			String emailFrom;
			String subject;
			String jobLocation;
			Date applied_on = applyLeave.getAppliedDate();
			LeaveAvailability leaveAvailability;
			EmpPersonal empPersonal = applyLeave.getEmpPersonal();
			emp_id = empPersonal.getId();
			EmpEmployment empEmployment = this.empEmploymentRepository.findOne(emp_id);
			emailFrom = empEmployment.getEmp_email();
			jobLocation = empEmployment.getLocations().getName();
			EmpPersonal supevisor = this.empEmploymentRepository.getSupervisor(emp_id);
			StringBuilder nameBuilder = new StringBuilder();
			nameBuilder.append(empPersonal.getEmp_fname()).append(" ").append(empPersonal.getEmp_mname()).append(" ")
					.append(empPersonal.getEmp_lname());
			emp_name = nameBuilder.toString();
			gender = empPersonal.getEmp_gender();
			supevisor_name = supevisor.getEmp_fname();
			Integer additionalSupevisorId = applyLeave.getAddSupId();
			if (applyLeave.getLeaveTypes() != null && applyLeave.getLeaveTypes().getId() != 6) {
				leave_type = applyLeave.getLeaveTypes().getLeave_type_name();
				DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
				DateTime dateTime = new DateTime().withZone(indianZone);
				leaveAvailability = leaveAvailabilityRepository.getLeaveAvailability(emp_id,
						applyLeave.getLeaveTypes().getId(), dateTime.getYear());
			} else {
				DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
				DateTime dateTime = new DateTime().withZone(indianZone);
				leave_type = applyLeave.getHalfDayTypeLeaveTypes().getLeave_type_name() + "( Half Day Request )";
				leaveAvailability = leaveAvailabilityRepository.getLeaveAvailability(emp_id,
						applyLeave.getHalfDayTypeLeaveTypes().getId(), dateTime.getYear());
			}
			designation = empEmployment.getDesignation().getDesignation();
			vertical_name = empEmployment.getVertical().getVertical();
			no_of_days = applyLeave.getNoOfDays().doubleValue();
			from_date = applyLeave.getFromDate();
			to_date = applyLeave.getToDate();
			Integer leaveRequestEmailStatus = 0;
			if (isLeaveDelete) {
				subject = "Leave cancelled";
				reason = null;
			} else if (isCancelRequest) {
				reason = applyLeave.getCancelReason();
				subject = "Leave cancel Request";
				leaveRequestEmailStatus = 11;
			} else {
				reason = applyLeave.getLeaveReason();
				subject = "Leave Request";
				leaveRequestEmailStatus = 2;
			}
			Integer emailApprovalStatus = 1;
			Integer emailRejectStatus = 2;
			leaves_remaining = leaveAvailability.getLeaves_remaining().doubleValue();
			leaves_taken = leaveAvailability.getLeaves_used().doubleValue();
			String leaveApproveUrl = this.leaveApproveUrl + applyLeave.getId() + "/" + emp_id + "/"
					+ leaveRequestEmailStatus + "/" + emailApprovalStatus;
			String leaveRejectUrl = this.leaveApproveUrl + applyLeave.getId() + "/" + emp_id + "/"
					+ leaveRequestEmailStatus + "/" + emailRejectStatus;
			Map<String, Object> emailModel = new HashMap<>();
			emailModel.put("supevisor_name", supevisor_name);
			emailModel.put("emp_name", emp_name);
			emailModel.put("leave_type", leave_type);
			emailModel.put("no_of_days", no_of_days);
			emailModel.put("from_date", from_date);
			emailModel.put("to_date", to_date);
			emailModel.put("reason", reason);
			emailModel.put("leaves_remaining", leaves_remaining);
			emailModel.put("leaves_taken", leaves_taken);
			emailModel.put("emp_id", emp_id);
			emailModel.put("vertical_name", vertical_name);
			emailModel.put("designation", designation);
			emailModel.put("gender", gender);
			emailModel.put("emailFrom", emailFrom);
			emailModel.put("subject", subject);
			emailModel.put("applied_on", applied_on);
			emailModel.put("leaveApproveUrl", leaveApproveUrl);
			emailModel.put("leaveRejectUrl", leaveRejectUrl);
			emailModel.put("jobLocation", jobLocation);

			if (isLeaveDelete) {
				EmpEmployment supervisorEmployment = this.empEmploymentRepository.findOne(supevisor.getId());
				emailModel.put("email", supervisorEmployment.getEmp_email());
				this.emailService.sendLeaveCancelEmail(emailModel);
				if (additionalSupevisorId != null && additionalSupevisorId != 0
						&& !additionalSupevisorId.equals(supevisor.getId())) {
					emailModel.remove("email");
					emailModel.remove("supevisor_name");
					EmpEmployment additionalSupervisorEmpEmployment = this.empEmploymentRepository
							.findOne(additionalSupevisorId);
					EmpPersonal addtionSupevisorPersonal = this.empPersonalRepository.findOne(additionalSupevisorId);
					emailModel.put("email", additionalSupervisorEmpEmployment.getEmp_email());
					emailModel.put("supevisor_name", addtionSupevisorPersonal.getEmp_fname());
					this.emailService.sendLeaveCancelEmail(emailModel);
				}

			} else if (isCancelRequest) {
				EmpEmployment supervisorEmployment = this.empEmploymentRepository.findOne(supevisor.getId());
				emailModel.put("email", supervisorEmployment.getEmp_email());
				this.emailService.sendLeaveCancelRequestEmail(emailModel);
				if (additionalSupevisorId != null && additionalSupevisorId != 0
						&& !additionalSupevisorId.equals(supevisor.getId())) {
					emailModel.remove("email");
					emailModel.remove("supevisor_name");
					EmpEmployment additionalSupervisorEmpEmployment = this.empEmploymentRepository
							.findOne(additionalSupevisorId);
					EmpPersonal addtionSupevisorPersonal = this.empPersonalRepository.findOne(additionalSupevisorId);
					emailModel.put("email", additionalSupervisorEmpEmployment.getEmp_email());
					emailModel.put("supevisor_name", addtionSupevisorPersonal.getEmp_fname());

					this.emailService.sendLeaveCancelRequestEmail(emailModel);
				}
			} else {
				EmpEmployment supervisorEmployment = this.empEmploymentRepository.findOne(supevisor.getId());
				emailModel.put("email", supervisorEmployment.getEmp_email());
				this.emailService.sendLeaveRequestEmail(emailModel);
				if (additionalSupevisorId != null && additionalSupevisorId != 0
						&& !additionalSupevisorId.equals(supevisor.getId())) {
					emailModel.remove("email");
					emailModel.remove("supevisor_name");
					EmpEmployment additionalSupervisorEmpEmployment = this.empEmploymentRepository
							.findOne(additionalSupevisorId);
					EmpPersonal addtionSupevisorPersonal = this.empPersonalRepository.findOne(additionalSupevisorId);
					emailModel.put("email", additionalSupervisorEmpEmployment.getEmp_email());
					emailModel.put("supevisor_name", addtionSupevisorPersonal.getEmp_fname());
					this.emailService.sendLeaveRequestEmail(emailModel);
				}
			}
		} catch (Exception e) {
			logger.error("Error occured during sending leave request email", e);
		}

	}

	// Leave approve email
	private void sendLeaveApproveEmail(ApplyLeave applyLeave, Boolean isLeaveCancel, Boolean isReject,
			Integer approverId) {
		logger.debug("Sending email for leave approval");
		try {
			Map<String, Object> emailModel = new HashMap();
			Boolean isReasonExists = false;
			String supevisor_name = null;
			String emp_name = null;
			String status = null;
			String leave_type = null;
			Double no_of_days = applyLeave.getNoOfDays().doubleValue();
			String subject = null;
			String emailFrom;
			String emailTo;
			String reason = null;
			Date applied_on = applyLeave.getAppliedDate();
			EmpPersonal empPersonal = applyLeave.getEmpPersonal();
			EmpEmployment empEmployment = this.empEmploymentRepository.findOne(empPersonal.getId());
			emailTo = empEmployment.getEmp_email();
			EmpPersonal superVisor = this.empPersonalRepository.findOne(approverId);
			EmpEmployment superViserEmployement = this.empEmploymentRepository.findOne(approverId);
			emailFrom = superViserEmployement.getEmp_email();
			StringBuilder stringBuilder = new StringBuilder();
			StringBuilder subjectBuilder = new StringBuilder();
			if (isLeaveCancel) {
				subjectBuilder.append("Leave Cancel request");
			} else {
				subjectBuilder.append("Leave request");
			}
			if (isReject) {
				status = AppConstants.REJECTED;
				subjectBuilder.append(" ").append(AppConstants.REJECTED);
				if (isLeaveCancel) {
					reason = applyLeave.getCancelApprovalNote();
				} else {
					reason = applyLeave.getSupApprovalNote();
					if (reason == null) {
						reason = applyLeave.getAddsupApprovalNote();
					}
				}

			} else {
				status = AppConstants.APPROVED;
				subjectBuilder.append(" ").append(AppConstants.APPROVED);
			}
			if (reason != null) {
				isReasonExists = true;
			}
			if (applyLeave.getLeaveTypes().getId() == 6) {
				leave_type = applyLeave.getHalfDayTypeLeaveTypes().getLeave_type_name() + " (Half day leave request)";
			} else {
				leave_type = applyLeave.getLeaveTypes().getLeave_type_name();
			}
			subject = subjectBuilder.toString();
			emp_name = empPersonal.getEmp_fname();
			stringBuilder.append(superVisor.getEmp_fname()).append(" ").append(superVisor.getEmp_mname()).append(" ")
					.append(superVisor.getEmp_lname());
			supevisor_name = stringBuilder.toString();
			emailModel.put("supevisor_name", supevisor_name);
			emailModel.put("emp_name", emp_name);
			emailModel.put("leave_type", leave_type);
			emailModel.put("no_of_days", no_of_days);
			emailModel.put("subject", subject);
			emailModel.put("applied_on", applied_on);
			emailModel.put(AppConstants.STATUS, status);
			emailModel.put("reason", reason);
			emailModel.put("isReasonExists", isReasonExists);
			emailModel.put("email", emailTo);
			emailModel.put("emailFrom", emailFrom);
			this.emailService.sendLeaveApprovalEmail(emailModel);
		} catch (Exception e) {
			logger.error("Error occured while sending leave approval email", e);
		}
	}

}
