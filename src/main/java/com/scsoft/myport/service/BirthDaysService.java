package com.scsoft.myport.service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Instant;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.scsoft.myport.domain.BirthdayWishes;
import com.scsoft.myport.domain.EmpEmployment;
import com.scsoft.myport.domain.EmpPersonal;
import com.scsoft.myport.domain.FcmDetail;
import com.scsoft.myport.error.InvalidRequestException;
import com.scsoft.myport.error.NotFoundException;
import com.scsoft.myport.error.OperationNotPossibleException;
import com.scsoft.myport.repository.BirthdayWishesRepository;
import com.scsoft.myport.repository.EmpEmploymentRepository;
import com.scsoft.myport.repository.EmpPersonalRepository;
import com.scsoft.myport.repository.FcmDetailRepository;
import com.scsoft.myport.rest.request.BirthDayWishRequest;
import com.scsoft.myport.service.DTO.BirthDaysDTO;
import com.scsoft.myport.service.DTO.GlobalPushDTO;
import com.scsoft.myport.service.comparator.BirthDayComparator;

@Service("birthDaysService")
public class BirthDaysService {
	private final Logger logger = LoggerFactory.getLogger(BirthDaysService.class);
	@Autowired
	private EmpEmploymentRepository empEmploymentRepository;
	@Autowired
	private EmpPersonalRepository empPersonalRepository;
	@Autowired
	private EmailService emailService;
	@Autowired
	private FcmDetailRepository fcmDetailRepository;
	@Autowired
	private BirthdayWishesRepository birthdayWishesRepository;
	@Autowired
	private FcmService fcmService;

	// retrieving the upcoming birthdays
	public List<BirthDaysDTO> getUpcomingBirthDays() {
		logger.debug("Fetching BirthDays from from DB");
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);
		Integer currentYear = dateTime.getYear();
		Integer start = dateTime.getMonthOfYear();
		Integer startDay = dateTime.getDayOfMonth();
		DateTime toTimeStamp = dateTime.plusDays(30);
		Integer toYear = toTimeStamp.getYear();
		Integer end = toTimeStamp.getMonthOfYear();
		Integer endDay = toTimeStamp.getDayOfMonth();
		List<BirthDaysDTO> birthDayList = empPersonalRepository.fetchBirthDays(start, end, startDay, endDay);
		birthDayList.stream().forEach(item -> {
			ZoneId zoneId = ZoneId.of("Asia/Kolkata");
			;
			ZonedDateTime instant = item.getDate().toInstant().atZone(zoneId);
			if (instant.getMonthValue() == start) {
				Integer monthOfYear = instant.getMonthValue();
				Integer dayOfMonth = instant.getDayOfMonth();
				DateTime birthDayDate = new DateTime(currentYear, monthOfYear, dayOfMonth, 0, 0);
				item.setDate(birthDayDate.toDate());
				item.setYear(birthDayDate.getYear());
			} else if (instant.getMonthValue() == end && currentYear != toYear) {
				Integer monthOfYear = instant.getMonthValue();
				Integer dayOfMonth = instant.getDayOfMonth();
				DateTime birthDayDate = new DateTime(toYear, monthOfYear, dayOfMonth, 0, 0);
				item.setDate(birthDayDate.toDate());
				item.setYear(birthDayDate.getYear());
			}
		});
		Collections.sort(birthDayList, new BirthDayComparator());
		return birthDayList;

	}

	// saving birthday wish
	public BirthdayWishes saveBirthdayWishes(BirthDayWishRequest birthDayWishRequest)
			throws NotFoundException, InvalidRequestException, OperationNotPossibleException {
		logger.debug("Saving BirthDay wish into DB");

		BirthdayWishes birthdayWishes = new BirthdayWishes();
		birthdayWishes.setBy_emp(birthDayWishRequest.getBy_emp());
		birthdayWishes.setFor_emp(birthDayWishRequest.getFor_emp());
		birthdayWishes.setWish(birthDayWishRequest.getWish());
		birthdayWishes.setApproved_by(0);
		birthdayWishes.setApproved_status(true);
		if (birthdayWishes.getBy_emp() == null || birthdayWishes.getBy_emp().equals(0)) {
			throw new NotFoundException("error", "Invalid ID");
		}
		if (birthdayWishes.getFor_emp() == null || birthdayWishes.getFor_emp().equals(0)) {
			throw new NotFoundException("error", "Invalid ID");
		}
		if (birthdayWishes.getWish() == null || birthdayWishes.getWish().isEmpty()) {
			throw new InvalidRequestException("error", "Message not Found");
		}

		EmpPersonal empPersonal = empPersonalRepository.findOne(birthDayWishRequest.getBy_emp());
		EmpEmployment wishByempEmployment = empEmploymentRepository.findOne(empPersonal.getId());
		EmpPersonal toEmp = empPersonalRepository.findOne(birthDayWishRequest.getFor_emp());
		EmpEmployment empEmployment = this.empEmploymentRepository.findOne(birthDayWishRequest.getFor_emp());
		if (wishByempEmployment == null || empPersonal == null || toEmp == null || empEmployment == null) {
			throw new NotFoundException("error", "User Not Found");
		}

		// Prevent multipile birthday wish by same employee
		Date dob = toEmp.getEmp_dob();
		DateTime birthDayDate = new DateTime(dob);
		int birthDayMonth = birthDayDate.getMonthOfYear();
		DateTime now = DateTime.now();
		int currentMonth = now.getMonthOfYear();
		if (birthDayMonth < currentMonth) {
			birthdayWishes.setBday_year(now.getYear() + 1);
		} else {
			birthdayWishes.setBday_year(now.getYear());
		}

		// Limiting the user to apply birthday only one time
		Boolean isBirthdayWishSentAlready = birthdayWishesRepository.isBirthdayWishSentAlready(
				birthDayWishRequest.getBy_emp(), birthDayWishRequest.getFor_emp(), birthdayWishes.getBday_year());
		if (isBirthdayWishSentAlready == true) {
			throw new OperationNotPossibleException("error", "Birthday Wish sent Already");
		}

		birthdayWishesRepository.saveAndFlush(birthdayWishes);

		if (empPersonal != null && wishByempEmployment != null) {
			String emialFrom = wishByempEmployment.getEmp_email();
			String avathar = empPersonal.getEmp_avatar();
			StringBuilder wishBy = new StringBuilder("");
			wishBy.append(empPersonal.getEmp_fname()).append(" ");
			if (empPersonal.getEmp_mname() != null) {
				wishBy.append(empPersonal.getEmp_mname()).append(" ");
			}
			wishBy.append(empPersonal.getEmp_lname());
			String byEmpName = wishBy.toString();
			if (toEmp != null && empEmployment != null) {
				// StringBuilder wishto= new StringBuilder("");
				// wishto.append(toEmp.getEmp_fname()).append(" ");
				// if(toEmp.getEmp_mname() != null) {
				// wishto.append(toEmp.getEmp_mname()).append(" ");
				// }
				// wishto.append(toEmp.getEmp_lname());
				// String toEmpName = wishto.toString();
				String toEmpName = toEmp.getEmp_fname();
				String email = empEmployment.getEmp_email();
				String message = birthDayWishRequest.getWish();
				// The Notification will send only on the Birthday date
				if (birthDayDate.toLocalDate().getDayOfYear() == new LocalDate().getDayOfYear()) {
					this.sendBirthDayWishEmail(byEmpName, toEmpName, message, email, emialFrom);
					this.sendPushNotification(message, byEmpName, empEmployment.getId(), avathar);
				}
			}
		}
		return birthdayWishes;

	}

	// @Async
	private void sendBirthDayWishEmail(String byEmpName, String toEmpName, String message, String email,
			String emailFrom) {
		try {
			logger.debug("Send birthday wish as email");
			String emp_name = toEmpName;
			String wish = message;
			String subject = byEmpName + " Wishes you Happy birthday";
			Map<String, Object> emailModel = new HashMap();
			emailModel.put("emp_name", emp_name);
			emailModel.put("wish", wish);
			emailModel.put("subject", subject);
			emailModel.put("email", email);
			emailModel.put("emailFrom", emailFrom);
			this.emailService.sendBirthdayWishEmail(emailModel);
		} catch (Exception e) {
			logger.error("Error Ocuured while sending email", e);
		}

	}

	// Send birthday wish push notification
	// @Async
	private void sendPushNotification(String message, String wishBy, Integer to, String avathar) {
		logger.debug("Send birthday wish as push notification");
		ObjectMapper mapper = new ObjectMapper();
		try {
			GlobalPushDTO globalPushDTO = new GlobalPushDTO();
			FcmDetail fcmDetail = this.fcmDetailRepository.findByEmpEmploymentIdAndIsAvailable(to, true);
			if (fcmDetail != null && wishBy != null && message != null) {
				String title = wishBy + " Wishes you Happy birthday";
				globalPushDTO.setBody(message);
				globalPushDTO.setTitle(title);
				globalPushDTO.setAvathar(avathar);
				this.fcmService.sendMessage(fcmDetail.getFcm_id(), null, mapper.writeValueAsString(globalPushDTO),
						"others");

			}
		} catch (Exception e) {
			logger.error("Error occured while sending push notification for birthday wish", e);
		}
	}

}
