package com.scsoft.myport.service;

import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.CharEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class EmailBaseService {
	private final Logger logger = LoggerFactory.getLogger(EmailBaseService.class);
	@Autowired
	private JavaMailSender javaMailSender;
	@Autowired
	private JavaMailSenderImpl javaMailSenderImpl;
	@Value("${myport.email.from}")
	private String emailFrom;
	@Value("${myport.email.employeeindia}")
	private String employeeindiaEmail;
	@Value("${myport.email.from.announcement}")
	private String announcementEmailFrom;
	@Value("${project.expense.email}")
	private String projectExpenseEmail;

	@Async("processExecutor")
	public void sendEmail(String to, String from, String subject, String content, boolean isMultipart, boolean isHtml) {
		logger.debug("Send e-mail[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}", isMultipart,
				isHtml, to, subject, content);
		try {
			MimeMessage mimeMessage = javaMailSenderImpl.createMimeMessage();
			MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, CharEncoding.UTF_8);
			if (to.contains(",")) {
				String[] mailToList = to.split(",");
				message.setTo(mailToList);
			} else {
				message.setTo(to);
				//message.setTo("jishnu.ms@scsoft.com");
			}
			message.setFrom(emailFrom, "Myport Communication");
			message.setReplyTo(from);
			message.setSubject(subject);
			message.setText(content, isHtml);
			javaMailSenderImpl.send(mimeMessage);
			logger.debug("Sent e-mail to User '{}'", to);
		} catch (Exception e) {
			logger.warn("E-mail could not be sent to user '{}', exception is: {}", to, e.getMessage());
		}
	}
	
	@Async("processExecutor")
	public void sendEmailBcc(String to,String subject, String content, boolean isMultipart, boolean isHtml) {
		logger.debug("Send e-mail[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}", isMultipart,
				isHtml, to, subject, content);
		try {
			MimeMessage mimeMessage = javaMailSenderImpl.createMimeMessage();
			MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, CharEncoding.UTF_8);
			if (to.contains(",")) {
				String[] mailToList = to.split(",");
				message.setTo(mailToList);
			} else {
				message.setTo(to);
				//message.setTo("jishnu.ms@scsoft.com");
			}
			message.setBcc(this.employeeindiaEmail);
			message.setFrom(announcementEmailFrom, "Employee Communication");
			message.setSubject(subject);
			message.setText(content, isHtml);
			javaMailSenderImpl.send(mimeMessage);
			logger.debug("Sent e-mail to User '{}'", to);
		} catch (Exception e) {
			logger.warn("E-mail could not be sent to user '{}', exception is: {}", to, e.getMessage());
		}
	}
}
