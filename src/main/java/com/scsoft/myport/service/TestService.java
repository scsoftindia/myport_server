package com.scsoft.myport.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.mail.MessagingException;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.scsoft.myport.domain.EmpEmployment;
import com.scsoft.myport.repository.EmpEmploymentRepository;
import com.scsoft.myport.repository.EmpPersonalRepository;

@Service
public class TestService {
	public static final String SAMPLE_XLSX_FILE_PATH = "Employees_Contact_Number_1.xlsx";
	@Autowired
	private EmailService emailService;
	@Autowired
	private EmpPersonalRepository empPersonalRepository;
	@Autowired
	private EmpEmploymentRepository empEmploymentRepository;

/*	public String emailTest() {
		Map<String, Object> emailModel = new HashMap();
		emailModel.put("email", "jishnu.ms@scsoft.com");
		emailModel.put("subject", "Test");
		emailModel.put("supervisorname", "Awad Test");
		emailModel.put("empname", "Jishnu M S ");
		emailModel.put("fromDate", new Date());
		emailModel.put("toDate", new Date());
		try {
			emailService.sendReportUploadMail(emailModel);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "succes";
	}*/

	/*@Transactional
	public Boolean copyPhoneNumbers() throws EncryptedDocumentException, InvalidFormatException, IOException {
		Resource resource = new ClassPathResource(SAMPLE_XLSX_FILE_PATH);
		File xlsxFile = resource.getFile();
		Workbook workbook = WorkbookFactory.create(xlsxFile);

		Sheet sheet = workbook.getSheetAt(0);
		DataFormatter dataFormatter = new DataFormatter();
		sheet.forEach(row -> {
			Iterator<Cell> cellIterator = row.cellIterator();
			while (cellIterator.hasNext()) {
				String cellValue = dataFormatter.formatCellValue(cellIterator.next());
				System.out.print(cellValue + "\t");
				String[] splited = cellValue.split(" ");
				
				
				String firstName = splited[0];
				String middleName="";
				
				if(splited.length==2)
				middleName=splited[1];
				String lastName=splited[2];
				EmpEmployment empEmployment = this.empEmploymentRepository.selectEmployeDetailByFirstName(firstName);
				if (empEmployment != null) {
					if (empEmployment.getIs_wfh() == null) {
						empEmployment.setIs_wfh("0");
					}
					if (empEmployment.getOfr_ltr_signed() != null && empEmployment.getOfr_ltr_signed().isEmpty()) {
						empEmployment.setOfr_ltr_signed(null);
					}
					if (empEmployment.getNda_signed() != null && empEmployment.getNda_signed().isEmpty()) {
						empEmployment.setNda_signed(null);
					}
					if (empEmployment.getDos_signed() != null && empEmployment.getDos_signed().isEmpty()) {
						empEmployment.setDos_signed(null);
					}

					String phoneNumber = dataFormatter.formatCellValue(cellIterator.next());
					empEmployment.setEmp_phone(phoneNumber);
					this.empEmploymentRepository.save(empEmployment);
				}
				
			}

		});
		return true;
	}*/

}
