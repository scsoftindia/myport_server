package com.scsoft.myport.service;


import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.scsoft.myport.domain.EmpEmployment;
import com.scsoft.myport.domain.EmpPersonal;
import com.scsoft.myport.domain.FcmDetail;
import com.scsoft.myport.error.NotFoundException;
import com.scsoft.myport.repository.ApplyHolidayRepository;
import com.scsoft.myport.repository.ApplyLeaveRepository;
import com.scsoft.myport.repository.EmpEmploymentRepository;
import com.scsoft.myport.repository.EmpPersonalRepository;
import com.scsoft.myport.repository.PaymentAdviceRepository;
import com.scsoft.myport.service.DTO.GlobalPushDTO;

@Service
public class SchedulerService {
	@Autowired
	private EmpEmploymentRepository empEmploymentRepository;
	@Autowired
	private ApplyLeaveRepository applyLeaveRepository;
	@Autowired
	private ApplyHolidayRepository applyHolidayRepository;
	@Autowired
	private PaymentAdviceRepository paymentAdviceRepository;
	@Autowired
	private EmpPersonalRepository empPersonalRepository;
	@Autowired
	private FcmService fcmService;
	@Autowired
	private PaymentLookupService paymentLookupService;
	private final Logger logger = LoggerFactory.getLogger(SchedulerService.class);

	@Scheduled(cron="0 00 9 ? * *",zone="Asia/Colombo")
	public void sendPendingTaskPushNotification() throws NotFoundException {
		logger.debug("Sending push notifications for pending tasks");
		// For Listing employees who are also responsible in the role as a supervisor.
		List<EmpPersonal> supervisorList = empPersonalRepository.getSupevisorList();
		ObjectMapper mapper = new ObjectMapper();
		for (EmpPersonal empPersonal : supervisorList) {
			Integer empId = empPersonal.getId();
			Boolean isPendingRequestsAvailable = false;
			EmpEmployment empEmployment = this.empEmploymentRepository.findOne(empId);
			if (empEmployment == null) {
				throw new NotFoundException("user", "Invalid user");
			}
			Boolean isSupervisor = this.empEmploymentRepository.isSupervisor(empId);
			String userRole = empEmployment.getUser_role();
			StringBuilder stringBuilder = new StringBuilder();
			if ("ADMIN".equals(userRole) || "MANAGER".equals(userRole) || (isSupervisor != null && isSupervisor == true)) {
				Integer leaveRequestCount = this.applyLeaveRepository.getLeaveRequestCount(empId);
				Integer holidayRequestCount = this.applyHolidayRepository.getHolidayRequestCount(empId);
				if ((leaveRequestCount != null && leaveRequestCount > 0)
						|| (holidayRequestCount != null && holidayRequestCount > 0)) {
					isPendingRequestsAvailable = true;
				}
				stringBuilder.append("Pending Leave Requests : ").append(leaveRequestCount).append("\n");
				stringBuilder.append("Pending Holiday Requests : ").append(holidayRequestCount).append("\n");
			}
			if ("ADMIN".equals(userRole)) {
				Integer paymentAdviceRequestCount = this.paymentAdviceRepository.getAdminApprovalCount();
				Integer forwadedCount = this.paymentAdviceRepository.getAdminForwardApprovalCount(empId);
				if (paymentAdviceRequestCount != null && forwadedCount != null
						&& (paymentAdviceRequestCount - forwadedCount) > 0) {
					isPendingRequestsAvailable = true;
				}
				stringBuilder.append("Pending Payment advice Requests : ")
						.append(paymentAdviceRequestCount - forwadedCount).append("\n");
			}
			if(isPendingRequestsAvailable) {
				FcmDetail fcmDetail = paymentLookupService.getEmployeeFcmDetail(empId);
				if (fcmDetail != null) {
					GlobalPushDTO globalPushDTO = new GlobalPushDTO();
					globalPushDTO.setTitle("Pending Requests");
					globalPushDTO.setBody(stringBuilder.toString());
					globalPushDTO.setAvathar(empPersonal.getEmp_avatar());
					try {
						fcmService.sendMessage(fcmDetail.getFcm_id(),null, mapper.writeValueAsString(globalPushDTO), "others");
					} catch (JsonProcessingException e) {
                        logger.error("Error occured while sending push notification",e);
					}
				}
			}
		}
	}

}
