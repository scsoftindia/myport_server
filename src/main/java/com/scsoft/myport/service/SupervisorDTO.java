package com.scsoft.myport.service;

public class SupervisorDTO {
	private String emp_fname;
	private String emp_mname;
	private String emp_lname;
	private Integer id;

	public String getEmp_fname() {
		return emp_fname;
	}

	public void setEmp_fname(String emp_fname) {
		this.emp_fname = emp_fname;
	}

	public String getEmp_mname() {
		return emp_mname;
	}

	public void setEmp_mname(String emp_mname) {
		this.emp_mname = emp_mname;
	}

	public String getEmp_lname() {
		return emp_lname;
	}

	public void setEmp_lname(String emp_lname) {
		this.emp_lname = emp_lname;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
