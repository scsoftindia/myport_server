package com.scsoft.myport.service;

import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.mail.MessagingException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.map.HashedMap;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.scsoft.myport.domain.ApprovalStatusType;
import com.scsoft.myport.domain.CurrencyType;
import com.scsoft.myport.domain.EmpEmployment;
import com.scsoft.myport.domain.EmpPersonal;
import com.scsoft.myport.domain.FcmDetail;
import com.scsoft.myport.domain.PayadviceCategories;
import com.scsoft.myport.domain.PayadviceSubCategories;
import com.scsoft.myport.domain.PaymentAdvice;
import com.scsoft.myport.domain.PaymentAdviceDocuments;
import com.scsoft.myport.domain.PaymentBank;
import com.scsoft.myport.domain.PaymentModes;
import com.scsoft.myport.domain.Vendors;
import com.scsoft.myport.error.ApprovalNotPossibleException;
import com.scsoft.myport.error.InvalidRequestException;
import com.scsoft.myport.error.NotFoundException;
import com.scsoft.myport.repository.ApprovalStatusTypeRepository;
import com.scsoft.myport.repository.CurrencyTypeRepository;
import com.scsoft.myport.repository.EmpEmploymentRepository;
import com.scsoft.myport.repository.EmpPersonalRepository;
import com.scsoft.myport.repository.FcmDetailRepository;
import com.scsoft.myport.repository.PayadviceCategoriesRepository;
import com.scsoft.myport.repository.PayadviceSubCategoriesRepository;
import com.scsoft.myport.repository.PaymentAdviceDocumentsRepository;
import com.scsoft.myport.repository.PaymentAdviceRepository;
import com.scsoft.myport.repository.PaymentBankRepository;
import com.scsoft.myport.repository.PaymentModesRepository;
import com.scsoft.myport.repository.VendorsRepository;
import com.scsoft.myport.rest.request.PayadviceCategoriesAproveRequest;
import com.scsoft.myport.rest.request.PayadviceCategoriesRequest;
import com.scsoft.myport.rest.request.PayadviceSubCategoriesRequest;
import com.scsoft.myport.rest.request.PaymentAdviceApproovalRequest;
import com.scsoft.myport.rest.request.PaymentAdviceRequest;
import com.scsoft.myport.rest.request.VendorsRequest;
import com.scsoft.myport.service.DTO.AprooverDTO;
import com.scsoft.myport.service.DTO.GlobalPushDTO;
import com.scsoft.myport.service.DTO.PaymentAdviceDTO;
import com.scsoft.myport.service.DTO.PaymentAdvicePushNotificationDTO;
import com.scsoft.myport.service.mapper.PayadviceCategoriesMapper;
import com.scsoft.myport.service.mapper.PayadviceSubCategoriesMapper;
import com.scsoft.myport.service.mapper.PaymentAdviceMapper;
import com.scsoft.myport.service.mapper.VendorsMapper;
import com.scsoft.myport.service.util.AppConstants;
import com.scsoft.myport.service.util.Encryptor;

@Service("payAdviceService")
public class PayAdviceService {
	@Autowired
	private PayadviceCategoriesRepository payadviceCategoriesRepository;
	@Autowired
	private PayadviceSubCategoriesRepository payadviceSubCategoriesRepository;
	@Autowired
	private VendorsRepository vendorsRepository;
	@Autowired
	private ApprovalStatusTypeRepository approvalStatusTypeRepository;
	@Autowired
	private EmpEmploymentRepository empEmploymentRepository;
	@Autowired
	private PayadviceCategoriesMapper payadviceCategoriesMapper;
	@Autowired
	private PayadviceSubCategoriesMapper payadviceSubCategoriesMapper;
	@Autowired
	private VendorsMapper vendorsMapper;
	@Autowired
	private PaymentAdviceMapper paymentAdviceMapper;
	@Autowired
	private PaymentAdviceRepository paymentAdviceRepository;
	@Autowired
	private PaymentModesRepository paymentModesRepository;
	@Autowired
	private CurrencyTypeRepository currencyTypeRepository;
	@Autowired
	private PaymentAdviceDocumentsRepository paymentAdviceDocumentsRepository;
	@Autowired
	private EmpPersonalRepository empPersonalRepository;
	@Autowired
	private PaymentBankRepository paymentBankRepository;
	@Autowired
	private PaymentLookupService paymentLookupService;
	@Value("${advice.file.path}")
	private String adviceFilePath;
	@Value("${advice.approve-email.url}")
	private String adviceApproveUrl;
	@Autowired
	private EmailService emailService;
	@Autowired
	private FcmService fcmService;

	private final Logger logger = LoggerFactory.getLogger(PayAdviceService.class);

	public PayadviceCategories savePayadviceCategories(PayadviceCategoriesRequest payadviceCategoriesRequest) {
		logger.debug("Saving PayadviceCategories into DB ");
		Boolean isNew = false;
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);
		PayadviceCategories payadviceCategories = payadviceCategoriesMapper
				.payadviceCategoriesRequestToPayadviceCategories(payadviceCategoriesRequest);
		EmpEmployment empEmployment = empEmploymentRepository.findOne(payadviceCategoriesRequest.getEmp_id());
		if (payadviceCategories.getId() == null) {
			isNew = true;
			payadviceCategories.setCreatedBy(empEmployment);
			payadviceCategories.setCreatedDate(dateTime.toDate());
			payadviceCategories.setIs_approved(false);
			payadviceCategories.setIs_rejected(false);
		} else {
			PayadviceCategories payadviceCategoriesOld = payadviceCategoriesRepository
					.findOne(payadviceCategories.getId());
			payadviceCategories.setCreatedBy(payadviceCategoriesOld.getCreatedBy());
			payadviceCategories.setCreatedDate(payadviceCategoriesOld.getCreatedDate());
			payadviceCategories.setIs_approved(payadviceCategoriesOld.getIs_approved());
			payadviceCategories.setIs_rejected(payadviceCategoriesOld.getIs_rejected());
		}
		payadviceCategories.setLastModifiedBy(empEmployment);
		payadviceCategories.setLastModifiedDate(dateTime.toDate());
		payadviceCategories = payadviceCategoriesRepository.saveAndFlush(payadviceCategories);
		if (payadviceCategories.getId() != null && isNew) {
			this.sendCategoryCreatedEmail(payadviceCategories, empEmployment, isNew);
		}
		return payadviceCategories;
	}

	public PayadviceCategories savePayadviceCategoriesStatus(
			PayadviceCategoriesAproveRequest payadviceCategoriesAproveRequest) {
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);
		logger.debug("Saving PayadviceCategories into DB by aprooval Status");
		PayadviceCategories payadviceCategories = payadviceCategoriesRepository
				.findOne(payadviceCategoriesAproveRequest.getId());
		ApprovalStatusType approvalStatusType = null;
		if (payadviceCategoriesAproveRequest.getStatus().toString().equalsIgnoreCase("APPROVE")) {
			payadviceCategories.setIs_approved(true);
			payadviceCategories.setIs_rejected(false);
		} else if (payadviceCategoriesAproveRequest.getStatus().toString().equalsIgnoreCase("REJECT")) {
			payadviceCategories.setIs_approved(false);
			payadviceCategories.setIs_rejected(true);
		}
		EmpEmployment empEmployment = empEmploymentRepository.findOne(payadviceCategoriesAproveRequest.getEmpId());
		EmpPersonal empPersonal = empPersonalRepository.findOne(empEmployment.getId());
		EmpPersonal requester = empPersonalRepository.findOne(payadviceCategories.getCreatedBy().getId());
		EmpEmployment requesterEmployment = empEmploymentRepository.findOne(payadviceCategories.getCreatedBy().getId());
		payadviceCategories.setLastModifiedBy(empEmployment);
		payadviceCategories.setLastModifiedDate(dateTime.toDate());
		payadviceCategories = payadviceCategoriesRepository.saveAndFlush(payadviceCategories);
		if (payadviceCategories.getId() != null) {
			String subject;
			String body;
			String emailFrom = empEmployment.getEmp_email();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(empPersonal.getEmp_fname()).append(" ").append(empPersonal.getEmp_mname()).append(" ")
					.append(empPersonal.getEmp_lname());
			String approverName = stringBuilder.toString();
			String empName = requester.getEmp_fname();
			String emailTo = requesterEmployment.getEmp_email();
			if ("APPROVE".equals(payadviceCategoriesAproveRequest.getStatus().toString())) {
				subject = "Expense Approved - " + payadviceCategories.getCat_name();
				body = "Expense named " + payadviceCategories.getCat_name() + " has been approved. Please verify.";

			} else {
				subject = "Expense Rejected - " + payadviceCategories.getCat_name();
				body = "Expense named " + payadviceCategories.getCat_name() + " has been rejected. Please verify.";
			}
			this.sendCategoryApproveEmail(subject, body, empName, approverName, emailFrom, emailTo);
		}
		return payadviceCategories;
	}

	public PayadviceCategories savePayadviceCategoriesStatusByEmail(Integer empId, Integer id, String status)
			throws NotFoundException, InvalidRequestException, ApprovalNotPossibleException {
		if (id == null || id == 0) {
			throw new NotFoundException("error", "Invalid ID");
		}
		if (empId == null || empId == 0) {
			throw new NotFoundException("error", "Invalid User");
		}
		PayadviceCategories payadviceCategories = this.payadviceCategoriesRepository.findOne(id);
		EmpEmployment empEmployment = empEmploymentRepository.getEmployeeById(empId);
		if (payadviceCategories == null) {
			throw new NotFoundException("error", "Invalid ID");
		}
		if (empEmployment == null) {
			throw new NotFoundException("error", "Invalid User");
		}
		if (!"APPROVE".equalsIgnoreCase(status) && !"REJECT".equalsIgnoreCase(status)) {
			throw new InvalidRequestException("error", "The status provided is not valid");
		}
		if (payadviceCategories.getIs_approved() || payadviceCategories.getIs_rejected()) {
			String currentStatus = null;
			if (payadviceCategories.getIs_approved()) {
				currentStatus = "Approved";
			} else {
				currentStatus = "Rejected";
			}
			throw new ApprovalNotPossibleException("category", "The Expense is already handled", AppConstants.SUCCES,
					currentStatus);
		}
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);
		if ("APPROVE".equalsIgnoreCase(status)) {
			payadviceCategories.setIs_approved(true);
			payadviceCategories.setIs_rejected(false);
		} else if ("REJECT".equalsIgnoreCase(status)) {
			payadviceCategories.setIs_approved(false);
			payadviceCategories.setIs_rejected(true);
		}
		EmpPersonal empPersonal = empEmployment.getEmpPersonal();
		EmpPersonal requester = empPersonalRepository.findOne(payadviceCategories.getCreatedBy().getId());
		EmpEmployment requesterEmployment = empEmploymentRepository.findOne(payadviceCategories.getCreatedBy().getId());
		payadviceCategories.setLastModifiedBy(empEmployment);
		payadviceCategories.setLastModifiedDate(dateTime.toDate());
		payadviceCategories = payadviceCategoriesRepository.saveAndFlush(payadviceCategories);
		if (payadviceCategories.getId() != null) {
			String subject;
			String body;
			String emailFrom = empEmployment.getEmp_email();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(empPersonal.getEmp_fname()).append(" ").append(empPersonal.getEmp_mname()).append(" ")
					.append(empPersonal.getEmp_lname());
			String approverName = stringBuilder.toString();
			String empName = requester.getEmp_fname();
			String emailTo = requesterEmployment.getEmp_email();
			if ("APPROVE".equalsIgnoreCase(status)) {
				subject = "Expense Approved - " + payadviceCategories.getCat_name();
				body = "Expense named " + payadviceCategories.getCat_name() + " has been approved. Please verify.";

			} else {
				subject = "Expense Rejected - " + payadviceCategories.getCat_name();
				body = "Expense named " + payadviceCategories.getCat_name() + " has been rejected. Please verify.";
			}
			this.sendCategoryApproveEmail(subject, body, empName, approverName, emailFrom, emailTo);
		}
		return payadviceCategories;
	}

	public PayadviceSubCategories savePayadviceSubCategories(
			PayadviceSubCategoriesRequest payadviceSubCategoriesRequest) {
		logger.debug("Saving PayadviceSubCategories into DB ");
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);
		Boolean isNew = false;
		PayadviceSubCategories payadviceSubCategories = payadviceSubCategoriesMapper
				.payadviceSubCategoriesRequestToPayadviceSubCategories(payadviceSubCategoriesRequest);
		payadviceSubCategories.setPayadviceCategories(
				payadviceCategoriesRepository.findOne(payadviceSubCategoriesRequest.getPay_advice_cat_id()));
		EmpEmployment empEmployment = empEmploymentRepository.findOne(payadviceSubCategoriesRequest.getEmp_id());
		if (payadviceSubCategories.getId() == null) {
			isNew = true;
			payadviceSubCategories.setCreatedBy(empEmployment);
			payadviceSubCategories.setCreatedDate(dateTime.toDate());
			payadviceSubCategories.setIs_approved(false);
			payadviceSubCategories.setIs_rejected(false);
		} else {
			PayadviceSubCategories payadviceSubCategoriesOld = payadviceSubCategoriesRepository
					.findOne(payadviceSubCategoriesRequest.getId());
			payadviceSubCategories.setCreatedBy(payadviceSubCategoriesOld.getCreatedBy());
			payadviceSubCategories.setCreatedDate(payadviceSubCategoriesOld.getCreatedDate());
			payadviceSubCategories.setIs_approved(payadviceSubCategoriesOld.getIs_approved());
			payadviceSubCategories.setIs_rejected(payadviceSubCategoriesOld.getIs_rejected());
		}
		payadviceSubCategories.setLastModifiedBy(empEmployment);
		payadviceSubCategories.setLastModifiedDate(dateTime.toDate());
		payadviceSubCategories = payadviceSubCategoriesRepository.saveAndFlush(payadviceSubCategories);

		if (payadviceSubCategories.getId() != null && isNew) {
			this.subCategoryCreationEmail(payadviceSubCategories);
		}
		return payadviceSubCategories;
	}

	public PayadviceSubCategories savePayadviceSubCategoriesStatus(
			PayadviceCategoriesAproveRequest payadviceCategoriesAproveRequest) {
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);
		logger.debug("Saving PayadviceSubCategories into DB by aprooval Status");
		PayadviceSubCategories payadviceSubCategories = payadviceSubCategoriesRepository
				.findOne(payadviceCategoriesAproveRequest.getId());
		if (payadviceCategoriesAproveRequest.getStatus().toString().equalsIgnoreCase("APPROVE")) {
			payadviceSubCategories.setIs_approved(true);
			payadviceSubCategories.setIs_rejected(false);
		} else if (payadviceCategoriesAproveRequest.getStatus().toString().equalsIgnoreCase("REJECT")) {
			payadviceSubCategories.setIs_approved(false);
			payadviceSubCategories.setIs_rejected(true);
		}

		EmpEmployment empEmployment = empEmploymentRepository.findOne(payadviceCategoriesAproveRequest.getEmpId());
		EmpPersonal empPersonal = empPersonalRepository.findOne(empEmployment.getId());
		EmpPersonal requester = empPersonalRepository.findOne(payadviceSubCategories.getCreatedBy().getId());
		EmpEmployment requesterEmployment = empEmploymentRepository
				.findOne(payadviceSubCategories.getCreatedBy().getId());

		payadviceSubCategories.setLastModifiedBy(empEmployment);
		payadviceSubCategories.setLastModifiedDate(dateTime.toDate());
		payadviceSubCategories = payadviceSubCategoriesRepository.saveAndFlush(payadviceSubCategories);
		String subject;
		String body;
		String emailFrom = empEmployment.getEmp_email();
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(empPersonal.getEmp_fname()).append(" ").append(empPersonal.getEmp_mname()).append(" ")
				.append(empPersonal.getEmp_lname());
		String approverName = stringBuilder.toString();
		String empName = requester.getEmp_fname();
		String emailTo = requesterEmployment.getEmp_email();
		if ("APPROVE".equals(payadviceCategoriesAproveRequest.getStatus().toString())) {
			subject = "Expense Type Approved - " + payadviceSubCategories.getPay_subcategory_name();
			body = "Expense Type named " + payadviceSubCategories.getPay_subcategory_name()
					+ " has been approved. Please verify.";

		} else {
			subject = "Expense Type Rejected - " + payadviceSubCategories.getPay_subcategory_name();
			body = "Expense Type named " + payadviceSubCategories.getPay_subcategory_name()
					+ " has been rejected. Please verify.";
		}
		this.subCategoryApproveEmail(subject, body, empName, approverName, emailFrom, emailTo);
		return payadviceSubCategories;
	}

	public PayadviceSubCategories savePayadviceSubCategoriesStatusByEmail(Integer empId, Integer id, String status)
			throws NotFoundException, InvalidRequestException, ApprovalNotPossibleException {
		if (id == null || id == 0) {
			throw new NotFoundException("error", "Invalid ID");
		}
		if (empId == null || empId == 0) {
			throw new NotFoundException("error", "Invalid User");
		}
		PayadviceSubCategories payadviceSubCategories = this.payadviceSubCategoriesRepository.findOne(id);
		EmpEmployment empEmployment = empEmploymentRepository.getEmployeeById(empId);
		if (payadviceSubCategories == null) {
			throw new NotFoundException("error", "Invalid ID");
		}
		if (empEmployment == null) {
			throw new NotFoundException("error", "Invalid User");
		}
		if (!"APPROVE".equalsIgnoreCase(status) && !"REJECT".equalsIgnoreCase(status)) {
			throw new InvalidRequestException("error", "The status provided is not valid");
		}
		if (payadviceSubCategories.getIs_approved() || payadviceSubCategories.getIs_rejected()) {
			String currentStatus = null;
			if (payadviceSubCategories.getIs_approved()) {
				currentStatus = "Approved";
			} else {
				currentStatus = "Rejected";
			}
			throw new ApprovalNotPossibleException("subCategory", "The Expense type is already handled",
					AppConstants.SUCCES, currentStatus);
		}
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);
		logger.debug("Saving PayadviceSubCategories into DB by aprooval Status");

		if ("APPROVE".equalsIgnoreCase(status)) {
			payadviceSubCategories.setIs_approved(true);
			payadviceSubCategories.setIs_rejected(false);
		} else if ("REJECT".equalsIgnoreCase(status)) {
			payadviceSubCategories.setIs_approved(false);
			payadviceSubCategories.setIs_rejected(true);
		}
		payadviceSubCategories.setLastModifiedBy(empEmployment);
		payadviceSubCategories.setLastModifiedDate(dateTime.toDate());
		EmpPersonal empPersonal = empEmployment.getEmpPersonal();
		EmpPersonal requester = empPersonalRepository.findOne(payadviceSubCategories.getCreatedBy().getId());
		EmpEmployment requesterEmployment = empEmploymentRepository
				.findOne(payadviceSubCategories.getCreatedBy().getId());

		payadviceSubCategories.setLastModifiedBy(empEmployment);
		payadviceSubCategories.setLastModifiedDate(dateTime.toDate());
		payadviceSubCategories = payadviceSubCategoriesRepository.saveAndFlush(payadviceSubCategories);
		String subject;
		String body;
		String emailFrom = empEmployment.getEmp_email();
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(empPersonal.getEmp_fname()).append(" ").append(empPersonal.getEmp_mname()).append(" ")
				.append(empPersonal.getEmp_lname());
		String approverName = stringBuilder.toString();
		String empName = requester.getEmp_fname();
		String emailTo = requesterEmployment.getEmp_email();
		if ("APPROVE".equalsIgnoreCase(status)) {
			subject = "Expense Type Approved - " + payadviceSubCategories.getPay_subcategory_name();
			body = "Expense Type named " + payadviceSubCategories.getPay_subcategory_name()
					+ " has been approved. Please verify.";

		} else {
			subject = "Expense Type Rejected - " + payadviceSubCategories.getPay_subcategory_name();
			body = "Expense Type named " + payadviceSubCategories.getPay_subcategory_name()
					+ " has been rejected. Please verify.";
		}
		this.subCategoryApproveEmail(subject, body, empName, approverName, emailFrom, emailTo);
		return payadviceSubCategories;
	}

	public Vendors saveVendors(VendorsRequest vendorsRequest) {
		logger.debug("Saving Vendors Detail into the DB");
		Boolean isNew = false;
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);
		Vendors vendors = vendorsMapper.VendorsRequestToVendors(vendorsRequest);
		ApprovalStatusType approvalStatusType = approvalStatusTypeRepository.findByName("waiting");
		vendors.setPayadviceCategories(payadviceCategoriesRepository.findOne(vendorsRequest.getCat_id()));
		vendors.setPayadviceSubCategories(payadviceSubCategoriesRepository.findOne(vendorsRequest.getSub_cat_id()));

		EmpEmployment empEmployment = empEmploymentRepository.findOne(vendorsRequest.getEmp_id());
		if (vendors.getId() == null) {
			vendors.setCreatedBy(empEmployment);
			vendors.setCreatedDate(dateTime.toDate());
			isNew = true;
			vendors.setStatus(false);
			vendors.setIs_rejected(false);
		} else {
			Vendors vendorsOld = vendorsRepository.findOne(vendorsRequest.getId());
			vendors.setCreatedBy(vendorsOld.getCreatedBy());
			vendors.setCreatedDate(vendorsOld.getCreatedDate());
			vendors.setStatus(vendorsOld.isStatus());
			vendors.setIs_rejected(vendorsOld.getIs_rejected());
		}
		vendors.setLastModifiedBy(empEmployment);
		vendors.setLastModifiedDate(dateTime.toDate());
		vendorsRepository.saveAndFlush(vendors);
		if (vendors.getId() != null && isNew) {
			this.vendorCreationEmail(vendors);
		}
		return vendors;
	}

	public Vendors saveVendorsStatus(PayadviceCategoriesAproveRequest payadviceCategoriesAproveRequest) {
		logger.debug("Saving PayadviceSubCategories into DB by aprooval Status");
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);
		Vendors vendors = vendorsRepository.findOne(payadviceCategoriesAproveRequest.getId());
		ApprovalStatusType approvalStatusType = null;
		if (payadviceCategoriesAproveRequest.getStatus().toString().equalsIgnoreCase("APPROVE")) {
			vendors.setStatus(true);
			vendors.setIs_rejected(false);
		} else if (payadviceCategoriesAproveRequest.getStatus().toString().equalsIgnoreCase("REJECT")) {
			vendors.setStatus(false);
			vendors.setIs_rejected(true);
		}
		EmpEmployment empEmployment = empEmploymentRepository.findOne(payadviceCategoriesAproveRequest.getEmpId());
		vendors.setLastModifiedBy(empEmployment);
		vendors.setLastModifiedDate(dateTime.toDate());
		vendorsRepository.saveAndFlush(vendors);
		EmpPersonal empPersonal = empPersonalRepository.findOne(empEmployment.getId());
		EmpPersonal requester = empPersonalRepository.findOne(vendors.getCreatedBy().getId());
		EmpEmployment requesterEmployment = empEmploymentRepository.findOne(vendors.getCreatedBy().getId());
		String subject;
		String body;
		String emailFrom = empEmployment.getEmp_email();
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(empPersonal.getEmp_fname()).append(" ").append(empPersonal.getEmp_mname()).append(" ")
				.append(empPersonal.getEmp_lname());
		String approverName = stringBuilder.toString();
		String empName = requester.getEmp_fname();
		String emailTo = requesterEmployment.getEmp_email();
		if ("APPROVE".equals(payadviceCategoriesAproveRequest.getStatus().toString())) {
			subject = "Vendor Approved - " + vendors.getVendor_name();
			body = "Vendor named " + vendors.getVendor_name() + " has been approved. Please verify.";

		} else {
			subject = "Vendor Rejected - " + vendors.getVendor_name();
			body = "Vendor named " + vendors.getVendor_name() + " has been rejected. Please verify.";
		}
		this.vendorApprovalEmail(subject, body, empName, approverName, emailFrom, emailTo);
		return vendors;
	}

	public Vendors saveVendorsStatusByEmail(Integer empId, Integer id, String status)
			throws ApprovalNotPossibleException, InvalidRequestException, NotFoundException {
		if (id == null || id == 0) {
			throw new NotFoundException("error", "Invalid ID");
		}
		if (empId == null || empId == 0) {
			throw new NotFoundException("error", "Invalid User");
		}
		Vendors vendors = vendorsRepository.findOne(id);
		EmpEmployment empEmployment = empEmploymentRepository.getEmployeeById(empId);
		if (vendors == null) {
			throw new NotFoundException("error", "Invalid ID");
		}
		if (empEmployment == null) {
			throw new NotFoundException("error", "Invalid User");
		}
		if (!"APPROVE".equalsIgnoreCase(status) && !"REJECT".equalsIgnoreCase(status)) {
			throw new InvalidRequestException("error", "The status provided is not valid");
		}
		if (vendors.isStatus() || vendors.getIs_rejected()) {
			String currentStatus = null;
			if (vendors.isStatus()) {
				currentStatus = "Approved";
			} else {
				currentStatus = "Rejected";
			}
			throw new ApprovalNotPossibleException("vendor", "The vendor is already handled", AppConstants.SUCCES,
					currentStatus);
		}
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);

		if ("APPROVE".equalsIgnoreCase(status)) {
			vendors.setStatus(true);
			vendors.setIs_rejected(false);
		} else if ("REJECT".equalsIgnoreCase(status)) {
			vendors.setStatus(false);
			vendors.setIs_rejected(true);
		}
		vendors.setLastModifiedBy(empEmployment);
		vendors.setLastModifiedDate(dateTime.toDate());
		vendorsRepository.saveAndFlush(vendors);
		EmpPersonal empPersonal = empEmployment.getEmpPersonal();
		EmpPersonal requester = empPersonalRepository.findOne(vendors.getCreatedBy().getId());
		EmpEmployment requesterEmployment = empEmploymentRepository.findOne(vendors.getCreatedBy().getId());
		String subject;
		String body;
		String emailFrom = empEmployment.getEmp_email();
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(empPersonal.getEmp_fname()).append(" ").append(empPersonal.getEmp_mname()).append(" ")
				.append(empPersonal.getEmp_lname());
		String approverName = stringBuilder.toString();
		String empName = requester.getEmp_fname();
		String emailTo = requesterEmployment.getEmp_email();
		if ("APPROVE".equalsIgnoreCase(status)) {
			subject = "Vendor Approved - " + vendors.getVendor_name();
			body = "Vendor named " + vendors.getVendor_name() + " has been approved. Please verify.";

		} else {
			subject = "Vendor Rejected - " + vendors.getVendor_name();
			body = "Vendor named " + vendors.getVendor_name() + " has been rejected. Please verify.";
		}
		this.vendorApprovalEmail(subject, body, empName, approverName, emailFrom, emailTo);
		return vendors;

	}

	public Boolean savePaymentAdviceList(List<PaymentAdviceRequest> paymentAdviceRequests, MultipartFile[] files,
			Integer empId) throws IllegalStateException, IOException {
		logger.debug("Saving Payment advice details into the server");
		Boolean result = true;
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);
		EmpEmployment empEmployment = empEmploymentRepository.getEmployeeById(empId);
		ApprovalStatusType approvalStatusType = approvalStatusTypeRepository.findByName("waiting");
		for (PaymentAdviceRequest paymentAdviceRequest : paymentAdviceRequests) {
			PaymentAdvice paymentAdvice = this.paymentAdviceMapper
					.paymentAdviceRequestToPaymentAdvice(paymentAdviceRequest);
			PayadviceCategories payadviceCategories = payadviceCategoriesRepository
					.findOne(paymentAdviceRequest.getCategoryId());
			PayadviceSubCategories payadviceSubCategories = payadviceSubCategoriesRepository
					.findOne(paymentAdviceRequest.getSubCategoryId());
			Vendors vendors = vendorsRepository.findOne(paymentAdviceRequest.getVendorId());
			PaymentModes paymentModes = paymentModesRepository.findOne(paymentAdviceRequest.getPaymentModeId());
			CurrencyType currencyType = currencyTypeRepository.findOne(paymentAdviceRequest.getCurrencyTypeId());
			if (paymentAdviceRequest.getPaymentBankId() != null && paymentAdviceRequest.getPaymentBankId() != 0) {
				PaymentBank paymentBank = this.paymentBankRepository.findOne(paymentAdviceRequest.getPaymentBankId());
				paymentAdvice.setPaymentBank(paymentBank);
			}
			if ("PART".equals(paymentAdviceRequest.getPaymentRemark())) {
				paymentAdvice.setPaymentRemark(PaymentAdvice.PaymentRemark.PART);
			} else {
				paymentAdvice.setPaymentRemark(PaymentAdvice.PaymentRemark.FULL);
			}
			paymentAdvice.setPayadviceCategories(payadviceCategories);
			paymentAdvice.setPayadviceSubCategories(payadviceSubCategories);
			paymentAdvice.setVendors(vendors);
			paymentAdvice.setPaymentModes(paymentModes);
			paymentAdvice.setCurrencyType(currencyType);
			EmpEmployment primaryAproover;
			EmpEmployment secondaryAproover;
			if (paymentAdviceRequest.getPrimaryAprooverId() == 10002) {
				primaryAproover = empEmploymentRepository.getEmployeeById(10002);
				secondaryAproover = empEmploymentRepository.getEmployeeById(10001);
			} else {
				primaryAproover = empEmploymentRepository.getEmployeeById(10001);
				secondaryAproover = empEmploymentRepository.getEmployeeById(10002);
			}

			String primaryAproverName = primaryAproover.getEmpPersonal().getEmp_fname() + " "
					+ primaryAproover.getEmpPersonal().getEmp_mname() + " "
					+ primaryAproover.getEmpPersonal().getEmp_lname();
			paymentAdvice.setPrimary_aproover_emp_name(primaryAproverName);
			paymentAdvice.setPrimary_aproover_emp_Id(primaryAproover.getId());
			String secondaryApprooverName = secondaryAproover.getEmpPersonal().getEmp_fname() + " "
					+ secondaryAproover.getEmpPersonal().getEmp_mname() + " "
					+ secondaryAproover.getEmpPersonal().getEmp_lname();
			paymentAdvice.setSecondary_aproover_emp_id(secondaryAproover.getId());
			paymentAdvice.setSecondary_aproover_name(secondaryApprooverName);
			if (paymentAdvice.getId() == null) {
				paymentAdvice.setCreatedDate(dateTime);
				paymentAdvice.setCreatedBy(empEmployment);
				String aplliedEmpName = empEmployment.getEmpPersonal().getEmp_fname() + " "
						+ empEmployment.getEmpPersonal().getEmp_mname() + " "
						+ empEmployment.getEmpPersonal().getEmp_lname();
				paymentAdvice.setApplied_emp_name(aplliedEmpName);
				paymentAdvice.setApplied_emp_Id(empEmployment.getId());
				paymentAdvice.setAprrovalStatus(approvalStatusType);
				paymentAdvice.setIs_admin_rejected(false);
				paymentAdvice.setIs_finance_rejected(false);
				paymentAdvice.setApplied_on(dateTime);
			}
			paymentAdvice.setLast_modified_date(dateTime);
			paymentAdvice.setLastModifiedBy(empEmployment);

			if (paymentAdvice.getBill_date() != null) {
				DateTime correctDate = new DateTime(paymentAdvice.getBill_date()).withZone(indianZone);
				paymentAdvice.setBill_date(correctDate.toDate());
			}
			if (paymentAdvice.getDue_date() != null) {
				DateTime correctDate = new DateTime(paymentAdvice.getDue_date()).withZone(indianZone);
				paymentAdvice.setDue_date(correctDate.toDate());
			}
			if (paymentAdvice.getBill_period_from() != null) {
				DateTime correctDate = new DateTime(paymentAdvice.getBill_period_from()).withZone(indianZone);
				paymentAdvice.setBill_period_from(correctDate.toDate());
			}
			if (paymentAdvice.getBill_peroid_to() != null) {
				DateTime correctDate = new DateTime(paymentAdvice.getBill_peroid_to()).withZone(indianZone);
				paymentAdvice.setBill_peroid_to(correctDate.toDate());
			}
			if (paymentAdvice.getPayment_cheque_date() != null) {
				DateTime correctDate = new DateTime(paymentAdvice.getPayment_cheque_date()).withZone(indianZone);
				paymentAdvice.setPayment_cheque_date(correctDate.toDate());
			}
			if (paymentAdvice.getPayment_online_date() != null) {
				DateTime correctDate = new DateTime(paymentAdvice.getPayment_online_date()).withZone(indianZone);
				paymentAdvice.setPayment_online_date(correctDate.toDate());
			}

			paymentAdviceRepository.saveAndFlush(paymentAdvice);
			paymentAdviceRequest.setId(paymentAdvice.getId());
			if (paymentAdvice.getId() == null) {
				result = false;
			}
			sendPaymentAdviceEmail(paymentAdvice, true);
			sendPaymentAdvicePushNotification(paymentAdvice);

		}
		if (CollectionUtils.isNotEmpty(Arrays.asList(files))) {
			for (MultipartFile file : files) {

				File convFile = new File(file.getOriginalFilename());
				String fileName = convFile.getName();
				Character indexValue = fileName.charAt(0);
				PaymentAdviceRequest paymentAdviceRequest = paymentAdviceRequests.stream()
						.filter(item -> item.getIndex() == Integer.parseInt(indexValue.toString())).findAny().get();
				if (paymentAdviceRequest != null) {
					PaymentAdvice paymentAdvice = paymentAdviceRepository.findOne(paymentAdviceRequest.getId());
					PaymentAdviceDocuments paymentAdviceDocuments = new PaymentAdviceDocuments();
					paymentAdviceDocuments.setPaymentAdvice(paymentAdvice);
					Long time = new Date().getTime();
					String storeFileName = empId + "-advice-" + time;
					String[] nameArray = file.getOriginalFilename().split("\\.");
					storeFileName = storeFileName + "." + file.getOriginalFilename().split("\\.")[nameArray.length - 1];
					file.transferTo(new File(adviceFilePath + "/" + storeFileName));
					paymentAdviceDocuments.setDocumentpath(storeFileName);
					paymentAdviceDocuments.setAvailableStatus(true);
					paymentAdviceDocumentsRepository.saveAndFlush(paymentAdviceDocuments);

				}

			}
		}
		return result;
	}

	public Page<PaymentAdviceDTO> getAllPaymentAdvices(Pageable pageable, Boolean isApproval, String approvalType,
			Integer categoryId, Integer SubCategoryId, Integer vendorId, Integer year, Integer month,
			String checkNumber) {
		logger.debug("Request to fetch all Payment advices");
		Page<PaymentAdvice> paymentAdvices;
		if (!isApproval) {
			paymentAdvices = paymentAdviceRepository.getPaymentAdviceList(pageable, categoryId, SubCategoryId, vendorId,
					year, month, checkNumber);
		} else {
			if ("finance".equals(approvalType)) {
				paymentAdvices = paymentAdviceRepository.getPaymentAdviceListForFinanceApproval(pageable, categoryId,
						SubCategoryId, vendorId, year, month, checkNumber);
			} else {
				paymentAdvices = paymentAdviceRepository.getPaymentAdviceListForAdminApproval(pageable, categoryId,
						SubCategoryId, vendorId, year, month, checkNumber);

			}

		}
		return paymentAdvices.map(item -> this.paymentAdviceMapper.paymentAdviceToPaymentAdviceDTO(item));

	}

	public PaymentAdvice updatePaymentAdvice(PaymentAdviceRequest paymentAdviceRequest, Integer empId) {
		logger.debug("Updating paymentAdvice Details Into the DB");
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);
		EmpEmployment empEmployment = empEmploymentRepository.getEmployeeById(empId);
		PaymentAdvice paymentAdvice = this.paymentAdviceRepository.findOne(paymentAdviceRequest.getId());
		paymentAdvice = this.paymentAdviceMapper
				.paymentAdviceRequestAndPaymentAdviceToPaymentAdvice(paymentAdviceRequest, paymentAdvice);
		PayadviceCategories payadviceCategories = payadviceCategoriesRepository
				.findOne(paymentAdviceRequest.getPayadviceCategories().getId());
		PayadviceSubCategories payadviceSubCategories = payadviceSubCategoriesRepository
				.findOne(paymentAdviceRequest.getPayadviceSubCategories().getId());
		Vendors vendors = vendorsRepository.findOne(paymentAdviceRequest.getVendors().getId());
		PaymentModes paymentModes = paymentModesRepository.findOne(paymentAdviceRequest.getPaymentModes().getId());
		CurrencyType currencyType = currencyTypeRepository.findOne(paymentAdviceRequest.getCurrencyType().getId());
		if (paymentAdviceRequest.getPaymentBank() != null && paymentAdviceRequest.getPaymentBank().getId() != 0) {
			PaymentBank paymentBank = this.paymentBankRepository.findOne(paymentAdviceRequest.getPaymentBank().getId());
			paymentAdvice.setPaymentBank(paymentBank);
		}
		if ("PART".equals(paymentAdviceRequest.getPaymentRemark())) {
			paymentAdvice.setPaymentRemark(PaymentAdvice.PaymentRemark.PART);
		} else {
			paymentAdvice.setPaymentRemark(PaymentAdvice.PaymentRemark.FULL);
		}
		if (paymentAdvice.getBill_date() != null) {
			DateTime correctDate = new DateTime(paymentAdvice.getBill_date()).withZone(indianZone);
			paymentAdvice.setBill_date(correctDate.toDate());
		}
		if (paymentAdvice.getDue_date() != null) {
			DateTime correctDate = new DateTime(paymentAdvice.getDue_date()).withZone(indianZone);
			paymentAdvice.setDue_date(correctDate.toDate());
		}
		if (paymentAdvice.getBill_period_from() != null) {
			DateTime correctDate = new DateTime(paymentAdvice.getBill_period_from()).withZone(indianZone);
			paymentAdvice.setBill_period_from(correctDate.toDate());
		}
		if (paymentAdvice.getBill_peroid_to() != null) {
			DateTime correctDate = new DateTime(paymentAdvice.getBill_peroid_to()).withZone(indianZone);
			paymentAdvice.setBill_peroid_to(correctDate.toDate());
		}
		if (paymentAdvice.getPayment_cheque_date() != null) {
			DateTime correctDate = new DateTime(paymentAdvice.getPayment_cheque_date()).withZone(indianZone);
			paymentAdvice.setBill_peroid_to(correctDate.toDate());
		}
		if (paymentAdvice.getPayment_online_date() != null) {
			DateTime correctDate = new DateTime(paymentAdvice.getPayment_online_date()).withZone(indianZone);
			paymentAdvice.setBill_peroid_to(correctDate.toDate());
		}
		paymentAdvice.setPayadviceCategories(payadviceCategories);
		paymentAdvice.setPayadviceSubCategories(payadviceSubCategories);
		paymentAdvice.setVendors(vendors);
		paymentAdvice.setPaymentModes(paymentModes);
		paymentAdvice.setCurrencyType(currencyType);
		paymentAdvice.setLast_modified_date(dateTime);
		paymentAdvice.setLastModifiedBy(empEmployment);
		
		
        if(paymentAdviceRequest.getIsUpdateProcess() == false) {
		EmpEmployment primaryAproover;
		EmpEmployment secondaryAproover;
		if (paymentAdviceRequest.getPrimaryAprooverId() == 10002) {
			primaryAproover = empEmploymentRepository.getEmployeeById(10002);
			secondaryAproover = empEmploymentRepository.getEmployeeById(10001);
		} else {
			primaryAproover = empEmploymentRepository.getEmployeeById(10001);
			secondaryAproover = empEmploymentRepository.getEmployeeById(10002);
		}

		String primaryAproverName = primaryAproover.getEmpPersonal().getEmp_fname() + " "
				+ primaryAproover.getEmpPersonal().getEmp_mname() + " "
				+ primaryAproover.getEmpPersonal().getEmp_lname();
		paymentAdvice.setPrimary_aproover_emp_name(primaryAproverName);
		paymentAdvice.setPrimary_aproover_emp_Id(primaryAproover.getId());
		String secondaryApprooverName = secondaryAproover.getEmpPersonal().getEmp_fname() + " "
				+ secondaryAproover.getEmpPersonal().getEmp_mname() + " "
				+ secondaryAproover.getEmpPersonal().getEmp_lname();
		paymentAdvice.setSecondary_aproover_emp_id(secondaryAproover.getId());
		paymentAdvice.setSecondary_aproover_name(secondaryApprooverName);

		if (paymentAdviceRequest.getIsUpdateByFinance()) {
			ApprovalStatusType approvalStatusType;
			String empName = empEmployment.getEmpPersonal().getEmp_fname() + " "
					+ empEmployment.getEmpPersonal().getEmp_mname() + " "
					+ empEmployment.getEmpPersonal().getEmp_lname();
			if (paymentAdviceRequest.getAprrovalStatus()
					.equals(PaymentAdviceApproovalRequest.AprrovalStatus.PROCESSED_BY_FINANCE)) {
				approvalStatusType = approvalStatusTypeRepository.findOne(4);
				paymentAdvice.setPayment_release_emp_name(empName);
				paymentAdvice.setPayment_release_emp_Id(empEmployment.getId());
				paymentAdvice.setPayment_release_time(dateTime);
				paymentAdvice.setRelease_note(paymentAdviceRequest.getReleaseNote());
				paymentAdvice.setIs_admin_rejected(false);
				paymentAdvice.setIs_finance_rejected(false);
			} else {
				approvalStatusType = approvalStatusTypeRepository.findOne(5);
				paymentAdvice.setReject_emp_name(empName);
				paymentAdvice.setReject_emp_Id(empEmployment.getId());
				paymentAdvice.setRelease_reject_note(paymentAdviceRequest.getReleaseNote());
				paymentAdvice.setPayment_release_reject_time(dateTime);
				paymentAdvice.setIs_admin_rejected(false);
				paymentAdvice.setIs_finance_rejected(true);
			}
			paymentAdvice.setAprrovalStatus(approvalStatusType);
		}
        }
		
		paymentAdviceRepository.saveAndFlush(paymentAdvice);
		if (paymentAdviceRequest.getIsUpdateProcess() == false && paymentAdviceRequest.getIsUpdateByFinance()) {
			this.sendPaymentAdviceApprooveEmail(paymentAdvice);
			this.sendPaymentAdvicePushNotification(paymentAdvice);
		}
		return paymentAdvice;

	}

	public Boolean isAdviceExists(Integer id) {
		logger.debug("Checking whether the advice exists");
		return this.paymentAdviceRepository.isExists(id);
	}

	public PaymentAdvice updatePaymentAdviceStatus(PaymentAdviceApproovalRequest paymentAdviceApproovalRequest)
			throws ApprovalNotPossibleException {
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);
		PaymentAdvice paymentAdvice = paymentAdviceRepository.findOne(paymentAdviceApproovalRequest.getAdviceId());
		EmpEmployment empEmployment = empEmploymentRepository.getEmployeeById(paymentAdviceApproovalRequest.getEmpId());
		String empName = empEmployment.getEmpPersonal().getEmp_fname() + " "
				+ empEmployment.getEmpPersonal().getEmp_mname() + " " + empEmployment.getEmpPersonal().getEmp_lname();
		PaymentAdviceApproovalRequest.AprrovalStatus status = paymentAdviceApproovalRequest.getAprrovalStatus();
		ApprovalStatusType approvalStatusType = null;
		Boolean IsNotApplicable = this.isNotApprovable(paymentAdvice, paymentAdviceApproovalRequest.getEmpId(),
				paymentAdviceApproovalRequest.getAprrovalStatus());
		Boolean isForwardRequest = false;
		if (!IsNotApplicable) {
			if (status == PaymentAdviceApproovalRequest.AprrovalStatus.APRROVED) {
				approvalStatusType = approvalStatusTypeRepository.findOne(2);
				paymentAdvice.setAprooved_emp_name(empName);
				paymentAdvice.setAprooved_emp_Id(empEmployment.getId());
				paymentAdvice.setApproval_comment(paymentAdviceApproovalRequest.getAprrovalNote());
				paymentAdvice.setApproved_date(dateTime);
				paymentAdvice.setIs_admin_rejected(false);
				paymentAdvice.setIs_finance_rejected(false);
			} else if (status == PaymentAdviceApproovalRequest.AprrovalStatus.REJECT) {
				approvalStatusType = approvalStatusTypeRepository.findOne(3);
				paymentAdvice.setReject_emp_name(empName);
				paymentAdvice.setReject_emp_Id(empEmployment.getId());
				paymentAdvice.setReject_date(dateTime);
				paymentAdvice.setReject_reson(paymentAdviceApproovalRequest.getRejectReason());
				paymentAdvice.setIs_admin_rejected(true);
				paymentAdvice.setIs_finance_rejected(false);
			} else if (status == PaymentAdviceApproovalRequest.AprrovalStatus.PROCESSED_BY_FINANCE) {
				approvalStatusType = approvalStatusTypeRepository.findOne(4);
				paymentAdvice.setPayment_release_emp_name(empName);
				paymentAdvice.setPayment_release_emp_Id(empEmployment.getId());
				paymentAdvice.setPayment_release_time(dateTime);
				paymentAdvice.setRelease_note(paymentAdviceApproovalRequest.getReleaseNote());
				paymentAdvice.setIs_admin_rejected(false);
				paymentAdvice.setIs_finance_rejected(false);
			} else if (status == PaymentAdviceApproovalRequest.AprrovalStatus.REJECTED_BY_FINANCE) {
				approvalStatusType = approvalStatusTypeRepository.findOne(5);
				paymentAdvice.setReject_emp_name(empName);
				paymentAdvice.setReject_emp_Id(empEmployment.getId());
				paymentAdvice.setRelease_reject_note(paymentAdviceApproovalRequest.getReleaseRejectNote());
				paymentAdvice.setPayment_release_reject_time(dateTime);
				paymentAdvice.setIs_admin_rejected(false);
				paymentAdvice.setIs_finance_rejected(true);
			} else if (status == PaymentAdviceApproovalRequest.AprrovalStatus.APPRROVE_AND_FORWARD) {
				isForwardRequest = true;
				approvalStatusType = approvalStatusTypeRepository.findOne(6);
				paymentAdvice.setAprooved_emp_name(empName);
				paymentAdvice.setAprooved_emp_Id(empEmployment.getId());
				paymentAdvice.setApproval_comment(paymentAdviceApproovalRequest.getAprrovalNote());
				paymentAdvice.setApproved_date(dateTime);
				paymentAdvice.setIs_admin_rejected(false);
				paymentAdvice.setIs_finance_rejected(false);
				// Secondary Approver email
			} else if (status == PaymentAdviceApproovalRequest.AprrovalStatus.FORWARD_REJECT) {
				approvalStatusType = approvalStatusTypeRepository.findOne(7);
				paymentAdvice.setIs_admin_rejected(false);
				paymentAdvice.setIs_finance_rejected(false);
				paymentAdvice.setIs_forward_rejected(true);
				paymentAdvice.setReject_emp_name(empName);
				paymentAdvice.setReject_emp_Id(empEmployment.getId());
				paymentAdvice.setReject_date(dateTime);
				paymentAdvice.setReject_reson(paymentAdviceApproovalRequest.getRejectReason());
				// email
			} else if (status == PaymentAdviceApproovalRequest.AprrovalStatus.FORWARD_APRROVED) {
				approvalStatusType = approvalStatusTypeRepository.findOne(8);
				paymentAdvice.setIs_admin_rejected(false);
				paymentAdvice.setIs_finance_rejected(false);
				paymentAdvice.setIs_forward_rejected(false);
				paymentAdvice.setForward_approve_comment(paymentAdviceApproovalRequest.getForward_approve_comment());
				// email
			}
			paymentAdvice.setAprrovalStatus(approvalStatusType);
			paymentAdvice.setLast_modified_date(dateTime);
			paymentAdvice.setLastModifiedBy(empEmployment);
			paymentAdviceRepository.saveAndFlush(paymentAdvice);
			if (isForwardRequest) {
				this.sendPaymentAdviceEmail(paymentAdvice, false);
			} else {
				this.sendPaymentAdviceApprooveEmail(paymentAdvice);
			}

			this.sendPaymentAdvicePushNotification(paymentAdvice);
			return paymentAdvice;
		} else {
			Integer aprovalStatusTypeId = paymentAdvice.getAprrovalStatus().getId();
			String currentStatus = null;
			if (aprovalStatusTypeId == 2) {
				currentStatus = AppConstants.APPROVED;
			} else if (aprovalStatusTypeId == 3) {
				currentStatus = "Rejected";
			} else if (aprovalStatusTypeId == 6) {
				currentStatus = "Forwarded";
			}
			throw new ApprovalNotPossibleException("paymentAdvice", "Operation already handled by another person",
					AppConstants.SUCCES, currentStatus);
		}
	}

	public PaymentAdviceDocuments getDocument(Integer id) {
		logger.debug("request to fetch document form DB");
		return paymentAdviceDocumentsRepository.findOne(id);

	}

	public List<Integer> getAdviceYears() {
		logger.debug("Retrieving years from DB");
		return this.paymentAdviceRepository.getYearList();
	}
   
	@Transactional(readOnly=true)
	public List<String> getCheckNumberList() {
		logger.debug("Retrieving check number list from DB");
		return this.paymentAdviceRepository.checkNumberList();
	}

	public PaymentAdviceDTO getPaymentAdvice(Integer id) {
		logger.debug("Retrieving payment advice by id");
		PaymentAdvice paymentAdvice = this.paymentAdviceRepository.findOne(id);
		PaymentAdviceDTO paymentAdviceDTO = this.paymentAdviceMapper.paymentAdviceToPaymentAdviceDTO(paymentAdvice);
		EmpPersonal primaryApprover = empPersonalRepository.findOne(paymentAdvice.getPrimary_aproover_emp_Id());
		AprooverDTO aprooverDTO = new AprooverDTO();
		aprooverDTO.setId(primaryApprover.getId());
		aprooverDTO.setFname(primaryApprover.getEmp_fname());
		aprooverDTO.setLname(primaryApprover.getEmp_mname());
		aprooverDTO.setLname(primaryApprover.getEmp_lname());
		paymentAdviceDTO.setAprooverDTO(aprooverDTO);
		return paymentAdviceDTO;

	}

	public PaymentAdvice updatePaymentAdviceStatusById(Integer id, String status, Integer empId)
			throws NotFoundException, InvalidRequestException, ApprovalNotPossibleException {
		if (id == null || id == 0) {
			throw new NotFoundException("error", "Invalid ID");
		}
		Boolean isValidStatus = this.isValidStatus(status);
		PaymentAdviceApproovalRequest.AprrovalStatus aprrovalStatus = this.getStatus(status);
		if (!isValidStatus || aprrovalStatus == null) {
			throw new InvalidRequestException("error", "The status provided is not valid");
		}

		PaymentAdvice paymentAdvice = this.paymentAdviceRepository.findOne(id);
		EmpEmployment empEmployment = empEmploymentRepository.getEmployeeById(empId);
		if (paymentAdvice == null) {
			throw new NotFoundException("error", "Invalid ID");
		}
		if (empEmployment == null) {
			throw new NotFoundException("error", "Invalid User");
		}
		Boolean IsNotApplicable = this.isNotApprovableByEmail(paymentAdvice, empId, aprrovalStatus);
		if (IsNotApplicable) {
			Integer aprovalStatusTypeId = paymentAdvice.getAprrovalStatus().getId();
			String currentStatus = null;
			if (aprovalStatusTypeId == 2) {
				currentStatus = AppConstants.APPROVED;
			} else if (aprovalStatusTypeId == 3) {
				currentStatus = "Rejected";
			} else if (aprovalStatusTypeId == 4) {
				currentStatus = "Processed by Finance";
			} else if (aprovalStatusTypeId == 5) {
				currentStatus = "Rejected by Finance";
			} else if (aprovalStatusTypeId == 6) {
				currentStatus = "Forwarded";
			} else if (aprovalStatusTypeId == 7) {
				currentStatus = "Forward Rejected";
			} else if (aprovalStatusTypeId == 8) {
				currentStatus = "Forward Approved";
			}
			throw new ApprovalNotPossibleException("paymentAdvice", "Operation already handled by another person",
					AppConstants.SUCCES, currentStatus);
		}
		Boolean isForwardRequest = false;
		String empName = empEmployment.getEmpPersonal().getEmp_fname() + " "
				+ empEmployment.getEmpPersonal().getEmp_mname() + " " + empEmployment.getEmpPersonal().getEmp_lname();

		ApprovalStatusType approvalStatusType = null;
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);
		if (aprrovalStatus.equals(PaymentAdviceApproovalRequest.AprrovalStatus.APRROVED)) {
			approvalStatusType = approvalStatusTypeRepository.findOne(2);
			paymentAdvice.setAprooved_emp_name(empName);
			paymentAdvice.setAprooved_emp_Id(empEmployment.getId());
			paymentAdvice.setApproval_comment(null);
			paymentAdvice.setApproved_date(dateTime);
			paymentAdvice.setIs_admin_rejected(false);
			paymentAdvice.setIs_finance_rejected(false);
		} else if (aprrovalStatus.equals(PaymentAdviceApproovalRequest.AprrovalStatus.REJECT)) {
			approvalStatusType = approvalStatusTypeRepository.findOne(3);
			paymentAdvice.setReject_emp_name(empName);
			paymentAdvice.setReject_emp_Id(empEmployment.getId());
			paymentAdvice.setReject_date(dateTime);
			paymentAdvice.setReject_reson(null);
			paymentAdvice.setIs_admin_rejected(true);
			paymentAdvice.setIs_finance_rejected(false);
		} else if (aprrovalStatus.equals(PaymentAdviceApproovalRequest.AprrovalStatus.PROCESSED_BY_FINANCE)) {
			approvalStatusType = approvalStatusTypeRepository.findOne(4);
			paymentAdvice.setPayment_release_emp_name(empName);
			paymentAdvice.setPayment_release_emp_Id(empEmployment.getId());
			paymentAdvice.setPayment_release_time(dateTime);
			paymentAdvice.setRelease_note(null);
			paymentAdvice.setIs_admin_rejected(false);
			paymentAdvice.setIs_finance_rejected(false);
		} else if (aprrovalStatus.equals(PaymentAdviceApproovalRequest.AprrovalStatus.REJECTED_BY_FINANCE)) {
			approvalStatusType = approvalStatusTypeRepository.findOne(5);
			paymentAdvice.setReject_emp_name(empName);
			paymentAdvice.setReject_emp_Id(empEmployment.getId());
			paymentAdvice.setRelease_reject_note(null);
			paymentAdvice.setPayment_release_reject_time(dateTime);
			paymentAdvice.setIs_admin_rejected(false);
			paymentAdvice.setIs_finance_rejected(true);
		} else if (aprrovalStatus.equals(PaymentAdviceApproovalRequest.AprrovalStatus.APPRROVE_AND_FORWARD)) {
			isForwardRequest = true;
			approvalStatusType = approvalStatusTypeRepository.findOne(6);
			paymentAdvice.setAprooved_emp_name(empName);
			paymentAdvice.setAprooved_emp_Id(empEmployment.getId());
			paymentAdvice.setApproval_comment(null);
			paymentAdvice.setApproved_date(dateTime);
			paymentAdvice.setIs_admin_rejected(false);
			paymentAdvice.setIs_finance_rejected(false);
			// Secondary Approver email
		} else if (aprrovalStatus.equals(PaymentAdviceApproovalRequest.AprrovalStatus.FORWARD_REJECT)) {
			approvalStatusType = approvalStatusTypeRepository.findOne(7);
			paymentAdvice.setIs_admin_rejected(false);
			paymentAdvice.setIs_finance_rejected(false);
			paymentAdvice.setIs_forward_rejected(true);
			paymentAdvice.setReject_emp_name(empName);
			paymentAdvice.setReject_emp_Id(empEmployment.getId());
			paymentAdvice.setReject_date(dateTime);
			paymentAdvice.setReject_reson(null);
			// email
		} else if (aprrovalStatus.equals(PaymentAdviceApproovalRequest.AprrovalStatus.FORWARD_APRROVED)) {
			approvalStatusType = approvalStatusTypeRepository.findOne(8);
			paymentAdvice.setIs_admin_rejected(false);
			paymentAdvice.setIs_finance_rejected(false);
			paymentAdvice.setIs_forward_rejected(false);
			paymentAdvice.setForward_approve_comment(null);
			// email
		}
		paymentAdvice.setAprrovalStatus(approvalStatusType);
		paymentAdvice.setLast_modified_date(dateTime);
		paymentAdvice.setLastModifiedBy(empEmployment);
		paymentAdviceRepository.saveAndFlush(paymentAdvice);
		if (isForwardRequest) {
			this.sendPaymentAdviceEmail(paymentAdvice, false);
		} else {
			this.sendPaymentAdviceApprooveEmail(paymentAdvice);
		}

		this.sendPaymentAdvicePushNotification(paymentAdvice);

		return paymentAdvice;
	}

	private Boolean isValidStatus(String status) {
		Boolean isValidStatus = false;

		switch (status) {
		case "APRROVED":
			isValidStatus = true;
			break;
		case "REJECT":
			isValidStatus = true;
			break;
		case "PROCESSED_BY_FINANCE":
			isValidStatus = true;
			break;
		case "REJECTED_BY_FINANCE":
			isValidStatus = true;
			break;
		case "APPRROVE_AND_FORWARD":
			isValidStatus = true;
			break;
		case "FORWARD_REJECT":
			isValidStatus = true;
			break;
		case "FORWARD_APRROVED":
			isValidStatus = true;
			break;
		default:
			isValidStatus = true;
			break;
		}
		return isValidStatus;
	}

	private PaymentAdviceApproovalRequest.AprrovalStatus getStatus(String status) {
		PaymentAdviceApproovalRequest.AprrovalStatus aprrovalStatus = null;
		switch (status) {
		case "APRROVED":
			aprrovalStatus = PaymentAdviceApproovalRequest.AprrovalStatus.APRROVED;
			break;
		case "REJECT":
			aprrovalStatus = PaymentAdviceApproovalRequest.AprrovalStatus.REJECT;
			break;
		case "PROCESSED_BY_FINANCE":
			aprrovalStatus = PaymentAdviceApproovalRequest.AprrovalStatus.PROCESSED_BY_FINANCE;
			break;
		case "REJECTED_BY_FINANCE":
			aprrovalStatus = PaymentAdviceApproovalRequest.AprrovalStatus.REJECTED_BY_FINANCE;
			break;
		case "APPRROVE_AND_FORWARD":
			aprrovalStatus = PaymentAdviceApproovalRequest.AprrovalStatus.APPRROVE_AND_FORWARD;
			break;
		case "FORWARD_REJECT":
			aprrovalStatus = PaymentAdviceApproovalRequest.AprrovalStatus.FORWARD_REJECT;
			break;
		case "FORWARD_APRROVED":
			aprrovalStatus = PaymentAdviceApproovalRequest.AprrovalStatus.FORWARD_APRROVED;
			break;
		default:
			aprrovalStatus = null;
			break;
		}
		return aprrovalStatus;
	}

	private void sendCategoryCreatedEmail(PayadviceCategories payadviceCategories, EmpEmployment empEmployment,
			Boolean isNew) {
		try {
			logger.debug("Sending email for new category");
			if (payadviceCategories != null && empEmployment != null) {
				String subject = null;
				String body = null;
				String emailFrom = null;
				String emailTo = null;
				String empName = null;
				Integer categoryId = payadviceCategories.getId();
				Integer empId = empEmployment.getId();
				if (payadviceCategories.getId() != null && isNew) {
					subject = "New Expense Created - " + payadviceCategories.getCat_name();
					body = "An Expense named " + payadviceCategories.getCat_name()
							+ " has been created and waiting for your approoval";
					emailFrom = empEmployment.getEmp_email();
					EmpEmployment financeLead = this.empEmploymentRepository.getFinanceLead();
					EmpPersonal financeLeadPersonal = this.empPersonalRepository.findOne(financeLead.getId());
					emailTo = financeLead.getEmp_email();
					empName = financeLeadPersonal.getEmp_fname();
				}
				Map<String, Object> emailModel = new HashedMap();
				emailModel.put("subject", subject);
				emailModel.put("body", body);
				emailModel.put("emailFrom", emailFrom);
				emailModel.put("email", emailTo);
				emailModel.put("emp_name", empName);
				emailModel.put("approval_status", Encryptor.encode("APPROVE"));
				emailModel.put("reject_status", Encryptor.encode("REJECT"));
				emailModel.put("id", Encryptor.encode(String.valueOf(categoryId)));
				emailModel.put("empId", Encryptor.encode(String.valueOf(empId)));
				emailModel.put("categoryApproveUrl", this.adviceApproveUrl);

				this.emailService.sendNewCategoryEmail(emailModel);
			}
		} catch (Exception e) {
			logger.error("Error occured while sending email", e);
		}
	}

	// @Async
	private void sendCategoryApproveEmail(String subject, String body, String empName, String approver_name,
			String emailFrom, String emailTo) {
		try {
			logger.debug("Sending email for category approoval");
			Map<String, Object> emailModel = new HashedMap();
			emailModel.put("subject", subject);
			emailModel.put("body", body);
			emailModel.put("emailFrom", emailFrom);
			emailModel.put("email", emailTo);
			emailModel.put("emp_name", empName);
			emailModel.put("approver_name", approver_name);
			this.emailService.sendCategoryApprooveEmail(emailModel);
		} catch (Exception e) {
			logger.error("Error occured while sending email", e);
		}
	}

	// @Async
	private void subCategoryCreationEmail(PayadviceSubCategories payadviceSubCategories) {
		try {
			logger.debug("Sending sub category email");
			EmpEmployment financeLead = this.empEmploymentRepository.getFinanceLead();
			EmpPersonal financeLeadPersonal = this.empPersonalRepository.findOne(financeLead.getId());
			String approover_name = financeLeadPersonal.getEmp_fname();
			String email = financeLead.getEmp_email();
			String emailFrom;
			String gender;
			String emp_name;
			String sub_category_name = payadviceSubCategories.getPay_subcategory_name();
			String category_name = payadviceSubCategories.getPayadviceCategories().getCat_name();
			EmpEmployment empEmployment = this.empEmploymentRepository
					.findOne(payadviceSubCategories.getCreatedBy().getId());
			EmpPersonal empPersonal = this.empPersonalRepository.findOne(payadviceSubCategories.getCreatedBy().getId());
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(empPersonal.getEmp_fname()).append(" ").append(empPersonal.getEmp_mname()).append(" ")
					.append(empPersonal.getEmp_lname());
			emp_name = stringBuilder.toString();
			gender = empPersonal.getEmp_gender();
			emailFrom = empEmployment.getEmp_email();
			StringBuilder subjectBuilder = new StringBuilder();
			subjectBuilder.append("New Expense Type Created - ").append(sub_category_name);
			String subject = subjectBuilder.toString();
			Map<String, Object> emailModel = new HashedMap();
			emailModel.put("subject", subject);
			emailModel.put("sub_category_name", sub_category_name);
			emailModel.put("category_name", category_name);
			emailModel.put("gender", gender);
			emailModel.put("emailFrom", emailFrom);
			emailModel.put("email", email);
			emailModel.put("emp_name", emp_name);
			emailModel.put("approover_name", approover_name);
			emailModel.put("id", Encryptor.encode(String.valueOf(payadviceSubCategories.getId())));
			emailModel.put("empId", Encryptor.encode(String.valueOf(financeLead.getId())));
			emailModel.put("subCategoryApproveUrl", this.adviceApproveUrl);
			emailModel.put("approval_status", Encryptor.encode("APPROVE"));
			emailModel.put("reject_status", Encryptor.encode("REJECT"));

			this.emailService.sendNewSubCategoryEmail(emailModel);
		} catch (Exception e) {
			logger.error("Error occured while sending sub category email", e);
		}
	}

	// @Async
	private void subCategoryApproveEmail(String subject, String body, String empName, String approver_name,
			String emailFrom, String emailTo) {
		try {
			logger.debug("Sending email for sub category approoval");
			Map<String, Object> emailModel = new HashedMap();
			emailModel.put("subject", subject);
			emailModel.put("body", body);
			emailModel.put("emailFrom", emailFrom);
			emailModel.put("email", emailTo);
			emailModel.put("emp_name", empName);
			emailModel.put("approver_name", approver_name);
			this.emailService.sendSubCategoryApprooveEmail(emailModel);
		} catch (Exception e) {
			logger.error("Error occured while sending email", e);
		}
	}

	// @Async
	private void vendorCreationEmail(Vendors vendors) {
		logger.debug("Sending vendor email");
		try {
			EmpEmployment financeLead = this.empEmploymentRepository.getFinanceLead();
			EmpPersonal financeLeadPersonal = this.empPersonalRepository.findOne(financeLead.getId());
			String approover_name = financeLeadPersonal.getEmp_fname();
			String email = financeLead.getEmp_email();
			String emailFrom;
			String gender;
			String emp_name;
			String vendor_name = vendors.getVendor_name();
			String sub_category_name = vendors.getPayadviceSubCategories().getPay_subcategory_name();
			String category_name = vendors.getPayadviceCategories().getCat_name();
			EmpEmployment empEmployment = this.empEmploymentRepository.findOne(vendors.getCreatedBy().getId());
			EmpPersonal empPersonal = this.empPersonalRepository.findOne(vendors.getCreatedBy().getId());
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(empPersonal.getEmp_fname()).append(" ").append(empPersonal.getEmp_mname()).append(" ")
					.append(empPersonal.getEmp_lname());
			emp_name = stringBuilder.toString();
			gender = empPersonal.getEmp_gender();
			emailFrom = empEmployment.getEmp_email();
			StringBuilder subjectBuilder = new StringBuilder();
			subjectBuilder.append("New Vendor Created - ").append(sub_category_name);
			String subject = subjectBuilder.toString();
			Map<String, Object> emailModel = new HashedMap();
			emailModel.put("subject", subject);
			emailModel.put("sub_category_name", sub_category_name);
			emailModel.put("category_name", category_name);
			emailModel.put("gender", gender);
			emailModel.put("emailFrom", emailFrom);
			emailModel.put("email", email);
			emailModel.put("emp_name", emp_name);
			emailModel.put("approover_name", approover_name);
			emailModel.put("vendor_name", vendor_name);
			emailModel.put("id", Encryptor.encode(String.valueOf(vendors.getId())));
			emailModel.put("empId", Encryptor.encode(String.valueOf(financeLead.getId())));
			emailModel.put("vendorApproveUrl", this.adviceApproveUrl);
			emailModel.put("approval_status", Encryptor.encode("APPROVE"));
			emailModel.put("reject_status", Encryptor.encode("REJECT"));
			this.emailService.sendNewVendorEmail(emailModel);
		} catch (Exception e) {
			logger.error("Error occured while sending email", e);
		}
	}

	// @Async
	private void vendorApprovalEmail(String subject, String body, String empName, String approver_name,
			String emailFrom, String emailTo) {
		try {
			logger.debug("Sending email for vendor approoval");
			Map<String, Object> emailModel = new HashedMap();
			emailModel.put("subject", subject);
			emailModel.put("body", body);
			emailModel.put("emailFrom", emailFrom);
			emailModel.put("email", emailTo);
			emailModel.put("emp_name", empName);
			emailModel.put("approver_name", approver_name);
			this.emailService.sendVendorApprovalEmail(emailModel);
		} catch (Exception e) {
			logger.error("Error occured while sending email", e);
		}
	}

	private void sendPaymentAdviceEmail(PaymentAdvice paymentAdvice, Boolean isNewAdvice) {
		try {
		Map<String, Object> emailModel = new HashedMap();
		String approover_name = null;
		Boolean isForwarded = false;
		String email = null;
		String subject = null;
		// Applied Employee name
		String emp_name = paymentAdvice.getApplied_emp_name();
		String expense_name = paymentAdvice.getPayadviceCategories().getCat_name();
		String description = paymentAdvice.getDescription();
		String expense_type_name = paymentAdvice.getPayadviceSubCategories().getPay_subcategory_name();
		String vendor_name = paymentAdvice.getVendors().getVendor_name();
		Date bill_date = paymentAdvice.getBill_date();
		String bill_number = paymentAdvice.getBill_number();
		Double bill_amount = paymentAdvice.getBill_amount();
		String currencyType = null;
		if (paymentAdvice.getCurrencyType() != null && paymentAdvice.getCurrencyType().getName() != null) {
			currencyType = paymentAdvice.getCurrencyType().getName();
		}
		if (paymentAdvice.getCreatedDate() != null) {
			Date created_on = paymentAdvice.getCreatedDate().toDate();
			emailModel.put("created_on", created_on);
		}
		if (paymentAdvice.getApproved_date() != null) {
			Date forwarded_on = paymentAdvice.getApproved_date().toDate();
			emailModel.put("forwarded_on", forwarded_on);
		}
		String emailFrom = null;
		String bill_amount_value = null;
		NumberFormat myFormat = NumberFormat.getInstance();
		myFormat.setGroupingUsed(true);
		String amountFormatedValue = myFormat.format(bill_amount);
		if (paymentAdvice.getCurrencyType().getId() == 1) {
			bill_amount_value = "US DOLLAR " + amountFormatedValue;
		} else {
			bill_amount_value = "INR " + amountFormatedValue;
		}
		// Applied employee Details
		EmpEmployment appliedEmployeEmpEmployment = this.empEmploymentRepository
				.findOne(paymentAdvice.getApplied_emp_Id());
		EmpPersonal appliedEmployeEmpPersonal = this.empPersonalRepository.findOne(paymentAdvice.getApplied_emp_Id());

		// Primary approver employee Details
		EmpEmployment primaryApproverEmpEmployment = this.empEmploymentRepository
				.findOne(paymentAdvice.getPrimary_aproover_emp_Id());
		EmpPersonal primaryApproverPersonal = this.empPersonalRepository
				.findOne(paymentAdvice.getPrimary_aproover_emp_Id());
		// Secondary approver employee Details
		EmpEmployment secondaryApproverEmployment = this.empEmploymentRepository
				.findOne(paymentAdvice.getSecondary_aproover_emp_id());
		EmpPersonal secondaryApproverPersonal = this.empPersonalRepository
				.findOne(paymentAdvice.getSecondary_aproover_emp_id());
		// Finance Lead Details
		EmpEmployment financeLead = this.empEmploymentRepository.getFinanceLead();
		EmpPersonal financeLeadPersonal = this.empPersonalRepository.findOne(financeLead.getId());

		List<EmpEmployment> hrAssosiateList = this.empEmploymentRepository.getHrAssosiateList();
		Optional<EmpEmployment> appliedHR = null;
		if (appliedEmployeEmpEmployment != null && CollectionUtils.isNotEmpty(hrAssosiateList)) {
			appliedHR = hrAssosiateList.stream()
					.filter(item -> item.getId().equals(appliedEmployeEmpEmployment.getId())).findFirst();
		}

		if (appliedEmployeEmpEmployment != null && financeLead != null
				&& financeLead.getId().equals(appliedEmployeEmpEmployment.getId())) {
			financeLead = null;
			financeLeadPersonal = null;
		}

		else if (appliedEmployeEmpEmployment != null && appliedHR != null && appliedHR.isPresent()) {
			hrAssosiateList.remove(appliedHR.get());
		}

		/*
		 * Setting all the details required in the email in the has map only Approver
		 * name and email should be changed
		 */

		emailModel.put("emp_name", emp_name);
		emailModel.put("expense_name", expense_name);
		emailModel.put("description", description);
		emailModel.put("expense_type_name", expense_type_name);
		emailModel.put("vendor_name", vendor_name);
		emailModel.put("bill_date", bill_date);
		emailModel.put("bill_number", bill_number);
		emailModel.put("bill_amount", bill_amount_value);
		emailModel.put("cheque_number", paymentAdvice.getPayment_cheque_number());
		emailModel.put("currency_type", currencyType);
		emailModel.put("isForwarded", false);
		emailModel.put("adviceApproveUrl", this.adviceApproveUrl);
		emailModel.put("APRROVED", Encryptor.encode("APRROVED"));
		emailModel.put("REJECT", Encryptor.encode("REJECT"));
		emailModel.put("APPRROVE_AND_FORWARD", Encryptor.encode("APPRROVE_AND_FORWARD"));
		emailModel.put("FORWARD_APRROVED", Encryptor.encode("FORWARD_APRROVED"));
		emailModel.put("FORWARD_REJECT", Encryptor.encode("FORWARD_REJECT"));

		// Setting Subject
		StringBuilder subjectBuilder = new StringBuilder();
		if (isNewAdvice) {
			subjectBuilder.append("New Payment Advice Raised");
			subjectBuilder.append(" on ").append(expense_name);
			subjectBuilder.append(" for ").append(vendor_name);
			subject = subjectBuilder.toString();
			emailFrom = appliedEmployeEmpEmployment.getEmp_email();
			emailModel.put("emailFrom", emailFrom);

		} else {
			subjectBuilder.append("Payment Advice Forwarded");
			// Setting Forwarded Employee Name
			StringBuilder primaryApproverNameBuilder = new StringBuilder();
			if (primaryApproverPersonal != null) {
				primaryApproverNameBuilder.append(primaryApproverPersonal.getEmp_fname());
				if (primaryApproverPersonal.getEmp_mname() != null) {
					primaryApproverNameBuilder.append(" ").append(primaryApproverPersonal.getEmp_mname());
				}
				primaryApproverNameBuilder.append(" ").append(primaryApproverPersonal.getEmp_lname());
				subjectBuilder.append(" by ").append(primaryApproverNameBuilder.toString());
			}
			subjectBuilder.append(" on ").append(expense_name);
			subjectBuilder.append(" for ").append(vendor_name);
			subject = subjectBuilder.toString();
			// Setting additional Details Required for forwarded advice
			emailModel.put("isForwarded", true);
			emailFrom = primaryApproverEmpEmployment.getEmp_email();
			emailModel.put("emailFrom", emailFrom);

		}
		emailModel.put("subject", subject);

		Integer paymentAdviceId = paymentAdvice.getId();

		// Sending email to primary Approver
		if (primaryApproverEmpEmployment != null && primaryApproverPersonal != null) {
			approover_name = primaryApproverPersonal.getEmp_fname();
			email = primaryApproverEmpEmployment.getEmp_email();
			emailModel.put("approover_name", approover_name);
			emailModel.put("email", email);
			emailModel.put("isNewAdvice", false);
			emailModel.put("isForwarded", false);
			if (isNewAdvice) {
				emailModel.put("isNewAdvice", isNewAdvice);
				emailModel.put("empId", Encryptor.encode(String.valueOf(primaryApproverEmpEmployment.getId())));
				emailModel.put("paymentAdviceId", Encryptor.encode(String.valueOf(paymentAdviceId)));
			}
			try {
				this.emailService.sendPaymentAdviceEmail(emailModel);
			} catch (MessagingException e) {
				logger.error("Error occured while sending email", e);
			}

		}
		// Sending email to secondary Approver
		if (secondaryApproverEmployment != null && secondaryApproverPersonal != null) {
			approover_name = secondaryApproverPersonal.getEmp_fname();
			email = secondaryApproverEmployment.getEmp_email();
			emailModel.put("approover_name", approover_name);
			emailModel.put("email", email);
			emailModel.put("isNewAdvice", false);
			emailModel.put("isForwarded", false);
			if (isNewAdvice) {
				emailModel.put("isNewAdvice", isNewAdvice);
				emailModel.put("empId", Encryptor.encode(String.valueOf(primaryApproverEmpEmployment.getId())));
				emailModel.put("paymentAdviceId", Encryptor.encode(String.valueOf(paymentAdviceId)));
			} else {
				emailModel.put("isForwarded", true);
				emailModel.put("empId", Encryptor.encode(String.valueOf(primaryApproverEmpEmployment.getId())));
				emailModel.put("paymentAdviceId", Encryptor.encode(String.valueOf(paymentAdviceId)));
			}
			try {
				this.emailService.sendPaymentAdviceEmail(emailModel);
			} catch (MessagingException e) {
				logger.error("Error occured while sending email", e);
			}
		}
		// Sending email to Finance lead
		if (financeLead != null && financeLeadPersonal != null) {
			approover_name = financeLeadPersonal.getEmp_fname();
			email = financeLead.getEmp_email();
			emailModel.put("approover_name", approover_name);
			emailModel.put("email", email);
			emailModel.put("isNewAdvice", false);
			emailModel.put("isForwarded", false);
			try {
				this.emailService.sendPaymentAdviceEmail(emailModel);
			} catch (MessagingException e) {
				logger.error("Error occured while sending email", e);
			}
		}

		if (CollectionUtils.isNotEmpty(hrAssosiateList)) {
			for (EmpEmployment hrAssosiate : hrAssosiateList) {
				EmpPersonal hrAssosiatePersonal = this.empPersonalRepository.findOne(hrAssosiate.getId());
				if (hrAssosiatePersonal != null) {
					approover_name = hrAssosiatePersonal.getEmp_fname();
					email = hrAssosiate.getEmp_email();
					emailModel.put("approover_name", approover_name);
					emailModel.put("email", email);
					emailModel.put("isNewAdvice", false);
					emailModel.put("isForwarded", false);
					try {
						this.emailService.sendPaymentAdviceEmail(emailModel);
					} catch (MessagingException e) {
						logger.error("Error occured while sending email", e);
					}
				}
			}

		}
		// }
		// Sending email to Applier
		if (appliedEmployeEmpEmployment != null && appliedEmployeEmpPersonal != null) {
			approover_name = appliedEmployeEmpPersonal.getEmp_fname();
			email = appliedEmployeEmpEmployment.getEmp_email();
			emailModel.put("approover_name", approover_name);
			emailModel.put("email", email);
			emailModel.put("isNewAdvice", false);
			emailModel.put("isForwarded", false);
			try {
				this.emailService.sendPaymentAdviceEmail(emailModel);
			} catch (MessagingException e) {
				logger.error("Error occured while sending email", e);
			}
		}
		}catch (Exception e) {
			logger.error("Error occured in sending email process", e);
		}

	}

	private void sendPaymentAdviceApprooveEmail(PaymentAdvice paymentAdvice) {
		try {
		logger.debug("Sending email for payment advice approval");
		Map<String, Object> emailModel = new HashedMap();
		String emp_name = paymentAdvice.getApplied_emp_name();
		String approover_name = null;
		String first_approover_name = paymentAdvice.getAprooved_emp_name();
		Boolean isApproved = false;
		String status = null;
		if (paymentAdvice.getCreatedDate() != null) {
			Date created_on = paymentAdvice.getCreatedDate().toDate();
			emailModel.put("created_on", created_on);
		}
		if (paymentAdvice.getApproved_date() != null) {
			Date approved_date = paymentAdvice.getApproved_date().toDate();
			emailModel.put("approved_date", approved_date);
		}
		if (paymentAdvice.getReject_date() != null) {
			Date rejected_date = paymentAdvice.getReject_date().toDate();
			emailModel.put("rejected_date", rejected_date);
		}
		String expense_name = paymentAdvice.getPayadviceCategories().getCat_name();
		String description = paymentAdvice.getDescription();
		String expense_type_name = paymentAdvice.getPayadviceSubCategories().getPay_subcategory_name();
		String vendor_name = paymentAdvice.getVendors().getVendor_name();
		Date bill_date = paymentAdvice.getBill_date();
		String bill_number = paymentAdvice.getBill_number();
		Double bill_amount = paymentAdvice.getBill_amount();
		String currencyType = null;
		if (paymentAdvice.getCurrencyType() != null && paymentAdvice.getCurrencyType().getName() != null) {
			currencyType = paymentAdvice.getCurrencyType().getName();
		}
		String forwarded_by = null;
		String email;
		String emailFrom = null;

		// Applied employee Details
		EmpEmployment appliedEmployeEmpEmployment = this.empEmploymentRepository
				.findOne(paymentAdvice.getApplied_emp_Id());
		EmpPersonal appliedEmployeEmpPersonal = this.empPersonalRepository.findOne(paymentAdvice.getApplied_emp_Id());

		// Primary approver employee Details
		EmpEmployment primaryApproverEmpEmployment = this.empEmploymentRepository
				.findOne(paymentAdvice.getPrimary_aproover_emp_Id());
		EmpPersonal primaryApproverPersonal = this.empPersonalRepository
				.findOne(paymentAdvice.getPrimary_aproover_emp_Id());
		// Secondary approver employee Details
		EmpEmployment secondaryApproverEmployment = this.empEmploymentRepository
				.findOne(paymentAdvice.getSecondary_aproover_emp_id());
		EmpPersonal secondaryApproverPersonal = this.empPersonalRepository
				.findOne(paymentAdvice.getSecondary_aproover_emp_id());
		// Finance Lead Details
		EmpEmployment financeLead = this.empEmploymentRepository.getFinanceLead();
		EmpPersonal financeLeadPersonal = this.empPersonalRepository.findOne(financeLead.getId());

		List<EmpEmployment> hrAssosiateList = this.empEmploymentRepository.getHrAssosiateList();
		Optional<EmpEmployment> appliedHR = null;
		if (appliedEmployeEmpEmployment != null && CollectionUtils.isNotEmpty(hrAssosiateList)) {
			appliedHR = hrAssosiateList.stream()
					.filter(item -> item.getId().equals(appliedEmployeEmpEmployment.getId())).findFirst();
		}

		if (appliedEmployeEmpEmployment != null && financeLead != null
				&& financeLead.getId().equals(appliedEmployeEmpEmployment.getId())) {
			financeLead = null;
			financeLeadPersonal = null;
		}

		else if (appliedEmployeEmpEmployment != null && appliedHR != null && appliedHR.isPresent()) {
			hrAssosiateList.remove(appliedHR.get());
		}

		String subject;
		Integer aprrovalStatusId = paymentAdvice.getAprrovalStatus().getId();
		// Constructing email subject
		StringBuilder subjectBuilder = new StringBuilder();
		if (aprrovalStatusId == 2) {
			approover_name = paymentAdvice.getAprooved_emp_name();
			status = " Approved";
			subjectBuilder.append("Payment Advice Approved - ").append(vendor_name);
		} else if (aprrovalStatusId == 3) {
			approover_name = paymentAdvice.getReject_emp_name();
			status = " Rejected";
			subjectBuilder.append("Payment Advice Rejected - ").append(vendor_name);
		} else if (aprrovalStatusId == 4) {
			approover_name = paymentAdvice.getPayment_release_emp_name();
			status = " Approved";
			subjectBuilder.append("Payment Processed By Finance  - ").append(vendor_name);
		} else if (aprrovalStatusId == 5) {
			approover_name = paymentAdvice.getReject_emp_name();
			status = " Rejected";
			subjectBuilder.append("Payment Advice Rejected By Finance - ").append(vendor_name);
		} else if (aprrovalStatusId == 6) {
			approover_name = paymentAdvice.getAprooved_emp_name();
			status = " Forwarded";
			subjectBuilder.append("Payment Advice - ").append(vendor_name);
			subjectBuilder.append(" Forwarded ");
			if (secondaryApproverPersonal != null && primaryApproverPersonal != null) {
				StringBuilder nameBuilder = new StringBuilder();
				nameBuilder.append("To ");
				nameBuilder.append(secondaryApproverPersonal.getEmp_fname());
				if (secondaryApproverPersonal.getEmp_mname() != null) {
					nameBuilder.append(" ").append(secondaryApproverPersonal.getEmp_mname());
				}
				nameBuilder.append(" ").append(secondaryApproverPersonal.getEmp_lname());
				nameBuilder.append("By ");
				nameBuilder.append(primaryApproverPersonal.getEmp_fname());
				if (primaryApproverPersonal.getEmp_mname() != null) {
					nameBuilder.append(" ").append(primaryApproverPersonal.getEmp_mname());
				}
				nameBuilder.append(" ").append(primaryApproverPersonal.getEmp_lname());
				subjectBuilder.append(nameBuilder.toString());
			}

		} else if (aprrovalStatusId == 7) {
			approover_name = paymentAdvice.getReject_emp_name();
			status = " Rejected";
			subjectBuilder.append("Payment Advice Rejected - ").append(vendor_name);
			if (secondaryApproverPersonal != null) {
				StringBuilder nameBuilder = new StringBuilder();
				nameBuilder.append(secondaryApproverPersonal.getEmp_fname());
				if (secondaryApproverPersonal.getEmp_mname() != null) {
					nameBuilder.append(" ").append(secondaryApproverPersonal.getEmp_mname());
				}
				nameBuilder.append(" ").append(secondaryApproverPersonal.getEmp_lname());
				subjectBuilder.append(" By ").append(nameBuilder.toString());
			}

		} else if (aprrovalStatusId == 8) {
			status = " Approved";
			subjectBuilder.append("Payment Advice Approved - ").append(vendor_name);
			if (secondaryApproverPersonal != null) {
				StringBuilder nameBuilder = new StringBuilder();
				nameBuilder.append(secondaryApproverPersonal.getEmp_fname());
				if (secondaryApproverPersonal.getEmp_mname() != null) {
					nameBuilder.append(" ").append(secondaryApproverPersonal.getEmp_mname());
				}
				nameBuilder.append(" ").append(secondaryApproverPersonal.getEmp_lname());
				subjectBuilder.append(" By ").append(nameBuilder.toString());
				approover_name = nameBuilder.toString();
			}
		}
		subject = subjectBuilder.toString();
		// Finding whether the advice is approved or not
		if (!paymentAdvice.getIs_admin_rejected() && !paymentAdvice.getIs_forward_rejected()) {
			isApproved = true;
		}
		// Finding the from Email
		if (primaryApproverPersonal != null) {
			StringBuilder nameBuilder = new StringBuilder();
			nameBuilder.append("( Forwarded By ");
			nameBuilder.append(primaryApproverPersonal.getEmp_fname());
			if (primaryApproverPersonal.getEmp_mname() != null) {
				nameBuilder.append(" ").append(primaryApproverPersonal.getEmp_mname());
			}
			nameBuilder.append(" ").append(primaryApproverPersonal.getEmp_lname()).append(" )");
			forwarded_by = nameBuilder.toString();
		}
		if (primaryApproverEmpEmployment != null && paymentAdvice.getAprooved_emp_Id() != null
				&& (paymentAdvice.getAprooved_emp_Id().equals(primaryApproverEmpEmployment.getId())
						&& aprrovalStatusId == 2)) {
			emailFrom = primaryApproverEmpEmployment.getEmp_email();
		} else if (secondaryApproverEmployment != null && paymentAdvice.getAprooved_emp_Id() != null
				&& paymentAdvice.getAprooved_emp_Id().equals(secondaryApproverEmployment.getId())
				&& aprrovalStatusId == 2) {
			emailFrom = secondaryApproverEmployment.getEmp_email();
		} else if (primaryApproverEmpEmployment != null && paymentAdvice.getReject_emp_Id() != null
				&& (paymentAdvice.getReject_emp_Id().equals(primaryApproverEmpEmployment.getId())
						&& aprrovalStatusId == 3)) {
			emailFrom = primaryApproverEmpEmployment.getEmp_email();
		} else if (secondaryApproverEmployment != null && paymentAdvice.getReject_emp_Id() != null
				&& paymentAdvice.getReject_emp_Id().equals(secondaryApproverEmployment.getId())
				&& aprrovalStatusId == 3) {
			emailFrom = secondaryApproverEmployment.getEmp_email();
		} else if (financeLead != null && paymentAdvice.getPayment_release_emp_Id() != null
				&& paymentAdvice.getPayment_release_emp_Id().equals(financeLead.getId()) && aprrovalStatusId == 4) {
			emailFrom = financeLead.getEmp_email();
		} else if (financeLead != null && paymentAdvice.getReject_emp_Id() != null
				&& paymentAdvice.getReject_emp_Id().equals(financeLead.getId()) && aprrovalStatusId == 5) {
			emailFrom = financeLead.getEmp_email();
		} else if (primaryApproverEmpEmployment != null && aprrovalStatusId == 6) {
			emailFrom = primaryApproverEmpEmployment.getEmp_email();
		} else if (secondaryApproverEmployment != null && aprrovalStatusId == 7) {
			emailFrom = secondaryApproverEmployment.getEmp_email();
		} else if (secondaryApproverEmployment != null && aprrovalStatusId == 8) {
			emailFrom = secondaryApproverEmployment.getEmp_email();
		} else if (primaryApproverEmpEmployment != null && paymentAdvice.getPayment_release_emp_Id() != null
				&& paymentAdvice.getPayment_release_emp_Id().equals(primaryApproverEmpEmployment.getId())
				&& aprrovalStatusId == 4) {
			emailFrom = primaryApproverEmpEmployment.getEmp_email();
		} else if (primaryApproverEmpEmployment != null && paymentAdvice.getReject_emp_Id() != null
				&& paymentAdvice.getReject_emp_Id().equals(primaryApproverEmpEmployment.getId())
				&& aprrovalStatusId == 5) {
			emailFrom = primaryApproverEmpEmployment.getEmp_email();
		} else if (secondaryApproverEmployment != null && paymentAdvice.getPayment_release_emp_Id() != null
				&& paymentAdvice.getPayment_release_emp_Id().equals(secondaryApproverEmployment.getId())
				&& aprrovalStatusId == 4) {
			emailFrom = secondaryApproverEmployment.getEmp_email();
		} else if (secondaryApproverEmployment != null && paymentAdvice.getReject_emp_Id() != null
				&& paymentAdvice.getReject_emp_Id().equals(secondaryApproverEmployment.getId())
				&& aprrovalStatusId == 5) {
			emailFrom = secondaryApproverEmployment.getEmp_email();
		} else if (CollectionUtils.isNotEmpty(hrAssosiateList)) {
			for (EmpEmployment hrAssosiate : hrAssosiateList) {
				if (paymentAdvice.getPayment_release_emp_Id().equals(hrAssosiate.getId())
						&& (aprrovalStatusId == 4 || aprrovalStatusId == 5)) {
					emailFrom = hrAssosiate.getEmp_email();
					break;
				}
			}
		} else if (appliedHR != null && appliedHR.isPresent()) {

			if (paymentAdvice.getPayment_release_emp_Id().equals(appliedHR.get().getId())
					&& (aprrovalStatusId == 4 || aprrovalStatusId == 5)) {
				emailFrom = appliedHR.get().getEmp_email();
			}

		}

		else if (emailFrom == null && appliedEmployeEmpEmployment != null
				&& (aprrovalStatusId == 4 || aprrovalStatusId == 5)) {
			emailFrom = appliedEmployeEmpEmployment.getEmp_email();
		}

		/*
		 * Setting all the details required in the email in the has map only Approver
		 * name and email should be changed
		 */
		emailModel.put("expense_name", expense_name);
		emailModel.put("description", description);
		emailModel.put("expense_type_name", expense_type_name);
		emailModel.put("vendor_name", vendor_name);
		emailModel.put("bill_date", bill_date);
		emailModel.put("bill_number", bill_number);
		emailModel.put("bill_amount", bill_amount);
		emailModel.put("cheque_number", paymentAdvice.getPayment_cheque_number());
		emailModel.put("currency_type", currencyType);
		emailModel.put("subject", subject);
		emailModel.put("approover_name", approover_name);
		emailModel.put("first_approover_name", first_approover_name);
		emailModel.put("isApproved", isApproved);
		emailModel.put("aprrovalStatusId", aprrovalStatusId);
		emailModel.put("forwarded_by", forwarded_by);
		emailModel.put("status", status);
		emailModel.put("approover_name", approover_name);
		emailModel.put("emailFrom", emailFrom);
		emailModel.put("adviceApproveUrl", this.adviceApproveUrl);
		emailModel.put("PROCESSED_BY_FINANCE", Encryptor.encode("PROCESSED_BY_FINANCE"));
		emailModel.put("REJECTED_BY_FINANCE", Encryptor.encode("REJECTED_BY_FINANCE"));

		Integer paymentAdviceId = paymentAdvice.getId();
		// Sending email to primary Approver
		if (primaryApproverEmpEmployment != null && primaryApproverPersonal != null) {
			emp_name = primaryApproverPersonal.getEmp_fname();
			email = primaryApproverEmpEmployment.getEmp_email();
			emailModel.put("emp_name", emp_name);
			emailModel.put("email", email);
			emailModel.put("isApprovalToFinance", false);
			try {
				this.emailService.sendPaymentAdviceAprroveEmail(emailModel);
			} catch (MessagingException e) {
				logger.error("Error occured while sending email", e);
			}

		}
		// Sending email to secondary Approver
		if (secondaryApproverEmployment != null && secondaryApproverPersonal != null) {
			emp_name = secondaryApproverPersonal.getEmp_fname();
			email = secondaryApproverEmployment.getEmp_email();
			emailModel.put("emp_name", emp_name);
			emailModel.put("email", email);
			emailModel.put("isApprovalToFinance", false);
			try {
				this.emailService.sendPaymentAdviceAprroveEmail(emailModel);
			} catch (MessagingException e) {
				logger.error("Error occured while sending email", e);
			}
		}
		// Sending email to Finance lead
		if (financeLead != null && financeLeadPersonal != null) {
			emp_name = financeLeadPersonal.getEmp_fname();
			email = financeLead.getEmp_email();
			emailModel.put("emp_name", emp_name);
			emailModel.put("email", email);
			emailModel.put("isApprovalToFinance", false);
			if (aprrovalStatusId.equals(2) || aprrovalStatusId.equals(8)) {
				emailModel.put("isApprovalToFinance", true);
				emailModel.put("empId", Encryptor.encode(String.valueOf(financeLead.getId())));
				emailModel.put("paymentAdviceId", Encryptor.encode(String.valueOf(paymentAdviceId)));
			}
			try {
				this.emailService.sendPaymentAdviceAprroveEmail(emailModel);
			} catch (MessagingException e) {
				logger.error("Error occured while sending email", e);
			}
		}
		if (CollectionUtils.isNotEmpty(hrAssosiateList)) {
			for (EmpEmployment hrAssosiate : hrAssosiateList) {
				EmpPersonal hrAssosiatePersonal = this.empPersonalRepository.findOne(hrAssosiate.getId());
				if (hrAssosiatePersonal != null) {
					emp_name = hrAssosiatePersonal.getEmp_fname();
					email = hrAssosiate.getEmp_email();
					emailModel.put("emp_name", emp_name);
					emailModel.put("email", email);
					emailModel.put("isApprovalToFinance", false);
					try {
						this.emailService.sendPaymentAdviceAprroveEmail(emailModel);
					} catch (MessagingException e) {
						logger.error("Error occured while sending email", e);
					}
				}
			}

		}
		// Sending email to Applier
		if (appliedEmployeEmpEmployment != null && appliedEmployeEmpPersonal != null) {
			emp_name = appliedEmployeEmpPersonal.getEmp_fname();
			email = appliedEmployeEmpEmployment.getEmp_email();
			emailModel.put("emp_name", emp_name);
			emailModel.put("email", email);
			emailModel.put("isApprovalToFinance", false);
			if ((aprrovalStatusId.equals(2) || aprrovalStatusId.equals(8))
					&& (financeLead == null && financeLeadPersonal == null)) {
				emailModel.put("isApprovalToFinance", true);
				emailModel.put("empId", Encryptor.encode(String.valueOf(appliedEmployeEmpEmployment.getId())));
				emailModel.put("paymentAdviceId", Encryptor.encode(String.valueOf(paymentAdviceId)));
			}
			try {
				this.emailService.sendPaymentAdviceAprroveEmail(emailModel);
			} catch (MessagingException e) {
				logger.error("Error occured while sending email", e);
			}
		}
		}catch (Exception e) {
			logger.error("Error occured in sending email process", e);
		}

	}

	// @Async
	private void sendPaymentAdvicePushNotification(PaymentAdvice paymentAdvice) {
		logger.debug("Sending push notification for payment advice");
		try {
			ObjectMapper mapper = new ObjectMapper();
			ApprovalStatusType approvalStatusType = paymentAdvice.getAprrovalStatus();
			Integer approvalStatusTypeId = approvalStatusType.getId();
			Integer primaryApproverId = paymentAdvice.getPrimary_aproover_emp_Id();
			Integer secondaryApproverId = paymentAdvice.getSecondary_aproover_emp_id();
			// primary fcm
			FcmDetail primaryFcmDetail = this.paymentLookupService.getEmployeeFcmDetail(primaryApproverId);
			// primary fcm
			FcmDetail secondaryFcmDetail = this.paymentLookupService.getEmployeeFcmDetail(secondaryApproverId);
			// primary fcm
			FcmDetail fcmApplier = this.paymentLookupService.getEmployeeFcmDetail(paymentAdvice.getApplied_emp_Id());
			FcmDetail fcmAdmin;
			if (paymentAdvice.getPrimary_aproover_emp_Id().equals(paymentAdvice.getAprooved_emp_Id())) {
				fcmAdmin = this.paymentLookupService.getEmployeeFcmDetail(paymentAdvice.getSecondary_aproover_emp_id());
			} else {
				fcmAdmin = this.paymentLookupService.getEmployeeFcmDetail(paymentAdvice.getPrimary_aproover_emp_Id());
			}
			EmpEmployment financeLead = this.empEmploymentRepository.getFinanceLead();

			// EmpEmployment hrAssosiate = this.empEmploymentRepository.getHrAssosiate();

			List<EmpEmployment> hrAssosiateList = this.empEmploymentRepository.getHrAssosiateList();
			List<Integer> hrIdList = hrAssosiateList.stream().map(item -> item.getId()).collect(Collectors.toList());

			// Integer hrId = hrAssosiate.getId();
			Integer financeLeadId = financeLead.getId();
			// fcm finance
			FcmDetail fcmFinance = this.paymentLookupService.getEmployeeFcmDetail(financeLeadId);
			// fcm hr
			List<FcmDetail> fcmHRList = new ArrayList();
			for (Integer hrId : hrIdList) {
				FcmDetail fcmHr = this.paymentLookupService.getEmployeeFcmDetail(hrId);
				fcmHRList.add(fcmHr);
			}
			Optional<FcmDetail> appliedHr = null;
			if (fcmApplier != null) {
				appliedHr = fcmHRList.stream()
						.filter(item -> item.getEmpEmployment().getId().equals(fcmApplier.getEmpEmployment().getId()))
						.findFirst();
			}

			if (fcmApplier != null && fcmApplier.getEmpEmployment().getId().equals(financeLeadId)) {
				fcmFinance = null;
			} else if (fcmApplier != null && CollectionUtils.isNotEmpty(fcmHRList)) {
				fcmHRList.removeIf(item -> item != null && item.getFcm_id().equals(fcmApplier.getFcm_id()));
			}
			if (approvalStatusTypeId == 1 || approvalStatusTypeId == 6) {
				PaymentAdvicePushNotificationDTO paymentAdvicePushNotificationDTO = new PaymentAdvicePushNotificationDTO();
				EmpPersonal empPersonal = this.empPersonalRepository.findOne(paymentAdvice.getApplied_emp_Id());
				paymentAdvicePushNotificationDTO.setId(paymentAdvice.getId());
				paymentAdvicePushNotificationDTO.setApplied_emp_avathar(empPersonal.getEmp_avatar());
				paymentAdvicePushNotificationDTO.setApplied_emp_Id(paymentAdvice.getApplied_emp_Id());
				paymentAdvicePushNotificationDTO.setApplied_emp_name(paymentAdvice.getApplied_emp_name());
				paymentAdvicePushNotificationDTO.setApplied_on(paymentAdvice.getApplied_on().toDate());
				paymentAdvicePushNotificationDTO.setBill_amount(paymentAdvice.getBill_amount());
				paymentAdvicePushNotificationDTO.setBill_period_from(paymentAdvice.getBill_period_from());
				paymentAdvicePushNotificationDTO.setBill_peroid_to(paymentAdvice.getBill_peroid_to());
				paymentAdvicePushNotificationDTO.setCurrency(paymentAdvice.getCurrencyType().getName());
				paymentAdvicePushNotificationDTO.setCategory_name(paymentAdvice.getPayadviceCategories().getCat_name());
				paymentAdvicePushNotificationDTO
						.setSub_category_name(paymentAdvice.getPayadviceSubCategories().getPay_subcategory_name());
				paymentAdvicePushNotificationDTO.setVendor_name(paymentAdvice.getVendors().getVendor_name());
				paymentAdvicePushNotificationDTO.setPaymentMode(paymentAdvice.getPaymentModes().getName());
				paymentAdvicePushNotificationDTO.setDescription(paymentAdvice.getDescription());

				// comment below when Bipin needs push for payment advice
				if (primaryApproverId == 10001)
					primaryFcmDetail = null;

				if (secondaryApproverId == 10001)
					secondaryFcmDetail = null;

				// Push notification should be gone to both primary and secondary approver
				// Only primary approver can forward the request to secondary approver
				if (approvalStatusTypeId == 1) {
					paymentAdvicePushNotificationDTO.setIsPrimaryApprover(true);
					if (primaryFcmDetail != null) {
						fcmService.sendMessage(primaryFcmDetail.getFcm_id(), null,
								mapper.writeValueAsString(paymentAdvicePushNotificationDTO), "paymentadvice");

					}
					if (secondaryFcmDetail != null) {
						paymentAdvicePushNotificationDTO.setIsPrimaryApprover(false);
						fcmService.sendMessage(secondaryFcmDetail.getFcm_id(), null,
								mapper.writeValueAsString(paymentAdvicePushNotificationDTO), "paymentadvice");
					}
				} else {
					// Only secondary approver can handle the forwarded advice
					paymentAdvicePushNotificationDTO.setIsForwardedRequest(true);
					paymentAdvicePushNotificationDTO.setIsPrimaryApprover(false);
					if (secondaryFcmDetail != null) {
						fcmService.sendMessage(secondaryFcmDetail.getFcm_id(), null,
								mapper.writeValueAsString(paymentAdvicePushNotificationDTO), "paymentadvice");
					}
				}
			} else {
				GlobalPushDTO globalPushDTO = new GlobalPushDTO();
				String title = null;
				String body = null;
				String avathar = null;
				StringBuilder stringBuilder = new StringBuilder();
				SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
				stringBuilder.append("The payment advice raised with \n ").append("Expense : ")
						.append(paymentAdvice.getPayadviceCategories().getCat_name()).append("\n")
						.append("Expense type : ")
						.append(paymentAdvice.getPayadviceSubCategories().getPay_subcategory_name()).append("\n")
						.append("vendor : ").append(paymentAdvice.getVendors().getVendor_name()).append("\n")
						.append("Amount : ").append(paymentAdvice.getBill_amount()).append("\n");
				List<String> fcmIdList = new ArrayList();
				if (paymentAdvice.getBill_period_from() != null) {
					stringBuilder.append("Bill From : ").append(dateFormat.format(paymentAdvice.getBill_period_from()))
							.append("\n");
				}
				if (paymentAdvice.getBill_peroid_to() != null) {
					stringBuilder.append("Bill To : ").append(dateFormat.format(paymentAdvice.getBill_peroid_to()));
				}

				if (approvalStatusTypeId == 2) {
					// Approved request push for henna and nandakumar sir , secondary approver
					title = "Payment Advice Approved";
					stringBuilder.append(" has been approved by ").append(paymentAdvice.getAprooved_emp_name());
					if (paymentAdvice.getApproval_comment() != null) {
						stringBuilder.append("\n").append("Note : ").append(paymentAdvice.getApproval_comment());
					}
					body = stringBuilder.toString();
					EmpPersonal empPersonal = this.empPersonalRepository.findOne(paymentAdvice.getAprooved_emp_Id());
					avathar = empPersonal.getEmp_avatar();
					globalPushDTO.setAvathar(avathar);
					globalPushDTO.setBody(body);
					globalPushDTO.setTitle(title);
					if (fcmApplier != null) {
						fcmIdList.add(fcmApplier.getFcm_id());
					}
					if (fcmAdmin != null) {
						fcmIdList.add(fcmAdmin.getFcm_id());
					}
					if (fcmFinance != null) {
						fcmIdList.add(fcmFinance.getFcm_id());
					}
					// if (fcmHr != null) {
					// fcmIdList.add(fcmHr.getFcm_id());
					// }
					if (CollectionUtils.isNotEmpty(fcmHRList)) {
						List<String> fcmIDHrList = new ArrayList<>();
						for (FcmDetail fcm : fcmHRList) {
							if (fcm != null) {
								fcmIDHrList.add(fcm.getFcm_id());
							}
						}

						fcmIdList.addAll(fcmIDHrList);
					}

				} else if (approvalStatusTypeId == 3) {
					// Rejected request push for henna ,secondary approver
					title = "Payment Advice Rejected";
					stringBuilder.append(" has been rejected by ").append(paymentAdvice.getReject_emp_name());
					if (paymentAdvice.getReject_reson() != null) {
						stringBuilder.append("\n").append("Reason : ").append(paymentAdvice.getReject_reson());
					}
					body = stringBuilder.toString();
					EmpPersonal empPersonal = this.empPersonalRepository.findOne(paymentAdvice.getReject_emp_Id());
					avathar = empPersonal.getEmp_avatar();
					globalPushDTO.setAvathar(avathar);
					globalPushDTO.setBody(body);
					globalPushDTO.setTitle(title);
					if (fcmApplier != null) {
						fcmIdList.add(fcmApplier.getFcm_id());

					}
					if (fcmAdmin != null) {
						fcmIdList.add(fcmAdmin.getFcm_id());
					}
					if (fcmFinance != null) {
						fcmIdList.add(fcmFinance.getFcm_id());
					}
					// if (fcmHr != null) {
					// fcmIdList.add(fcmHr.getFcm_id());
					// }
					if (CollectionUtils.isNotEmpty(fcmHRList)) {
						List<String> fcmIDHrList = new ArrayList<>();
						for(FcmDetail fcm:fcmHRList) {
							if(fcm != null) {
								fcmIDHrList.add(fcm.getFcm_id());	
							}
						}
						
						fcmIdList.addAll(fcmIDHrList);
					}
				} else if (approvalStatusTypeId == 4) {
					// Finance processed request push for henna , and admin team
					title = "Payment Advice Processed By Finance";
					stringBuilder.append(" has been processed by ").append(paymentAdvice.getPayment_release_emp_name());
					if (paymentAdvice.getRelease_note() != null) {
						stringBuilder.append("\n").append("Note : ").append(paymentAdvice.getRelease_note());
					}
					body = stringBuilder.toString();
					EmpPersonal empPersonal = this.empPersonalRepository.findOne(financeLeadId);
					avathar = empPersonal.getEmp_avatar();
					globalPushDTO.setAvathar(avathar);
					globalPushDTO.setBody(body);
					globalPushDTO.setTitle(title);
					if (fcmApplier != null) {
						fcmIdList.add(fcmApplier.getFcm_id());
					}
					if (primaryFcmDetail != null) {
						fcmIdList.add(primaryFcmDetail.getFcm_id());

					}
					if (secondaryFcmDetail != null) {
						fcmIdList.add(secondaryFcmDetail.getFcm_id());
					}
					if (fcmFinance != null) {
						fcmIdList.add(fcmFinance.getFcm_id());
					}
					// if (fcmHr != null) {
					// fcmIdList.add(fcmHr.getFcm_id());
					// }
					if (CollectionUtils.isNotEmpty((fcmHRList))) {
						List<String> fcmIDHrList = new ArrayList<>();
						for(FcmDetail fcm:fcmHRList) {
							if(fcm != null) {
								fcmIDHrList.add(fcm.getFcm_id());	
							}
						}
						
						fcmIdList.addAll(fcmIDHrList);
					}
				} else if (approvalStatusTypeId == 5) {
					// Finance rejected request push for henna , and admin team
					title = "Payment Advice Rejected By Finance";
					stringBuilder.append(" has been rejected by ").append(paymentAdvice.getReject_emp_name());
					if (paymentAdvice.getRelease_reject_note() != null) {
						stringBuilder.append("\n").append("Reason : ").append(paymentAdvice.getRelease_reject_note());
					}
					body = stringBuilder.toString();
					EmpPersonal empPersonal = this.empPersonalRepository.findOne(financeLeadId);
					avathar = empPersonal.getEmp_avatar();
					globalPushDTO.setAvathar(avathar);
					globalPushDTO.setBody(body);
					globalPushDTO.setTitle(title);
					if (fcmApplier != null) {
						fcmIdList.add(fcmApplier.getFcm_id());

					}
					if (primaryFcmDetail != null) {
						fcmIdList.add(primaryFcmDetail.getFcm_id());

					}
					if (secondaryFcmDetail != null) {
						fcmIdList.add(secondaryFcmDetail.getFcm_id());
					}
					if (fcmFinance != null) {
						fcmIdList.add(fcmFinance.getFcm_id());
					}
					// if (fcmHr != null) {
					// fcmIdList.add(fcmHr.getFcm_id());
					// }
					if (CollectionUtils.isNotEmpty((fcmHRList))) {
						List<String> fcmIDHrList = new ArrayList<>();
						for(FcmDetail fcm:fcmHRList) {
							if(fcm != null) {
								fcmIDHrList.add(fcm.getFcm_id());	
							}
						}
						
						fcmIdList.addAll(fcmIDHrList);
					}
				} else if (approvalStatusTypeId == 7) {
					// admin rejected for ward request push for henna and primary approver
					title = "Payment Advice Rejected";
					stringBuilder.append(" has been rejected by ").append(paymentAdvice.getSecondary_aproover_name());
					if (paymentAdvice.getReject_reson() != null) {
						stringBuilder.append("\n").append("Reason : ").append(paymentAdvice.getReject_reson());
					}
					body = stringBuilder.toString();
					EmpPersonal empPersonal = this.empPersonalRepository.findOne(paymentAdvice.getReject_emp_Id());
					avathar = empPersonal.getEmp_avatar();
					globalPushDTO.setAvathar(avathar);
					globalPushDTO.setBody(body);
					globalPushDTO.setTitle(title);
					if (fcmApplier != null) {
						fcmIdList.add(fcmApplier.getFcm_id());

					}
					if (primaryFcmDetail != null) {
						fcmIdList.add(primaryFcmDetail.getFcm_id());
					}
					if (fcmFinance != null) {
						fcmIdList.add(fcmFinance.getFcm_id());
					}
					if (CollectionUtils.isNotEmpty((fcmHRList))) {
						List<String> fcmIDHrList = new ArrayList<>();
						for(FcmDetail fcm:fcmHRList) {
							if(fcm != null) {
								fcmIDHrList.add(fcm.getFcm_id());	
							}
						}
						
						fcmIdList.addAll(fcmIDHrList);
					}

				} else if (approvalStatusTypeId == 8) {
					// admin approved for ward request push for henna and primary approver and
					// nandakumar sir

					title = "Payment Advice Approved";
					stringBuilder.append(" has been approved by ").append(paymentAdvice.getSecondary_aproover_name());
					if (paymentAdvice.getForward_approve_comment() != null) {
						stringBuilder.append("\n").append("Note : ").append(paymentAdvice.getForward_approve_comment());
					}
					body = stringBuilder.toString();
					EmpPersonal empPersonal = this.empPersonalRepository
							.findOne(paymentAdvice.getSecondary_aproover_emp_id());
					avathar = empPersonal.getEmp_avatar();
					globalPushDTO.setAvathar(avathar);
					globalPushDTO.setBody(body);
					globalPushDTO.setTitle(title);
					if (fcmApplier != null) {
						fcmIdList.add(fcmApplier.getFcm_id());
					}
					if (primaryFcmDetail != null) {
						fcmIdList.add(primaryFcmDetail.getFcm_id());
					}
					if (fcmFinance != null) {
						fcmIdList.add(fcmFinance.getFcm_id());
					}
					// if (fcmHr != null) {
					// fcmIdList.add(fcmHr.getFcm_id());
					// }
					if (CollectionUtils.isNotEmpty((fcmHRList))) {
						List<String> fcmIDHrList = new ArrayList<>();
						for(FcmDetail fcm:fcmHRList) {
							if(fcm != null) {
								fcmIDHrList.add(fcm.getFcm_id());	
							}
						}
						
						fcmIdList.addAll(fcmIDHrList);
					}
				}

				// remove bipin fcm id if present in list
				FcmDetail bipinFcmDetail = this.paymentLookupService.getEmployeeFcmDetail(10001);
				if (bipinFcmDetail != null) {
					String bipinFcmID = bipinFcmDetail.getFcm_id();
					fcmIdList.remove(bipinFcmID);
				}

				fcmService.sendMessage(null, fcmIdList, mapper.writeValueAsString(globalPushDTO), "others");

			}
		} catch (

		Exception e) {
			logger.error("Error occured while sending push notification", e);
		}
	}

	private Boolean isNotApprovable(PaymentAdvice paymentAdvice, Integer empId,
			PaymentAdviceApproovalRequest.AprrovalStatus status) {
		Boolean result = false;
		Integer approvalStatusId = paymentAdvice.getAprrovalStatus().getId();
		if (status == PaymentAdviceApproovalRequest.AprrovalStatus.APRROVED
				&& (approvalStatusId == 2 || approvalStatusId == 3 || approvalStatusId == 6)) {
			result = true;
		}
		if (status == PaymentAdviceApproovalRequest.AprrovalStatus.REJECT
				&& (approvalStatusId == 2 || approvalStatusId == 3 || approvalStatusId == 6)) {
			result = true;
		}
		if (status == PaymentAdviceApproovalRequest.AprrovalStatus.APPRROVE_AND_FORWARD
				&& (approvalStatusId == 2 || approvalStatusId == 3 || approvalStatusId == 6)) {
			result = true;
		}
		return result;
	}

	private Boolean isNotApprovableByEmail(PaymentAdvice paymentAdvice, Integer empId,
			PaymentAdviceApproovalRequest.AprrovalStatus status) {
		Boolean result = false;
		Integer approvalStatusId = paymentAdvice.getAprrovalStatus().getId();
		if ((status.equals(PaymentAdviceApproovalRequest.AprrovalStatus.APRROVED)
				|| status.equals(PaymentAdviceApproovalRequest.AprrovalStatus.REJECT)
				|| status.equals(PaymentAdviceApproovalRequest.AprrovalStatus.APPRROVE_AND_FORWARD))
				&& !approvalStatusId.equals(1)) {
			result = true;
		}
		if ((status.equals(PaymentAdviceApproovalRequest.AprrovalStatus.FORWARD_APRROVED)
				|| status.equals(PaymentAdviceApproovalRequest.AprrovalStatus.FORWARD_REJECT))
				&& !approvalStatusId.equals(6)) {
			result = true;
		}
		if ((status.equals(PaymentAdviceApproovalRequest.AprrovalStatus.PROCESSED_BY_FINANCE)
				|| status.equals(PaymentAdviceApproovalRequest.AprrovalStatus.REJECTED_BY_FINANCE))
				&& (!approvalStatusId.equals(2) && !approvalStatusId.equals(8))) {
			result = true;
		}
		return result;
	}

}
