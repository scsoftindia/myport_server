package com.scsoft.myport.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.scsoft.myport.domain.Announcements;
import com.scsoft.myport.domain.EmpEmployment;
import com.scsoft.myport.domain.EmpPersonal;
import com.scsoft.myport.domain.FcmDetail;
import com.scsoft.myport.repository.AnnouncementsRepository;
import com.scsoft.myport.repository.EmpEmploymentRepository;
import com.scsoft.myport.repository.EmpPersonalRepository;
import com.scsoft.myport.rest.request.AnnouncementRequest;
import com.scsoft.myport.service.DTO.AnnouncementDTO;
import com.scsoft.myport.service.DTO.GlobalPushDTO;
import com.scsoft.myport.service.mapper.AnnouncementsMapper;

@Service("announcementsService")
public class AnnouncementsService {
	@Autowired
	private AnnouncementsRepository announcementsRepository;
	@Autowired
	private AnnouncementsMapper announcementsMapper;
	@Autowired
	private EmpPersonalRepository empPersonalRepository;
	@Autowired
	private EmpEmploymentRepository empEmploymentRepository;
	@Autowired
	private PaymentLookupService paymentLookupService;
	@Autowired
	private EmailService emailService;
	@Autowired
	private FcmService fcmService;
	private final Logger logger = LoggerFactory.getLogger(AnnouncementsService.class);

	// Retrieving Announcement from db
	public List<AnnouncementDTO> findAllAnnouncements() {
		logger.debug("Retrieving Announcements from DB");

		List<Announcements> announcements = announcementsRepository.findAllAnnouncements();
		List<AnnouncementDTO> announcementDTOs = new ArrayList();
		announcements.forEach(item -> {
			announcementDTOs.add(announcementsMapper.announcementsToAnnouncementDTO(item));
		});

		return announcementDTOs;
	}

	// Retrieving single Announcement from db
	public Announcements findAnnouncements(Integer id) {
		logger.debug("Retrieving Announcements from DB by" + id);
		return announcementsRepository.findOne(id);
	}

	// Saving Announcement into db
	public Announcements save(AnnouncementRequest announcementRequest) throws ParseException {
		Announcements announcements = new Announcements();
		announcements.setAnnouncement(announcementRequest.getAnnouncement());
		announcements.setAnnouncementHeading(announcementRequest.getAnnouncementHeading());
		//Announcements announcements = this.announcementsMapper.announcementRequestToAnnouncements(announcementRequest);
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
		if(announcementRequest.getAnnouncementDate() != null) {
			Date announcementDate = sdf.parse(announcementRequest.getAnnouncementDate());
			announcements.setAnnouncementDate(new DateTime(announcementDate).withZone(indianZone).toDate());
		}
		if(announcementRequest.getStartDate() != null) {
			Date startDate = sdf.parse(announcementRequest.getStartDate());
			announcements.setStartDate(new DateTime(startDate).withZone(indianZone).toDate());
		}
		EmpPersonal empEmployment = this.empPersonalRepository.findOne(announcementRequest.getAnnouncementBy());
		if(empEmployment != null) {
		announcements.setAnnouncementBy(empEmployment);
		this.announcementsRepository.saveAndFlush(announcements);
		if(announcements.getId() != null) {
		this.sendPushNotification(announcements);
		this.sendEmail(announcements);
		}
		return announcements;
		}
		return null;
		

	}

	// Sending push notification
	// @Async
	private void sendPushNotification(Announcements announcements) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			String tittle = "Announcement By - " + announcements.getAnnouncementBy().getEmp_fname() + " "
					+ announcements.getAnnouncementBy().getEmp_mname() + " "
					+ announcements.getAnnouncementBy().getEmp_lname();
			String body = announcements.getAnnouncementHeading() + "\n" + announcements.getAnnouncement();
			String avathar = announcements.getAnnouncementBy().getEmp_avatar();
			GlobalPushDTO globalPushDTO = new GlobalPushDTO();
			if (tittle != null && body != null) {
				globalPushDTO.setTitle(tittle);
				globalPushDTO.setBody(body);
				globalPushDTO.setAvathar(avathar);
				List<String> fcmIdList = new ArrayList();
				List<FcmDetail> fcmDetails = this.paymentLookupService.getAllActiveEmployyeFcmDetail();
				fcmDetails.stream().forEach(item ->{
					if(item != null) {
						fcmIdList.add(item.getFcm_id());
					}
				});
				this.fcmService.sendMessage(null,fcmIdList, mapper.writeValueAsString(globalPushDTO),
						"others");
			}
		} catch (Exception e) {
			logger.error("Error occured during sending push notification for announcements",e);
		}
	}

	// sending email
	// @Async
	private void sendEmail(Announcements announcements) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			String subject = announcements.getAnnouncementHeading();
			String body = announcements.getAnnouncement();
			EmpEmployment empEmployment = this.empEmploymentRepository
					.findOne(announcements.getAnnouncementBy().getId());
			Map<String, Object> emailModel = new HashedMap();
			emailModel.put("announcement", body);
			emailModel.put("email", "employee.communication@scsoft.in");
			emailModel.put("subject", subject);
			emailModel.put("emailFrom", empEmployment.getEmp_email());
			emailService.sendAnnoucementEmail(emailModel);
		} catch (Exception e) {
			logger.error("Error occured during sending email for announcements",e);
		}

	}

}
