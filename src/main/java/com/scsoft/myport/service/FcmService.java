package com.scsoft.myport.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.scsoft.myport.fcm.util.FCMadmin;


@Service
public class FcmService {
	@Autowired
	FCMadmin fcMadmin;
	private final Logger logger = LoggerFactory.getLogger(FcmService.class);
	
	@Async("processExecutor")
	public void sendMessage(String fcmId,List<String> fcmIdList,String data,String type) {
		logger.debug("Sending message via push notification data is"+data);
		try {
			this.fcMadmin.sendMsg(fcmId, fcmIdList, data, type);
//			Thread.sleep(1000L);
		} catch (Exception e) {
			logger.error("Error occured while sending push notification",e);
		}
	}

}
