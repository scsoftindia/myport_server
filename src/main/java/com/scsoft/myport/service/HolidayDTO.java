package com.scsoft.myport.service;

import java.util.Date;

public class HolidayDTO {
	private Integer id;
	private Integer holiDayId;
	private String holidayName;
	private String status;
	private String supevisorName;
	private Date holiDayDate;
	private String cancelReason;
	private Boolean isWithdrawable;
	private Boolean isApplicable;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getHoliDayId() {
		return holiDayId;
	}

	public void setHoliDayId(Integer holiDayId) {
		this.holiDayId = holiDayId;
	}

	public String getHolidayName() {
		return holidayName;
	}

	public void setHolidayName(String holidayName) {
		this.holidayName = holidayName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSupevisorName() {
		return supevisorName;
	}

	public void setSupevisorName(String supevisorName) {
		this.supevisorName = supevisorName;
	}

	public Date getHoliDayDate() {
		return holiDayDate;
	}

	public void setHoliDayDate(Date holiDayDate) {
		this.holiDayDate = holiDayDate;
	}

	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public Boolean getIsWithdrawable() {
		if(this.isWithdrawable == null) {
			this.isWithdrawable = false;
		}
		return isWithdrawable;
	}

	public void setIsWithdrawable(Boolean isWithdrawable) {
		this.isWithdrawable = isWithdrawable;
	}

	public Boolean getIsApplicable() {
		if(this.isApplicable == null) {
			this.isApplicable = false;
		}
		return isApplicable;
	}

	public void setIsApplicable(Boolean isApplicable) {
		this.isApplicable = isApplicable;
	}
	
	

}
