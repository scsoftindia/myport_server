package com.scsoft.myport.service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.mail.MessagingException;

import org.apache.commons.collections.map.HashedMap;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.scsoft.myport.domain.ApplyWfh;
import com.scsoft.myport.domain.EmpEmployment;
import com.scsoft.myport.domain.EmpPersonal;
import com.scsoft.myport.domain.FcmDetail;
import com.scsoft.myport.error.ApprovalNotPossibleException;
import com.scsoft.myport.error.InvalidRequestException;
import com.scsoft.myport.error.NotFoundException;
import com.scsoft.myport.error.OperationNotPossibleException;
import com.scsoft.myport.repository.ApplyWfhRepository;
import com.scsoft.myport.repository.EmpEmploymentRepository;
import com.scsoft.myport.repository.EmpPersonalRepository;
import com.scsoft.myport.rest.request.WfhApproveRequest;
import com.scsoft.myport.rest.request.WfhCancellRequest;
import com.scsoft.myport.rest.request.WfhRequest;
import com.scsoft.myport.service.DTO.GlobalPushDTO;
import com.scsoft.myport.service.DTO.WfhDTO;
import com.scsoft.myport.service.DTO.WfhEmpDTO;
import com.scsoft.myport.service.mapper.WfhMapper;

@Service
public class ApplyWfhService {
	private final Logger logger = LoggerFactory.getLogger(ApplyWfhService.class);

	@Autowired
	private ApplyWfhRepository applyWfhRepository;
	@Autowired
	private WfhMapper whMapper;
	@Autowired
	private EmpPersonalRepository empPersonalRepository;
	@Autowired
	private EmpEmploymentRepository empEmploymentRepository;
	@Autowired
	private EmailService emailService;
	@Autowired
	private FcmService fcmService;
	@Autowired
	private PaymentLookupService paymentLookupService;
	@Value("${allowed.wfh.days.per.month}")
	private Integer wfhAllowedPerMonth;

	// public Integer getWfhTaken(Integer empId) {
	// logger.debug("REtrieving the wfh taken for current month from db");
	// DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
	// DateTime dateTime = new DateTime().withZone(indianZone);
	// Integer year = dateTime.getYear();
	// Integer month = dateTime.getMonthOfYear();
	// BigDecimal wfhTaken = this.applyWfhRepository.getNoOfWfhByMonth(empId, year,
	// month);
	// Integer result = 0;
	// if (wfhTaken != null) {
	// result = wfhTaken.intValue();
	// }
	//
	// return result;
	//
	// }

	public List<WfhDTO> getPendingWfhRequests(Integer empId) {
		logger.debug("Retrieving pending efh requests from DB");
		return this.applyWfhRepository.getPendingWfhRequests(empId);

	}

	public List<WfhEmpDTO> getWfhHistory(Integer empId) {
		logger.debug("Getting wfh history by EmpId");
		List<ApplyWfh> applyWfhs = this.applyWfhRepository.findByEmpPersonalId(empId);
		return applyWfhs.stream().map(item -> this.whMapper.ApplyWfhToWfhEmpDTO(item)).collect(Collectors.toList());
	}

	public Boolean applyWfh(WfhRequest wfhRequest)
			throws NotFoundException, InvalidRequestException, OperationNotPossibleException, ParseException {
		logger.debug("Request to save wfh into db");
		if (wfhRequest.getEmpId() == null || wfhRequest.getEmpId() == 0) {
			throw new NotFoundException("user", "Invalid user");
		}
		EmpPersonal empPersonal = this.empPersonalRepository.findOne(wfhRequest.getEmpId());
		EmpEmployment empEmployment = this.empEmploymentRepository.findOne(wfhRequest.getEmpId());
		if (empPersonal == null || empEmployment == null) {
			throw new NotFoundException("user", "Invalid user");
		}
		if (wfhRequest.getFromDate() == null || wfhRequest.getToDate() == null || wfhRequest.getNo_of_days() == null) {
			throw new InvalidRequestException("invalidrequest", "Invalid request");
		}
		if (!empEmployment.getIs_wfh().equals("1")) {
			throw new OperationNotPossibleException("error", "User not permitted to apply Work from home");
		}
		Boolean result = false;
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);
		ApplyWfh applyWfh = new ApplyWfh();
		applyWfh.setAdd_sup_id(wfhRequest.getAdd_sup_id());
		applyWfh.setWfh_reason(wfhRequest.getWfh_reason());
		// ApplyWfh applyWfh = this.whMapper.wfhRequestToApplyWfh(wfhRequest);
		if (applyWfh.getAdd_sup_id() == null) {
			applyWfh.setAdd_sup_id(0);
		}

		applyWfh.setEmpPersonal(empPersonal);
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
		if (wfhRequest.getFromDate() != null) {
			Date fromDate = sdf.parse(wfhRequest.getFromDate());
			applyWfh.setFrom_date(new DateTime(fromDate).withZoneRetainFields(indianZone).toDate());
		}
		if (wfhRequest.getToDate() != null) {
			Date toDate = sdf.parse(wfhRequest.getToDate());
			applyWfh.setTo_date(new DateTime(toDate).withZoneRetainFields(indianZone).toDate());
		}

		LocalDateTime fromTime = LocalDateTime.fromDateFields(applyWfh.getFrom_date());
		LocalDateTime toTime = LocalDateTime.fromDateFields(applyWfh.getTo_date());
		Integer noOfDays = Days.daysBetween(fromTime, toTime).getDays();
		noOfDays = noOfDays + 1;
		if (noOfDays > wfhAllowedPerMonth) {
			throw new OperationNotPossibleException("error",
					"Maximum "+wfhAllowedPerMonth+" days can be takes as Work from Home per month");
		}
		BigDecimal wfhAlreadyApplied = applyWfhRepository.getCurrentMonthWorkFromHomeCount(empPersonal.getId(),
				fromTime.getMonthOfYear(), toTime.getMonthOfYear(),fromTime.getYear(),toTime.getYear());
		if (wfhAlreadyApplied != null) {
			int alreadyAppliedCount = wfhAlreadyApplied.intValue();
			if (alreadyAppliedCount == wfhAllowedPerMonth || (alreadyAppliedCount + noOfDays) > wfhAllowedPerMonth) {
				throw new OperationNotPossibleException("error",
						"Maximum "+wfhAllowedPerMonth+" days can be takes as Work from Home per month");
			}
		}

		Boolean isAlreadyApplied = applyWfhRepository.isAlreadyAppliedForTheDate(empPersonal.getId(),
				applyWfh.getFrom_date(), applyWfh.getTo_date());

		if (isAlreadyApplied == true) {
			throw new OperationNotPossibleException("error", "Work from home already applied for the date");
		}

		applyWfh.setNo_of_days(BigDecimal.valueOf((long) noOfDays));
		applyWfh.setApplied_date(dateTime.toDate());
		applyWfh.setApplication_status(2);
		applyWfh.setSup_approval("Pending");
		this.applyWfhRepository.saveAndFlush(applyWfh);
		if (applyWfh.getId() != null) {
			// sending email
			this.sendWorkFromHomeEmail(applyWfh);
			// sending push notification
			this.sendWfhPushNotification(applyWfh);
			result = true;
		}

		return result;

	}

	public Boolean cancellWfh(WfhCancellRequest cancellRequest)
			throws NotFoundException, OperationNotPossibleException {
		if (cancellRequest.getId() == null || cancellRequest.getId() == 0) {
			throw new NotFoundException("id", "Invalid ID");
		}
		ApplyWfh applyWfh = this.applyWfhRepository.findOne(cancellRequest.getId());
		if (applyWfh == null) {
			throw new NotFoundException("id", "Invalid ID");
		}
		if (!applyWfh.getApplication_status().equals(2) && !applyWfh.getApplication_status().equals(9)) {
			throw new OperationNotPossibleException("error", "Can not cancell the request");
		}

		if (applyWfh.getApplication_status().equals(2)) {
			this.applyWfhRepository.delete(applyWfh);
			return true;
		}
		Boolean result = false;
		applyWfh.setCancel_reason(cancellRequest.getCancelReason());
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);
		applyWfh.setCancel_date(dateTime.toDate());
		applyWfh.setApplication_status(11);
		applyWfh.setCancel_approval("Pending");
		this.applyWfhRepository.saveAndFlush(applyWfh);
		if (applyWfh.getApplication_status().equals(11)) {
			result = true;
		}
		// Email and Push notification to supervisor
		this.sendWfhCancellEmail(applyWfh);
		// sending push notification
		this.sendWfhPushNotification(applyWfh);

		return result;
	}

	// WFH approval
	public Boolean approveWfh(WfhApproveRequest wfhApproveRequest)
			throws NotFoundException, InvalidRequestException, ApprovalNotPossibleException {
		logger.debug("Saving wfh approval into DB");
		if (wfhApproveRequest.getId() == null || wfhApproveRequest.getId() == 0) {
			throw new NotFoundException("id", "Invalid ID");
		}
		if (wfhApproveRequest.getApproverId() == null || wfhApproveRequest.getApproverId() == 0) {
			throw new NotFoundException("user", "Invalid User");
		}
		if (wfhApproveRequest.getType() == null || wfhApproveRequest.getStatus() == null) {
			throw new InvalidRequestException("invalidrequest", "Invalid request");
		}
		ApplyWfh applyWfh = this.applyWfhRepository.findOne(wfhApproveRequest.getId());
		if (applyWfh == null) {
			throw new NotFoundException("id", "Employee already cancelled the request");
		}
		Integer applicationStatus = applyWfh.getApplication_status();
		if (applicationStatus != null && !applicationStatus.equals(2)
				&& wfhApproveRequest.getType().toString().equals("NEW")) {
			String currentStatus = null;
			if (applyWfh.getSup_approval() != null && applyWfh.getSup_approval() != "0") {
				currentStatus = applyWfh.getSup_approval();
			} else {
				currentStatus = applyWfh.getAddsup_approval();
			}
			throw new ApprovalNotPossibleException("wfh", "Wfh request is already handled", "error", currentStatus);
		}
		if (applicationStatus != null && !applicationStatus.equals(11)
				&& wfhApproveRequest.getType().toString().equals("CANCELL")) {

			throw new ApprovalNotPossibleException("wfh", "Wfh cacell request is already handled", "error",
					applyWfh.getCancel_approval());
		}

		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);
		Integer empId = applyWfh.getEmpPersonal().getId();
		EmpPersonal supevisor = this.empEmploymentRepository.getSupervisor(empId);
		// Finding whether the request is approved by Supervisor or additional
		// supervisor
		Boolean isApprovedBySupevisor = false;
		if (wfhApproveRequest.getApproverId() != null && wfhApproveRequest.getApproverId().equals(supevisor.getId())) {
			isApprovedBySupevisor = true;
		}

		// Finding new Request approval or cancellation Request approval
		if (wfhApproveRequest.getType().toString().equals("NEW")) {
			if (isApprovedBySupevisor) {
				applyWfh.setSup_approval_date(dateTime.toDate());
				applyWfh.setSup_approval_note(wfhApproveRequest.getComment());
			} else {
				applyWfh.setAddsup_approval_date(dateTime.toDate());
				applyWfh.setAddsup_approval_note(wfhApproveRequest.getComment());
			}
			if (wfhApproveRequest.getComment() == null && isApprovedBySupevisor) {
				applyWfh.setSup_approval_note("0");
			}
			// Finding application status
			if (wfhApproveRequest.getStatus().toString().equals("Approved")) {
				applicationStatus = 9;
				if (isApprovedBySupevisor) {
					applyWfh.setSup_approval("Approved");
				} else {
					applyWfh.setAddsup_approval("Approved");
				}
			} else {
				applicationStatus = 10;
				if (isApprovedBySupevisor) {
					applyWfh.setSup_approval("Rejected");
				} else {
					applyWfh.setAddsup_approval("Rejected");
				}
			}
		} else {
			applyWfh.setCancel_approval_date(dateTime.toDate());
			applyWfh.setCancel_approval_by(wfhApproveRequest.getApproverId());
			if (wfhApproveRequest.getComment() != null) {
				applyWfh.setCancel_approval_note(wfhApproveRequest.getComment());
			} else {
				applyWfh.setCancel_approval_note("0");
			}
			// Finding application status
			if (wfhApproveRequest.getStatus().toString().equals("Approved")) {
				applicationStatus = 12;
				applyWfh.setCancel_approval("Approved");

			} else {
				applicationStatus = 13;
				applyWfh.setCancel_approval("Rejected");
			}
		}
		applyWfh.setApplication_status(applicationStatus);
		this.applyWfhRepository.saveAndFlush(applyWfh);
		Boolean result = false;
		if (applyWfh.getApplication_status().equals(9) || applyWfh.getApplication_status().equals(12)
				|| applyWfh.getApplication_status().equals(10) || applyWfh.getApplication_status().equals(13)) {
			result = true;
		}
		// Email and push notification to Requester
		this.sendWfhApproveEmail(applyWfh);
		this.sendWfhApprovePushNotification(applyWfh, wfhApproveRequest.getApproverId());

		return result;
	}

	// Apply wfh email
	private void sendWorkFromHomeEmail(ApplyWfh applyWfh) {
		logger.debug("Sedning wfh email");
		String supevisor_name = null;
		String gender;
		Integer emp_id;
		String emp_name;
		Integer no_of_days = applyWfh.getNo_of_days().intValue();
		Date from_date = applyWfh.getFrom_date();
		Date to_date = applyWfh.getTo_date();
		String reason = applyWfh.getWfh_reason();
		EmpPersonal empPersonal = applyWfh.getEmpPersonal();
		EmpEmployment empEmployment = this.empEmploymentRepository.findOne(empPersonal.getId());
		StringBuilder nameBuilder = new StringBuilder();
		nameBuilder.append(empPersonal.getEmp_fname()).append(" ").append(empPersonal.getEmp_mname()).append(" ")
				.append(empPersonal.getEmp_lname());
		emp_name = nameBuilder.toString();
		gender = empPersonal.getEmp_gender();
		emp_id = empEmployment.getId();
		EmpPersonal superVisor = this.empEmploymentRepository.getSupervisor(empEmployment.getId());
		EmpEmployment supervisorEmployment = this.empEmploymentRepository.findOne(superVisor.getId());
		supevisor_name = superVisor.getEmp_fname();
		String vertical_name = empEmployment.getVertical().getVertical();
		String designation = empEmployment.getDesignation().getDesignation();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String subject = "Work from Home Request - " + emp_name;
		Map<String, Object> emailModel = new HashedMap();
		emailModel.put("supevisor_name", supevisor_name);
		emailModel.put("gender", gender);
		emailModel.put("emp_id", emp_id);
		emailModel.put("emp_name", emp_name);
		emailModel.put("no_of_days", no_of_days);
		emailModel.put("from_date", sdf.format(from_date));
		emailModel.put("to_date", sdf.format(to_date));
		emailModel.put("reason", reason);
		emailModel.put("vertical_name", vertical_name);
		emailModel.put("designation", designation);
		emailModel.put("emailFrom", empEmployment.getEmp_email());
		emailModel.put("subject", subject);
		emailModel.put("email", supervisorEmployment.getEmp_email());
		// Sending email to supervisor
		try {
			this.emailService.sendApplyWfhEmail(emailModel);
		} catch (MessagingException e) {
			logger.error("Error occured while sending email", e);
		}
		if (applyWfh.getAdd_sup_id() != null && applyWfh.getAdd_sup_id() != 0
				&& !applyWfh.getAdd_sup_id().equals(superVisor.getId())) {
			EmpEmployment additionalSupervisor = this.empEmploymentRepository.findOne(applyWfh.getAdd_sup_id());
			EmpPersonal additionalSupervisorPersonal = this.empPersonalRepository.findOne(applyWfh.getAdd_sup_id());
			if (additionalSupervisor != null && additionalSupervisorPersonal != null) {
				emailModel.put("email", additionalSupervisor.getEmp_email());
				emailModel.put("supevisor_name", additionalSupervisorPersonal.getEmp_fname());
				try {
					this.emailService.sendApplyWfhEmail(emailModel);
				} catch (MessagingException e) {
					logger.error("Error occured while sending email", e);
				}
			}
		}

	}

	private void sendWfhCancellEmail(ApplyWfh applyWfh) {
		logger.debug("Sedning wfh email");
		String supevisor_name = null;
		String gender;
		Integer emp_id;
		String emp_name;
		Integer no_of_days = applyWfh.getNo_of_days().intValue();
		Date from_date = applyWfh.getFrom_date();
		Date to_date = applyWfh.getTo_date();
		String reason = applyWfh.getWfh_reason();
		String cancell_reason = applyWfh.getCancel_reason();
		EmpPersonal empPersonal = applyWfh.getEmpPersonal();
		EmpEmployment empEmployment = this.empEmploymentRepository.findOne(empPersonal.getId());
		StringBuilder nameBuilder = new StringBuilder();
		nameBuilder.append(empPersonal.getEmp_fname()).append(" ").append(empPersonal.getEmp_mname()).append(" ")
				.append(empPersonal.getEmp_lname());
		emp_name = nameBuilder.toString();
		gender = empPersonal.getEmp_gender();
		emp_id = empEmployment.getId();
		EmpPersonal superVisor = this.empEmploymentRepository.getSupervisor(empEmployment.getId());
		EmpEmployment supervisorEmployment = this.empEmploymentRepository.findOne(superVisor.getId());
		supevisor_name = superVisor.getEmp_fname();
		String vertical_name = empEmployment.getVertical().getVertical();
		String designation = empEmployment.getDesignation().getDesignation();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String subject = "Work from Home Cancellation Request - " + emp_name;
		Map<String, Object> emailModel = new HashedMap();
		emailModel.put("supevisor_name", supevisor_name);
		emailModel.put("gender", gender);
		emailModel.put("emp_id", emp_id);
		emailModel.put("emp_name", emp_name);
		emailModel.put("no_of_days", no_of_days);
		emailModel.put("from_date", sdf.format(from_date));
		emailModel.put("to_date", sdf.format(to_date));
		emailModel.put("reason", reason);
		emailModel.put("cancell_reason", cancell_reason);
		emailModel.put("vertical_name", vertical_name);
		emailModel.put("designation", designation);
		emailModel.put("emailFrom", empEmployment.getEmp_email());
		emailModel.put("subject", subject);
		emailModel.put("email", supervisorEmployment.getEmp_email());
		// Sending email to supervisor
		try {
			this.emailService.sendCancellWfhEmail(emailModel);
		} catch (MessagingException e) {
			logger.error("Error occured while sending email", e);
		}
		if (applyWfh.getAdd_sup_id() != null && applyWfh.getAdd_sup_id() != 0
				&& !applyWfh.getAdd_sup_id().equals(superVisor.getId())) {
			EmpEmployment additionalSupervisor = this.empEmploymentRepository.findOne(applyWfh.getAdd_sup_id());
			EmpPersonal additionalSupervisorPersonal = this.empPersonalRepository.findOne(applyWfh.getAdd_sup_id());
			if (additionalSupervisor != null && additionalSupervisorPersonal != null) {
				emailModel.put("email", additionalSupervisor.getEmp_email());
				emailModel.put("supevisor_name", additionalSupervisorPersonal.getEmp_fname());
				try {
					this.emailService.sendCancellWfhEmail(emailModel);
				} catch (MessagingException e) {
					logger.error("Error occured while sending email", e);
				}
			}
		}
	}

	private void sendWfhApproveEmail(ApplyWfh applyWfh) {
		logger.debug("Sedning wfh Approve email");
		try {
			String supevisor_name = null;
			String gender;
			String emp_name;
			Integer no_of_days = applyWfh.getNo_of_days().intValue();
			Date from_date = applyWfh.getFrom_date();
			Date to_date = applyWfh.getTo_date();
			String reason = applyWfh.getWfh_reason();
			EmpPersonal empPersonal = applyWfh.getEmpPersonal();
			EmpEmployment empEmployment = this.empEmploymentRepository.findOne(empPersonal.getId());
			EmpPersonal superVisor = this.empEmploymentRepository.getSupervisor(empEmployment.getId());
			EmpEmployment supervisorEmployment = this.empEmploymentRepository.findOne(superVisor.getId());
			emp_name = empPersonal.getEmp_fname();
			String email = empEmployment.getEmp_email();
			String emailFrom = supervisorEmployment.getEmp_email();
			StringBuilder nameBuilder = new StringBuilder();
			nameBuilder.append(superVisor.getEmp_fname()).append(" ").append(superVisor.getEmp_mname()).append(" ")
					.append(superVisor.getEmp_lname());
			supevisor_name = nameBuilder.toString();
			gender = superVisor.getEmp_gender();
			String status = null;
			String request_name = null;
			String note = null;
			String subject = null;
			if (applyWfh.getApplication_status().equals(9) || applyWfh.getApplication_status().equals(12)) {
				status = "Approved";
				request_name = "Work from home request";
				if (applyWfh.getApplication_status().equals(9)) {
					if (applyWfh.getSup_approval_note() != null && !applyWfh.getSup_approval_note().equals("0")) {
						note = applyWfh.getSup_approval_note();
					} else if ((applyWfh.getSup_approval_note() == null || applyWfh.getSup_approval_note().equals("0"))
							&& applyWfh.getAddsup_approval_note() != null
							&& !applyWfh.getAddsup_approval_note().equals("0")) {
						note = applyWfh.getAddsup_approval_note();
					}
					subject = "Work from Home request Approved";
				} else {
					if (applyWfh.getCancel_approval_note() != null && !applyWfh.getCancel_approval_note().equals("0")) {
						note = applyWfh.getCancel_approval_note();
					}

					subject = "Work from Home Cancellation request Approved";
				}

			} else if (applyWfh.getApplication_status().equals(10) || applyWfh.getApplication_status().equals(13)) {
				status = "Rejected";
				request_name = "Work from home cancellation request";
				if (applyWfh.getApplication_status().equals(10)) {
					if (applyWfh.getSup_approval_note() != null && !applyWfh.getSup_approval_note().equals("0")) {
						note = applyWfh.getSup_approval_note();
					} else if ((applyWfh.getSup_approval_note() == null || applyWfh.getSup_approval_note().equals("0"))
							&& applyWfh.getAddsup_approval_note() != null
							&& !applyWfh.getAddsup_approval_note().equals("0")) {
						note = applyWfh.getAddsup_approval_note();
					}
					subject = "Work from Home request Rejected";
				} else {
					if (applyWfh.getCancel_approval_note() != null && !applyWfh.getCancel_approval_note().equals("0")) {
						note = applyWfh.getCancel_approval_note();
					}
					subject = "Work from Home Cancellation request Rejected";
				}
			}
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Map<String, Object> emailModel = new HashedMap();
			emailModel.put("supevisor_name", supevisor_name);
			emailModel.put("gender", gender);
			emailModel.put("emp_name", emp_name);
			emailModel.put("no_of_days", no_of_days);
			emailModel.put("from_date", sdf.format(from_date));
			emailModel.put("to_date", sdf.format(to_date));
			emailModel.put("reason", reason);
			emailModel.put("emailFrom", empEmployment.getEmp_email());
			emailModel.put("status", status);
			emailModel.put("email", supervisorEmployment.getEmp_email());
			emailModel.put("status", status);
			emailModel.put("request_name", request_name);
			emailModel.put("note", note);
			emailModel.put("emailFrom", emailFrom);
			emailModel.put("email", email);
			emailModel.put("subject", subject);
			this.emailService.sendApproveWfhEmail(emailModel);
		} catch (Exception e) {
			logger.error("Error occured while sending wfh approve email", e);
		}

	}

	private void sendWfhPushNotification(ApplyWfh applyWfh) {
		try {
			logger.debug("Sending Push notification for wfh");
			EmpEmployment empEmployment = null;
			WfhDTO wfhDTO = null;
			if (applyWfh != null && applyWfh.getEmpPersonal() != null) {
				empEmployment = this.empEmploymentRepository.findOne(applyWfh.getEmpPersonal().getId());
			}
			if (empEmployment != null) {
				wfhDTO = new WfhDTO(applyWfh, empEmployment);
			}
			if (wfhDTO != null && wfhDTO.getId() != null) {
				Integer empId = empEmployment.getId();
				EmpPersonal superVisor = this.empEmploymentRepository.getSupervisor(empId);
				ObjectMapper mapper = new ObjectMapper();
				List<String> fcmIdList = new ArrayList();
				if (superVisor != null) {
					FcmDetail fcm = this.paymentLookupService.getEmployeeFcmDetail(superVisor.getId());
					if (fcm != null) {
						fcmIdList.add(fcm.getFcm_id());
					}
				}

				if (applyWfh.getAdd_sup_id() != null && applyWfh.getAdd_sup_id() != 0
						&& !applyWfh.getAdd_sup_id().equals(superVisor.getId())) {
					FcmDetail fcm = this.paymentLookupService.getEmployeeFcmDetail(applyWfh.getAdd_sup_id());
					if (fcm != null) {
						fcmIdList.add(fcm.getFcm_id());
					}
				}

				this.fcmService.sendMessage(null, fcmIdList, mapper.writeValueAsString(wfhDTO), "wfh");
			}
		} catch (Exception e) {
			logger.error("Error occured while sending push notification", e);
		}
	}

	private void sendWfhApprovePushNotification(ApplyWfh applyWfh, Integer approverId) {
		logger.debug("Sending wfh approve push notification");
		try {
			ObjectMapper mapper = new ObjectMapper();
			EmpPersonal empPersonal = this.empPersonalRepository.findOne(approverId);
			if (empPersonal != null) {
				GlobalPushDTO globalPushDTO = new GlobalPushDTO();
				String message = null;
				String title = null;
				StringBuilder nameBuilder = new StringBuilder();
				nameBuilder.append(empPersonal.getEmp_fname()).append(" ");
				nameBuilder.append(empPersonal.getEmp_mname()).append(" ");
				nameBuilder.append(empPersonal.getEmp_lname());
				String supervisorName = nameBuilder.toString();
				String avatar = empPersonal.getEmp_avatar();
				StringBuilder stringBuilder = new StringBuilder();
				Integer applicationStatus = applyWfh.getApplication_status();
				if (applicationStatus.equals(9) || applicationStatus.equals(10)) {
					stringBuilder.append("The Work from Home request you have applied on")
							.append(applyWfh.getApplied_date()).append(" ");
					if (applicationStatus.equals(9)) {
						stringBuilder.append("has been approved by ").append(supervisorName).append("\n");
						title = "Work from home approved";
					} else {
						stringBuilder.append("has been rejected by ").append(supervisorName).append("\n");
						title = "Work from home rejected";
					}
					if (applyWfh.getSup_approval_note() != null && !applyWfh.getSup_approval_note().equals("0")) {
						stringBuilder.append("Note : ").append(applyWfh.getSup_approval_note());
					} else if (applyWfh.getAddsup_approval_note() != null
							&& !applyWfh.getAddsup_approval_note().equals("0")) {
						stringBuilder.append("Note : ").append(applyWfh.getAddsup_approval_note());
					}
				} else if (applicationStatus.equals(12) || applicationStatus.equals(13)) {
					stringBuilder.append("The Work from Home cancellation request you have applied on")
							.append(applyWfh.getCancel_date()).append(" ");
					;
					if (applicationStatus.equals(12)) {
						stringBuilder.append(" has been approved by ").append(supervisorName).append("\n");
						title = "Work from home cancellation approved";
					} else {
						stringBuilder.append(" has been rejected by ").append(supervisorName).append("\n");
						title = "Work from home cancellation rejected";
					}
					if (applyWfh.getCancel_approval_note() != null && !applyWfh.getCancel_approval_note().equals("0")) {
						stringBuilder.append("Note : ").append(applyWfh.getCancel_approval_note());
					}
				}
				message = stringBuilder.toString();
				FcmDetail fcmDetail = this.paymentLookupService.getEmployeeFcmDetail(applyWfh.getEmpPersonal().getId());
				if (fcmDetail != null && message != null && title != null) {

					globalPushDTO.setTitle(title);
					globalPushDTO.setBody(message);
					globalPushDTO.setAvathar(avatar);
					this.fcmService.sendMessage(fcmDetail.getFcm_id(), null, mapper.writeValueAsString(globalPushDTO),
							"others");
				}

			}
		} catch (Exception e) {
			logger.error("Error occured while push notification", e);
		}

	}
}
