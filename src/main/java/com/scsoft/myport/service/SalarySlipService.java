package com.scsoft.myport.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.scsoft.myport.domain.EmpEmployment;
import com.scsoft.myport.domain.EmpPersonal;
import com.scsoft.myport.domain.FcmDetail;
import com.scsoft.myport.domain.SalarySlipRequests;
import com.scsoft.myport.domain.SalarySlipRequestsYear;
import com.scsoft.myport.error.OperationNotPossibleException;
import com.scsoft.myport.repository.EmpEmploymentRepository;
import com.scsoft.myport.repository.EmpPersonalRepository;
import com.scsoft.myport.repository.SalarySlipRequestsRepository;
import com.scsoft.myport.repository.SalarySlipRequestsYearRepository;
import com.scsoft.myport.rest.request.SalarySlipRequest;
import com.scsoft.myport.rest.response.SalarySlipDetails;
import com.scsoft.myport.service.DTO.GlobalPushDTO;

@Service
public class SalarySlipService {
	@Autowired
	private SalarySlipRequestsRepository salarySlipRequestsRepository;
	@Autowired
	private SalarySlipRequestsYearRepository salarySlipRequestsYearRepository;
	@Autowired
	private EmpPersonalRepository empPersonalRepository;
	@Autowired
	private EmpEmploymentRepository empEmploymentRepository;
	@Autowired
	private PaymentLookupService paymentLookupService;
	@Autowired
	private EmailService emailService;
	@Autowired
	private FcmService fcmService;
	private Logger logger = LoggerFactory.getLogger(SalarySlipService.class);

	public SalarySlipRequests saveSalarySlipRequest(SalarySlipRequest salarySlipRequest)
			throws OperationNotPossibleException {

		logger.debug("Saving salary slip request into DB");
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);
		EmpEmployment empEmployment = empEmploymentRepository.findOne(salarySlipRequest.getRequested_by());
		Date employeeDateOfJoining = empEmployment.getEmp_doj();
		LocalDate dateOfJoining = LocalDate.fromDateFields(employeeDateOfJoining);
		if (isSalarySlipRequestedBeforeJoiningDate(dateOfJoining, salarySlipRequest)) {
			throw new OperationNotPossibleException("error",
					"Not possible to apply Salary slip for the date before joining");
		}
		if (isSalarySlipRequestForFutureDate(salarySlipRequest)) {
			throw new OperationNotPossibleException("error", "Not possible to apply Salary slip for the future dates");
		}

		SalarySlipRequests salarySlipRequests = new SalarySlipRequests();
		salarySlipRequests.setRequested_by(salarySlipRequest.getRequested_by());
		salarySlipRequests.setRequested_date(dateTime.toDate());
		salarySlipRequests.setRequest_status("Requested to HR");
		salarySlipRequests.setRequest_reason(salarySlipRequest.getRequest_reason());
		salarySlipRequestsRepository.saveAndFlush(salarySlipRequests);
		Integer id = salarySlipRequests.getId();
		SalarySlipRequestsYear salarySlipRequestsYear = new SalarySlipRequestsYear();
		salarySlipRequestsYear.setRequest_id(id);
		salarySlipRequestsYear.setYear(salarySlipRequest.getRequest_year());
		String months = salarySlipRequest.getRequest_months().stream().map(item -> item.toString())
				.collect(Collectors.joining(","));
		salarySlipRequestsYear.setMonths(months);
		salarySlipRequestsYearRepository.saveAndFlush(salarySlipRequestsYear);
		if (salarySlipRequestsYear.getId() != null) {
			salarySlipRequests.setRequest_year(salarySlipRequestsYear.getYear().toString());
			salarySlipRequests.setRequest_months(salarySlipRequestsYear.getMonths().toString());
			salarySlipRequestsRepository.saveAndFlush(salarySlipRequests);
			this.sendSalarySlipEmail(salarySlipRequests);
			this.sendSalarySlipPushNotification(salarySlipRequests);
		}
		return salarySlipRequests;

	}

	// @Async
	private void sendSalarySlipPushNotification(SalarySlipRequests salarySlipRequests) {
		logger.debug("sending push notification for salary slip request");
		ObjectMapper mapper = new ObjectMapper();
		GlobalPushDTO globalPushDTO = new GlobalPushDTO();
		try {
			String title;
			String body;
			String avathar;
			String empName;
			EmpPersonal empPersonal = this.empPersonalRepository.findOne(salarySlipRequests.getRequested_by());
			avathar = empPersonal.getEmp_avatar();
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(empPersonal.getEmp_fname()).append(" ").append(empPersonal.getEmp_mname()).append(" ")
					.append(empPersonal.getEmp_lname());
			empName = stringBuilder.toString();
			title = "Salary slip request by - " + empName;
			StringBuilder content = new StringBuilder();
			content.append("Year : ").append(salarySlipRequests.getRequest_year()).append("\n").append(" Months : ")
					.append(this.getMonths(salarySlipRequests.getRequest_months())).append("\n").append("Reason : ")
					.append(salarySlipRequests.getRequest_reason());
			body = content.toString();
			globalPushDTO.setAvathar(avathar);
			globalPushDTO.setBody(body);
			globalPushDTO.setTitle(title);
			List<String> fcmIdList = new ArrayList();
			List<EmpEmployment> adminList = this.empEmploymentRepository.getAdmins();
			for (EmpEmployment admin : adminList) {
				FcmDetail fcmDetail = this.paymentLookupService.getEmployeeFcmDetail(admin.getId());
				if (fcmDetail != null) {
					fcmIdList.add(fcmDetail.getFcm_id());
				}
			}
			EmpEmployment hrManager = this.empEmploymentRepository.getHrManager();
			if (hrManager != null) {
				FcmDetail fcmDetail = this.paymentLookupService.getEmployeeFcmDetail(hrManager.getId());
				if (fcmDetail != null) {
					fcmIdList.add(fcmDetail.getFcm_id());
				}
			}

			fcmService.sendMessage(null, fcmIdList, mapper.writeValueAsString(globalPushDTO), "others");

		} catch (Exception e) {
			logger.error("Error occured while sending push notification for salary slip request", e);
		}
	}

	public List<SalarySlipDetails> getSalarySlipDetailsByEmployee(Integer empId) {
		try {
			return salarySlipRequestsRepository.getSalarySlipDetailsByEmployee(empId);
		} catch (Exception e) {
			logger.error("Error occured while fethcing salarySlip details", e);
			return null;
		}
	}

	// @Async
	private void sendSalarySlipEmail(SalarySlipRequests salarySlipRequests) {
		logger.debug("Sending email to hr manager for salary slip request");
		try {
			String empName;
			String approverName;
			String gender;
			EmpEmployment empEmployment = this.empEmploymentRepository.findOne(salarySlipRequests.getRequested_by());
			EmpPersonal empPersonal = this.empPersonalRepository.findOne(salarySlipRequests.getRequested_by());
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(empPersonal.getEmp_fname()).append(" ").append(empPersonal.getEmp_mname()).append(" ")
					.append(empPersonal.getEmp_lname());
			empName = stringBuilder.toString();
			gender = empPersonal.getEmp_gender();
			String year = salarySlipRequests.getRequest_year();
			String months = salarySlipRequests.getRequest_months();
			String reason = salarySlipRequests.getRequest_reason();
			String emailFrom = empEmployment.getEmp_email();
			String jobLocation = empEmployment.getLocations().getName();
			String subject = "Salary slip request";
			Date doj = empEmployment.getEmp_doj();
			Integer empId = empEmployment.getId();
			Map<String, Object> emailModel = new HashMap();
			emailModel.put("gender", gender);
			emailModel.put("emp_name", empName);
			emailModel.put("year", year);
			String monthValue = this.getMonths(months);
			emailModel.put("months", monthValue);
			emailModel.put("reason", reason);
			emailModel.put("doj", doj);
			emailModel.put("empId", empId);
			emailModel.put("emailFrom", emailFrom);
			emailModel.put("subject", subject);
			emailModel.put("email", "hr.india@scsoft.com");
			emailModel.put("approover_name", "HR India");
			emailModel.put("jobLocation", jobLocation);
			this.emailService.sendSalarySlipEmail(emailModel);
		} catch (Exception e) {
			logger.error("Error occured during sending email", e);
		}

	}

	private String getMonths(String monthsValues) {
		String returnValue = null;
		String[] months = new String[13];
		months[0] = null;
		months[1] = "January";
		months[2] = "February";
		months[3] = "March";
		months[4] = "April";
		months[5] = "May";
		months[6] = "June";
		months[7] = "July";
		months[8] = "August";
		months[9] = "September";
		months[10] = "October";
		months[11] = "November";
		months[12] = "December";
		List<String> items = Arrays.asList(monthsValues.split("\\s*,\\s*"));
		StringBuilder stringBuilder = new StringBuilder();
		for (String item : items) {
			stringBuilder.append(months[Integer.parseInt(item)]).append(",");
		}
		String result = stringBuilder.toString();
		returnValue = result.substring(0, result.length() - 1);
		return returnValue;
	}

	private Boolean isSalarySlipRequestedBeforeJoiningDate(LocalDate joiningDate, SalarySlipRequest request) {
		if (request.getRequest_year() < joiningDate.getYear()) {
			return true;
		}
		if (joiningDate.getYear() == request.getRequest_year()) {

			Boolean isRequestMonthBeforeJoining = request.getRequest_months().stream()
					.filter(month -> month < joiningDate.getMonthOfYear()).findAny().isPresent();
			return isRequestMonthBeforeJoining;
		}
		return false;

	}

	private Boolean isSalarySlipRequestForFutureDate(SalarySlipRequest request) {
		LocalDate currentDate = LocalDate.now();
		if(request.getRequest_year() > currentDate.getYear()) {
			return true;
		}
		if (currentDate.getYear() == request.getRequest_year()) {
			Boolean isSalarySlipRequestForFutureDate = request.getRequest_months().stream()
					.filter(month -> month > currentDate.getMonthOfYear()).findAny().isPresent();
			return isSalarySlipRequestForFutureDate;
		}
		return false;
	}

}
