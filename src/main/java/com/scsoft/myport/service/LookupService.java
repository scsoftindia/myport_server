package com.scsoft.myport.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.scsoft.myport.domain.EmpEmployment;
import com.scsoft.myport.domain.HolidayAvailability;
import com.scsoft.myport.domain.LeaveAvailability;
import com.scsoft.myport.error.NotFoundException;
import com.scsoft.myport.repository.ApplyHolidayRepository;
import com.scsoft.myport.repository.ApplyLeaveRepository;
import com.scsoft.myport.repository.ApplyWfhRepository;
import com.scsoft.myport.repository.EmpEmploymentRepository;
import com.scsoft.myport.repository.HolidayAvailabilityRepository;
import com.scsoft.myport.repository.LeaveAvailabilityRepository;
import com.scsoft.myport.repository.PaymentAdviceRepository;
import com.scsoft.myport.repository.ServicedistruptionRepository;
import com.scsoft.myport.service.DTO.DashboardCountDTO;
import com.scsoft.myport.service.DTO.LeaveAvailabilityDTO;

@Service
public class LookupService {

	private final Logger logger = LoggerFactory.getLogger(LookupService.class);
	@Autowired
	private EmpEmploymentRepository empEmploymentRepository;
	@Autowired
	private ApplyLeaveRepository applyLeaveRepository;
	@Autowired
	private ApplyHolidayRepository applyHolidayRepository;
	@Autowired
	private PaymentAdviceRepository paymentAdviceRepository;
	@Autowired
	private LeaveAvailabilityRepository leaveAvailabilityRepository;
	@Autowired
	private HolidayAvailabilityRepository holidayAvailabilityRepository;
	@Autowired
	private ApplyWfhRepository applyWfhRepository;
	@Autowired
	private ServicedistruptionRepository servicedistruptionRepository;

	public DashboardCountDTO getAllDashBoardCounts(Integer empId) throws NotFoundException {
		DashboardCountDTO dashboardCountDTO = new DashboardCountDTO();
		logger.debug("Fething Dashboard counts from DB");
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);
		EmpEmployment empEmployment = this.empEmploymentRepository.findOne(empId);
		if(empEmployment == null) {
			throw new NotFoundException("user","Invalid user");
		}
		String userRole = empEmployment.getUser_role();
		Boolean isSupevisor = this.empEmploymentRepository.isSupervisor(empId);
		if("ADMIN".equals(userRole) || "MANAGER".equals(userRole) || isSupevisor) {
			Integer leaveRequestCount = this.applyLeaveRepository.getLeaveRequestCount(empId);
			Integer holidayRequestCount = this.applyHolidayRepository.getHolidayRequestCount(empId);
			Integer wfhCount = this.applyWfhRepository.getWfhRequestCount(empId);
			dashboardCountDTO.setLeaveRequestPending(leaveRequestCount);
			dashboardCountDTO.setHolidaysRequestPending(holidayRequestCount);	
			dashboardCountDTO.setWfhPending(wfhCount);
		}
		if("ADMIN".equals(userRole)){
			Integer paymentAdviceRequestCount = this.paymentAdviceRepository.getAdminApprovalCount();
			Integer forwadedCount = this.paymentAdviceRepository.getAdminForwardApprovalCount(empId);
			dashboardCountDTO.setPaymentAdvicePending(paymentAdviceRequestCount - forwadedCount);
		}
		if(!"ADMIN".equals(userRole) ) {
		Integer year = dateTime.getYear();
		HolidayAvailability holidayAvailability = holidayAvailabilityRepository.getUserHoliDayAvailability(empId,
				year);
		BigDecimal hlidaysRemaining = holidayAvailability.getHolidays_remaining();
		Integer holidaysTaken = this.applyHolidayRepository.holidaysTaken(empId, dateTime.getYear());
		if(hlidaysRemaining != null) {
		dashboardCountDTO.setHolidaysRemaining(hlidaysRemaining.intValueExact());
		}else {
			dashboardCountDTO.setHolidaysRemaining(null);
		}
		dashboardCountDTO.setHolidaysTaken(holidaysTaken);
		List<LeaveAvailability> leaveAvailabilityList = leaveAvailabilityRepository.getLeaveAvailabilityByEmployId(empId,
				year);
		if(!CollectionUtils.isEmpty(leaveAvailabilityList)) {
			BigDecimal causualLeaveCount = leaveAvailabilityList.stream()
					                    .filter(item -> item.getLeaveTypes().getId().equals(1))
					                     .findAny().get().getLeaves_remaining();
			if(causualLeaveCount != null && !causualLeaveCount.equals(BigDecimal.ZERO)) {
				dashboardCountDTO.setCasualLeaveCount(causualLeaveCount.doubleValue());
			}
		}
		List<LeaveAvailabilityDTO> leaveAvailabilityDTOs = new ArrayList();
		for(LeaveAvailability leaveAvailability:leaveAvailabilityList) {
			LeaveAvailabilityDTO leaveAvailabilityDTO = new LeaveAvailabilityDTO();
			String leaveType = leaveAvailability.getLeaveTypes().getLeave_type_name();
			Double leavesRemaining = leaveAvailability.getLeaves_remaining().doubleValue();
			Double leavesUsed = 0.0;;
			if(leaveAvailability.getLeaves_used() != null) {
				 leavesUsed = leaveAvailability.getLeaves_used().doubleValue();
			}else {
				
			}
			leaveAvailabilityDTO.setLeaveType(leaveType);
			leaveAvailabilityDTO.setLeavesRemaining(leavesRemaining);
			leaveAvailabilityDTO.setLeavesTaken(leavesUsed);
			leaveAvailabilityDTOs.add(leaveAvailabilityDTO);
		}	
		dashboardCountDTO.setLeaveAvailabilityDTOs(leaveAvailabilityDTOs);
		//Integer wfhTaken = this.applyWfhService.getWfhTaken(empId);
		//Integer wfhRemaining = 3 - wfhTaken;
		//dashboardCountDTO.setWfhTaken(wfhTaken);
		//dashboardCountDTO.setWfhRemaining(wfhRemaining);
		}
		Integer sdnCount = servicedistruptionRepository.getOpenSdnCount();
		dashboardCountDTO.setSdn_count(sdnCount);
		
		
		
		
		return dashboardCountDTO;
		
	}

}
