package com.scsoft.myport.service;

import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import freemarker.template.Configuration;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.apache.commons.lang3.CharEncoding;

@Service("emailService")
public class EmailService {
	private final Logger logger = LoggerFactory.getLogger(EmailService.class);
	@Value("${myport.email.from}")
	private String emailFrom;
	@Autowired
	private EmailBaseService emailBaseService;

	// @Async
	public void sendHolidayRequestEmail(Map<String, Object> model) throws MessagingException {
		String content;
		Configuration cfg = new Configuration(Configuration.getVersion());
		try {
			cfg.setClassForTemplateLoading(this.getClass(), "/templates/");
			content = (FreeMarkerTemplateUtils.processTemplateIntoString(cfg.getTemplate("holiday_request.ftl"),
					model));
		} catch (Exception e) {
			logger.error("Error occurred while sending mail for holiday request -", e);
			throw new MessagingException("Error occurred while sending mail");
		}

		this.emailBaseService.sendEmail((String) model.get("email"), (String) model.get("emailFrom"),
				(String) model.get("subject"), content, false, true);

	}

	public void sendHolidayApprovalEmail(Map<String, Object> model) throws MessagingException {
		String content;
		Configuration cfg = new Configuration(Configuration.getVersion());
		try {
			cfg.setClassForTemplateLoading(this.getClass(), "/templates/");
			content = (FreeMarkerTemplateUtils.processTemplateIntoString(cfg.getTemplate("holiday_approve.ftl"),
					model));
		} catch (Exception e) {
			logger.error("Error occurred while sending mail for leave request approval -", e);
			throw new MessagingException("Error occurred while sending mail");
		}

		this.emailBaseService.sendEmail((String) model.get("email"), (String) model.get("emailFrom"),
				(String) model.get("subject"), content, false, true);
	}

	// @Async
	public void sendLeaveRequestEmail(Map<String, Object> model) throws MessagingException {
		String content;
		Configuration cfg = new Configuration(Configuration.getVersion());
		try {
			cfg.setClassForTemplateLoading(this.getClass(), "/templates/");
			content = (FreeMarkerTemplateUtils.processTemplateIntoString(cfg.getTemplate("apply_leave.ftl"), model));
		} catch (Exception e) {
			logger.error("Error occurred while sending mail for leave request -", e);
			throw new MessagingException("Error occurred while sending mail");
		}

		this.emailBaseService.sendEmail((String) model.get("email"), (String) model.get("emailFrom"),
				(String) model.get("subject"), content, false, true);

	}

	// @Async
	public void sendLeaveCancelRequestEmail(Map<String, Object> model) throws MessagingException {
		String content;
		Configuration cfg = new Configuration(Configuration.getVersion());
		try {
			cfg.setClassForTemplateLoading(this.getClass(), "/templates/");
			content = (FreeMarkerTemplateUtils.processTemplateIntoString(cfg.getTemplate("cancel_leave_request.ftl"),
					model));
		} catch (Exception e) {
			logger.error("Error occurred while sending mail for leave request -", e);
			throw new MessagingException("Error occurred while sending mail");
		}

		this.emailBaseService.sendEmail((String) model.get("email"), (String) model.get("emailFrom"),
				(String) model.get("subject"), content, false, true);

	}

	// @Async
	public void sendLeaveCancelEmail(Map<String, Object> model) throws MessagingException {
		String content;
		Configuration cfg = new Configuration(Configuration.getVersion());
		try {
			cfg.setClassForTemplateLoading(this.getClass(), "/templates/");
			content = (FreeMarkerTemplateUtils.processTemplateIntoString(cfg.getTemplate("leave_cancel.ftl"), model));
		} catch (Exception e) {
			logger.error("Error occurred while sending mail for leave request -", e);
			throw new MessagingException("Error occurred while sending mail");
		}

		this.emailBaseService.sendEmail((String) model.get("email"), (String) model.get("emailFrom"),
				(String) model.get("subject"), content, false, true);

	}

	// @Async
	public void sendLeaveApprovalEmail(Map<String, Object> model) throws MessagingException {
		String content;
		Configuration cfg = new Configuration(Configuration.getVersion());
		try {
			cfg.setClassForTemplateLoading(this.getClass(), "/templates/");
			content = (FreeMarkerTemplateUtils.processTemplateIntoString(cfg.getTemplate("leave_approve.ftl"), model));
		} catch (Exception e) {
			logger.error("Error occurred while sending mail for leave request approval -", e);
			throw new MessagingException("Error occurred while sending mail");
		}

		this.emailBaseService.sendEmail((String) model.get("email"), (String) model.get("emailFrom"),
				(String) model.get("subject"), content, false, true);
	}

	// @Async
	public void sendAnnoucementEmail(Map<String, Object> model) throws MessagingException {
		String content;
		Configuration cfg = new Configuration(Configuration.getVersion());
		try {
			cfg.setClassForTemplateLoading(this.getClass(), "/templates/");
			content = (FreeMarkerTemplateUtils.processTemplateIntoString(cfg.getTemplate("announcement.ftl"), model));
		} catch (Exception e) {
			logger.error("Error occurred while sending mail for Announcement -", e);
			throw new MessagingException("Error occurred while sending mail");
		}

		this.emailBaseService.sendEmailBcc((String) model.get("email"), (String) model.get("subject"), content, false,
				true);
	}

	// @Async
	public void sendBirthdayWishEmail(Map<String, Object> model) throws MessagingException {
		String content;
		Configuration cfg = new Configuration(Configuration.getVersion());
		try {
			cfg.setClassForTemplateLoading(this.getClass(), "/templates/");
			content = (FreeMarkerTemplateUtils.processTemplateIntoString(cfg.getTemplate("birthday_wish.ftl"), model));
		} catch (Exception e) {
			logger.error("Error occurred while sending mail for Announcement -", e);
			throw new MessagingException("Error occurred while sending mail");
		}

		this.emailBaseService.sendEmail((String) model.get("email"), (String) model.get("emailFrom"),
				(String) model.get("subject"), content, false, true);
	}

	// @Async
	public void sendComplaintEmail(Map<String, Object> model) throws MessagingException {
		String content;
		Configuration cfg = new Configuration(Configuration.getVersion());
		try {
			cfg.setClassForTemplateLoading(this.getClass(), "/templates/");
			content = (FreeMarkerTemplateUtils.processTemplateIntoString(cfg.getTemplate("compiants_concern.ftl"),
					model));
		} catch (Exception e) {
			logger.error("Error occurred while sending mail for Complaint -", e);
			throw new MessagingException("Error occurred while sending mail");
		}

		this.emailBaseService.sendEmail((String) model.get("email"), (String) model.get("emailFrom"),
				(String) model.get("subject"), content, false, true);
	}

	// @Async
	public void sendComplaintResolveEmail(Map<String, Object> model) throws MessagingException {
		String content;
		Configuration cfg = new Configuration(Configuration.getVersion());
		try {
			cfg.setClassForTemplateLoading(this.getClass(), "/templates/");
			content = (FreeMarkerTemplateUtils
					.processTemplateIntoString(cfg.getTemplate("complaint_concern_resolve.ftl"), model));
		} catch (Exception e) {
			logger.error("Error occurred while sending mail for Complaint -", e);
			throw new MessagingException("Error occurred while sending mail");
		}

		this.emailBaseService.sendEmail((String) model.get("email"), (String) model.get("emailFrom"),
				(String) model.get("subject"), content, false, true);
	}

	// @Async
	public void sendNewCategoryEmail(Map<String, Object> model) throws MessagingException {
		String content;
		Configuration cfg = new Configuration(Configuration.getVersion());
		try {
			cfg.setClassForTemplateLoading(this.getClass(), "/templates/");
			content = (FreeMarkerTemplateUtils.processTemplateIntoString(cfg.getTemplate("new_category.ftl"), model));
		} catch (Exception e) {
			logger.error("Error occurred while sending mail for new Category", e);
			throw new MessagingException("Error occurred while sending mail");
		}

		this.emailBaseService.sendEmail((String) model.get("email"), (String) model.get("emailFrom"),
				(String) model.get("subject"), content, false, true);
	}

	// @Async
	public void sendCategoryApprooveEmail(Map<String, Object> model) throws MessagingException {
		String content;
		Configuration cfg = new Configuration(Configuration.getVersion());
		try {
			cfg.setClassForTemplateLoading(this.getClass(), "/templates/");
			content = (FreeMarkerTemplateUtils.processTemplateIntoString(cfg.getTemplate("category_approoval.ftl"),
					model));
		} catch (Exception e) {
			logger.error("Error occurred while sending mail for Category Approoval -", e);
			throw new MessagingException("Error occurred while sending mail");
		}

		this.emailBaseService.sendEmail((String) model.get("email"), (String) model.get("emailFrom"),
				(String) model.get("subject"), content, false, true);
	}

	// @Async
	public void sendNewSubCategoryEmail(Map<String, Object> model) throws MessagingException {
		String content;
		Configuration cfg = new Configuration(Configuration.getVersion());
		try {
			cfg.setClassForTemplateLoading(this.getClass(), "/templates/");
			content = (FreeMarkerTemplateUtils.processTemplateIntoString(cfg.getTemplate("new_subcategory.ftl"),
					model));
		} catch (Exception e) {
			logger.error("Error occurred while sending mail for new Category -", e);
			throw new MessagingException("Error occurred while sending mail");
		}

		this.emailBaseService.sendEmail((String) model.get("email"), (String) model.get("emailFrom"),
				(String) model.get("subject"), content, false, true);
	}

	// @Async
	public void sendSubCategoryApprooveEmail(Map<String, Object> model) throws MessagingException {
		String content;
		Configuration cfg = new Configuration(Configuration.getVersion());
		try {
			cfg.setClassForTemplateLoading(this.getClass(), "/templates/");
			content = (FreeMarkerTemplateUtils.processTemplateIntoString(cfg.getTemplate("sub_category_approoval.ftl"),
					model));
		} catch (Exception e) {
			logger.error("Error occurred while sending mail for sub Category Approoval -", e);
			throw new MessagingException("Error occurred while sending mail");
		}

		this.emailBaseService.sendEmail((String) model.get("email"), (String) model.get("emailFrom"),
				(String) model.get("subject"), content, false, true);
	}

	// @Async
	public void sendNewVendorEmail(Map<String, Object> model) throws MessagingException {
		String content;
		Configuration cfg = new Configuration(Configuration.getVersion());
		try {
			cfg.setClassForTemplateLoading(this.getClass(), "/templates/");
			content = (FreeMarkerTemplateUtils.processTemplateIntoString(cfg.getTemplate("new_vendor.ftl"), model));
		} catch (Exception e) {
			logger.error("Error occurred while sending mail for new vendor -", e);
			throw new MessagingException("Error occurred while sending mail");
		}

		this.emailBaseService.sendEmail((String) model.get("email"), (String) model.get("emailFrom"),
				(String) model.get("subject"), content, false, true);
	}

	// @Async
	public void sendVendorApprovalEmail(Map<String, Object> model) throws MessagingException {
		String content;
		Configuration cfg = new Configuration(Configuration.getVersion());
		try {
			cfg.setClassForTemplateLoading(this.getClass(), "/templates/");
			content = (FreeMarkerTemplateUtils.processTemplateIntoString(cfg.getTemplate("vendor_approoval.ftl"),
					model));
		} catch (Exception e) {
			logger.error("Error occurred while sending mail for new vendor approval -", e);
			throw new MessagingException("Error occurred while sending mail");
		}

		this.emailBaseService.sendEmail((String) model.get("email"), (String) model.get("emailFrom"),
				(String) model.get("subject"), content, false, true);
	}

	// public void sendPaymentAdviceEmail(Map<String, Object> model) throws
	// MessagingException {
	// String content;
	// Configuration cfg = new Configuration(Configuration.getVersion());
	// try {
	// cfg.setClassForTemplateLoading(this.getClass(), "/templates/");
	// content = (FreeMarkerTemplateUtils
	// .processTemplateIntoString(cfg.getTemplate("payment_advice_email.ftl"),
	// model));
	// } catch (Exception e) {
	// logger.error("Error occurred while sending mail for new payment advice -",
	// e);
	// throw new MessagingException("Error occurred while sending mail");
	// }
	//
	// this.emailBaseService.sendEmail((String) model.get("email"),(String)
	// model.get("emailFrom"), (String) model.get("subject"), content, false, true);
	// }

	// Payment advice email - both new advice and forwarded requests are handled
	// here
	public void sendPaymentAdviceEmail(Map<String, Object> model) throws MessagingException {
		String content;
		Configuration cfg = new Configuration(Configuration.getVersion());
		try {
			cfg.setClassForTemplateLoading(this.getClass(), "/templates/");
			content = (FreeMarkerTemplateUtils.processTemplateIntoString(cfg.getTemplate("payment_advice_email.ftl"),
					model));
		} catch (Exception e) {
			logger.error("Error occurred while sending mail for new payment advice -", e);
			throw new MessagingException("Error occurred while sending mail");
		}

		String email = (String) model.get("email");
		if (!"bipin.kr@scsoft.com".equals(email))
			this.emailBaseService.sendEmail(email, (String) model.get("emailFrom"), (String) model.get("subject"),
					content, false, true);
	}

	// public void sendPaymentAdviceAprroveEmail(Map<String, Object> model) throws
	// MessagingException {
	// String content;
	// Configuration cfg = new Configuration(Configuration.getVersion());
	// try {
	// cfg.setClassForTemplateLoading(this.getClass(), "/templates/");
	// content = (FreeMarkerTemplateUtils
	// .processTemplateIntoString(cfg.getTemplate("payment_advice_approove.ftl"),
	// model));
	// } catch (Exception e) {
	// logger.error("Error occurred while sending mail for new payment advice
	// apprrooval -", e);
	// throw new MessagingException("Error occurred while sending mail");
	// }
	//
	// this.emailBaseService.sendEmail((String) model.get("email"),(String)
	// model.get("emailFrom"), (String) model.get("subject"), content, false, true);
	// }
	// Payment Advice Approve email
	public void sendPaymentAdviceAprroveEmail(Map<String, Object> model) throws MessagingException {
		String content;
		Configuration cfg = new Configuration(Configuration.getVersion());
		try {
			cfg.setClassForTemplateLoading(this.getClass(), "/templates/");
			content = (FreeMarkerTemplateUtils.processTemplateIntoString(cfg.getTemplate("payment_advice_approove.ftl"),
					model));
		} catch (Exception e) {
			logger.error("Error occurred while sending mail for new payment advice apprrooval -", e);
			throw new MessagingException("Error occurred while sending mail");
		}
		String email = (String) model.get("email");
		if (!"bipin.kr@scsoft.com".equals(email))
			this.emailBaseService.sendEmail((String) model.get("email"), (String) model.get("emailFrom"),
					(String) model.get("subject"), content, false, true);
	}

	public void sendSalarySlipEmail(Map<String, Object> model) throws MessagingException {
		String content;
		Configuration cfg = new Configuration(Configuration.getVersion());
		try {
			cfg.setClassForTemplateLoading(this.getClass(), "/templates/");
			content = (FreeMarkerTemplateUtils.processTemplateIntoString(cfg.getTemplate("salarySlipRequest.ftl"),
					model));
		} catch (Exception e) {
			logger.error("Error occurred while sending mail for new payment advice apprrooval -", e);
			throw new MessagingException("Error occurred while sending mail");
		}

		this.emailBaseService.sendEmail((String) model.get("email"), (String) model.get("emailFrom"),
				(String) model.get("subject"), content, false, true);
	}
	
	public void sendApplyWfhEmail(Map<String, Object> model) throws MessagingException {
		String content;
		Configuration cfg = new Configuration(Configuration.getVersion());
		try {
			cfg.setClassForTemplateLoading(this.getClass(), "/templates/");
			content = (FreeMarkerTemplateUtils.processTemplateIntoString(cfg.getTemplate("apply_wfh.ftl"),
					model));
		} catch (Exception e) {
			logger.error("Error occurred while sending mail for wfh -", e);
			throw new MessagingException("Error occurred while sending mail");
		}

		this.emailBaseService.sendEmail((String) model.get("email"), (String) model.get("emailFrom"),
				(String) model.get("subject"), content, false, true);
	}
	public void sendCancellWfhEmail(Map<String, Object> model) throws MessagingException {
		String content;
		Configuration cfg = new Configuration(Configuration.getVersion());
		try {
			cfg.setClassForTemplateLoading(this.getClass(), "/templates/");
			content = (FreeMarkerTemplateUtils.processTemplateIntoString(cfg.getTemplate("cancell_wfh.ftl"),
					model));
		} catch (Exception e) {
			logger.error("Error occurred while sending mail for wfh -", e);
			throw new MessagingException("Error occurred while sending mail");
		}

		this.emailBaseService.sendEmail((String) model.get("email"), (String) model.get("emailFrom"),
				(String) model.get("subject"), content, false, true);
	}
	public void sendApproveWfhEmail(Map<String, Object> model) throws MessagingException {
		String content;
		Configuration cfg = new Configuration(Configuration.getVersion());
		try {
			cfg.setClassForTemplateLoading(this.getClass(), "/templates/");
			content = (FreeMarkerTemplateUtils.processTemplateIntoString(cfg.getTemplate("approved_wfh.ftl"),
					model));
		} catch (Exception e) {
			logger.error("Error occurred while sending mail for wfh -", e);
			throw new MessagingException("Error occurred while sending mail");
		}

		this.emailBaseService.sendEmail((String) model.get("email"), (String) model.get("emailFrom"),
				(String) model.get("subject"), content, false, true);
	}
	public void sendAnniversaryWishEmail(Map<String, Object> model) throws MessagingException {
		String content;
		Configuration cfg = new Configuration(Configuration.getVersion());
		try {
			cfg.setClassForTemplateLoading(this.getClass(), "/templates/");
			content = (FreeMarkerTemplateUtils.processTemplateIntoString(cfg.getTemplate("anniversary_wish.ftl"), model));
		} catch (Exception e) {
			logger.error("Error occurred while sending mail for new payment advice apprrooval -", e);
			throw new MessagingException("Error occurred while sending mail");
		}

		this.emailBaseService.sendEmail((String) model.get("email"), (String) model.get("emailFrom"),
				(String) model.get("subject"), content, false, true);
	}

	public void sendTestEmail(Map<String, Object> model) throws MessagingException {
		String content;
		Configuration cfg = new Configuration(Configuration.getVersion());
		try {
			cfg.setClassForTemplateLoading(this.getClass(), "/templates/");
			content = (FreeMarkerTemplateUtils.processTemplateIntoString(cfg.getTemplate("test.ftl"), model));
		} catch (Exception e) {
			logger.error("Error occurred while sending mail for new payment advice apprrooval -", e);
			throw new MessagingException("Error occurred while sending mail");
		}

		this.emailBaseService.sendEmail((String) model.get("email"), (String) model.get("emailFrom"),
				(String) model.get("subject"), content, false, true);
	}
	
}
