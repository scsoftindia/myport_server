package com.scsoft.myport.service.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.Date;

import org.joda.time.DateTime;

public class DateSerializer extends JsonSerializer<DateTime> {

    @Override
    public void serialize(DateTime date, JsonGenerator jsonParser, SerializerProvider serializerProvider) throws IOException {
        jsonParser.writeNumber(date.getMillis());
    }
}
