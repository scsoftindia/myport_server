package com.scsoft.myport.service.util;

public class AppConstants {

	//Common Strings
	  //For success status in response
	 public static final String SUCCES = "success";
	 public static final String ERROR = "error";
	 public static final String STATUS = "status";
	 public static final String APPROVED = "Approved";
	 public static final String REJECTED = "Rejected";
	 public static final String CANCELLED = "Cancelled";

}
