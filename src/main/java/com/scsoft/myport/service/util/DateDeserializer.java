package com.scsoft.myport.service.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateDeserializer extends JsonDeserializer<Date> {
	@Override
	public Date deserialize(JsonParser parser, DeserializationContext deserializationContext) throws IOException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String date = parser.getText();
		String converted=date.split(" ")[0];
		System.out.println(converted);
		try {
			return format.parse(converted);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}

}
