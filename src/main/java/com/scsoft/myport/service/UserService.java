package com.scsoft.myport.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.scsoft.myport.domain.EmpEmployment;
import com.scsoft.myport.domain.EmpPersonal;
import com.scsoft.myport.domain.FcmDetail;
import com.scsoft.myport.domain.UserMobileInfo;
import com.scsoft.myport.error.NotFoundException;
import com.scsoft.myport.error.OperationNotPossibleException;
import com.scsoft.myport.repository.EmpEmploymentRepository;
import com.scsoft.myport.repository.EmpPersonalRepository;
import com.scsoft.myport.repository.UserMobileInfoRepository;
import com.scsoft.myport.rest.request.EditProfileRequest;
import com.scsoft.myport.rest.request.TrackRequest;
import com.scsoft.myport.rest.request.UserMobileInfoRequest;
import com.scsoft.myport.rest.request.XyzRequest;
import com.scsoft.myport.service.DTO.EmployeeDTO;
import com.scsoft.myport.service.comparator.EmployeeComparator;
import com.scsoft.myport.service.mapper.EmployeeMapper;
import com.scsoft.myport.service.mapper.EmployeePersonalMapper;
import com.scsoft.myport.service.mapper.UserMobileInfoMapper;
import org.apache.commons.collections.map.HashedMap;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.ldap.client.api.LdapConnection;
import org.apache.directory.ldap.client.api.LdapConnectionConfig;
import org.apache.directory.ldap.client.api.LdapNetworkConnection;

@Service("userService")
public class UserService {
	private final Logger logger = LoggerFactory.getLogger(UserService.class);
	@Autowired
	private EmpEmploymentRepository empEmploymentRepository;
	@Autowired
	private EmpPersonalRepository empPersonalRepository;
	@Autowired
	private UserMobileInfoRepository userMobileInfoRepository;
	@Autowired
	private EmployeeMapper employeeMapper;
	@Autowired
	private UserMobileInfoMapper userMobileInfoMapper;
	@Autowired
	private EmployeePersonalMapper employeePersonalMapper;
	@Value("${ldap.server.url}")
	private String ldapUrl;
	@Value("${ldap.server.port}")
	private String port;
	@Value("${ldap.server.domain}")
	private String domain;

	@Autowired
	private PaymentLookupService paymentLookupService;
	@Autowired
	private FcmService fcmService;

	public EmployeeDTO getEmployeeDetails(String username, String password) throws LdapException {
		logger.debug("Authenticating User and Fetching user Details from DB");

		LdapConnectionConfig config = new LdapConnectionConfig();
		config.setLdapHost(ldapUrl);
		config.setLdapPort(Integer.parseInt(port));
		config.setName(username + domain);
		config.setCredentials(password);
		LdapConnection connection = new LdapNetworkConnection(config);
		connection.bind();
		if (connection.isAuthenticated()) {

			EmpEmployment empEmployment = empEmploymentRepository.getEmployeeByUserNameAndPassword(username);
			EmployeeDTO employeeDTO = this.employeeMapper.empEmploymentToEmployeeDTO(empEmployment);
			Boolean isSuperVisor = this.empEmploymentRepository.isSupervisor(empEmployment.getId());
			employeeDTO.setIsSupervisor(isSuperVisor);

			try {
				connection.close();
			} catch (IOException e) {
				logger.error("Error in connection close" + e.getMessage());
			}

			return employeeDTO;

		} else {
			try {
				connection.close();
			} catch (IOException e) {
				logger.error("Error in connection close" + e.getMessage());
			}
			return null;
		}

	}

	public List<EmployeeDTO> findAllEmployess() {
		logger.debug("Request to get all Employees");
		List<EmpEmployment> result = new ArrayList();
		result = empEmploymentRepository.getEmployeeList();
		List<EmployeeDTO> employeeDTOs = new ArrayList();
		result.stream().forEach(item -> {
			if (item.getEmpPersonal() != null) {
				employeeDTOs.add(employeeMapper.empEmploymentToEmployeeDTO(item));
			}
		});
		Collections.sort(employeeDTOs, new EmployeeComparator());
		return employeeDTOs;
	}

	public List<EmployeeDTO> getAllEmployess() {
		logger.debug("Request to get all Employees");
		List<EmpEmployment> result = new ArrayList();
		result = empEmploymentRepository.getAllEmployeeList();
		List<EmployeeDTO> employeeDTOs = new ArrayList();
		result.stream().forEach(item -> {
			if (item.getEmpPersonal() != null) {
				employeeDTOs.add(employeeMapper.empEmploymentToEmployeeDTO(item));
			}
		});
		Collections.sort(employeeDTOs, new EmployeeComparator());
		return employeeDTOs;
	}

	public UserMobileInfo saveUserInfo(UserMobileInfoRequest userMobileInfoRequest) throws NotFoundException {
		logger.debug("Saving user Mobile informations");
		DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
		DateTime dateTime = new DateTime().withZone(indianZone);
		UserMobileInfo userMobileInfo;
		EmpEmployment empEmployment = this.empEmploymentRepository.findOne(userMobileInfoRequest.getEmp_id());
		if (empEmployment == null) {
			throw new NotFoundException("user", "Invalid user");
		}
		userMobileInfo = this.userMobileInfoRepository.findByEmpEmploymentIdAndMacAdress(
				userMobileInfoRequest.getEmp_id(), userMobileInfoRequest.getMacAdress());
		if (userMobileInfo == null) {
			userMobileInfo = this.userMobileInfoMapper.userMobileInfoRequestToUserMobileInfo(userMobileInfoRequest);
			userMobileInfo.setEmpEmployment(empEmployment);
		}
		userMobileInfo.setLast_login_time(dateTime.toDate());
		return this.userMobileInfoRepository.saveAndFlush(userMobileInfo);
	}

	public UserMobileInfo updateUserMobileInfo(String macAdress, Integer empId) throws NotFoundException {
		try {
			logger.debug("Updating logout time in user mobile info");
			DateTimeZone indianZone = DateTimeZone.forID("Asia/Kolkata");
			DateTime dateTime = new DateTime().withZone(indianZone);
			UserMobileInfo userMobileInfo = this.userMobileInfoRepository.findByEmpEmploymentIdAndMacAdress(empId,
					macAdress);
			if (userMobileInfo != null) {
				userMobileInfo.setLast_logout_time(dateTime.toDate());
			} else {
				throw new NotFoundException("user", "Invalid Id");
			}
			return this.userMobileInfoRepository.saveAndFlush(userMobileInfo);
		} catch (Exception e) {
			logger.error("Error ocuured while updating mobile data", e);
		}
		return null;
	}

	public String performTrack(TrackRequest trackRequest) {
		logger.debug("Performing track operation");
		try {
			ObjectMapper mapper = new ObjectMapper();
			FcmDetail fcmDetail = this.paymentLookupService.getFcmDetailByEmpId(trackRequest.getTo());
			if (fcmDetail != null) {
				Map<String, Object> dataMap = new HashedMap();
				dataMap.put("from", trackRequest.getFrom());
				dataMap.put("data", trackRequest.getData());
				this.fcmService.sendMessage(fcmDetail.getFcm_id(), null, mapper.writeValueAsString(dataMap), "track");
			}
		} catch (Exception ex) {
			logger.error("Error occured while tracking", ex);
			return "error";
		}
		return "success";
	}

	public Boolean editProfile(EditProfileRequest editProfileRequest)
			throws NotFoundException, OperationNotPossibleException {
		logger.debug("Saving profile updates into DB");
		Boolean result = false;
		Boolean updateAvailable = false;
		if (editProfileRequest.getEmpId() == null) {
			throw new NotFoundException("error", "Invalid User ID");
		}
		EmpPersonal empPersonal = this.empPersonalRepository.findOne(editProfileRequest.getEmpId());
		if (empPersonal == null) {
			throw new NotFoundException("error", "Invalid User");
		}
		if (editProfileRequest.getPermanantAdresss() != null && !editProfileRequest.getPermanantAdresss().isEmpty()) {
			updateAvailable = true;
			empPersonal.setEmp_perm_addr(editProfileRequest.getPermanantAdresss());
		}
		if (editProfileRequest.getTemparotyAddress() != null && !editProfileRequest.getTemparotyAddress().isEmpty()) {
			updateAvailable = true;
			empPersonal.setEmp_temp_addr(editProfileRequest.getTemparotyAddress());
		}
		if (editProfileRequest.getPhone() != null && !editProfileRequest.getPhone().isEmpty()) {
			updateAvailable = true;
			empPersonal.setEmp_personal_phone(editProfileRequest.getPhone());
		}
		if (!updateAvailable) {
			throw new OperationNotPossibleException("error", "Operation not possible with empty value sets");
		} else {
			this.empPersonalRepository.saveAndFlush(empPersonal);
			result = true;
		}

		return result;

	}

	public List<SupervisorDTO> getSupervisorList() {
		logger.debug("Retrieving all the supervisor details from DB");
		List<EmpPersonal> supervisorList = empPersonalRepository.getSupevisorList();
		List<SupervisorDTO> supervisorDTOs = new ArrayList<>();
		supervisorList.stream().forEach(item -> {
			supervisorDTOs.add(employeePersonalMapper.empPersonalToSupervisorDTO(item));
		});
		return supervisorDTOs;
	}

	public Boolean isActiveEmployee(Integer empId) {
		logger.debug("To check whether employee is active or not");
		return this.empEmploymentRepository.isActiveEmployee(empId);
	}

}
