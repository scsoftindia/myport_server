package com.scsoft.myport.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import com.scsoft.myport.domain.ApplyLeave;
import com.scsoft.myport.service.DTO.EmployessOnLeaveDTO;
import com.scsoft.myport.service.DTO.LeaveDTO;

@Mapper(componentModel = "spring",uses = {})
public interface ApplyLeaveMapper {
	@Mapping(target = "emp_fname", source = "applyLeave.empPersonal.emp_fname")
	@Mapping(target = "emp_mname", source = "applyLeave.empPersonal.emp_mname")
	@Mapping(target = "emp_lname", source = "applyLeave.empPersonal.emp_lname")
	@Mapping(target = "emp_avatar", source = "applyLeave.empPersonal.emp_avatar")
	LeaveDTO applyLeaveToLeaveDTO(ApplyLeave applyLeave);


}
