package com.scsoft.myport.service.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.scsoft.myport.domain.PaymentAdvice;
import com.scsoft.myport.rest.request.PaymentAdviceRequest;
import com.scsoft.myport.service.DTO.PaymentAdviceDTO;

@Mapper(componentModel="spring")
public interface PaymentAdviceMapper {
	@Mapping(target="payadviceCategories",ignore=true)
	@Mapping(target="payadviceSubCategories",ignore=true)
	@Mapping(target="vendors",ignore=true)
	@Mapping(target="paymentModes",ignore=true)
	@Mapping(target="paymentBank",ignore=true)
	PaymentAdvice paymentAdviceRequestToPaymentAdvice(PaymentAdviceRequest paymentAdviceRequest);
	
	@Mapping(target="category_name",source="payadviceCategories.cat_name")
	@Mapping(target="category_id",source="payadviceCategories.id")
	@Mapping(target="sub_category_name",source="payadviceSubCategories.pay_subcategory_name")
	@Mapping(target="sub_category_id",source="payadviceSubCategories.id")
	@Mapping(target="vendor_name",source="vendors.vendor_name")
	@Mapping(target="vendor_id",source="vendors.id")
	@Mapping(target="currency_type_name",source="currencyType.name")
	@Mapping(target="currency_type_id",source="currencyType.id")
	@Mapping(target="payment_mode_name",source="paymentModes.name")
	@Mapping(target="payment_mode_id",source="paymentModes.id")
	@Mapping(target="aprrovalStatus_name",source="aprrovalStatus.name")
	PaymentAdviceDTO paymentAdviceToPaymentAdviceDTO(PaymentAdvice paymentAdvice);
	
	
	@Mapping(target="payadviceCategories",source="paymentAdvice.payadviceCategories")
	@Mapping(target="payadviceSubCategories",source="paymentAdvice.payadviceSubCategories")
	@Mapping(target="vendors",source="paymentAdvice.vendors")
	@Mapping(target="paymentModes",source="paymentAdvice.paymentModes")
	@Mapping(target="id",source="paymentAdvice.id")
	@Mapping(target="paymentAdviceDocumentsList",source="paymentAdvice.paymentAdviceDocumentsList")
	@Mapping(target="payment_release_time",source="paymentAdvice.payment_release_time")
	@Mapping(target="payment_release_reject_time",source="paymentAdvice.payment_release_reject_time")
	@Mapping(target="release_note",source="paymentAdvice.release_note")
	@Mapping(target="release_reject_note",source="paymentAdvice.release_reject_note")
	@Mapping(target="applied_on",source="paymentAdvice.applied_on")
	@Mapping(target="last_modified_date",source="paymentAdvice.last_modified_date")
	@Mapping(target="lastModifiedBy",source="paymentAdvice.lastModifiedBy")
	@Mapping(target="createdDate",source="paymentAdvice.createdDate")
	@Mapping(target="createdBy",source="paymentAdvice.createdBy")
	@Mapping(target="approved_date",source="paymentAdvice.approved_date")
	@Mapping(target="approval_comment",source="paymentAdvice.approval_comment")
	@Mapping(target="reject_reson",source="paymentAdvice.reject_reson")
	@Mapping(target="reject_date",source="paymentAdvice.reject_date")
	@Mapping(target="aprrovalStatus",source="paymentAdvice.aprrovalStatus")
	@Mapping(target="applied_emp_name",source="paymentAdvice.applied_emp_name")
	@Mapping(target="applied_emp_Id",source="paymentAdvice.applied_emp_Id")
	@Mapping(target="payment_release_emp_name",source="paymentAdvice.payment_release_emp_name")
	@Mapping(target="payment_release_emp_Id",source="paymentAdvice.payment_release_emp_Id")
	@Mapping(target="reject_emp_name",source="paymentAdvice.reject_emp_name")
	@Mapping(target="reject_emp_Id",source="paymentAdvice.reject_emp_Id")
	@Mapping(target="aprooved_emp_name",source="paymentAdvice.aprooved_emp_name")
	@Mapping(target="aprooved_emp_Id",source="paymentAdvice.aprooved_emp_Id")
	@Mapping(target="primary_aproover_emp_name",source="paymentAdvice.primary_aproover_emp_name")
	@Mapping(target="primary_aproover_emp_Id",source="paymentAdvice.primary_aproover_emp_Id")
	@Mapping(target="secondary_aproover_emp_id",source="paymentAdvice.secondary_aproover_emp_id")
	@Mapping(target="secondary_aproover_name",source="paymentAdvice.secondary_aproover_name")
	@Mapping(target="is_admin_rejected",source="paymentAdvice.is_admin_rejected")
	@Mapping(target="is_finance_rejected",source="paymentAdvice.is_finance_rejected")
	@Mapping(target="is_forward_rejected",source="paymentAdvice.is_forward_rejected")	
	@Mapping(target="description",source="paymentAdviceRequest.description")
	@Mapping(target="bill_date",source="paymentAdviceRequest.bill_date")
	@Mapping(target="due_date",source="paymentAdviceRequest.due_date")
	@Mapping(target="bill_number",source="paymentAdviceRequest.bill_number")
	@Mapping(target="bill_amount",source="paymentAdviceRequest.bill_amount")
	@Mapping(target="bill_period_from",source="paymentAdviceRequest.bill_period_from")
	@Mapping(target="bill_peroid_to",source="paymentAdviceRequest.bill_peroid_to")
	@Mapping(target="payment_cash_billno",source="paymentAdviceRequest.payment_cash_billno")
	@Mapping(target="payment_cheque_number",source="paymentAdviceRequest.payment_cheque_number")
	@Mapping(target="payment_cheque_date",source="paymentAdviceRequest.payment_cheque_date")
	@Mapping(target="payment_online_date",source="paymentAdviceRequest.payment_online_date")
	@Mapping(target="paymentRemark",source="paymentAdviceRequest.paymentRemark")
	@Mapping(target="currencyType",source="paymentAdviceRequest.currencyType")
	@Mapping(target="paymentBank",ignore = true)
	PaymentAdvice paymentAdviceRequestAndPaymentAdviceToPaymentAdvice(PaymentAdviceRequest paymentAdviceRequest,PaymentAdvice paymentAdvice);
}
