package com.scsoft.myport.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import com.scsoft.myport.domain.EmpPersonal;
import com.scsoft.myport.service.SupervisorDTO;
import com.scsoft.myport.service.DTO.AprooverDTO;

@Mapper(componentModel = "spring")
public interface EmployeePersonalMapper {

	SupervisorDTO empPersonalToSupervisorDTO(EmpPersonal empPersonal);
	@Mapping(target="fname",source="empPersonal.emp_fname")
	@Mapping(target="lname",source="empPersonal.emp_lname")
	@Mapping(target="mname",source="empPersonal.emp_mname")
	@Mapping(target="id",source="empPersonal.id")
	AprooverDTO  empPersonalToAprooverDTO(EmpPersonal empPersonal);

	

}
