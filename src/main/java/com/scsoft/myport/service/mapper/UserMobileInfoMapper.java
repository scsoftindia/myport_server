package com.scsoft.myport.service.mapper;

import org.mapstruct.Mapper;

import com.scsoft.myport.domain.UserMobileInfo;
import com.scsoft.myport.rest.request.UserMobileInfoRequest;

@Mapper(componentModel="spring")
public interface UserMobileInfoMapper {
	
	UserMobileInfo userMobileInfoRequestToUserMobileInfo(UserMobileInfoRequest userMobileInfoRequest);

}
