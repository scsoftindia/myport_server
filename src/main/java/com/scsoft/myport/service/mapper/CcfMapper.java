package com.scsoft.myport.service.mapper;

import org.mapstruct.Mapper;

import com.scsoft.myport.domain.Ccf;
import com.scsoft.myport.rest.request.CcfRequest;

@Mapper(componentModel = "spring")
public interface CcfMapper {
	
	Ccf CcfRequestToCcf(CcfRequest ccfRequest);
	

}
