package com.scsoft.myport.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.scsoft.myport.domain.PayadviceSubCategories;
import com.scsoft.myport.rest.request.PayadviceSubCategoriesRequest;

@Mapper(componentModel="spring")
public interface PayadviceSubCategoriesMapper {
	@Mapping(target="payadviceCategories",ignore=true)
	PayadviceSubCategories payadviceSubCategoriesRequestToPayadviceSubCategories(PayadviceSubCategoriesRequest payadviceSubCategoriesRequest);

}
