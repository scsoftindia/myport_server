package com.scsoft.myport.service.mapper;

import org.mapstruct.Mapper;

import com.scsoft.myport.domain.ApplyWfh;
import com.scsoft.myport.rest.request.WfhRequest;
import com.scsoft.myport.service.DTO.WfhEmpDTO;

@Mapper(componentModel = "spring")
public interface WfhMapper {

	ApplyWfh wfhRequestToApplyWfh(WfhRequest wfhRequest);
	
	WfhEmpDTO ApplyWfhToWfhEmpDTO(ApplyWfh applyWfh);

}
