package com.scsoft.myport.service.mapper;

import org.mapstruct.Mapper;

import com.scsoft.myport.domain.PayadviceCategories;
import com.scsoft.myport.domain.PaymentAdvice;
import com.scsoft.myport.rest.request.PayadviceCategoriesAproveRequest;
import com.scsoft.myport.rest.request.PayadviceCategoriesRequest;
import com.scsoft.myport.service.DTO.PayadviceCategoriesDTO;

@Mapper(componentModel="spring")
public interface PayadviceCategoriesMapper {
	
	PayadviceCategories payadviceCategoriesRequestToPayadviceCategories(PayadviceCategoriesRequest payadviceCategoriesRequest);
	PayadviceCategoriesDTO payadviceCategoriesToPayadviceCategoriesDTO(PayadviceCategories payadviceCategories);
}
