package com.scsoft.myport.service.mapper;

import org.mapstruct.Mapper;

import com.scsoft.myport.domain.PaymentBank;
import com.scsoft.myport.service.DTO.PaymentBankDTO;

@Mapper(componentModel = "spring")
public interface PaymentBankMapper {
	PaymentBank paymentBankDTOToPaymentBank(PaymentBankDTO paymentBankDTO);
	PaymentBankDTO paymentBankToPaymentBankDTO(PaymentBank paymentBank);

}
