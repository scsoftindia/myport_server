package com.scsoft.myport.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import com.scsoft.myport.domain.Announcements;
import com.scsoft.myport.domain.EmpPersonal;
import com.scsoft.myport.rest.request.AnnouncementRequest;
import com.scsoft.myport.service.DTO.AnnouncementDTO;

@Mapper(componentModel="spring")
public interface AnnouncementsMapper {
	@Mapping(target="announcementBy", qualifiedByName = "announCementBy" )
	AnnouncementDTO announcementsToAnnouncementDTO(Announcements announcements);
	@Mapping(target="announcementBy", ignore = true )
	Announcements announcementRequestToAnnouncements(AnnouncementRequest announcementRequest);
	
	@Named("announCementBy")
    default String stringsFromLeaseTerms(EmpPersonal announcementBy) {
       String fullname = announcementBy.getEmp_fname() + " "
    		   + announcementBy.getEmp_mname() + " "
    		   +announcementBy.getEmp_lname();
       return fullname;
    }

}
