package com.scsoft.myport.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;

import com.scsoft.myport.domain.EmpEmployment;
import com.scsoft.myport.repository.EmpEmploymentRepository;
import com.scsoft.myport.service.DTO.EmployeeDTO;

@Mapper(componentModel = "spring")
public interface EmployeeMapper {


	@Mapping(target = "emp_fname", source = "empEmployment.empPersonal.emp_fname")
	@Mapping(target = "emp_mname", source = "empEmployment.empPersonal.emp_mname")
	@Mapping(target = "emp_lname", source = "empEmployment.empPersonal.emp_lname")
	@Mapping(target = "emp_dob", source = "empEmployment.empPersonal.emp_dob")
	@Mapping(target = "emp_gender", source = "empEmployment.empPersonal.emp_gender")
	@Mapping(target = "emp_bloodgroup", source = "empEmployment.empPersonal.emp_bloodgroup")
	@Mapping(target = "emp_fathername", source = "empEmployment.empPersonal.emp_fathername")
	@Mapping(target = "emp_marital_status", source = "empEmployment.empPersonal.emp_marital_status")
	@Mapping(target = "emp_anniversary", source = "empEmployment.empPersonal.emp_anniversary")
	@Mapping(target = "emp_temp_addr", source = "empEmployment.empPersonal.emp_temp_addr")
	@Mapping(target = "emp_perm_addr", source = "empEmployment.empPersonal.emp_perm_addr")
	@Mapping(target = "emp_avatar", source = "empEmployment.empPersonal.emp_avatar")
	@Mapping(target = "supervispr_id", source = "empEmployment.superwiser.id")
	@Mapping(target = "supervispr_fname", source = "empEmployment.superwiser.emp_fname")
	@Mapping(target = "supervispr_mname", ignore=true)
	@Mapping(target = "supervispr_lname", source = "empEmployment.superwiser.emp_lname")
	@Mapping(target = "emp_personal_phone", source = "empEmployment.emp_personal_phone")
	EmployeeDTO empEmploymentToEmployeeDTO(EmpEmployment empEmployment);
}
