package com.scsoft.myport.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.scsoft.myport.domain.Vendors;
import com.scsoft.myport.rest.request.VendorsRequest;

@Mapper(componentModel="spring")
public interface VendorsMapper {
	@Mapping(target="payadviceCategories",ignore=true)
	@Mapping(target="payadviceSubCategories",ignore=true)
	Vendors VendorsRequestToVendors(VendorsRequest vendorsRequest);
}
