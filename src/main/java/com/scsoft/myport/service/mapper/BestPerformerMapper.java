package com.scsoft.myport.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.scsoft.myport.domain.BestPerformer;
import com.scsoft.myport.service.DTO.BestPerformerDTO;

@Mapper(componentModel = "spring")
public interface BestPerformerMapper {
	@Mapping(target="fname",source="bestPerformer.empPersonal.emp_fname")
	@Mapping(target="mname",source="bestPerformer.empPersonal.emp_mname")
	@Mapping(target="lname",source="bestPerformer.empPersonal.emp_lname")
	@Mapping(target="empId",source="bestPerformer.empPersonal.id")
	@Mapping(target="avathar",source="bestPerformer.empPersonal.emp_avatar")
	@Mapping(target="achieve1",ignore=true)
	@Mapping(target="achieve2",ignore=true)
	@Mapping(target="achieve3",ignore=true)
	@Mapping(target="achieve4",ignore=true)
	@Mapping(target="achieve5",ignore=true)
	@Mapping(target="app1",ignore=true)
	@Mapping(target="app2",ignore=true)
	@Mapping(target="app3",ignore=true)
	@Mapping(target="app4",ignore=true)
	@Mapping(target="app5",ignore=true)
	@Mapping(target="app1By",ignore=true)
	@Mapping(target="app2By",ignore=true)
	@Mapping(target="app3By",ignore=true)
	@Mapping(target="app4By",ignore=true)
	@Mapping(target="app5By",ignore=true)
	BestPerformerDTO bestPerformerToBestPerformerDTO(BestPerformer bestPerformer);

}
