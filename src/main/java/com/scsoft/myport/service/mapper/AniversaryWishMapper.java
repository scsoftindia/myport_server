package com.scsoft.myport.service.mapper;

import org.mapstruct.Mapper;

import com.scsoft.myport.domain.AnniversaryWish;
import com.scsoft.myport.rest.request.AniversaryWishRequest;

@Mapper(componentModel="spring")
public interface AniversaryWishMapper {
	
	AnniversaryWish aniversaryWishRequestToAnniversaryWish(AniversaryWishRequest aniversaryWishRequest);

}
