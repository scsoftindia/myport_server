package com.scsoft.myport.fcm.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.AndroidConfig;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.Message;
import com.scsoft.myport.service.DTO.GlobalPushDTO;
@Service
public class FCMadmin {
	@Value("${fcm.file.path}")
	private String fcmFilePath;

	public void sendMsg(String fcmID, List<String> fcmIdList, String data, String type)
			throws InterruptedException, ExecutionException {
		
		
		if(FirebaseApp.getApps().isEmpty()) {
			
		FileInputStream serviceAccount;
		try {
			File file = new File(fcmFilePath);
			serviceAccount = new FileInputStream(file);
			FirebaseOptions options = new FirebaseOptions.Builder()
					.setCredentials(GoogleCredentials.fromStream(serviceAccount))
					.setDatabaseUrl("https://myportscsoft.firebaseio.com").build();

			FirebaseApp.initializeApp(options);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		}
		 
		if (!CollectionUtils.isEmpty(fcmIdList)) {

			for (String fcmIDs : fcmIdList) {
				Message message = Message.builder()
						.setAndroidConfig(AndroidConfig.builder().setPriority(AndroidConfig.Priority.HIGH)
								.setTtl(48 * 3600 * 1000) // 48 hours , 2 days
								.putData("data", data + "," + "\"type\":\"" + type + "\"}").build())
						.setToken(fcmIDs).build();

				String response = FirebaseMessaging.getInstance().sendAsync(message).get();

				// Response is a message ID string.
				System.out.println("Successfully sent message: " + response);
			}

		} else {
			// Set registration token that can be retrieved
			// from Android entity(mobile devices,browser front-end apps) when calling
			// FirebaseInstanceId.getInstance().getToken();
			Message message = Message.builder()
					.setAndroidConfig(
							AndroidConfig.builder().setPriority(AndroidConfig.Priority.HIGH).setTtl(48 * 3600 * 1000) // 48 hours, 2 days
									.putData("data", data + "," + "\"type\":\"" + type + "\"}").build())
					.setToken(fcmID).build();

			System.out.println("data" + data + "," + "\"type\":\"" + type + "\"}");
			String response = FirebaseMessaging.getInstance().sendAsync(message).get();
			// Response is a message ID string.
			System.out.println("Successfully sent message: " + response);

		}
	 
	}

}
