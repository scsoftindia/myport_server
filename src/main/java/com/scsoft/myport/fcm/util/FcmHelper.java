//package com.scsoft.myport.fcm.util;
//
//import java.util.List;
//
//import org.json.JSONException;
//import org.springframework.util.CollectionUtils;
//
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.scsoft.myport.fcm.model.EntityMessage;
//import com.scsoft.myport.fcm.model.FcmResponse;
//import com.scsoft.myport.service.DTO.GlobalPushDTO;
//
//public class FcmHelper {
//
//	public static void pushNotification(String fcmID, List<String> fcmIdList, String data, String type)
//			throws Exception {
//
//		FCMadmin.sendMsg(fcmID, fcmIdList, data, type);
//
//	}
//
//	public static void main(String[] args) {
//		FcmClient client = new FcmClient();
//		// You can get from firebase console.
//		// "select your project>project settings>cloud messaging"
//		client.setAPIKey("AIzaSyD44mO5X3gZuOKysRymRIYcUVOJbpyHZbU");
//
//		// Data model for sending messages to specific entity(mobile devices,browser
//		// front-end apps)s
//		EntityMessage msg = new EntityMessage();
//
//		String fcmID = "fKQotSe8kFc:APA91bHwfiULYld_8ljOaqiR9PaVn4VW5qCYcFwxaYrl-zU9w3nLXPcZw6e5JFnS3nBTj_Iall2ELx7-tfxwYg8CLAzmES3GDml9N2evAySbABee4stjCs0AfscY7tnilKDsOISU1JF_";
//		msg.addRegistrationToken(fcmID);
//
//		GlobalPushDTO dto = new GlobalPushDTO();
//		dto.setBody("hello test");
//		dto.setTitle("sunil from fcm");
//		dto.setAvathar(null);
//
//		// Add key value pair into payload
//		try {
//			System.out.println(new ObjectMapper().writeValueAsString(dto));
//			msg.putStringData("data", new ObjectMapper().writeValueAsString(dto));
//		} catch (JsonProcessingException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//		msg.putStringData("type", "others");
//
//		// push
//		FcmResponse res = null;
//		try {
//			res = client.pushToEntities(msg);
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//		System.out.println(res.toString());
//
//	}
//
//}
