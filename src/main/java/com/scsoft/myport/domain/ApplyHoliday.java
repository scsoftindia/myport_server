package com.scsoft.myport.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.scsoft.myport.service.util.DateDeserializer;
import com.scsoft.myport.service.util.DateSerializer;

import java.util.Date;

@Entity
@Table(name = "apply_holiday")
public class ApplyHoliday {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "empId")
	private Integer emp_id;
	@Column(name = "holiday_id")
	private Integer holidayId;
	@Column(name = "applied_on")
	private Date appliedOn;
	@Column(name = "holidays_remaining")
	private Integer holidaysRemaining;
	@Column(name = "add_sup_id")
	private Integer addSupId;
	@Column(name = "cancel_date")
	private Date cancelDate;
	@Column(name = "cancel_reason")
	private String cancelReason;
	@Column(name = "status")
	private Integer status;
	@Column(name = "status_note")
	private String statusNote;
	@Column(name = "reviewed_by")
	private Integer reviewedBy;
	@Column(name = "reviewed_date")
	private Date reviewedDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEmp_id() {
		return emp_id;
	}

	public void setEmp_id(Integer emp_id) {
		this.emp_id = emp_id;
	}

	public Integer getHolidayId() {
		return holidayId;
	}

	public void setHolidayId(Integer holidayId) {
		this.holidayId = holidayId;
	}

	public Date getAppliedOn() {
		return appliedOn;
	}

	public void setAppliedOn(Date appliedOn) {
		this.appliedOn = appliedOn;
	}

	public Integer getHolidaysRemaining() {
		return holidaysRemaining;
	}

	public void setHolidaysRemaining(Integer holidaysRemaining) {
		this.holidaysRemaining = holidaysRemaining;
	}

	public Integer getAddSupId() {
		return addSupId;
	}

	public void setAddSupId(Integer addSupId) {
		this.addSupId = addSupId;
	}

	public Date getCancelDate() {
		return cancelDate;
	}

	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}

	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getStatusNote() {
		return statusNote;
	}

	public void setStatusNote(String statusNote) {
		this.statusNote = statusNote;
	}

	public Integer getReviewedBy() {
		return reviewedBy;
	}

	public void setReviewedBy(Integer reviewedBy) {
		this.reviewedBy = reviewedBy;
	}

	public Date getReviewedDate() {
		return reviewedDate;
	}

	public void setReviewedDate(Date reviewedDate) {
		this.reviewedDate = reviewedDate;
	}

}
