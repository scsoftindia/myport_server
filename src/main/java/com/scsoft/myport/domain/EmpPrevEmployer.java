package com.scsoft.myport.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.scsoft.myport.service.util.DateDeserializer;
import com.scsoft.myport.service.util.DateSerializer;

import java.util.Date;

@Entity
@Table(name = "emp_prev_employer")
public class EmpPrevEmployer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	private int emp_id;
	private String employer_name;
//	@JsonDeserialize(using = DateDeserializer.class)
//	@JsonSerialize(using = DateSerializer.class)
	private Date emp_doj;
//	@JsonDeserialize(using = DateDeserializer.class)
//	@JsonSerialize(using = DateSerializer.class)
	private Date emp_dor;
	private String emp_last_designation;
	private String emp_last_ctc;
	private String emp_resg_reason;
	private int entered_by;
//	@JsonDeserialize(using = DateDeserializer.class)
//	@JsonSerialize(using = DateSerializer.class)
	private Date entered_on;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEmp_id() {
		return emp_id;
	}

	public void setEmp_id(int emp_id) {
		this.emp_id = emp_id;
	}

	public String getEmployer_name() {
		return employer_name;
	}

	public void setEmployer_name(String employer_name) {
		this.employer_name = employer_name;
	}

	public Date getEmp_doj() {
		return emp_doj;
	}

	public void setEmp_doj(Date emp_doj) {
		this.emp_doj = emp_doj;
	}

	public Date getEmp_dor() {
		return emp_dor;
	}

	public void setEmp_dor(Date emp_dor) {
		this.emp_dor = emp_dor;
	}

	public String getEmp_last_designation() {
		return emp_last_designation;
	}

	public void setEmp_last_designation(String emp_last_designation) {
		this.emp_last_designation = emp_last_designation;
	}

	public String getEmp_last_ctc() {
		return emp_last_ctc;
	}

	public void setEmp_last_ctc(String emp_last_ctc) {
		this.emp_last_ctc = emp_last_ctc;
	}

	public String getEmp_resg_reason() {
		return emp_resg_reason;
	}

	public void setEmp_resg_reason(String emp_resg_reason) {
		this.emp_resg_reason = emp_resg_reason;
	}

	public int getEntered_by() {
		return entered_by;
	}

	public void setEntered_by(int entered_by) {
		this.entered_by = entered_by;
	}

	public Date getEntered_on() {
		return entered_on;
	}

	public void setEntered_on(Date entered_on) {
		this.entered_on = entered_on;
	}

}
