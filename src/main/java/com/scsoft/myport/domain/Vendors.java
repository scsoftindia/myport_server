package com.scsoft.myport.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "vendors")
public class Vendors {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@JsonIgnore
	@ManyToOne(targetEntity = PayadviceCategories.class,fetch=FetchType.LAZY)
	@JoinColumn(name = "cat_id", referencedColumnName = "id")
	private PayadviceCategories payadviceCategories;
	@JsonIgnore
	@ManyToOne(targetEntity = PayadviceSubCategories.class,fetch=FetchType.LAZY)
	@JoinColumn(name = "sub_cat_id", referencedColumnName = "id")
	private PayadviceSubCategories payadviceSubCategories;
	private String vendor_name;
	private String vendor_address;
	private String gst;
	private String contact_person;
	private String phone_number;
	private String email_id;
	private String website;
	private String bank_name;
	private String account_no;
	private String ifsc_code;
	@Column(name = "status", columnDefinition = "TINYInteger")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean status;
	private Boolean is_rejected;
	@JsonIgnore
	@ManyToOne(targetEntity=EmpEmployment.class)
	@JoinColumn(name="created_by",referencedColumnName="emp_id")
	private EmpEmployment createdBy;
	@Column(name="created_date")
	private Date createdDate;
	@JsonIgnore
	@ManyToOne(targetEntity=EmpEmployment.class)
	@JoinColumn(name="modified_by",referencedColumnName="emp_id")
	private EmpEmployment lastModifiedBy;
	@Column(name="modified_date")
	private Date lastModifiedDate;
	@Transient
	private String cretaed_emp_fname;
	@Transient
	private String cretaed_emp_mname;
	@Transient
	private String cretaed_emp_lname;
	@Transient
	private String created_emp_full_name;
	@Transient
	private Integer cretaed_emp_id;
	@Transient
	private String modified_emp_fname;
	@Transient
	private String modified_emp_mname;
	@Transient
	private String modified_emp_lname;
	@Transient
	private String modified_emp_fullname;
	@Transient
	private Integer modified_emp_id;
	@Transient
	private String category_name;
	@Transient
	private Integer category_id;
	@Transient
	private String sub_category_name;
	@Transient
	private Integer sub_category_id;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public PayadviceCategories getPayadviceCategories() {
		return payadviceCategories;
	}

	public void setPayadviceCategories(PayadviceCategories payadviceCategories) {
		this.payadviceCategories = payadviceCategories;
	}

	public PayadviceSubCategories getPayadviceSubCategories() {
		return payadviceSubCategories;
	}

	public void setPayadviceSubCategories(PayadviceSubCategories payadviceSubCategories) {
		this.payadviceSubCategories = payadviceSubCategories;
	}

	public String getVendor_name() {
		return vendor_name;
	}

	public void setVendor_name(String vendor_name) {
		this.vendor_name = vendor_name;
	}

	public String getVendor_address() {
		return vendor_address;
	}

	public void setVendor_address(String vendor_address) {
		this.vendor_address = vendor_address;
	}

	public String getGst() {
		return gst;
	}

	public void setGst(String gst) {
		this.gst = gst;
	}

	public String getContact_person() {
		return contact_person;
	}

	public void setContact_person(String contact_person) {
		this.contact_person = contact_person;
	}

	public String getPhone_number() {
		return phone_number;
	}

	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}

	public String getEmail_id() {
		return email_id;
	}

	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getBank_name() {
		return bank_name;
	}

	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}

	public String getAccount_no() {
		return account_no;
	}

	public void setAccount_no(String account_no) {
		this.account_no = account_no;
	}

	public String getIfsc_code() {
		return ifsc_code;
	}

	public void setIfsc_code(String ifsc_code) {
		this.ifsc_code = ifsc_code;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
		

	public Boolean getIs_rejected() {
		return is_rejected;
	}

	public void setIs_rejected(Boolean is_rejected) {
		this.is_rejected = is_rejected;
	}

	public EmpEmployment getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(EmpEmployment createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public EmpEmployment getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(EmpEmployment lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getCretaed_emp_fname() {
		return cretaed_emp_fname;
	}

	public void setCretaed_emp_fname(String cretaed_emp_fname) {
		this.cretaed_emp_fname = cretaed_emp_fname;
	}

	public String getCretaed_emp_mname() {
		return cretaed_emp_mname;
	}

	public void setCretaed_emp_mname(String cretaed_emp_mname) {
		this.cretaed_emp_mname = cretaed_emp_mname;
	}

	public String getCretaed_emp_lname() {
		return cretaed_emp_lname;
	}

	public void setCretaed_emp_lname(String cretaed_emp_lname) {
		this.cretaed_emp_lname = cretaed_emp_lname;
	}

	public String getCreated_emp_full_name() {
		StringBuilder name = new StringBuilder("");
		name.append(this.cretaed_emp_fname).append(" ");
		name.append(this.cretaed_emp_mname).append(" ");
		name.append(this.cretaed_emp_lname);
		this.created_emp_full_name = name.toString();
		return created_emp_full_name;
	}

	public void setCreated_emp_full_name(String created_emp_full_name) {
		this.created_emp_full_name = created_emp_full_name;
	}

	public Integer getCretaed_emp_id() {
		return cretaed_emp_id;
	}

	public void setCretaed_emp_id(Integer cretaed_emp_id) {
		this.cretaed_emp_id = cretaed_emp_id;
	}

	public String getModified_emp_fname() {
		return modified_emp_fname;
	}

	public void setModified_emp_fname(String modified_emp_fname) {
		this.modified_emp_fname = modified_emp_fname;
	}

	public String getModified_emp_mname() {
		return modified_emp_mname;
	}

	public void setModified_emp_mname(String modified_emp_mname) {
		this.modified_emp_mname = modified_emp_mname;
	}

	public String getModified_emp_lname() {
		return modified_emp_lname;
	}

	public void setModified_emp_lname(String modified_emp_lname) {
		this.modified_emp_lname = modified_emp_lname;
	}

	public String getModified_emp_fullname() {
		StringBuilder name = new StringBuilder("");
		name.append(this.modified_emp_fname).append(" ");
		name.append(this.modified_emp_mname).append(" ");
		name.append(this.modified_emp_lname);
		this.modified_emp_fullname = name.toString();
		return modified_emp_fullname;
	}

	public void setModified_emp_fullname(String modified_emp_fullname) {
		this.modified_emp_fullname = modified_emp_fullname;
	}

	public Integer getModified_emp_id() {
		return modified_emp_id;
	}

	public void setModified_emp_id(Integer modified_emp_id) {
		this.modified_emp_id = modified_emp_id;
	}

	public String getCategory_name() {
		if(this.payadviceCategories != null) {
			this.category_name = this.payadviceCategories.getCat_name();
		}
		return category_name;
	}

	public void setCategory_name(String category_name) {
		
		this.category_name = category_name;
	}

	public Integer getCategory_id() {
		if(this.payadviceCategories != null) {
			this.category_id = this.payadviceCategories.getId();
		}
		return category_id;
	}

	public void setCategory_id(Integer category_id) {
		this.category_id = category_id;
	}

	public String getSub_category_name() {
		if(this.payadviceSubCategories != null) {
			this.sub_category_name = this.payadviceSubCategories.getPay_subcategory_name();
		}
		return sub_category_name;
	}

	public void setSub_category_name(String sub_category_name) {
		this.sub_category_name = sub_category_name;
	}

	public Integer getSub_category_id() {
		if(this.payadviceSubCategories != null) {
			this.sub_category_id = this.payadviceSubCategories.getId();
		}
		return sub_category_id;
	}

	public void setSub_category_id(Integer sub_category_id) {
		this.sub_category_id = sub_category_id;
	}
	public Vendors() {
		
	}

	public Vendors(Integer id, String vendor_name, String vendor_address, String gst, String contact_person,
			String phone_number, String email_id, String website, String bank_name, String account_no, String ifsc_code,
			boolean status, Date createdDate, Date lastModifiedDate,PayadviceCategories payadviceCategories,PayadviceSubCategories payadviceSubCategories,
			String cretaed_emp_fname, String cretaed_emp_mname,String cretaed_emp_lname, Integer cretaed_emp_id,
			String modified_emp_fname,String modified_emp_mname, String modified_emp_lname,  Integer modified_emp_id) {
		this.id = id;
		this.vendor_name = vendor_name;
		this.vendor_address = vendor_address;
		this.gst = gst;
		this.contact_person = contact_person;
		this.phone_number = phone_number;
		this.email_id = email_id;
		this.website = website;
		this.bank_name = bank_name;
		this.account_no = account_no;
		this.ifsc_code = ifsc_code;
		this.status = status;
		this.createdDate = createdDate;
		this.lastModifiedDate = lastModifiedDate;
		this.category_name = payadviceCategories.getCat_name();
		this.category_id = payadviceCategories.getId();
		this.sub_category_name = payadviceSubCategories.getPay_subcategory_name();
		this.sub_category_id = payadviceSubCategories.getId();
		this.cretaed_emp_fname = cretaed_emp_fname;
		this.cretaed_emp_mname = cretaed_emp_mname;
		this.cretaed_emp_lname = cretaed_emp_lname;
		this.cretaed_emp_id = cretaed_emp_id;
		this.modified_emp_fname = modified_emp_fname;
		this.modified_emp_mname = modified_emp_mname;
		this.modified_emp_lname = modified_emp_lname;
		this.modified_emp_id = modified_emp_id;
	
	}
	
	

		
}
