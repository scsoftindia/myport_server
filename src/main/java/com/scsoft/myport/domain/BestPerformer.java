package com.scsoft.myport.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "best_performer")
public class BestPerformer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	// @Column(name = "performer_id")
	// private Integer performerId;
	@ManyToOne(targetEntity = EmpPersonal.class)
	@JoinColumn(name = "performerId", referencedColumnName = "emp_id")
	private EmpPersonal empPersonal;
	@Column(name = "f_year")
	private String fYear;
	@Column(name = "quarter")
	private String quarter;
	@Column(name = "achieve1")
	private String achieve1;
	@Column(name = "achieve2")
	private String achieve2;
	@Column(name = "achieve3")
	private String achieve3;
	@Column(name = "achieve4")
	private String achieve4;
	@Column(name = "achieve5")
	private String achieve5;
	@Column(name = "app1")
	private String app1;
	@Column(name = "app1_by")
	private String app1By;
	@Column(name = "app2")
	private String app2;
	@Column(name = "app2_by")
	private String app2By;
	@Column(name = "app3")
	private String app3;
	@Column(name = "app3_by")
	private String app3By;
	@Column(name = "app4")
	private String app4;
	@Column(name = "app4_by")
	private String app4By;
	@Column(name = "app5")
	private String app5;
	@Column(name = "app5_by")
	private String app5By;
	@Column(name = "entered_by")
	@JsonIgnore
	private Integer enteredBy;
	@Column(name = "entered_on")
	@JsonIgnore
	private Date enteredOn;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public EmpPersonal getEmpPersonal() {
		return empPersonal;
	}

	public void setEmpPersonal(EmpPersonal empPersonal) {
		this.empPersonal = empPersonal;
	}

	public String getfYear() {
		return fYear;
	}

	public void setfYear(String fYear) {
		this.fYear = fYear;
	}

	public String getQuarter() {
		return quarter;
	}

	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}

	public String getAchieve1() {
		return achieve1;
	}

	public void setAchieve1(String achieve1) {
		this.achieve1 = achieve1;
	}

	public String getAchieve2() {
		return achieve2;
	}

	public void setAchieve2(String achieve2) {
		this.achieve2 = achieve2;
	}

	public String getAchieve3() {
		return achieve3;
	}

	public void setAchieve3(String achieve3) {
		this.achieve3 = achieve3;
	}

	public String getAchieve4() {
		return achieve4;
	}

	public void setAchieve4(String achieve4) {
		this.achieve4 = achieve4;
	}

	public String getAchieve5() {
		return achieve5;
	}

	public void setAchieve5(String achieve5) {
		this.achieve5 = achieve5;
	}

	public String getApp1() {
		return app1;
	}

	public void setApp1(String app1) {
		this.app1 = app1;
	}

	public String getApp1By() {
		return app1By;
	}

	public void setApp1By(String app1By) {
		this.app1By = app1By;
	}

	public String getApp2() {
		return app2;
	}

	public void setApp2(String app2) {
		this.app2 = app2;
	}

	public String getApp2By() {
		return app2By;
	}

	public void setApp2By(String app2By) {
		this.app2By = app2By;
	}

	public String getApp3() {
		return app3;
	}

	public void setApp3(String app3) {
		this.app3 = app3;
	}

	public String getApp3By() {
		return app3By;
	}

	public void setApp3By(String app3By) {
		this.app3By = app3By;
	}

	public String getApp4() {
		return app4;
	}

	public void setApp4(String app4) {
		this.app4 = app4;
	}

	public String getApp4By() {
		return app4By;
	}

	public void setApp4By(String app4By) {
		this.app4By = app4By;
	}

	public String getApp5() {
		return app5;
	}

	public void setApp5(String app5) {
		this.app5 = app5;
	}

	public String getApp5By() {
		return app5By;
	}

	public void setApp5By(String app5By) {
		this.app5By = app5By;
	}

	public Integer getEnteredBy() {
		return enteredBy;
	}

	public void setEnteredBy(Integer enteredBy) {
		this.enteredBy = enteredBy;
	}

	public Date getEnteredOn() {
		return enteredOn;
	}

	public void setEnteredOn(Date enteredOn) {
		this.enteredOn = enteredOn;
	}

}
