package com.scsoft.myport.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.scsoft.myport.service.util.DateDeserializer;
import com.scsoft.myport.service.util.DateSerializer;

import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "apply_leave")
public class ApplyLeave {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@ManyToOne(targetEntity = EmpPersonal.class)
	@JoinColumn(name = "emp_id", referencedColumnName = "emp_id")
	private EmpPersonal empPersonal;
	@ManyToOne(targetEntity = LeaveTypes.class)
	@JoinColumn(name = "leaveTypeId", referencedColumnName = "leave_type_id")
	private LeaveTypes leaveTypes;
	@ManyToOne(targetEntity = LeaveTypes.class)
	@JoinColumn(name = "half_day_type", referencedColumnName = "leave_type_id")
	private LeaveTypes halfDayTypeLeaveTypes;
	@Column(name = "applied_date")
	private Date appliedDate;
	@Column(name = "from_date")
	private Date fromDate;
	@Column(name = "to_date")
	private Date toDate;
	@Column(name = "no_of_days")
	private BigDecimal noOfDays;
	@Column(name = "leave_type_remaining")
	private BigDecimal leaveTypeRemaining;
	@Column(name = "leave_reason")
	private String leaveReason;
	@Column(name = "accept_lop", columnDefinition = "enum('Yes','No')")
	private String acceptLop;
	@Column(name = "hr_approval", columnDefinition = "enum('Not required','Pending','Approved','Rejected')")
	private String hrApproval;
	@Column(name = "hr_approval_note")
	private String hrApprovalNote;
	@Column(name = "hr_approval_date")
	private Date hrApprovalDate;
	@Column(name = "sup_approval", columnDefinition = "enum('Pending','Approved','Rejected')")
	private String supApproval;
	@Column(name = "sup_approval_note")
	private String supApprovalNote;
	@Column(name = "sup_approval_date")
	private Date supApprovalDate;
	@Column(name = "add_sup_id")
	private Integer addSupId;
	@Column(name = "addsup_approval", columnDefinition = " enum('Pending','Approved','Rejected')")
	private String addsup_approval;
	@Column(name = "addsup_approval_note")
	private String addsupApprovalNote;
	@Column(name = "addsup_approval_date")
	private Date addsupApprovalDate;
	@Column(name = "attached_doc")
	private String attachedDoc;
	@Column(name = "cancel_date")
	private Date cancelDate;
	@Column(name = "cancel_reason")
	private String cancelReason;
	@Column(name = "cancel_approval", columnDefinition = "enum('Pending','Approved','Rejected')")
	private String cancel_approval;
	@Column(name = "cancel_approval_note")
	private String cancelApprovalNote;
	@Column(name = "cancel_approval_date")
	private Date cancelApprovalDate;
	@Column(name = "cancel_approval_by")
	private Integer cancelApprovalBy;
	@Column(name = "application_status")
	private Integer applicationStatus;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public EmpPersonal getEmpPersonal() {
		return empPersonal;
	}

	public void setEmpPersonal(EmpPersonal empPersonal) {
		this.empPersonal = empPersonal;
	}

	public LeaveTypes getLeaveTypes() {
		return leaveTypes;
	}

	public void setLeaveTypes(LeaveTypes leaveTypes) {
		this.leaveTypes = leaveTypes;
	}

	public LeaveTypes getHalfDayTypeLeaveTypes() {
		return halfDayTypeLeaveTypes;
	}

	public void setHalfDayTypeLeaveTypes(LeaveTypes halfDayTypeLeaveTypes) {
		this.halfDayTypeLeaveTypes = halfDayTypeLeaveTypes;
	}

	public Date getAppliedDate() {
		return appliedDate;
	}

	public void setAppliedDate(Date appliedDate) {
		this.appliedDate = appliedDate;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public BigDecimal getNoOfDays() {
		return noOfDays;
	}

	public void setNoOfDays(BigDecimal noOfDays) {
		this.noOfDays = noOfDays;
	}

	public BigDecimal getLeaveTypeRemaining() {
		return leaveTypeRemaining;
	}

	public void setLeaveTypeRemaining(BigDecimal leaveTypeRemaining) {
		this.leaveTypeRemaining = leaveTypeRemaining;
	}

	public String getLeaveReason() {
		return leaveReason;
	}

	public void setLeaveReason(String leaveReason) {
		this.leaveReason = leaveReason;
	}

	public String getAcceptLop() {
		return acceptLop;
	}

	public void setAcceptLop(String acceptLop) {
		this.acceptLop = acceptLop;
	}

	public String getHrApproval() {
		return hrApproval;
	}

	public void setHrApproval(String hrApproval) {
		this.hrApproval = hrApproval;
	}

	public String getHrApprovalNote() {
		return hrApprovalNote;
	}

	public void setHrApprovalNote(String hrApprovalNote) {
		this.hrApprovalNote = hrApprovalNote;
	}

	public Date getHrApprovalDate() {
		return hrApprovalDate;
	}

	public void setHrApprovalDate(Date hrApprovalDate) {
		this.hrApprovalDate = hrApprovalDate;
	}

	public String getSupApproval() {
		return supApproval;
	}

	public void setSupApproval(String supApproval) {
		this.supApproval = supApproval;
	}

	public String getSupApprovalNote() {
		return supApprovalNote;
	}

	public void setSupApprovalNote(String supApprovalNote) {
		this.supApprovalNote = supApprovalNote;
	}

	public Date getSupApprovalDate() {
		return supApprovalDate;
	}

	public void setSupApprovalDate(Date supApprovalDate) {
		this.supApprovalDate = supApprovalDate;
	}

	public Integer getAddSupId() {
		return addSupId;
	}

	public void setAddSupId(Integer addSupId) {
		this.addSupId = addSupId;
	}

	public String getAddsup_approval() {
		return addsup_approval;
	}

	public void setAddsup_approval(String addsup_approval) {
		this.addsup_approval = addsup_approval;
	}

	public String getAddsupApprovalNote() {
		return addsupApprovalNote;
	}

	public void setAddsupApprovalNote(String addsupApprovalNote) {
		this.addsupApprovalNote = addsupApprovalNote;
	}

	public Date getAddsupApprovalDate() {
		return addsupApprovalDate;
	}

	public void setAddsupApprovalDate(Date addsupApprovalDate) {
		this.addsupApprovalDate = addsupApprovalDate;
	}

	public String getAttachedDoc() {
		return attachedDoc;
	}

	public void setAttachedDoc(String attachedDoc) {
		this.attachedDoc = attachedDoc;
	}

	public Date getCancelDate() {
		return cancelDate;
	}

	public void setCancelDate(Date cancelDate) {
		this.cancelDate = cancelDate;
	}

	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public String getCancel_approval() {
		return cancel_approval;
	}

	public void setCancel_approval(String cancel_approval) {
		this.cancel_approval = cancel_approval;
	}

	public String getCancelApprovalNote() {
		return cancelApprovalNote;
	}

	public void setCancelApprovalNote(String cancelApprovalNote) {
		this.cancelApprovalNote = cancelApprovalNote;
	}

	public Date getCancelApprovalDate() {
		return cancelApprovalDate;
	}

	public void setCancelApprovalDate(Date cancelApprovalDate) {
		this.cancelApprovalDate = cancelApprovalDate;
	}

	public Integer getCancelApprovalBy() {
		return cancelApprovalBy;
	}

	public void setCancelApprovalBy(Integer cancelApprovalBy) {
		this.cancelApprovalBy = cancelApprovalBy;
	}

	public Integer getApplicationStatus() {
		return applicationStatus;
	}

	public void setApplicationStatus(Integer applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

}
