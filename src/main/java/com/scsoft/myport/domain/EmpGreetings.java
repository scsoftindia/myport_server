package com.scsoft.myport.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.scsoft.myport.service.util.DateDeserializer;
import com.scsoft.myport.service.util.DateSerializer;

import java.util.Date;
@Entity
@Table(name = "emp_greetings")
public class EmpGreetings {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	private int greeting_id;
	private String greetings_emp;
	private int by_emp;
//	@JsonDeserialize(using = DateDeserializer.class)
//	@JsonSerialize(using = DateSerializer.class)
	private Date posted_on;
	private int status;
	private int reviewed_by;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getGreeting_id() {
		return greeting_id;
	}
	public void setGreeting_id(int greeting_id) {
		this.greeting_id = greeting_id;
	}
	public String getGreetings_emp() {
		return greetings_emp;
	}
	public void setGreetings_emp(String greetings_emp) {
		this.greetings_emp = greetings_emp;
	}
	public int getBy_emp() {
		return by_emp;
	}
	public void setBy_emp(int by_emp) {
		this.by_emp = by_emp;
	}
	public Date getPosted_on() {
		return posted_on;
	}
	public void setPosted_on(Date posted_on) {
		this.posted_on = posted_on;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getReviewed_by() {
		return reviewed_by;
	}
	public void setReviewed_by(int reviewed_by) {
		this.reviewed_by = reviewed_by;
	}
	
}
