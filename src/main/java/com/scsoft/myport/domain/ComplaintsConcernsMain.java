package com.scsoft.myport.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.scsoft.myport.service.util.DateDeserializer;
import com.scsoft.myport.service.util.DateSerializer;

@Entity
@Table(name = "complaints_concerns_main")
public class ComplaintsConcernsMain {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	private Integer emp_id;
	private Integer cc_id;
	private String cc_subject;
	private String cc_remark;
	private String cc_doc;
	private Integer cc_forward;
	@Column(name = "cc_resolved", columnDefinition = "enum('Forwarded','Yes','No','Pending')")
	private String cc_resolved;
	private String cc_resolve_remark;
	private Date cc_resolved_on;
	private Integer cc_resolved_by;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEmp_id() {
		return emp_id;
	}

	public void setEmp_id(Integer emp_id) {
		this.emp_id = emp_id;
	}

	public Integer getCc_id() {
		return cc_id;
	}

	public void setCc_id(Integer cc_id) {
		this.cc_id = cc_id;
	}

	public String getCc_subject() {
		return cc_subject;
	}

	public void setCc_subject(String cc_subject) {
		this.cc_subject = cc_subject;
	}

	public String getCc_remark() {
		return cc_remark;
	}

	public void setCc_remark(String cc_remark) {
		this.cc_remark = cc_remark;
	}

	public String getCc_doc() {
		return cc_doc;
	}

	public void setCc_doc(String cc_doc) {
		this.cc_doc = cc_doc;
	}

	public Integer getCc_forward() {
		return cc_forward;
	}

	public void setCc_forward(Integer cc_forward) {
		this.cc_forward = cc_forward;
	}

	public String getCc_resolved() {
		return cc_resolved;
	}

	public void setCc_resolved(String cc_resolved) {
		this.cc_resolved = cc_resolved;
	}

	public String getCc_resolve_remark() {
		return cc_resolve_remark;
	}

	public void setCc_resolve_remark(String cc_resolve_remark) {
		this.cc_resolve_remark = cc_resolve_remark;
	}

	public Date getCc_resolved_on() {
		return cc_resolved_on;
	}

	public void setCc_resolved_on(Date cc_resolved_on) {
		this.cc_resolved_on = cc_resolved_on;
	}

	public Integer getCc_resolved_by() {
		return cc_resolved_by;
	}

	public void setCc_resolved_by(Integer cc_resolved_by) {
		this.cc_resolved_by = cc_resolved_by;
	}

	

}
