package com.scsoft.myport.domain;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "leave_availability")
public class LeaveAvailability {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	private int emp_id;
//	private int leave_type_id;
	@ManyToOne(targetEntity=LeaveTypes.class)
	@JoinColumn(name="leave_type_id",referencedColumnName="leave_type_id")
	private LeaveTypes leaveTypes;
	private BigDecimal leaves_remaining;
	private BigDecimal leaves_used;
	@Column(name="year",columnDefinition="YEAR")
	private Integer year;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEmp_id() {
		return emp_id;
	}

	public void setEmp_id(int emp_id) {
		this.emp_id = emp_id;
	}

//	public int getLeave_type_id() {
//		return leave_type_id;
//	}
//
//	public void setLeave_type_id(int leave_type_id) {
//		this.leave_type_id = leave_type_id;
//	}
	

	public BigDecimal getLeaves_remaining() {
		return leaves_remaining;
	}

	public LeaveTypes getLeaveTypes() {
		return leaveTypes;
	}

	public void setLeaveTypes(LeaveTypes leaveTypes) {
		this.leaveTypes = leaveTypes;
	}

	public void setLeaves_remaining(BigDecimal leaves_remaining) {
		this.leaves_remaining = leaves_remaining;
	}

	public BigDecimal getLeaves_used() {
		return leaves_used;
	}

	public void setLeaves_used(BigDecimal leaves_used) {
		this.leaves_used = leaves_used;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}
	

}
