package com.scsoft.myport.domain;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "holiday_availability")
public class HolidayAvailability {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	private int emp_id;
	private BigDecimal holidays_availed;
	private BigDecimal holidays_remaining;
	@Column(name = "year" ,columnDefinition="YEAR")
	private Integer year;
	@Transient
	private Integer holidays_taken;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEmp_id() {
		return emp_id;
	}

	public void setEmp_id(int emp_id) {
		this.emp_id = emp_id;
	}

	public BigDecimal getHolidays_availed() {
		return holidays_availed;
	}

	public void setHolidays_availed(BigDecimal holidays_availed) {
		this.holidays_availed = holidays_availed;
	}

	public BigDecimal getHolidays_remaining() {
		return holidays_remaining;
	}

	public void setHolidays_remaining(BigDecimal holidays_remaining) {
		this.holidays_remaining = holidays_remaining;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Integer getHolidays_taken() {
		return holidays_taken;
	}

	public void setHolidays_taken(Integer holidays_taken) {
		this.holidays_taken = holidays_taken;
	}
	
	

}
