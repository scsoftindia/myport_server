package com.scsoft.myport.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "leave_types")
public class LeaveTypes {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "leave_type_id")
	private Integer id;
	private String leave_type_name;
	private String leave_type_desc;
	private int leave_type_status;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLeave_type_name() {
		return leave_type_name;
	}

	public void setLeave_type_name(String leave_type_name) {
		this.leave_type_name = leave_type_name;
	}

	public String getLeave_type_desc() {
		return leave_type_desc;
	}

	public void setLeave_type_desc(String leave_type_desc) {
		this.leave_type_desc = leave_type_desc;
	}

	public int getLeave_type_status() {
		return leave_type_status;
	}

	public void setLeave_type_status(int leave_type_status) {
		this.leave_type_status = leave_type_status;
	}

}
