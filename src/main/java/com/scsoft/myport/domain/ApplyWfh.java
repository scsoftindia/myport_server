package com.scsoft.myport.domain;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "apply_wfh")
public class ApplyWfh {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@ManyToOne(targetEntity = EmpPersonal.class)
	@JoinColumn(name = "emp_id", referencedColumnName = "emp_id")
	private EmpPersonal empPersonal;
	private Date applied_date;
	private Date from_date;
	private Date to_date;
	@Column(name= "no_of_days", columnDefinition="decimal", precision=5, scale=2)
	private BigDecimal no_of_days;
	private String wfh_reason;
	@Column(name = "sup_approval", columnDefinition = "enum('Pending','Approved','Rejected')")
	private String sup_approval;
	private String sup_approval_note;
	private Date sup_approval_date;
	private Integer add_sup_id;
	@Column(name = "addsup_approval", columnDefinition = "enum('Pending','Approved','Rejected')")
	private String addsup_approval;
	private String addsup_approval_note;
	private Date addsup_approval_date;
	private Date cancel_date;
	private String cancel_reason;
	@Column(name = "cancel_approval", columnDefinition = "enum('Pending','Approved','Rejected')")
	private String cancel_approval;
	private String cancel_approval_note;
	private Date cancel_approval_date;
	private Integer cancel_approval_by;
	private Integer application_status;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public EmpPersonal getEmpPersonal() {
		return empPersonal;
	}

	public void setEmpPersonal(EmpPersonal empPersonal) {
		this.empPersonal = empPersonal;
	}

	public Date getApplied_date() {
		return applied_date;
	}

	public void setApplied_date(Date applied_date) {
		this.applied_date = applied_date;
	}

	public Date getFrom_date() {
		return from_date;
	}

	public void setFrom_date(Date from_date) {
		this.from_date = from_date;
	}

	public Date getTo_date() {
		return to_date;
	}

	public void setTo_date(Date to_date) {
		this.to_date = to_date;
	}

	public BigDecimal getNo_of_days() {
		return no_of_days;
	}

	public void setNo_of_days(BigDecimal no_of_days) {
		this.no_of_days = no_of_days;
	}

	public String getWfh_reason() {
		return wfh_reason;
	}

	public void setWfh_reason(String wfh_reason) {
		this.wfh_reason = wfh_reason;
	}

	public String getSup_approval() {
		return sup_approval;
	}

	public void setSup_approval(String sup_approval) {
		this.sup_approval = sup_approval;
	}

	public String getSup_approval_note() {
		return sup_approval_note;
	}

	public void setSup_approval_note(String sup_approval_note) {
		this.sup_approval_note = sup_approval_note;
	}

	public Date getSup_approval_date() {
		return sup_approval_date;
	}

	public void setSup_approval_date(Date sup_approval_date) {
		this.sup_approval_date = sup_approval_date;
	}

	public Integer getAdd_sup_id() {
		return add_sup_id;
	}

	public void setAdd_sup_id(Integer add_sup_id) {
		this.add_sup_id = add_sup_id;
	}

	public String getAddsup_approval() {
		return addsup_approval;
	}

	public void setAddsup_approval(String addsup_approval) {
		this.addsup_approval = addsup_approval;
	}

	public String getAddsup_approval_note() {
		return addsup_approval_note;
	}

	public void setAddsup_approval_note(String addsup_approval_note) {
		this.addsup_approval_note = addsup_approval_note;
	}

	public Date getAddsup_approval_date() {
		return addsup_approval_date;
	}

	public void setAddsup_approval_date(Date addsup_approval_date) {
		this.addsup_approval_date = addsup_approval_date;
	}

	public Date getCancel_date() {
		return cancel_date;
	}

	public void setCancel_date(Date cancel_date) {
		this.cancel_date = cancel_date;
	}

	public String getCancel_reason() {
		return cancel_reason;
	}

	public void setCancel_reason(String cancel_reason) {
		this.cancel_reason = cancel_reason;
	}

	public String getCancel_approval() {
		return cancel_approval;
	}

	public void setCancel_approval(String cancel_approval) {
		this.cancel_approval = cancel_approval;
	}

	public String getCancel_approval_note() {
		return cancel_approval_note;
	}

	public void setCancel_approval_note(String cancel_approval_note) {
		this.cancel_approval_note = cancel_approval_note;
	}

	public Date getCancel_approval_date() {
		return cancel_approval_date;
	}

	public void setCancel_approval_date(Date cancel_approval_date) {
		this.cancel_approval_date = cancel_approval_date;
	}

	public Integer getCancel_approval_by() {
		return cancel_approval_by;
	}

	public void setCancel_approval_by(Integer cancel_approval_by) {
		this.cancel_approval_by = cancel_approval_by;
	}

	public Integer getApplication_status() {
		return application_status;
	}

	public void setApplication_status(Integer application_status) {
		this.application_status = application_status;
	}

}
