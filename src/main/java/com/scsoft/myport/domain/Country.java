package com.scsoft.myport.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "country")
public class Country {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "country_id")
	private int id;
	private char iso2;
	private String short_name;
	private String long_name;
	private char iso3;
	private String numcode;
	private String un_member;
	private String calling_code;
	private String cctld;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public char getIso2() {
		return iso2;
	}

	public void setIso2(char iso2) {
		this.iso2 = iso2;
	}

	public String getShort_name() {
		return short_name;
	}

	public void setShort_name(String short_name) {
		this.short_name = short_name;
	}

	public String getLong_name() {
		return long_name;
	}

	public void setLong_name(String long_name) {
		this.long_name = long_name;
	}

	public char getIso3() {
		return iso3;
	}

	public void setIso3(char iso3) {
		this.iso3 = iso3;
	}

	public String getNumcode() {
		return numcode;
	}

	public void setNumcode(String numcode) {
		this.numcode = numcode;
	}

	public String getUn_member() {
		return un_member;
	}

	public void setUn_member(String un_member) {
		this.un_member = un_member;
	}

	public String getCalling_code() {
		return calling_code;
	}

	public void setCalling_code(String calling_code) {
		this.calling_code = calling_code;
	}

	public String getCctld() {
		return cctld;
	}

	public void setCctld(String cctld) {
		this.cctld = cctld;
	}
}
