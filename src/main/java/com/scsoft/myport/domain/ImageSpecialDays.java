package com.scsoft.myport.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.scsoft.myport.service.util.DateDeserializer;
import com.scsoft.myport.service.util.DateSerializer;

import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "image_special_days")
public class ImageSpecialDays {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	private String special_day;
//	@JsonDeserialize(using = DateDeserializer.class)
//	@JsonSerialize(using = DateSerializer.class)
	private Date special_date;
	private String image;
	@Column(name = "is_deleted", columnDefinition = "TINYINT")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean is_deleted;
	private Timestamp  created_on;
	private int created_by;
	private Timestamp  last_modified_on;
	private int last_modified_by;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSpecial_day() {
		return special_day;
	}

	public void setSpecial_day(String special_day) {
		this.special_day = special_day;
	}

	public Date getSpecial_date() {
		return special_date;
	}

	public void setSpecial_date(Date special_date) {
		this.special_date = special_date;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public boolean isIs_deleted() {
		return is_deleted;
	}

	public void setIs_deleted(boolean is_deleted) {
		this.is_deleted = is_deleted;
	}

	public Timestamp  getCreated_on() {
		return created_on;
	}

	public void setCreated_on(Timestamp  created_on) {
		this.created_on = created_on;
	}

	public int getCreated_by() {
		return created_by;
	}

	public void setCreated_by(int created_by) {
		this.created_by = created_by;
	}

	public Timestamp  getLast_modified_on() {
		return last_modified_on;
	}

	public void setLast_modified_on(Timestamp  last_modified_on) {
		this.last_modified_on = last_modified_on;
	}

	public int getLast_modified_by() {
		return last_modified_by;
	}

	public void setLast_modified_by(int last_modified_by) {
		this.last_modified_by = last_modified_by;
	}

}
