package com.scsoft.myport.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.scsoft.myport.service.util.DateDeserializer;
import com.scsoft.myport.service.util.DateSerializer;

@Entity
@Table(name = "albums")
public class Albums {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "album_id")
	private Integer id;
	@Column(name = "album_name")
	private String name;
	@Column(name = "event_date")
	private Date eventDate;
	@Column(name = "description")
	private String description;
	@Column(name = "entered_by")
	private Integer entered_by;
	@Column(name = "entered_on")
	private Date enteredOn;
	@Column(name = "last_modified_by")
	private Integer lastModifiedBy;
	@Column(name = "last_modified_on")
	private Date last_modified_on;
	@Transient
	private List<String> albumImages;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getEntered_by() {
		return entered_by;
	}

	public void setEntered_by(Integer entered_by) {
		this.entered_by = entered_by;
	}

	public Date getEnteredOn() {
		return enteredOn;
	}

	public void setEnteredOn(Date enteredOn) {
		this.enteredOn = enteredOn;
	}

	public Integer getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(Integer lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public Date getLast_modified_on() {
		return last_modified_on;
	}

	public void setLast_modified_on(Date last_modified_on) {
		this.last_modified_on = last_modified_on;
	}

	public List<String> getAlbumImages() {
		return albumImages;
	}

	public void setAlbumImages(List<String> albumImages) {
		this.albumImages = albumImages;
	}


}
