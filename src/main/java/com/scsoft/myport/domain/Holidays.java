package com.scsoft.myport.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.scsoft.myport.service.util.DateDeserializer;
import com.scsoft.myport.service.util.DateSerializer;

import java.util.Date;

@Entity
@Table(name = "holidays")
public class Holidays {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	private String holiday_name;
	private Date holiday_date;
	private String holiday_desc;
	@Column(name = "holiday_year", columnDefinition = "YEAR")
	private Integer holiday_year;
	private String holiday_status;
	private Integer entered_by;
	private Date entered_on;
	private Integer last_edited_by;
	private Date last_edited_on;
	@Column(name = "holiday_loc")
	private Integer holidayLocation;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getHoliday_name() {
		return holiday_name;
	}

	public void setHoliday_name(String holiday_name) {
		this.holiday_name = holiday_name;
	}

	public Date getHoliday_date() {
		return holiday_date;
	}

	public void setHoliday_date(Date holiday_date) {
		this.holiday_date = holiday_date;
	}

	public String getHoliday_desc() {
		return holiday_desc;
	}

	public void setHoliday_desc(String holiday_desc) {
		this.holiday_desc = holiday_desc;
	}

	public Integer getHoliday_year() {
		return holiday_year;
	}

	public void setHoliday_year(Integer holiday_year) {
		this.holiday_year = holiday_year;
	}

	public String getHoliday_status() {
		return holiday_status;
	}

	public void setHoliday_status(String holiday_status) {
		this.holiday_status = holiday_status;
	}

	public Integer getEntered_by() {
		return entered_by;
	}

	public void setEntered_by(Integer entered_by) {
		this.entered_by = entered_by;
	}

	public Date getEntered_on() {
		return entered_on;
	}

	public void setEntered_on(Date entered_on) {
		this.entered_on = entered_on;
	}

	public Integer getLast_edited_by() {
		return last_edited_by;
	}

	public void setLast_edited_by(Integer last_edited_by) {
		this.last_edited_by = last_edited_by;
	}

	public Date getLast_edited_on() {
		return last_edited_on;
	}

	public void setLast_edited_on(Date last_edited_on) {
		this.last_edited_on = last_edited_on;
	}

	public Integer getHolidayLocation() {
		return holidayLocation;
	}

	public void setHolidayLocation(Integer holidayLocation) {
		this.holidayLocation = holidayLocation;
	}
	
	

}
