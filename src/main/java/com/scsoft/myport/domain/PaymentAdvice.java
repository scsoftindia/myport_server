package com.scsoft.myport.domain;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.joda.time.DateTime;

import java.util.Date;
import java.util.List;

@Entity
@Table(name = "payment_advice")
public class PaymentAdvice {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pay_advice_id ")
	private Integer id;
	@ManyToOne(targetEntity = PayadviceCategories.class)
	@JoinColumn(name = "category_id", referencedColumnName = "id")
	private PayadviceCategories payadviceCategories;
	@ManyToOne(targetEntity = PayadviceSubCategories.class)
	@JoinColumn(name = "sub_category_id", referencedColumnName = "id")
	private PayadviceSubCategories payadviceSubCategories;
	@ManyToOne(targetEntity = Vendors.class)
	@JoinColumn(name = "vendor_id", referencedColumnName = "id")
	private Vendors vendors;
	private String description;
	private Date bill_date;
	private Date due_date;
	private String bill_number;
	private double bill_amount;
	@Column(name="bill_period_from")
	private Date bill_period_from;
	@Column(name="bill_peroid_to")
	private Date bill_peroid_to;
	@ManyToOne(targetEntity = CurrencyType.class)
	@JoinColumn(name = "currency_type_id", referencedColumnName = "id")
	private CurrencyType currencyType;
	@ManyToOne(targetEntity = PaymentModes.class)
	@JoinColumn(name = "payment_mode", referencedColumnName = "id")
	private PaymentModes paymentModes;
	@Column(columnDefinition = "DATETIME")
	private DateTime payment_release_time;
	@Column(columnDefinition = "DATETIME")
	private DateTime payment_release_reject_time;
	private String release_note;
	private String release_reject_note;
	private String payment_cash_billno;
	private String payment_cheque_number;
	private Date payment_cheque_date;
	private Date payment_online_date;
	@Column(columnDefinition = "DATETIME")
	private DateTime applied_on;
	@ManyToOne(targetEntity = ApprovalStatusType.class)
	@JoinColumn(name = "aprroval_status", referencedColumnName = "id")
	private ApprovalStatusType aprrovalStatus;
	@Column(columnDefinition = "DATETIME")
	private DateTime last_modified_date;
	@ManyToOne(targetEntity = EmpEmployment.class)
	@JoinColumn(name = "last_modified_by", referencedColumnName = "emp_id")
	private EmpEmployment lastModifiedBy;
	@Column(name = "created_date",columnDefinition = "DATETIME")
	private DateTime createdDate;
	@ManyToOne(targetEntity = EmpEmployment.class)
	@JoinColumn(name = "created_by", referencedColumnName = "emp_id")
	private EmpEmployment createdBy;
	@Column(columnDefinition = "DATETIME")
	private DateTime approved_date;
	private String approval_comment;
	private String reject_reson;
	@Column(columnDefinition = "DATETIME")
	private DateTime reject_date;
	@Column(name = "payment_remark", columnDefinition = "enum('PART','FULL')")
	@Enumerated(EnumType.STRING)
	private PaymentRemark paymentRemark;
	@OneToMany(fetch = FetchType.EAGER, targetEntity = PaymentAdviceDocuments.class)
	@JoinColumn(name = "pay_advice_id", referencedColumnName = "pay_advice_id", updatable = false, insertable = false)
	private List<PaymentAdviceDocuments> paymentAdviceDocumentsList;
	private String applied_emp_name;
	private Integer applied_emp_Id;
	private String payment_release_emp_name;
	private Integer payment_release_emp_Id;
	private String reject_emp_name;
	private Integer reject_emp_Id;
	private String aprooved_emp_name;
	private Integer aprooved_emp_Id;
	private String primary_aproover_emp_name;
	private Integer primary_aproover_emp_Id;
	private Integer secondary_aproover_emp_id;
	private String secondary_aproover_name;
	private Boolean is_admin_rejected;
	private Boolean is_finance_rejected;
	private Boolean is_forward_rejected;
	@ManyToOne(targetEntity=PaymentBank.class)
	@JoinColumn(name="payment_bank_id",referencedColumnName="id")
	private PaymentBank paymentBank;
	private String forward_approve_comment;

	public enum PaymentRemark {
		PART, FULL
	}

	public PaymentAdvice() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public PayadviceCategories getPayadviceCategories() {
		return payadviceCategories;
	}

	public void setPayadviceCategories(PayadviceCategories payadviceCategories) {
		this.payadviceCategories = payadviceCategories;
	}

	public PayadviceSubCategories getPayadviceSubCategories() {
		return payadviceSubCategories;
	}

	public void setPayadviceSubCategories(PayadviceSubCategories payadviceSubCategories) {
		this.payadviceSubCategories = payadviceSubCategories;
	}

	public Vendors getVendors() {
		return vendors;
	}

	public void setVendors(Vendors vendors) {
		this.vendors = vendors;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getBill_date() {
		return bill_date;
	}
	
	

	public Date getDue_date() {
		return due_date;
	}

	public void setDue_date(Date due_date) {
		this.due_date = due_date;
	}

	public void setBill_date(Date bill_date) {
		this.bill_date = bill_date;
	}

	public String getBill_number() {
		return bill_number;
	}

	public void setBill_number(String bill_number) {
		this.bill_number = bill_number;
	}

	public double getBill_amount() {
		return bill_amount;
	}

	public void setBill_amount(double bill_amount) {
		this.bill_amount = bill_amount;
	}

	public Date getBill_period_from() {
		return bill_period_from;
	}

	public void setBill_period_from(Date bill_period_from) {
		this.bill_period_from = bill_period_from;
	}

	public Date getBill_peroid_to() {
		return bill_peroid_to;
	}

	public void setBill_peroid_to(Date bill_peroid_to) {
		this.bill_peroid_to = bill_peroid_to;
	}

	public CurrencyType getCurrencyType() {
		return currencyType;
	}

	public void setCurrencyType(CurrencyType currencyType) {
		this.currencyType = currencyType;
	}

	public PaymentModes getPaymentModes() {
		return paymentModes;
	}

	public void setPaymentModes(PaymentModes paymentModes) {
		this.paymentModes = paymentModes;
	}

	public DateTime getPayment_release_time() {
		return payment_release_time;
	}

	public void setPayment_release_time(DateTime payment_release_time) {
		this.payment_release_time = payment_release_time;
	}

	public DateTime getPayment_release_reject_time() {
		return payment_release_reject_time;
	}

	public void setPayment_release_reject_time(DateTime payment_release_reject_time) {
		this.payment_release_reject_time = payment_release_reject_time;
	}

	public String getRelease_note() {
		return release_note;
	}

	public void setRelease_note(String release_note) {
		this.release_note = release_note;
	}

	public String getRelease_reject_note() {
		return release_reject_note;
	}

	public void setRelease_reject_note(String release_reject_note) {
		this.release_reject_note = release_reject_note;
	}

	public String getPayment_cash_billno() {
		return payment_cash_billno;
	}

	public void setPayment_cash_billno(String payment_cash_billno) {
		this.payment_cash_billno = payment_cash_billno;
	}

	public String getPayment_cheque_number() {
		return payment_cheque_number;
	}

	public void setPayment_cheque_number(String payment_cheque_number) {
		this.payment_cheque_number = payment_cheque_number;
	}

	public Date getPayment_cheque_date() {
		return payment_cheque_date;
	}

	public void setPayment_cheque_date(Date payment_cheque_date) {
		this.payment_cheque_date = payment_cheque_date;
	}

	public Date getPayment_online_date() {
		return payment_online_date;
	}

	public void setPayment_online_date(Date payment_online_date) {
		this.payment_online_date = payment_online_date;
	}

	public DateTime getApplied_on() {
		return applied_on;
	}

	public void setApplied_on(DateTime applied_on) {
		this.applied_on = applied_on;
	}

	public ApprovalStatusType getAprrovalStatus() {
		return aprrovalStatus;
	}

	public void setAprrovalStatus(ApprovalStatusType aprrovalStatus) {
		this.aprrovalStatus = aprrovalStatus;
	}

	public DateTime getLast_modified_date() {
		return last_modified_date;
	}

	public void setLast_modified_date(DateTime last_modified_date) {
		this.last_modified_date = last_modified_date;
	}

	public EmpEmployment getLastModifiedBy() {
		return lastModifiedBy;
	}

	public void setLastModifiedBy(EmpEmployment lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public DateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(DateTime createdDate) {
		this.createdDate = createdDate;
	}

	public EmpEmployment getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(EmpEmployment createdBy) {
		this.createdBy = createdBy;
	}



	public DateTime getApproved_date() {
		return approved_date;
	}

	public void setApproved_date(DateTime approved_date) {
		this.approved_date = approved_date;
	}

	public String getApproval_comment() {
		return approval_comment;
	}

	public void setApproval_comment(String approval_comment) {
		this.approval_comment = approval_comment;
	}

	public String getReject_reson() {
		return reject_reson;
	}

	public void setReject_reson(String reject_reson) {
		this.reject_reson = reject_reson;
	}



	public DateTime getReject_date() {
		return reject_date;
	}

	public void setReject_date(DateTime reject_date) {
		this.reject_date = reject_date;
	}

	public PaymentRemark getPaymentRemark() {
		return paymentRemark;
	}

	public void setPaymentRemark(PaymentRemark paymentRemark) {
		this.paymentRemark = paymentRemark;
	}

	public List<PaymentAdviceDocuments> getPaymentAdviceDocumentsList() {
		return paymentAdviceDocumentsList;
	}

	public void setPaymentAdviceDocumentsList(List<PaymentAdviceDocuments> paymentAdviceDocumentsList) {
		this.paymentAdviceDocumentsList = paymentAdviceDocumentsList;
	}

	public String getApplied_emp_name() {
		return applied_emp_name;
	}

	public void setApplied_emp_name(String applied_emp_name) {
		this.applied_emp_name = applied_emp_name;
	}

	public Integer getApplied_emp_Id() {
		return applied_emp_Id;
	}

	public void setApplied_emp_Id(Integer applied_emp_Id) {
		this.applied_emp_Id = applied_emp_Id;
	}

	public String getPayment_release_emp_name() {
		return payment_release_emp_name;
	}

	public void setPayment_release_emp_name(String payment_release_emp_name) {
		this.payment_release_emp_name = payment_release_emp_name;
	}

	public Integer getPayment_release_emp_Id() {
		return payment_release_emp_Id;
	}

	public void setPayment_release_emp_Id(Integer payment_release_emp_Id) {
		this.payment_release_emp_Id = payment_release_emp_Id;
	}

	public String getReject_emp_name() {
		return reject_emp_name;
	}

	public void setReject_emp_name(String reject_emp_name) {
		this.reject_emp_name = reject_emp_name;
	}

	public Integer getReject_emp_Id() {
		return reject_emp_Id;
	}

	public void setReject_emp_Id(Integer reject_emp_Id) {
		this.reject_emp_Id = reject_emp_Id;
	}

	public String getAprooved_emp_name() {
		return aprooved_emp_name;
	}

	public void setAprooved_emp_name(String aprooved_emp_name) {
		this.aprooved_emp_name = aprooved_emp_name;
	}

	public Integer getAprooved_emp_Id() {
		return aprooved_emp_Id;
	}

	public void setAprooved_emp_Id(Integer aprooved_emp_Id) {
		this.aprooved_emp_Id = aprooved_emp_Id;
	}

	public String getPrimary_aproover_emp_name() {
		return primary_aproover_emp_name;
	}

	public void setPrimary_aproover_emp_name(String primary_aproover_emp_name) {
		this.primary_aproover_emp_name = primary_aproover_emp_name;
	}

	public Integer getPrimary_aproover_emp_Id() {
		return primary_aproover_emp_Id;
	}

	public void setPrimary_aproover_emp_Id(Integer primary_aproover_emp_Id) {
		this.primary_aproover_emp_Id = primary_aproover_emp_Id;
	}

	public Boolean getIs_admin_rejected() {
		if(this.is_admin_rejected == null) {
			this.is_admin_rejected = false;
		}
		return is_admin_rejected;
	}

	public void setIs_admin_rejected(Boolean is_admin_rejected) {
		this.is_admin_rejected = is_admin_rejected;
	}

	public Boolean getIs_finance_rejected() {
		return is_finance_rejected;
	}

	public void setIs_finance_rejected(Boolean is_finance_rejected) {
		this.is_finance_rejected = is_finance_rejected;
	}

	public PaymentBank getPaymentBank() {
		return paymentBank;
	}

	public void setPaymentBank(PaymentBank paymentBank) {
		this.paymentBank = paymentBank;
	}

	public Integer getSecondary_aproover_emp_id() {
		return secondary_aproover_emp_id;
	}

	public void setSecondary_aproover_emp_id(Integer secondary_aproover_emp_id) {
		this.secondary_aproover_emp_id = secondary_aproover_emp_id;
	}

	public String getSecondary_aproover_name() {
		return secondary_aproover_name;
	}

	public void setSecondary_aproover_name(String secondary_aproover_name) {
		this.secondary_aproover_name = secondary_aproover_name;
	}

	public Boolean getIs_forward_rejected() {
		if(this.is_forward_rejected == null) {
			this.is_forward_rejected = false;
		}
		return is_forward_rejected;
	}

	public void setIs_forward_rejected(Boolean is_forward_rejected) {
		this.is_forward_rejected = is_forward_rejected;
	}

	public String getForward_approve_comment() {
		return forward_approve_comment;
	}

	public void setForward_approve_comment(String forward_approve_comment) {
		this.forward_approve_comment = forward_approve_comment;
	}
	
	

}
