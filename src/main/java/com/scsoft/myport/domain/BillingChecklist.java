package com.scsoft.myport.domain;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.scsoft.myport.service.util.DateDeserializer;
import com.scsoft.myport.service.util.DateSerializer;

@Entity
@Table(name = "billing_checklist")
public class BillingChecklist {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "billing_id")
	private int id;
	@Column(name = "type",columnDefinition="TEXT")
	private String type;
	@Column(name = "vertical_id")
	private int verticalId;
	@Column(name = "client_id")
	private int clientId;
	@Column(name = "project_id")
	private int projectId;
	@Column(name = "client_contact")
	private String clientContact;
	@Column(name = "end_client")
	private String endClient;
	@Column(name = "po_number")
	private String poNumber;
	@Column(name = "prepared_by")
	private String preparedBy;
	@Column(name = "prepared_date")
//	@JsonDeserialize(using = DateDeserializer.class)
//	@JsonSerialize(using = DateSerializer.class)
	private Date preparedDate;
	@Column(name = "reviewed_by")
	private String reviewedBy;
	@Column(name = "reviewed_date")
//	@JsonDeserialize(using = DateDeserializer.class)
//	@JsonSerialize(using = DateSerializer.class)
	private Date reviewedDate;
	@Column(name = "invoice_no")
	private String invoiceNo;
	@Column(name = "invoice_doc")
	private String invoiceDoc;
	@Column(name = "invoice_date")
//	@JsonDeserialize(using = DateDeserializer.class)
//	@JsonSerialize(using = DateSerializer.class)
	private Date invoiceDate;
	@Column(name = "invoiced_by")
	private int invoicedBy;
	@Column(name = "remark")
	private String remark;
	@Column(name = "currency")
	private String currency;
	@Column(name = "total_value")
	private BigDecimal totalValue;
	@Column(name = "status", columnDefinition = "enum('Pending','Pending Approval','Invoiced','Approved','Rejected','Completed','Cancelled')")
	private String status;
	@Column(name = "reason",columnDefinition="TEXT")
	private String reason;
	@Column(name = "is_active" ,columnDefinition="TINYINT")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean  isActive;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getVerticalId() {
		return verticalId;
	}

	public void setVerticalId(int verticalId) {
		this.verticalId = verticalId;
	}

	public int getClientId() {
		return clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public String getClientContact() {
		return clientContact;
	}

	public void setClientContact(String clientContact) {
		this.clientContact = clientContact;
	}

	public String getEndClient() {
		return endClient;
	}

	public void setEndClient(String endClient) {
		this.endClient = endClient;
	}

	public String getPoNumber() {
		return poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public String getPreparedBy() {
		return preparedBy;
	}

	public void setPreparedBy(String preparedBy) {
		this.preparedBy = preparedBy;
	}

	public Date getPreparedDate() {
		return preparedDate;
	}

	public void setPreparedDate(Date preparedDate) {
		this.preparedDate = preparedDate;
	}

	public String getReviewedBy() {
		return reviewedBy;
	}

	public void setReviewedBy(String reviewedBy) {
		this.reviewedBy = reviewedBy;
	}

	public Date getReviewedDate() {
		return reviewedDate;
	}

	public void setReviewedDate(Date reviewedDate) {
		this.reviewedDate = reviewedDate;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getInvoiceDoc() {
		return invoiceDoc;
	}

	public void setInvoiceDoc(String invoiceDoc) {
		this.invoiceDoc = invoiceDoc;
	}

	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public int getInvoicedBy() {
		return invoicedBy;
	}

	public void setInvoicedBy(int invoicedBy) {
		this.invoicedBy = invoicedBy;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public BigDecimal getTotalValue() {
		return totalValue;
	}

	public void setTotalValue(BigDecimal totalValue) {
		this.totalValue = totalValue;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}

}
