package com.scsoft.myport.domain;

import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.scsoft.myport.service.util.DateDeserializer;
import com.scsoft.myport.service.util.DateSerializer;

@Entity
@Table(name = "timesheetentries")
public class Timesheetentries {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	private int emp_id;
	private int vertical;
	private Time actual_effort;
	private Time planned_effort;
	private Time effort_change;
	private Time last_planned;
	private Time list_planned_effort;
	private String pid;
	private String mid;
	private String month;
//	@JsonDeserialize(using = DateDeserializer.class)
//	@JsonSerialize(using = DateSerializer.class)
	private Date created_date;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEmp_id() {
		return emp_id;
	}

	public void setEmp_id(int emp_id) {
		this.emp_id = emp_id;
	}

	public int getVertical() {
		return vertical;
	}

	public void setVertical(int vertical) {
		this.vertical = vertical;
	}

	public Time getActual_effort() {
		return actual_effort;
	}

	public void setActual_effort(Time actual_effort) {
		this.actual_effort = actual_effort;
	}

	public Time getPlanned_effort() {
		return planned_effort;
	}

	public void setPlanned_effort(Time planned_effort) {
		this.planned_effort = planned_effort;
	}

	public Time getEffort_change() {
		return effort_change;
	}

	public void setEffort_change(Time effort_change) {
		this.effort_change = effort_change;
	}

	public Time getLast_planned() {
		return last_planned;
	}

	public void setLast_planned(Time last_planned) {
		this.last_planned = last_planned;
	}

	public Time getList_planned_effort() {
		return list_planned_effort;
	}

	public void setList_planned_effort(Time list_planned_effort) {
		this.list_planned_effort = list_planned_effort;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public Date getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

}
