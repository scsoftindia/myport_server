package com.scsoft.myport.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.scsoft.myport.service.util.DateDeserializer;
import com.scsoft.myport.service.util.DateSerializer;

import java.util.Date;

@Entity
@Table(name = "announcements")
public class Announcements {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(name = "announcement_heading")
	private String announcementHeading;
	@Column(name = "announcement")
	private String announcement;
	@ManyToOne(targetEntity = EmpPersonal.class)
	@JoinColumn(name = "announcement_by", referencedColumnName = "emp_id")
	private EmpPersonal announcementBy;
	@Column(name = "start_date")
	private Date startDate;
	@Column(name = "announcement_date")
	private Date announcementDate;
	@Column(name = "announcement_file")
	private String announcementFile;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAnnouncementHeading() {
		return announcementHeading;
	}

	public void setAnnouncementHeading(String announcementHeading) {
		this.announcementHeading = announcementHeading;
	}

	public String getAnnouncement() {
		return announcement;
	}

	public void setAnnouncement(String announcement) {
		this.announcement = announcement;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getAnnouncementDate() {
		return announcementDate;
	}

	public void setAnnouncementDate(Date announcementDate) {
		this.announcementDate = announcementDate;
	}

	public String getAnnouncementFile() {
		return announcementFile;
	}

	public void setAnnouncementFile(String announcementFile) {
		this.announcementFile = announcementFile;
	}

	public EmpPersonal getAnnouncementBy() {
		return announcementBy;
	}

	public void setAnnouncementBy(EmpPersonal announcementBy) {
		this.announcementBy = announcementBy;
	}

	

}
