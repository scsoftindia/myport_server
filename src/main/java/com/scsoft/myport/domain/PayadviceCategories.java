package com.scsoft.myport.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "payadvice_categories")
public class PayadviceCategories {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String cat_name;
	private String cat_description;
	@JsonIgnore
	@ManyToOne(targetEntity=EmpEmployment.class)
	@JoinColumn(name="created_by",referencedColumnName="emp_id")
	private EmpEmployment createdBy;
	@Column(name="created_date")
	private Date createdDate;
	@JsonIgnore
	@ManyToOne(targetEntity=EmpEmployment.class)
	@JoinColumn(name="modified_by",referencedColumnName="emp_id")
	private EmpEmployment lastModifiedBy;
	@Column(name="modified_date")
	private Date lastModifiedDate;
	private Boolean is_approved;
	private Boolean is_rejected;
	@Transient
	private String cretaed_emp_fname;
	@Transient
	private String cretaed_emp_mname;
	@Transient
	private String cretaed_emp_lname;
	@Transient
	private Integer cretaed_emp_id;
	@Transient
	private String modified_emp_fname;
	@Transient
	private String modified_emp_mname;
	@Transient
	private String modified_emp_lname;
	@Transient
	private Integer modified_emp_id;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCat_name() {
		return cat_name;
	}
	public void setCat_name(String cat_name) {
		this.cat_name = cat_name;
	}
	public String getCat_description() {
		return cat_description;
	}
	public void setCat_description(String cat_description) {
		this.cat_description = cat_description;
	}
	public EmpEmployment getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(EmpEmployment createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public EmpEmployment getLastModifiedBy() {
		return lastModifiedBy;
	}
	public void setLastModifiedBy(EmpEmployment lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	public Boolean getIs_approved() {
		return is_approved;
	}
	public void setIs_approved(Boolean is_approved) {
		this.is_approved = is_approved;
	}
	public String getCretaed_emp_fname() {
		return cretaed_emp_fname;
	}
	public void setCretaed_emp_fname(String cretaed_emp_fname) {
		this.cretaed_emp_fname = cretaed_emp_fname;
	}
	public String getCretaed_emp_mname() {
		return cretaed_emp_mname;
	}
	public void setCretaed_emp_mname(String cretaed_emp_mname) {
		this.cretaed_emp_mname = cretaed_emp_mname;
	}
	public String getCretaed_emp_lname() {
		return cretaed_emp_lname;
	}
	public void setCretaed_emp_lname(String cretaed_emp_lname) {
		this.cretaed_emp_lname = cretaed_emp_lname;
	}
	public Integer getCretaed_emp_id() {
		return cretaed_emp_id;
	}
	public void setCretaed_emp_id(Integer cretaed_emp_id) {
		this.cretaed_emp_id = cretaed_emp_id;
	}
	
	public String getModified_emp_fname() {
		return modified_emp_fname;
	}
	public void setModified_emp_fname(String modified_emp_fname) {
		this.modified_emp_fname = modified_emp_fname;
	}
	public String getModified_emp_mname() {
		return modified_emp_mname;
	}
	public void setModified_emp_mname(String modified_emp_mname) {
		this.modified_emp_mname = modified_emp_mname;
	}
	public String getModified_emp_lname() {
		return modified_emp_lname;
	}
	public void setModified_emp_lname(String modified_emp_lname) {
		this.modified_emp_lname = modified_emp_lname;
	}
	public Integer getModified_emp_id() {
		return modified_emp_id;
	}
	public void setModified_emp_id(Integer modified_emp_id) {
		this.modified_emp_id = modified_emp_id;
	}
	
	public Boolean getIs_rejected() {
		return is_rejected;
	}
	public void setIs_rejected(Boolean is_rejected) {
		this.is_rejected = is_rejected;
	}
	public PayadviceCategories() {
		
	}
	public PayadviceCategories(Integer id, String cat_name, String cat_description, Boolean is_approved,
			String cretaed_emp_fname, String cretaed_emp_mname, String cretaed_emp_lname,
			Integer cretaed_emp_id,String modified_emp_fname,String modified_emp_mname,String modified_emp_lname,
			Integer modified_emp_id,Date createdDate,Date lastModifiedDate  ) {
		this.id = id;
		this.cat_name = cat_name;
		this.cat_description = cat_description;
		this.is_approved = is_approved;
		this.cretaed_emp_fname = cretaed_emp_fname;
		this.cretaed_emp_mname = cretaed_emp_mname;
		this.cretaed_emp_lname = cretaed_emp_lname;
		this.cretaed_emp_id = cretaed_emp_id;
		this.modified_emp_fname = modified_emp_fname;
		this.modified_emp_mname = modified_emp_mname;
		this.modified_emp_lname = modified_emp_lname;
		this.modified_emp_id = modified_emp_id;
		this.createdDate = createdDate;
		this.lastModifiedDate = lastModifiedDate;
	}
	
	


	
	

}
