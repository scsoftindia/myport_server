package com.scsoft.myport.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.scsoft.myport.service.util.DateDeserializer;
import com.scsoft.myport.service.util.DateSerializer;

import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "emp_education")
public class EmpEducation {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	private int emp_id;
	private String institution;
	private String degree;
	private String major;
	@Column(name = "completion_month",columnDefinition="enum('January','February','March','April','May','June','July','August','September','October','November','December')")
	private String completion_month;
	private String completion_year;
	private String country;
	private String city;
	private BigDecimal grade;
	private String description;
	private int entered_by;
//	@JsonDeserialize(using = DateDeserializer.class)
//	@JsonSerialize(using = DateSerializer.class)
	private Date entered_on;
	private int last_modified_by;
//	@JsonDeserialize(using = DateDeserializer.class)
//	@JsonSerialize(using = DateSerializer.class)
	private Date last_modified_on;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEmp_id() {
		return emp_id;
	}

	public void setEmp_id(int emp_id) {
		this.emp_id = emp_id;
	}

	public String getInstitution() {
		return institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}

	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public String getCompletion_month() {
		return completion_month;
	}

	public void setCompletion_month(String completion_month) {
		this.completion_month = completion_month;
	}

	public String getCompletion_year() {
		return completion_year;
	}

	public void setCompletion_year(String completion_year) {
		this.completion_year = completion_year;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public BigDecimal getGrade() {
		return grade;
	}

	public void setGrade(BigDecimal grade) {
		this.grade = grade;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getEntered_by() {
		return entered_by;
	}

	public void setEntered_by(int entered_by) {
		this.entered_by = entered_by;
	}

	public Date getEntered_on() {
		return entered_on;
	}

	public void setEntered_on(Date entered_on) {
		this.entered_on = entered_on;
	}

	public int getLast_modified_by() {
		return last_modified_by;
	}

	public void setLast_modified_by(int last_modified_by) {
		this.last_modified_by = last_modified_by;
	}

	public Date getLast_modified_on() {
		return last_modified_on;
	}

	public void setLast_modified_on(Date last_modified_on) {
		this.last_modified_on = last_modified_on;
	}
}
