package com.scsoft.myport.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.joda.time.DateTime;

@Table(name="ccf")
@Entity
public class Ccf {
	@Column(name="ccf_id")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@NotNull
	private String title_of_change;
	@NotNull
	private String description_of_change;
	@NotNull
	private Integer change_requester_id;
	@NotNull
	private String team;
	@NotNull
	private String service_owner;
	@NotNull
	private String group_team;
	@NotNull
	private String service_affected;
	@NotNull
	private String cis_affected;
	@NotNull
	private String sd_tool_ticket_number;
	@NotNull
	private String comments_details;
	@NotNull
	private String names_of_affected_systems;
	@NotNull
	private String number_of_affected_systems;
	@NotNull
	private String impact_on_service;
	@NotNull
	private String impact_description;
	@NotNull
	private String consequence;
	@NotNull
	private String probability_of_failuire;
	@NotNull
	private String impact_of_the_failure;
	@NotNull
	private String overall_subjective_risk;
	@NotNull
	private String implemetation_step;
	@NotNull
	private String backout_plan_step;
	@NotNull
	private String verfication_plan_steps;
	@NotNull
	private String change_type;
	@Column(columnDefinition = "DATETIME")
	@NotNull
	private DateTime scheduled_start_date;
	@Column(columnDefinition = "DATETIME")
	@NotNull
	private DateTime scheduled_end_date;
	
	@Column(columnDefinition = "DATETIME")
	@NotNull
	private DateTime change_expires_on;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle_of_change() {
		return title_of_change;
	}

	public void setTitle_of_change(String title_of_change) {
		this.title_of_change = title_of_change;
	}

	public String getDescription_of_change() {
		return description_of_change;
	}

	public void setDescription_of_change(String description_of_change) {
		this.description_of_change = description_of_change;
	}

	public Integer getChange_requester_id() {
		return change_requester_id;
	}

	public void setChange_requester_id(Integer change_requester_id) {
		this.change_requester_id = change_requester_id;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public String getService_owner() {
		return service_owner;
	}

	public void setService_owner(String service_owner) {
		this.service_owner = service_owner;
	}

	public String getGroup_team() {
		return group_team;
	}

	public void setGroup_team(String group_team) {
		this.group_team = group_team;
	}

	public String getService_affected() {
		return service_affected;
	}

	public void setService_affected(String service_affected) {
		this.service_affected = service_affected;
	}

	public String getCis_affected() {
		return cis_affected;
	}

	public void setCis_affected(String cis_affected) {
		this.cis_affected = cis_affected;
	}

	public String getSd_tool_ticket_number() {
		return sd_tool_ticket_number;
	}

	public void setSd_tool_ticket_number(String sd_tool_ticket_number) {
		this.sd_tool_ticket_number = sd_tool_ticket_number;
	}

	public String getComments_details() {
		return comments_details;
	}

	public void setComments_details(String comments_details) {
		this.comments_details = comments_details;
	}

	public String getNames_of_affected_systems() {
		return names_of_affected_systems;
	}

	public void setNames_of_affected_systems(String names_of_affected_systems) {
		this.names_of_affected_systems = names_of_affected_systems;
	}

	public String getNumber_of_affected_systems() {
		return number_of_affected_systems;
	}

	public void setNumber_of_affected_systems(String number_of_affected_systems) {
		this.number_of_affected_systems = number_of_affected_systems;
	}

	public String getImpact_on_service() {
		return impact_on_service;
	}

	public void setImpact_on_service(String impact_on_service) {
		this.impact_on_service = impact_on_service;
	}

	public String getImpact_description() {
		return impact_description;
	}

	public void setImpact_description(String impact_description) {
		this.impact_description = impact_description;
	}

	public String getConsequence() {
		return consequence;
	}

	public void setConsequence(String consequence) {
		this.consequence = consequence;
	}

	public String getProbability_of_failuire() {
		return probability_of_failuire;
	}

	public void setProbability_of_failuire(String probability_of_failuire) {
		this.probability_of_failuire = probability_of_failuire;
	}

	public String getImpact_of_the_failure() {
		return impact_of_the_failure;
	}

	public void setImpact_of_the_failure(String impact_of_the_failure) {
		this.impact_of_the_failure = impact_of_the_failure;
	}

	public String getOverall_subjective_risk() {
		return overall_subjective_risk;
	}

	public void setOverall_subjective_risk(String overall_subjective_risk) {
		this.overall_subjective_risk = overall_subjective_risk;
	}

	public String getImplemetation_step() {
		return implemetation_step;
	}

	public void setImplemetation_step(String implemetation_step) {
		this.implemetation_step = implemetation_step;
	}

	public String getBackout_plan_step() {
		return backout_plan_step;
	}

	public void setBackout_plan_step(String backout_plan_step) {
		this.backout_plan_step = backout_plan_step;
	}

	public String getVerfication_plan_steps() {
		return verfication_plan_steps;
	}

	public void setVerfication_plan_steps(String verfication_plan_steps) {
		this.verfication_plan_steps = verfication_plan_steps;
	}

	public String getChange_type() {
		return change_type;
	}

	public void setChange_type(String change_type) {
		this.change_type = change_type;
	}

	public DateTime getScheduled_start_date() {
		return scheduled_start_date;
	}

	public void setScheduled_start_date(DateTime scheduled_start_date) {
		this.scheduled_start_date = scheduled_start_date;
	}

	public DateTime getScheduled_end_date() {
		return scheduled_end_date;
	}

	public void setScheduled_end_date(DateTime scheduled_end_date) {
		this.scheduled_end_date = scheduled_end_date;
	}

	public DateTime getChange_expires_on() {
		return change_expires_on;
	}

	public void setChange_expires_on(DateTime change_expires_on) {
		this.change_expires_on = change_expires_on;
	}
	
	
	
	
}
