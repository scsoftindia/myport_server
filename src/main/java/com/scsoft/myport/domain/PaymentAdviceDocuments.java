package com.scsoft.myport.domain;


import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "payment_advice_documents")
public class PaymentAdviceDocuments {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@JsonIgnore
	@ManyToOne(targetEntity = PaymentAdvice.class,fetch=FetchType.LAZY)
	@JoinColumn(name = "pay_advice_id", referencedColumnName = "pay_advice_id")
	private PaymentAdvice paymentAdvice;
	private String documentpath;
	private Boolean availableStatus;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public PaymentAdvice getPaymentAdvice() {
		return paymentAdvice;
	}

	public void setPaymentAdvice(PaymentAdvice paymentAdvice) {
		this.paymentAdvice = paymentAdvice;
	}

	public String getDocumentpath() {
		return documentpath;
	}

	public void setDocumentpath(String documentpath) {
		this.documentpath = documentpath;
	}

	public Boolean getAvailableStatus() {
		if(this.availableStatus == null) {
			this.availableStatus = false;
		}
		return availableStatus;
	}

	public void setAvailableStatus(Boolean availableStatus) {
		this.availableStatus = availableStatus;
	}

}
