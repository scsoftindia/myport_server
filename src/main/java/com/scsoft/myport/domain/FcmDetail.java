package com.scsoft.myport.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table(name = "fcm_detail")
@Entity
public class FcmDetail {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String fcm_id; 
	@ManyToOne(targetEntity = EmpEmployment.class)
	@JoinColumn(name = "emp_id", referencedColumnName = "emp_id")
	private EmpEmployment empEmployment;
	@Column(name="is_available")
	private Boolean isAvailable;

	public String getFcm_id() {
		return fcm_id;
	}

	public void setFcm_id(String fcm_id) {
		this.fcm_id = fcm_id;
	}

	public EmpEmployment getEmpEmployment() {
		return empEmployment;
	}

	public void setEmpEmployment(EmpEmployment empEmployment) {
		this.empEmployment = empEmployment;
	}

	public Boolean getIsAvailable() {
		return isAvailable;
	}

	public void setIsAvailable(Boolean is_available) {
		this.isAvailable = is_available;
	}
	

}
