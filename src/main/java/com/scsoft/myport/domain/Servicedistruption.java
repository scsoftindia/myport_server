package com.scsoft.myport.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="service_distruption")
public class Servicedistruption {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	private String status;
	private String description;
	private String reason;
	private String impact;
	private String services_affected;
	private java.util.Date start_time_date;
	private String expected_downtime;
	private String update_description;
	private String resolution;
	private java.util.Date added_on;
	private Integer added_by;
	private java.util.Date updated_on;
	private Integer updated_by;
	private java.util.Date closed_on;
	private Integer closed_by;
	private String elapsed_downtime;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getImpact() {
		return impact;
	}

	public void setImpact(String impact) {
		this.impact = impact;
	}

	public String getServices_affected() {
		return services_affected;
	}

	public void setServices_affected(String services_affected) {
		this.services_affected = services_affected;
	}

	public java.util.Date getStart_time_date() {
		return start_time_date;
	}

	public void setStart_time_date(java.util.Date start_time_date) {
		this.start_time_date = start_time_date;
	}

	public String getExpected_downtime() {
		return expected_downtime;
	}

	public void setExpected_downtime(String expected_downtime) {
		this.expected_downtime = expected_downtime;
	}

	public String getUpdate_description() {
		return update_description;
	}

	public void setUpdate_description(String update_description) {
		this.update_description = update_description;
	}

	public String getResolution() {
		return resolution;
	}

	public void setResolution(String resolution) {
		this.resolution = resolution;
	}

	public java.util.Date getAdded_on() {
		return added_on;
	}

	public void setAdded_on(java.util.Date added_on) {
		this.added_on = added_on;
	}

	public Integer getAdded_by() {
		return added_by;
	}

	public void setAdded_by(Integer added_by) {
		this.added_by = added_by;
	}

	public java.util.Date getUpdated_on() {
		return updated_on;
	}

	public void setUpdated_on(java.util.Date updated_on) {
		this.updated_on = updated_on;
	}

	public Integer getUpdated_by() {
		return updated_by;
	}

	public void setUpdated_by(Integer updated_by) {
		this.updated_by = updated_by;
	}

	public java.util.Date getClosed_on() {
		return closed_on;
	}

	public void setClosed_on(java.util.Date closed_on) {
		this.closed_on = closed_on;
	}

	public Integer getClosed_by() {
		return closed_by;
	}

	public void setClosed_by(Integer closed_by) {
		this.closed_by = closed_by;
	}

	public String getElapsed_downtime() {
		return elapsed_downtime;
	}

	public void setElapsed_downtime(String elapsed_downtime) {
		this.elapsed_downtime = elapsed_downtime;
	}

}
