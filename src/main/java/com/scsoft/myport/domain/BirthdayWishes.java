package com.scsoft.myport.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "birthday_wishes")
public class BirthdayWishes {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "wish_id")
	private Integer wish_id;
	@Column(name = "bday_year" ,columnDefinition="YEAR")
	private Integer bday_year;
	private Integer for_emp;
	private Integer by_emp;
	private String wish;
	@Column(name = "approved_status" ,columnDefinition="TINYINT")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean approved_status;
	private Integer approved_by;
	public Integer getWish_id() {
		return wish_id;
	}
	public void setWish_id(Integer wish_id) {
		this.wish_id = wish_id;
	}
	public Integer getBday_year() {
		return bday_year;
	}
	public void setBday_year(Integer bday_year) {
		this.bday_year = bday_year;
	}
	public Integer getFor_emp() {
		return for_emp;
	}
	public void setFor_emp(Integer for_emp) {
		this.for_emp = for_emp;
	}
	public Integer getBy_emp() {
		return by_emp;
	}
	public void setBy_emp(Integer by_emp) {
		this.by_emp = by_emp;
	}
	public String getWish() {
		return wish;
	}
	public void setWish(String wish) {
		this.wish = wish;
	}
	public boolean isApproved_status() {
		return approved_status;
	}
	public void setApproved_status(boolean approved_status) {
		this.approved_status = approved_status;
	}
	public Integer getApproved_by() {
		return approved_by;
	}
	public void setApproved_by(Integer approved_by) {
		this.approved_by = approved_by;
	}
	

}
