package com.scsoft.myport.domain.converter;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.joda.time.DateTime;

@Converter(autoApply = true)
public class LocalDateTimeAttributeConverter implements AttributeConverter<DateTime, Timestamp> {

	@Override
	public Timestamp convertToDatabaseColumn(DateTime dateTime) {
		if(dateTime == null) {
			return null;
		}else {
			Long timeInMills = dateTime.getMillis();
			LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(timeInMills), ZoneId.of("Asia/Kolkata"));
			return Timestamp.valueOf(localDateTime);
		}
		
//		return (dateTime == null ? null :  new Timestamp(dateTime.getMillis()));
	}

	@Override
	public DateTime convertToEntityAttribute(Timestamp sqlTimestamp) {
		return (sqlTimestamp == null ? null :  new DateTime(sqlTimestamp));
	}
  
//	@Override
//    public Timestamp convertToDatabaseColumn(LocalDateTime locDateTime) {
//    	return (locDateTime == null ? null : Timestamp.valueOf(locDateTime));
//    }
//
//    @Override
//    public LocalDateTime convertToEntityAttribute(Timestamp sqlTimestamp) {
//    	return (sqlTimestamp == null ? null : sqlTimestamp.toLocalDateTime());
//    }

}