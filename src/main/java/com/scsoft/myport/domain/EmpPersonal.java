package com.scsoft.myport.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.scsoft.myport.service.util.DateDeserializer;
import com.scsoft.myport.service.util.DateSerializer;

import java.util.Date;

@Entity
@Table(name = "emp_personal")
public class EmpPersonal {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "emp_id")
	private Integer id;
	private String emp_fname;
	private String emp_mname;
	private String emp_lname;
	private Date emp_dob;
	private String emp_gender;
	private String emp_bloodgroup;
	private String emp_fathername;
	private Date emp_fatherdob;
	private String emp_panno;
	private String emp_mothername;
	private Date emp_motherdob;
	private String emp_insuranceid;
	private String emp_personal_phone;
	private String emp_personal_email;
	private Date emp_dateofinsurance;
	private String emp_marital_status;
	private String emp_spouse_name;
	private Date emp_spouse_dob;
	private Date emp_anniversary;
	private Integer emp_no_of_children;
	private String emp_child_name1;
	private Date emp_child_dob1;
	private String emp_child_name2;
	private Date emp_child_dob2;
	private String emp_child_name3;
	private Date emp_child_dob3;
	private String emp_temp_addr;
	private String emp_perm_addr;
	private String emp_avatar;
	private Integer entered_by;
	private Date entered_on;
	@JsonIgnore
	private Integer last_modified_by;
	@JsonIgnore
	private Date last_modified_on;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getEmp_fname() {
		return emp_fname;
	}
	public void setEmp_fname(String emp_fname) {
		this.emp_fname = emp_fname;
	}
	public String getEmp_mname() {
		return emp_mname;
	}
	public void setEmp_mname(String emp_mname) {
		this.emp_mname = emp_mname;
	}
	public String getEmp_lname() {
		return emp_lname;
	}
	public void setEmp_lname(String emp_lname) {
		this.emp_lname = emp_lname;
	}
	public Date getEmp_dob() {
		return emp_dob;
	}
	public void setEmp_dob(Date emp_dob) {
		this.emp_dob = emp_dob;
	}
	public String getEmp_gender() {
		return emp_gender;
	}
	public void setEmp_gender(String emp_gender) {
		this.emp_gender = emp_gender;
	}
	public String getEmp_bloodgroup() {
		return emp_bloodgroup;
	}
	public void setEmp_bloodgroup(String emp_bloodgroup) {
		this.emp_bloodgroup = emp_bloodgroup;
	}
	public String getEmp_fathername() {
		return emp_fathername;
	}
	public void setEmp_fathername(String emp_fathername) {
		this.emp_fathername = emp_fathername;
	}
	public Date getEmp_fatherdob() {
		return emp_fatherdob;
	}
	public void setEmp_fatherdob(Date emp_fatherdob) {
		this.emp_fatherdob = emp_fatherdob;
	}
	public String getEmp_panno() {
		return emp_panno;
	}
	public void setEmp_panno(String emp_panno) {
		this.emp_panno = emp_panno;
	}
	public String getEmp_mothername() {
		return emp_mothername;
	}
	public void setEmp_mothername(String emp_mothername) {
		this.emp_mothername = emp_mothername;
	}
	public Date getEmp_motherdob() {
		return emp_motherdob;
	}
	public void setEmp_motherdob(Date emp_motherdob) {
		this.emp_motherdob = emp_motherdob;
	}
	public String getEmp_insuranceid() {
		return emp_insuranceid;
	}
	public void setEmp_insuranceid(String emp_insuranceid) {
		this.emp_insuranceid = emp_insuranceid;
	}
	public String getEmp_personal_phone() {
		return emp_personal_phone;
	}
	public void setEmp_personal_phone(String emp_personal_phone) {
		this.emp_personal_phone = emp_personal_phone;
	}
	public String getEmp_personal_email() {
		return emp_personal_email;
	}
	public void setEmp_personal_email(String emp_personal_email) {
		this.emp_personal_email = emp_personal_email;
	}
	public Date getEmp_dateofinsurance() {
		return emp_dateofinsurance;
	}
	public void setEmp_dateofinsurance(Date emp_dateofinsurance) {
		this.emp_dateofinsurance = emp_dateofinsurance;
	}
	public String getEmp_marital_status() {
		return emp_marital_status;
	}
	public void setEmp_marital_status(String emp_marital_status) {
		this.emp_marital_status = emp_marital_status;
	}
	public String getEmp_spouse_name() {
		return emp_spouse_name;
	}
	public void setEmp_spouse_name(String emp_spouse_name) {
		this.emp_spouse_name = emp_spouse_name;
	}
	public Date getEmp_spouse_dob() {
		return emp_spouse_dob;
	}
	public void setEmp_spouse_dob(Date emp_spouse_dob) {
		this.emp_spouse_dob = emp_spouse_dob;
	}
	public Date getEmp_anniversary() {
		return emp_anniversary;
	}
	public void setEmp_anniversary(Date emp_anniversary) {
		this.emp_anniversary = emp_anniversary;
	}
	public Integer getEmp_no_of_children() {
		return emp_no_of_children;
	}
	public void setEmp_no_of_children(Integer emp_no_of_children) {
		this.emp_no_of_children = emp_no_of_children;
	}
	public String getEmp_child_name1() {
		return emp_child_name1;
	}
	public void setEmp_child_name1(String emp_child_name1) {
		this.emp_child_name1 = emp_child_name1;
	}
	public Date getEmp_child_dob1() {
		return emp_child_dob1;
	}
	public void setEmp_child_dob1(Date emp_child_dob1) {
		this.emp_child_dob1 = emp_child_dob1;
	}
	public String getEmp_child_name2() {
		return emp_child_name2;
	}
	public void setEmp_child_name2(String emp_child_name2) {
		this.emp_child_name2 = emp_child_name2;
	}
	public Date getEmp_child_dob2() {
		return emp_child_dob2;
	}
	public void setEmp_child_dob2(Date emp_child_dob2) {
		this.emp_child_dob2 = emp_child_dob2;
	}
	public String getEmp_child_name3() {
		return emp_child_name3;
	}
	public void setEmp_child_name3(String emp_child_name3) {
		this.emp_child_name3 = emp_child_name3;
	}
	public Date getEmp_child_dob3() {
		return emp_child_dob3;
	}
	public void setEmp_child_dob3(Date emp_child_dob3) {
		this.emp_child_dob3 = emp_child_dob3;
	}
	public String getEmp_temp_addr() {
		return emp_temp_addr;
	}
	public void setEmp_temp_addr(String emp_temp_addr) {
		this.emp_temp_addr = emp_temp_addr;
	}
	public String getEmp_perm_addr() {
		return emp_perm_addr;
	}
	public void setEmp_perm_addr(String emp_perm_addr) {
		this.emp_perm_addr = emp_perm_addr;
	}
	public String getEmp_avatar() {
		return emp_avatar;
	}
	public void setEmp_avatar(String emp_avatar) {
		this.emp_avatar = emp_avatar;
	}
	public Integer getEntered_by() {
		return entered_by;
	}
	public void setEntered_by(Integer entered_by) {
		this.entered_by = entered_by;
	}
	public Date getEntered_on() {
		return entered_on;
	}
	public void setEntered_on(Date entered_on) {
		this.entered_on = entered_on;
	}
	public Integer getLast_modified_by() {
		return last_modified_by;
	}
	public void setLast_modified_by(Integer last_modified_by) {
		this.last_modified_by = last_modified_by;
	}
	public Date getLast_modified_on() {
		return last_modified_on;
	}
	public void setLast_modified_on(Date last_modified_on) {
		this.last_modified_on = last_modified_on;
	}
	

}
