package com.scsoft.myport.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "greeting_type")
public class GreetingType {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "greeting_id")
	private int id;
	private String greeting_name;
	private String greeting_desc;
}
