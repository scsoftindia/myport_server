package com.scsoft.myport.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.scsoft.myport.service.util.DateDeserializer;
import com.scsoft.myport.service.util.DateSerializer;

import java.util.Date;

@Entity
@Table(name = "salary_slip_requests")
public class SalarySlipRequests {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "request_id")
	private Integer id;
	private Integer requested_by;
	private String request_year;
	private String request_months;
	private String request_reason;
//	@JsonDeserialize(using = DateDeserializer.class)
//	@JsonSerialize(using = DateSerializer.class)
	private Date requested_date;
	@Column(name = "request_status", columnDefinition = "enum('Requested to HR','Forwarded to finance dept.','HR rejected','Finance dept. rejected','Admin rejected','Salary slip issued')")
	private String request_status;
	private Integer addressed_by;
//	@JsonDeserialize(using = DateDeserializer.class)
//	@JsonSerialize(using = DateSerializer.class)
	private Date addressed_on;
	private String addressee_note;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRequested_by() {
		return requested_by;
	}

	public void setRequested_by(Integer requested_by) {
		this.requested_by = requested_by;
	}

	public String getRequest_year() {
		return request_year;
	}

	public void setRequest_year(String request_year) {
		this.request_year = request_year;
	}

	public String getRequest_months() {
		return request_months;
	}

	public void setRequest_months(String request_months) {
		this.request_months = request_months;
	}

	public String getRequest_reason() {
		return request_reason;
	}

	public void setRequest_reason(String request_reason) {
		this.request_reason = request_reason;
	}

	public Date getRequested_date() {
		return requested_date;
	}

	public void setRequested_date(Date requested_date) {
		this.requested_date = requested_date;
	}

	public String getRequest_status() {
		return request_status;
	}

	public void setRequest_status(String request_status) {
		this.request_status = request_status;
	}

	public Integer getAddressed_by() {
		return addressed_by;
	}

	public void setAddressed_by(Integer addressed_by) {
		this.addressed_by = addressed_by;
	}

	public Date getAddressed_on() {
		return addressed_on;
	}

	public void setAddressed_on(Date addressed_on) {
		this.addressed_on = addressed_on;
	}

	public String getAddressee_note() {
		return addressee_note;
	}

	public void setAddressee_note(String addressee_note) {
		this.addressee_note = addressee_note;
	}

}
