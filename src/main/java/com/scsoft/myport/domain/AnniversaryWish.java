package com.scsoft.myport.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "anniversary_wish")
public class AnniversaryWish {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	private Integer for_emp;
	private Integer by_emp;
	private String wish;
	private Integer anniversary_year;
	private Integer approved_by;
	@Column(name = "approved_status" ,columnDefinition="TINYINT")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean approved_status;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getFor_emp() {
		return for_emp;
	}
	public void setFor_emp(Integer for_emp) {
		this.for_emp = for_emp;
	}
	public Integer getBy_emp() {
		return by_emp;
	}
	public void setBy_emp(Integer by_emp) {
		this.by_emp = by_emp;
	}
	public String getWish() {
		return wish;
	}
	public void setWish(String wish) {
		this.wish = wish;
	}
	public Integer getAnniversary_year() {
		return anniversary_year;
	}
	public void setAnniversary_year(Integer anniversary_year) {
		this.anniversary_year = anniversary_year;
	}
	public Integer getApproved_by() {
		return approved_by;
	}
	public void setApproved_by(Integer approved_by) {
		this.approved_by = approved_by;
	}
	public boolean isApproved_status() {
		return approved_status;
	}
	public void setApproved_status(boolean approved_status) {
		this.approved_status = approved_status;
	}
	
	

}
