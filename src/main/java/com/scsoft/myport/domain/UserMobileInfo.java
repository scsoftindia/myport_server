package com.scsoft.myport.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
@Table(name="user_mobile_info")
public class UserMobileInfo {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	private String model;
	private String device;
	private String manufacturer;
	private String brand;
	private String serial_number;
	@Column(name="mac_adress")
	private String macAdress;
	private String sim_one_imei;
	private String sim_two_imei;
	@Temporal(TemporalType.TIMESTAMP)
	private Date last_login_time;
	@Temporal(TemporalType.TIMESTAMP)
	private Date last_logout_time;
	@ManyToOne(targetEntity=EmpEmployment.class)
	@JoinColumn(name="emp_id",referencedColumnName="emp_id")
	EmpEmployment empEmployment;
	private String carrier_name;
	private String carrier_operator;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getSerial_number() {
		return serial_number;
	}
	public void setSerial_number(String serial) {
		this.serial_number = serial;
	}
	public String getMacAdress() {
		return macAdress;
	}
	public void setMacAdress(String mac_adress) {
		this.macAdress = mac_adress;
	}
	public String getSim_one_imei() {
		return sim_one_imei;
	}
	public void setSim_one_imei(String sim_one_imei) {
		this.sim_one_imei = sim_one_imei;
	}
	public String getSim_two_imei() {
		return sim_two_imei;
	}
	public void setSim_two_imei(String sim_two_imei) {
		this.sim_two_imei = sim_two_imei;
	}
	public Date getLast_login_time() {
		return last_login_time;
	}
	public void setLast_login_time(Date last_login_time) {
		this.last_login_time = last_login_time;
	}
	public Date getLast_logout_time() {
		return last_logout_time;
	}
	public void setLast_logout_time(Date last_logout_time) {
		this.last_logout_time = last_logout_time;
	}
	public EmpEmployment getEmpEmployment() {
		return empEmployment;
	}
	public void setEmpEmployment(EmpEmployment empEmployment) {
		this.empEmployment = empEmployment;
	}
	public String getCarrier_name() {
		return carrier_name;
	}
	public void setCarrier_name(String carrier_name) {
		this.carrier_name = carrier_name;
	}
	public String getCarrier_operator() {
		return carrier_operator;
	}
	public void setCarrier_operator(String carrier_operator) {
		this.carrier_operator = carrier_operator;
	}
	
	
	
	
	
	
}
