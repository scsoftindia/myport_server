package com.scsoft.myport.domain;

import javax.inject.Inject;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.PostLoad;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.scsoft.myport.repository.EmpPersonalRepository;
import com.scsoft.myport.service.util.DateDeserializer;
import com.scsoft.myport.service.util.DateSerializer;

import java.util.Date;

@Entity
@Table(name = "emp_employment")
public class EmpEmployment {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "emp_id")
	private Integer id;
	@Transient
	private EmpPersonal empPersonal;
	private String probation_extension_reason;
	private String ad_username;
	private Date emp_doj;
	private String emp_email;
	private String emp_password;
	private String emp_phone;
	private String emp_phone_ext;
	private String emp_fax;
	@ManyToOne(targetEntity = Vertical.class)
	@JoinColumn(name = "emp_vertical", referencedColumnName = "smu_code")
	private Vertical vertical;
	@ManyToOne(targetEntity = Designation.class)
	@JoinColumn(name = "emp_designation", referencedColumnName = "des_id")
	private Designation designation;
	@ManyToOne(targetEntity = ShiftTypes.class)
	@JoinColumn(name = "emp_shift_id", referencedColumnName = "shift_id")
	private ShiftTypes shiftTypes;
	@ManyToOne(targetEntity = Locations.class)
	@JoinColumn(name = "location", referencedColumnName = "id")
	private Locations locations;
	private String emp_ctc;
	private String emp_total_exp_yr;
	private String emp_total_exp_mn;
	private String emp_rel_exp_yr;
	private String emp_rel_exp_mn;
	@Column(name = "nda_signed", columnDefinition = "enum('Yes','No')")
	private String nda_signed;
	@Column(name = "dos_signed", columnDefinition = "enum('Yes','No')")
	private String dos_signed;
	@Column(name = "ofr_ltr_signed", columnDefinition = "enum('Yes','No')")
	private String ofr_ltr_signed;
	private String emp_addr_proof;
	@Column(name = "emp_job_status", columnDefinition = "enum('trainee','probation','active','notice','resigned','terminated','absconding')")
	private String emp_job_status;
	private Date last_working_day;
	private Integer entered_by;
	private Date entered_on;
	private Integer last_modified_by;
	private Date last_modified_on;
	@Column(name = "user_role", columnDefinition = "enum('ADMIN','HR','FINANCE','MANAGER','LEAD','EMPLOYEE','PROCESS_LEAD')")
	private String user_role;
	private Integer emp_supervisor;
	@Transient
	private EmpPersonal superwiser;
	@Column(name = "status_flag", columnDefinition = "TINYInteger")
	@Type(type = "org.hibernate.type.NumericBooleanType")
	private boolean status_flag;
	private Integer leave_status;
	private Date last_login_date;
	private Date emp_activedate;
	private Integer timesheet;
	@Column(name = "is_wfh", columnDefinition = "enum('1','0')")
	private String is_wfh;
	private Date probation_extended_upto;
	@Transient
	private String emp_personal_phone;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getProbation_extension_reason() {
		return probation_extension_reason;
	}

	public void setProbation_extension_reason(String probation_extension_reason) {
		this.probation_extension_reason = probation_extension_reason;
	}

	public String getAd_username() {
		return ad_username;
	}

	public void setAd_username(String ad_username) {
		this.ad_username = ad_username;
	}

	public Date getEmp_doj() {
		return emp_doj;
	}

	public void setEmp_doj(Date emp_doj) {
		this.emp_doj = emp_doj;
	}

	public String getEmp_email() {
		return emp_email;
	}

	public void setEmp_email(String emp_email) {
		this.emp_email = emp_email;
	}

	public String getEmp_password() {
		return emp_password;
	}

	public void setEmp_password(String emp_password) {
		this.emp_password = emp_password;
	}

	public String getEmp_phone() {
		return emp_phone;
	}

	public void setEmp_phone(String emp_phone) {
		this.emp_phone = emp_phone;
	}

	public String getEmp_phone_ext() {
		return emp_phone_ext;
	}

	public void setEmp_phone_ext(String emp_phone_ext) {
		this.emp_phone_ext = emp_phone_ext;
	}

	public String getEmp_fax() {
		return emp_fax;
	}

	public void setEmp_fax(String emp_fax) {
		this.emp_fax = emp_fax;
	}

	public Vertical getVertical() {
		return vertical;
	}

	public void setVertical(Vertical vertical) {
		this.vertical = vertical;
	}

	public Designation getDesignation() {
		return designation;
	}

	public void setDesignation(Designation designation) {
		this.designation = designation;
	}

	public ShiftTypes getShiftTypes() {
		return shiftTypes;
	}

	public void setShiftTypes(ShiftTypes shiftTypes) {
		this.shiftTypes = shiftTypes;
	}

	public String getEmp_ctc() {
		return emp_ctc;
	}

	public void setEmp_ctc(String emp_ctc) {
		this.emp_ctc = emp_ctc;
	}

	public String getEmp_total_exp_yr() {
		return emp_total_exp_yr;
	}

	public void setEmp_total_exp_yr(String emp_total_exp_yr) {
		this.emp_total_exp_yr = emp_total_exp_yr;
	}

	public String getEmp_total_exp_mn() {
		return emp_total_exp_mn;
	}

	public void setEmp_total_exp_mn(String emp_total_exp_mn) {
		this.emp_total_exp_mn = emp_total_exp_mn;
	}

	public String getEmp_rel_exp_yr() {
		return emp_rel_exp_yr;
	}

	public void setEmp_rel_exp_yr(String emp_rel_exp_yr) {
		this.emp_rel_exp_yr = emp_rel_exp_yr;
	}

	public String getEmp_rel_exp_mn() {
		return emp_rel_exp_mn;
	}

	public void setEmp_rel_exp_mn(String emp_rel_exp_mn) {
		this.emp_rel_exp_mn = emp_rel_exp_mn;
	}

	public String getNda_signed() {
		return nda_signed;
	}

	public void setNda_signed(String nda_signed) {
		this.nda_signed = nda_signed;
	}

	public String getDos_signed() {
		return dos_signed;
	}

	public void setDos_signed(String dos_signed) {
		this.dos_signed = dos_signed;
	}

	public String getOfr_ltr_signed() {
		return ofr_ltr_signed;
	}

	public void setOfr_ltr_signed(String ofr_ltr_signed) {
		this.ofr_ltr_signed = ofr_ltr_signed;
	}

	public String getEmp_addr_proof() {
		return emp_addr_proof;
	}

	public void setEmp_addr_proof(String emp_addr_proof) {
		this.emp_addr_proof = emp_addr_proof;
	}

	public String getEmp_job_status() {
		return emp_job_status;
	}

	public void setEmp_job_status(String emp_job_status) {
		this.emp_job_status = emp_job_status;
	}

	public Date getLast_working_day() {
		return last_working_day;
	}

	public void setLast_working_day(Date last_working_day) {
		this.last_working_day = last_working_day;
	}

	public Integer getEntered_by() {
		return entered_by;
	}

	public void setEntered_by(Integer entered_by) {
		this.entered_by = entered_by;
	}

	public Date getEntered_on() {
		return entered_on;
	}

	public void setEntered_on(Date entered_on) {
		this.entered_on = entered_on;
	}

	public Integer getLast_modified_by() {
		return last_modified_by;
	}

	public void setLast_modified_by(Integer last_modified_by) {
		this.last_modified_by = last_modified_by;
	}

	public Date getLast_modified_on() {
		return last_modified_on;
	}

	public void setLast_modified_on(Date last_modified_on) {
		this.last_modified_on = last_modified_on;
	}

	public String getUser_role() {
		return user_role;
	}

	public void setUser_role(String user_role) {
		this.user_role = user_role;
	}

	public Integer getEmp_supervisor() {
		return emp_supervisor;
	}

	public void setEmp_supervisor(Integer emp_supervisor) {
		this.emp_supervisor = emp_supervisor;
	}

	public boolean isStatus_flag() {
		return status_flag;
	}

	public void setStatus_flag(boolean status_flag) {
		this.status_flag = status_flag;
	}

	public Integer getLeave_status() {
		return leave_status;
	}

	public void setLeave_status(Integer leave_status) {
		this.leave_status = leave_status;
	}

	public Date getLast_login_date() {
		return last_login_date;
	}

	public void setLast_login_date(Date last_login_date) {
		this.last_login_date = last_login_date;
	}

	public Date getEmp_activedate() {
		return emp_activedate;
	}

	public void setEmp_activedate(Date emp_activedate) {
		this.emp_activedate = emp_activedate;
	}

	public Integer getTimesheet() {
		return timesheet;
	}

	public void setTimesheet(Integer timesheet) {
		this.timesheet = timesheet;
	}

	public String getIs_wfh() {
		return is_wfh;
	}

	public void setIs_wfh(String is_wfh) {
		this.is_wfh = is_wfh;
	}

	public Date getProbation_extended_upto() {
		return probation_extended_upto;
	}

	public void setProbation_extended_upto(Date probation_extended_upto) {
		this.probation_extended_upto = probation_extended_upto;
	}

	public EmpPersonal getEmpPersonal() {
		return empPersonal;
	}

	public void setEmpPersonal(EmpPersonal empPersonal) {
		this.empPersonal = empPersonal;
	}

	public EmpPersonal getSuperwiser() {
		return superwiser;
	}

	public void setSuperwiser(EmpPersonal superwiser) {
		this.superwiser = superwiser;
	}

	public String getEmp_personal_phone() {
		return emp_personal_phone;
	}

	public void setEmp_personal_phone(String emp_personal_phone) {
		this.emp_personal_phone = emp_personal_phone;
	}
	
	public Locations getLocations() {
		return locations;
	}

	public void setLocations(Locations locations) {
		this.locations = locations;
	}

	public EmpEmployment(Integer id, String probation_extension_reason, String ad_username, Date emp_doj,
			String emp_email, String emp_phone, String emp_phone_ext, String emp_fax, Vertical vertical,
			Designation designation, ShiftTypes shiftTypes, String emp_ctc, String emp_total_exp_yr,
			String emp_total_exp_mn, String emp_rel_exp_yr, String emp_rel_exp_mn, String emp_job_status,
			String user_role, Integer emp_supervisor, boolean status_flag, Integer leave_status, String is_wfh,
			Date probation_extended_upto, EmpPersonal superwiser, EmpPersonal empPersonal,Locations locations) {
		super();
		this.id = id;
		this.probation_extension_reason = probation_extension_reason;
		this.ad_username = ad_username;
		this.emp_doj = emp_doj;
		this.emp_email = emp_email;
		this.emp_phone = emp_phone;
		this.emp_phone_ext = emp_phone_ext;
		this.emp_fax = emp_fax;
		this.vertical = vertical;
		this.designation = designation;
		this.shiftTypes = shiftTypes;
		this.emp_ctc = emp_ctc;
		this.emp_total_exp_yr = emp_total_exp_yr;
		this.emp_total_exp_mn = emp_total_exp_mn;
		this.emp_rel_exp_yr = emp_rel_exp_yr;
		this.emp_rel_exp_mn = emp_rel_exp_mn;
		this.emp_job_status = emp_job_status;
		this.user_role = user_role;
		this.emp_supervisor = emp_supervisor;
		this.status_flag = status_flag;
		this.leave_status = leave_status;
		this.is_wfh = is_wfh;
		this.probation_extended_upto = probation_extended_upto;
		this.superwiser = superwiser;
		this.empPersonal = empPersonal;
		if (this.empPersonal != null) {
			this.emp_personal_phone = this.empPersonal.getEmp_personal_phone();
		} else {
			this.emp_personal_phone = "Not Available";
		}
		this.locations = locations;

	}

	public EmpEmployment() {
	}

}
