package com.scsoft.myport.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.scsoft.myport.service.util.DateDeserializer;
import com.scsoft.myport.service.util.DateSerializer;

import java.util.Date;

@Entity
@Table(name = "greetings")
public class Greetings {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "greeting_id")
	private int id;
	private int for_emp;
	private int greeting_type;
	@Column(name = "greeting_year" ,columnDefinition="YEAR")
	private int greeting_year;
	private String greetings;
	private String greeting_heading;
//	@JsonDeserialize(using = DateDeserializer.class)
//	@JsonSerialize(using = DateSerializer.class)
	private Date greeting_date;
	private int created_by;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getFor_emp() {
		return for_emp;
	}
	public void setFor_emp(int for_emp) {
		this.for_emp = for_emp;
	}
	public int getGreeting_type() {
		return greeting_type;
	}
	public void setGreeting_type(int greeting_type) {
		this.greeting_type = greeting_type;
	}
	public int getGreeting_year() {
		return greeting_year;
	}
	public void setGreeting_year(int greeting_year) {
		this.greeting_year = greeting_year;
	}
	public String getGreetings() {
		return greetings;
	}
	public void setGreetings(String greetings) {
		this.greetings = greetings;
	}
	public String getGreeting_heading() {
		return greeting_heading;
	}
	public void setGreeting_heading(String greeting_heading) {
		this.greeting_heading = greeting_heading;
	}
	public Date getGreeting_date() {
		return greeting_date;
	}
	public void setGreeting_date(Date greeting_date) {
		this.greeting_date = greeting_date;
	}
	public int getCreated_by() {
		return created_by;
	}
	public void setCreated_by(int created_by) {
		this.created_by = created_by;
	}
	
}
