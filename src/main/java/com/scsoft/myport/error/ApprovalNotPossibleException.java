package com.scsoft.myport.error;

public class ApprovalNotPossibleException extends Exception {
	
	String type;
	String message;
	String status;
	String currentStatus;
	
	
	public ApprovalNotPossibleException() {
	}


	public ApprovalNotPossibleException(String type, String message, String status,String currentStatus) {
		this.type = type;
		this.message = message;
		this.status = status;
		this.currentStatus = currentStatus;
	}


	public String getType() {
		return type;
	}


	public String getMessage() {
		return message;
	}


	public String getStatus() {
		return status;
	}


	public String getCurrentStatus() {
		return currentStatus;
	}


	
	
	
	

}
