package com.scsoft.myport.error;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Error {
	@JsonProperty("message")
	private final String message;
	@JsonProperty("description")
	private final String description;

	public Error(String message, String description) {
		this.message = message;
		this.description = description;
	}

}
