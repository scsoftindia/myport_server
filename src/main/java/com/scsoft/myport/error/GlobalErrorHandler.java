package com.scsoft.myport.error;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.ConcurrencyFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fasterxml.jackson.core.JsonParseException;
import com.scsoft.myport.rest.response.ApiResponse;
import com.scsoft.myport.rest.response.FieldErrorResponse;

import java.util.ArrayList;
import java.util.List;

import org.apache.directory.api.ldap.model.exception.LdapAuthenticationException;

@ControllerAdvice
public class GlobalErrorHandler {
	private final Logger logger = LoggerFactory.getLogger(GlobalErrorHandler.class);

	@ExceptionHandler(LdapAuthenticationException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public Error processConcurrencyError(LdapAuthenticationException ex) {
		return new Error("Authentication failed", "Invalid username or password");
	}

	@ExceptionHandler(JsonParseException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public Error jsonParseExceptionError(JsonParseException ex) {
		return new Error("Invalid Data", "The input data is not in a valid format");
	}

	@ExceptionHandler(ApprovalNotPossibleException.class)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public ApiResponse approvalNotPossibleExceptionError(ApprovalNotPossibleException ex) {
		String message = null;
		if ("paymentAdvice".equals(ex.getType())) {

			message = "The payment advice is already " + ex.getCurrentStatus();

		} else if ("leave".equals(ex.getType())) {

			message = "The leave request is already " + ex.getCurrentStatus();

		} else if ("holiday".equals(ex.getType())) {

			message = "The holiday request is already " + ex.getCurrentStatus();

		} else if ("complaint".equals(ex.getType())) {

			message = "The Complaint/Concern is already " + ex.getCurrentStatus();

		}
		else if ("wfh".equals(ex.getType())) {

			message = "The Work from home request is already " + ex.getCurrentStatus();

		}
		else if ("subCategory".equals(ex.getType())) {

			message = "The Expense Type request is already  " + ex.getCurrentStatus();

		}
		else if ("category".equals(ex.getType())) {

			message = "The Expense request is already  " + ex.getCurrentStatus();

		}
		else if ("vendor".equals(ex.getType())) {

			message = "The Vendor request is already  " + ex.getCurrentStatus();

		}	
		
		
		return new ApiResponse("error", message);
	}

	@ExceptionHandler(NotFoundException.class)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public ApiResponse notFoundExceptionError(NotFoundException ex) {
		String message = null;
		message = ex.getMessage();
		return new ApiResponse("error", message);
	}
	@ExceptionHandler(OperationNotPossibleException.class)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public ApiResponse operationNotPossibleExceptionError(OperationNotPossibleException ex) {
		String message = null;
		message = ex.getMessage();
		return new ApiResponse("error", message);
	}
	@ExceptionHandler(InvalidRequestException.class)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public ApiResponse InvalidRequestExceptionError(InvalidRequestException ex) {
		String message = null;
		message = ex.getMessage();
		return new ApiResponse("error", message);
	}
	
	@ExceptionHandler(ValidationException.class)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<FieldErrorResponse> validationExceptionError(ValidationException ex) {
		List<FieldError> fieldErrors = ex.getFieldErrors();
		List<FieldErrorResponse> response = new ArrayList();
		fieldErrors.stream().forEach(item ->{
			FieldErrorResponse fieldErrorResponse = new FieldErrorResponse();
			fieldErrorResponse.setFiled(item.getField());
			fieldErrorResponse.setMessage(item.getCode());
			response.add(fieldErrorResponse);
		});
		return response;
	}
	
	
	

}
