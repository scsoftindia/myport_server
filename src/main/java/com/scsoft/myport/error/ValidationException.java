package com.scsoft.myport.error;

import java.util.List;

import org.springframework.validation.FieldError;

public class ValidationException extends Exception  {
	List<FieldError> fieldErrors;

	public ValidationException(List<FieldError> fieldErrors) {
		this.fieldErrors = fieldErrors;
	}

	public List<FieldError> getFieldErrors() {
		return fieldErrors;
	}
	
	
}
