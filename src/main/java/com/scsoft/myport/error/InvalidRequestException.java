package com.scsoft.myport.error;

public class InvalidRequestException extends Exception {
	String type;
	String message;

	public InvalidRequestException() {
	}

	public InvalidRequestException(String type, String message) {
		this.type = type;
		this.message = message;
	}

	public String getType() {
		return type;
	}

	public String getMessage() {
		return message;
	}

}
