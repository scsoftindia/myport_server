package com.scsoft.myport.error;

public class OperationNotPossibleException extends Exception {
	String message;
	String status;
	public OperationNotPossibleException(String status,String message) {
		this.message = message;
		this.status = status;
	}
	public OperationNotPossibleException() {
	}
	public String getMessage() {
		return message;
	}
	public String getStatus() {
		return status;
	}
	
	
	
}
