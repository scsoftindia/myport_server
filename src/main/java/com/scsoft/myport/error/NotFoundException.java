package com.scsoft.myport.error;

public class NotFoundException extends Exception {
	String type;
	String message;
	
	
	public NotFoundException() {
		super();
	}
	
	public NotFoundException(String type, String message) {
		this.type = type;
		this.message = message;
	}

	public String getType() {
		return type;
	}
	public String getMessage() {
		return message;
	}
	
	
	
}
