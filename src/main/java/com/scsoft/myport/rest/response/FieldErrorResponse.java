package com.scsoft.myport.rest.response;

public class FieldErrorResponse {
	private String filed;
	private String message;

	public String getFiled() {
		return filed;
	}

	public void setFiled(String filed) {
		this.filed = filed;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
