package com.scsoft.myport.rest.response;

import java.util.List;

import com.scsoft.myport.domain.HolidayAvailability;
import com.scsoft.myport.domain.LeaveAvailability;
import com.scsoft.myport.domain.LeaveTypes;
import com.scsoft.myport.service.HolidayDTO;
import com.scsoft.myport.service.SupervisorDTO;
import com.scsoft.myport.service.DTO.LeaveDTO;
import com.scsoft.myport.service.DTO.UserHoliDayDTO;

public class UserHoliDayResponse {
	private HolidayAvailability holidayAvailability;
	private Integer holidaysRemaining;
	private List<LeaveAvailability> leaveAvailability;
	private List<UserHoliDayDTO> userHoliDayDTOs;
	private String supervisorName;
	private Integer supervisorId;
	private List<SupervisorDTO> supervisors;
	private List<LeaveDTO> leaveHistory;
	private List<LeaveTypes> leaveTypes;
	private List<HolidayDTO> holidayHistory;

	public HolidayAvailability getHolidayAvailability() {
		return holidayAvailability;
	}

	public void setHolidayAvailability(HolidayAvailability holidayAvailability) {
		this.holidayAvailability = holidayAvailability;
	}
	
	
	public Integer getHolidaysRemaining() {
		return holidaysRemaining;
	}

	public void setHolidaysRemaining(Integer holidaysRemaining) {
		this.holidaysRemaining = holidaysRemaining;
	}

	public List<LeaveAvailability> getLeaveAvailability() {
		return leaveAvailability;
	}

	public void setLeaveAvailability(List<LeaveAvailability> leaveAvailability) {
		this.leaveAvailability = leaveAvailability;
	}

	public List<UserHoliDayDTO> getUserHoliDayDTOs() {
		return userHoliDayDTOs;
	}

	public void setUserHoliDayDTOs(List<UserHoliDayDTO> userHoliDayDTOs) {
		this.userHoliDayDTOs = userHoliDayDTOs;
	}

	public String getSupervisorName() {
		return supervisorName;
	}

	public void setSupervisorName(String supervisorName) {
		this.supervisorName = supervisorName;
	}

	public Integer getSupervisorId() {
		return supervisorId;
	}

	public void setSupervisorId(Integer supervisorId) {
		this.supervisorId = supervisorId;
	}

	public List<SupervisorDTO> getSupervisors() {
		return supervisors;
	}

	public void setSupervisors(List<SupervisorDTO> supervisors) {
		this.supervisors = supervisors;
	}

	public List<LeaveDTO> getLeaveHistory() {
		return leaveHistory;
	}

	public void setLeaveHistory(List<LeaveDTO> leaveHistory) {
		this.leaveHistory = leaveHistory;
	}

	public List<LeaveTypes> getLeaveTypes() {
		return leaveTypes;
	}

	public void setLeaveTypes(List<LeaveTypes> leaveTypes) {
		this.leaveTypes = leaveTypes;
	}

	public List<HolidayDTO> getHolidayHistory() {
		return holidayHistory;
	}

	public void setHolidayHistory(List<HolidayDTO> holidayHistory) {
		this.holidayHistory = holidayHistory;
	}

	
	
	
	
	

}
