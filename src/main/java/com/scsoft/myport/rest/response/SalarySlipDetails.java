package com.scsoft.myport.rest.response;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.util.StringUtils;

public class SalarySlipDetails {
	private String request_reason;
	private String request_year;
	private String request_months;
	private String requested_date;
	private String request_status;
	private String addressed_on;
	private String addressee_note;
	private static transient SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");

	public SalarySlipDetails() {

	}

	public SalarySlipDetails(String request_reason, String request_year, String request_months, Date requested_date,
			String request_status, Date addressed_on, String addressee_note) {
		this.request_reason = request_reason;
		this.request_year = request_year;
		this.request_months = request_months;
		if (requested_date != null) {
			this.requested_date = sdf.format(requested_date);
		}
		this.request_status = request_status;
		if (addressed_on != null) {
			this.addressed_on = sdf.format(addressed_on);
		}
		this.addressee_note = addressee_note;

	}

	public String getRequest_reason() {
		return request_reason;
	}

	public void setRequest_reason(String request_reason) {
		this.request_reason = request_reason;
	}

	public String getRequest_year() {
		return request_year;
	}

	public void setRequest_year(String request_year) {
		this.request_year = request_year;
	}

	public String getRequest_months() {
		return request_months;
	}

	public void setRequest_months(String request_months) {
		this.request_months = request_months;
	}

	public String getRequested_date() {
		return requested_date;
	}

	public void setRequested_date(String requested_date) {
		this.requested_date = requested_date;
	}

	public String getRequest_status() {
		return request_status;
	}

	public void setRequest_status(String request_status) {
		this.request_status = request_status;
	}

	public String getAddressed_on() {
		return addressed_on;
	}

	public void setAddressed_on(String addressed_on) {
		this.addressed_on = addressed_on;
	}

	public String getAddressee_note() {
		return addressee_note;
	}

	public void setAddressee_note(String addressee_note) {
		this.addressee_note = addressee_note;
	}

}
