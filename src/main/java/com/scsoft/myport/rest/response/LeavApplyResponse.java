package com.scsoft.myport.rest.response;

public class LeavApplyResponse {
	private Integer leaveId;
	private String message;
	private Boolean isLeaveCancel;
	private Boolean isLeaveDeleted;
	public Integer getLeaveId() {
		return leaveId;
	}
	public void setLeaveId(Integer leaveId) {
		this.leaveId = leaveId;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Boolean getIsLeaveCancel() {
		if(this.isLeaveCancel == null) {
			this.isLeaveCancel = false;
		}
		return isLeaveCancel;
	}
	public void setIsLeaveCancel(Boolean isLeaveCancel) {
		this.isLeaveCancel = isLeaveCancel;
	}
	public Boolean getIsLeaveDeleted() {
		return isLeaveDeleted;
	}
	public void setIsLeaveDeleted(Boolean isLeaveDeleted) {
		if(this.isLeaveDeleted == null) {
			this.isLeaveDeleted = false;
		}
		this.isLeaveDeleted = isLeaveDeleted;
	}
	
	
}
