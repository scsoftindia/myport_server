package com.scsoft.myport.rest.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ApiResponse {
	@JsonProperty("status")
    private String status;
	@JsonProperty("message")
    private String message;
    public ApiResponse(String responseStatus, String responseMessage) {
        this.status = responseStatus;
        this.message = responseMessage;

    }

    public ApiResponse(String responseStatus) {

        this.status = responseStatus;
    }
}
