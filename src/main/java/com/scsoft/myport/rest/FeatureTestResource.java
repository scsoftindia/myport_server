package com.scsoft.myport.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.bugsnag.Bugsnag;
import com.scsoft.myport.service.FeatureTestService;

@RestController
@RequestMapping("/api")
public class FeatureTestResource {
	@Autowired
	FeatureTestService featureTestService;
	@Autowired
	Bugsnag bugsnag;

	@GetMapping("/testpsuh/{fcmId}")
	public String testPush(@PathVariable String fcmId) {
		return featureTestService.testPush(fcmId);
	}

	@GetMapping("/testemail")
	public String testEmail() {
		return featureTestService.testEmail();
	}

	@GetMapping("/testbugreport")
	public String testBugReport() {
		bugsnag.notify(new RuntimeException("Bug report test"));
		return null;
	}

	

	@GetMapping("/testpage")
	public ModelAndView testPage() {

		ModelAndView mav = new ModelAndView("test.ftl");
		return mav;
	}

}
