package com.scsoft.myport.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.scsoft.myport.domain.SalarySlipRequests;
import com.scsoft.myport.error.OperationNotPossibleException;
import com.scsoft.myport.rest.request.SalarySlipRequest;
import com.scsoft.myport.rest.response.ApiResponse;
import com.scsoft.myport.rest.response.SalarySlipDetails;
import com.scsoft.myport.service.SalarySlipService;
import com.scsoft.myport.service.util.AppConstants;

@RestController
@RequestMapping("/api")
public class SalarySlipRequestResource {
	private final Logger logger = LoggerFactory.getLogger(SalarySlipRequestResource.class);
	@Autowired
	private SalarySlipService salarySlipService;

	@PostMapping("/salarysliprequest")
	public ResponseEntity<ApiResponse> saveSalarySlipRequest(@RequestBody SalarySlipRequest salarySlipRequest) throws OperationNotPossibleException {
		logger.debug("Request to save salary slip request");
		SalarySlipRequests salarySlipRequests = salarySlipService.saveSalarySlipRequest(salarySlipRequest);
		ApiResponse apiResponse = null;
		HttpStatus httpStatus = null;
		if (salarySlipRequests.getId() != null) {
			apiResponse = new ApiResponse(AppConstants.SUCCES, "Salary slip request send succesfully");
			httpStatus = HttpStatus.OK;
		} else {
			apiResponse = new ApiResponse(AppConstants.ERROR, "Salary slip request send failed");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		return new ResponseEntity(apiResponse, httpStatus);
	}

	@GetMapping("/salarysliphistory/{empId}")
	public ResponseEntity<List<SalarySlipDetails>> getSalarySlipHistory(@PathVariable("empId") Integer empId) {
		logger.debug("Request to list salary slip request");
		List<SalarySlipDetails> salarySlipDetails = salarySlipService.getSalarySlipDetailsByEmployee(empId);
		if (salarySlipDetails == null) {
			return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity(salarySlipDetails, HttpStatus.OK);
	}

}
