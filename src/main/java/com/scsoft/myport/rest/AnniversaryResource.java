package com.scsoft.myport.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.scsoft.myport.domain.AnniversaryWish;
import com.scsoft.myport.error.InvalidRequestException;
import com.scsoft.myport.error.NotFoundException;
import com.scsoft.myport.rest.request.AniversaryWishRequest;
import com.scsoft.myport.rest.response.ApiResponse;
import com.scsoft.myport.service.AnniversaryService;
import com.scsoft.myport.service.DTO.AnniversaryDTO;
import com.scsoft.myport.service.util.AppConstants;

@RestController
@RequestMapping("/api")
public class AnniversaryResource {
	private final Logger logger = LoggerFactory.getLogger(AnniversaryResource.class);
	@Autowired
	private AnniversaryService anniversaryService;

	@GetMapping("/upcominganniversaries")
	public ResponseEntity<List<AnniversaryDTO>> getUpComingAnniversaries() {
		logger.debug("Request to fetch Upcoming anniversaries");
		List<AnniversaryDTO> anniversaryDTOs = this.anniversaryService.getUpcomingAnniversaries();
		return new ResponseEntity(anniversaryDTOs, HttpStatus.OK);
	}

	@PostMapping("/saveanniversarywish")
	public ResponseEntity<ApiResponse> saveAnniversaryWish(@RequestBody AniversaryWishRequest aniversaryWishRequest) throws NotFoundException, InvalidRequestException {
		logger.debug("Request to save anniversary wish");
		ApiResponse apiResponse;
		HttpStatus httpStatus;
		AnniversaryWish anniversaryWish = this.anniversaryService.saveAnniversaryWish(aniversaryWishRequest);
		if (anniversaryWish != null && anniversaryWish.getId() != null) {
			apiResponse = new ApiResponse(AppConstants.SUCCES, "Anniversary wish send successfully");
			httpStatus = HttpStatus.OK;
		} else {
			apiResponse = new ApiResponse(AppConstants.ERROR, "Error occured while sending Anniversary wish");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		return new ResponseEntity(apiResponse, httpStatus);
	}

}
