package com.scsoft.myport.rest;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.websocket.server.PathParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.scsoft.myport.domain.ApplyHoliday;
import com.scsoft.myport.domain.ApplyLeave;
//import com.scsoft.myport.domain.FcmDetail;
import com.scsoft.myport.domain.LeaveAvailability;
import com.scsoft.myport.error.ApprovalNotPossibleException;
import com.scsoft.myport.error.NotFoundException;
import com.scsoft.myport.error.OperationNotPossibleException;
import com.scsoft.myport.fcm.model.EntityMessage;
import com.scsoft.myport.fcm.util.FcmClient;
import com.scsoft.myport.rest.request.ApplyHoliDayApproveRequest;
import com.scsoft.myport.rest.request.ApplyLeaveApproovalRequest;
import com.scsoft.myport.rest.request.ApplyLeaveRequest;
import com.scsoft.myport.rest.request.HoliDayRequest;
import com.scsoft.myport.rest.response.ApiResponse;
import com.scsoft.myport.rest.response.UserHoliDayResponse;
import com.scsoft.myport.service.HolidaysService;
import com.scsoft.myport.service.DTO.AppliedHoliDayDTO;
import com.scsoft.myport.service.DTO.AppliedLeaveDTO;
import com.scsoft.myport.service.DTO.HolidayHistroryByEmpDTO;
import com.scsoft.myport.service.DTO.LeaveDTO;
import com.scsoft.myport.service.DTO.LeaveHistoryByEmpDTO;
import com.scsoft.myport.service.util.AppConstants;

// This Controller handles all the requests related with leaves and holidays.
@RestController
@RequestMapping("/api")
public class HoliDayLeaveResource {
	private final Logger logger = LoggerFactory.getLogger(HoliDayLeaveResource.class);
	@Autowired
	private HolidaysService holidaysService;


	// REST method to fetch complete leave and holiday details of a user.
	@GetMapping("/holidaysandleaves")
	public UserHoliDayResponse getHolidays(@RequestParam Integer empId) {
		logger.debug("REquest to get User Holidays details");
		return holidaysService.getUserHoliDayDetails(empId);
	}

	// REST Method to save multiple Holiday request simultaneously.
	@PostMapping("/applyholiday")
	public ResponseEntity<ApiResponse> applyHoliDay(@RequestBody List<HoliDayRequest> holiDayRequest) throws OperationNotPossibleException {
		logger.debug("Request to save HoliDay Information");

		ApiResponse apiResponse = null;
		HttpStatus httpStatus = null;
		String result = holidaysService.saveHolidayRequest(holiDayRequest);
		if (result == null) {
			apiResponse = new ApiResponse(AppConstants.ERROR, "Bad request");
			httpStatus = HttpStatus.BAD_REQUEST;
		} else if (holiDayRequest.get(0).getIsCancelHoliday()) {
			apiResponse = new ApiResponse(AppConstants.SUCCES, "Holiday withdrawal send successfully");
			httpStatus = HttpStatus.OK;
		} else {
			apiResponse = new ApiResponse(AppConstants.SUCCES, "Holiday applied successfully");
			httpStatus = HttpStatus.OK;
		}
		return new ResponseEntity<ApiResponse>(apiResponse, httpStatus);
	}

	// REST Method to retrieve list of employees who is in leave for today.
	@GetMapping("/employessonleave")
	public List<LeaveDTO> getEployessOnleave() {
		logger.debug("Request to get employess on leave");
		return holidaysService.getEmployessOnLeave();

	}

	// REST Method to retrieve use leave availability status
	@GetMapping("/employeeleavevailability/{userid}")
	public List<LeaveAvailability> getEployessleaveAvailability(@PathVariable Integer userid) {
		logger.debug("Request to get employee leave availability");
		return holidaysService.getEmployeeLeaveAvailability(userid);

	}

	// REST Method to save leave request
	@RequestMapping(path = "/applyleave", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ApiResponse> applyLeave(@RequestParam(value = "data") String data,
			@RequestParam(value = "file", required = false) MultipartFile file)
			throws JsonParseException, JsonMappingException, IOException, NotFoundException, OperationNotPossibleException {
		logger.debug("Request apply/cancel employee leave");
		logger.debug("received " + data);
		ObjectMapper mapper = new ObjectMapper();
		ApplyLeaveRequest applyLeaveRequest = mapper.readValue(data, ApplyLeaveRequest.class);
		Map<String, String> response = holidaysService.applyLeaveForEmp(applyLeaveRequest, file);
		ApiResponse apiResponse = new ApiResponse(response.get("status"), response.get("message"));
		HttpStatus httpStatus = null;
		if (response.get(AppConstants.STATUS).equals(AppConstants.SUCCES)) {
			httpStatus = HttpStatus.OK;
		} else {
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		return new ResponseEntity<ApiResponse>(apiResponse, httpStatus);
	}

	// REST Method to get leave requests which are waiting for approval.
	@GetMapping("/applieadleavelist/{id}")
	public List<AppliedLeaveDTO> getApplieadLaves(@PathVariable("id") Integer id) {
		logger.debug("Request to fetch all leave details");
		return holidaysService.getLeaveRequests(id);
	}
	
	@GetMapping("/empleavehistorybysupervisor")
	public ResponseEntity<List<LeaveHistoryByEmpDTO>> getApprovedLeaveHistory(@RequestParam("empId") Integer empId,
			@RequestParam("size") Integer size,
					@RequestParam("pagenumber") Integer pagenumber, @RequestParam("sortdirection") String sortdirection,
					@RequestParam("sortProperty") String sortProperty){
		Pageable pageable = new PageRequest(pagenumber, size,
				new Sort(Sort.Direction.fromString(sortdirection), sortProperty));
		Page<LeaveHistoryByEmpDTO> leaveHisotry = this.holidaysService.getEmployeeLeaveHisotryBySupevisor(empId, pageable);
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("totalelements", String.valueOf(leaveHisotry.getTotalElements()));
		responseHeaders.set("totalpages", String.valueOf(leaveHisotry.getTotalPages()));
		return new ResponseEntity<List<LeaveHistoryByEmpDTO>>(leaveHisotry.getContent(),HttpStatus.OK);
		
	}
	
	@GetMapping("/empholidayhistorybysupervisor")
	public ResponseEntity<List<HolidayHistroryByEmpDTO>> getApprovedHolidayHistory(@RequestParam("empId") Integer empId,
			@RequestParam("size") Integer size,
					@RequestParam("pagenumber") Integer pagenumber, @RequestParam("sortdirection") String sortdirection,
					@RequestParam("sortProperty") String sortProperty){
		Pageable pageable = new PageRequest(pagenumber, size,
				new Sort(Sort.Direction.fromString(sortdirection), sortProperty));
		Page<HolidayHistroryByEmpDTO> holidayHisotry = this.holidaysService.getEmpHolidayHistoryBySupervisor(empId, pageable);
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("totalelements", String.valueOf(holidayHisotry.getTotalElements()));
		responseHeaders.set("totalpages", String.valueOf(holidayHisotry.getTotalPages()));
		return new ResponseEntity<List<HolidayHistroryByEmpDTO>>(holidayHisotry.getContent(),HttpStatus.OK);
		
	}

	// REST Method to approve or reject leave
	@PostMapping("/approveleave")
	public ResponseEntity<ApiResponse> aprooveLeave(
			@RequestBody ApplyLeaveApproovalRequest applyLeaveApproovalRequest) throws ApprovalNotPossibleException, NotFoundException {
		logger.debug("Request to save Leave approval status");
		String result = holidaysService.aprroveLeave(applyLeaveApproovalRequest);
		ApiResponse apiResponse = null;
		HttpStatus httpStatus = null;
		if (result != null) {
			if (AppConstants.APPROVED.equals(result)) {
				if (applyLeaveApproovalRequest.getIsApproveCancelLeave()) {
					apiResponse = new ApiResponse(AppConstants.SUCCES, "Leave cancel request approved successfully");
				} else {
					apiResponse = new ApiResponse(AppConstants.SUCCES, "Leave approved successfully");
				}
			} else {
				if (applyLeaveApproovalRequest.getIsRejectCancelLeave()) {
					apiResponse = new ApiResponse(AppConstants.SUCCES, "Leave cancel request rejected successfully");
				} else {
					apiResponse = new ApiResponse(AppConstants.SUCCES, "Leave rejected successfully");
				}

			}
			httpStatus = HttpStatus.OK;
		} else {
			apiResponse = new ApiResponse(AppConstants.ERROR, "Operation failed");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		return new ResponseEntity<>(apiResponse, httpStatus);
	}

	// REST Method to get holiday requests which are waiting for approval.
	@GetMapping("/applieadholidaylist/{id}")
	public List<AppliedHoliDayDTO> getAppliedHolidays(@PathVariable("id") Integer id) {
		logger.debug("Request to fetch all Hiliday request details");
		return holidaysService.getAppliedHoliDays(id);
	}

	// REST Method to approve or reject holiday
	@PostMapping("/approveholiday")
	public ResponseEntity<ApiResponse> approoveholiday(
			@RequestBody ApplyHoliDayApproveRequest applyHoliDayApproveRequest) throws ApprovalNotPossibleException, NotFoundException {
		logger.debug("Request to save Holiday approval status");
		String result = holidaysService.aprroveHoliDay(applyHoliDayApproveRequest);
		ApiResponse apiResponse = null;
		HttpStatus httpStatus = null;
		if (result != null) {
			if (AppConstants.APPROVED.equals(result)) {
				if (applyHoliDayApproveRequest.getIsApproveCancelHoliday()) {
					apiResponse = new ApiResponse(AppConstants.SUCCES, "Holiday cancel request approved successfully");
				} else {
					apiResponse = new ApiResponse(AppConstants.SUCCES, "Holiday approved successfully");
				}
			} else {
				if (applyHoliDayApproveRequest.getIsApproveCancelHoliday()) {
					apiResponse = new ApiResponse(AppConstants.SUCCES, "Holiday cancel request rejected successfully");
				} else {
					apiResponse = new ApiResponse(AppConstants.SUCCES, "Holiday rejected successfully");
				}

			}
			httpStatus = HttpStatus.OK;
		} else {
			apiResponse = new ApiResponse(AppConstants.ERROR, "Operation failed");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		return new ResponseEntity<>(apiResponse, httpStatus);
	}

	// REST Method to approve or reject multiple holiday simultaneously.
	@PostMapping("/approvemultipleholiday")
	public ResponseEntity<ApiResponse> approoveMultipleholiday(
			@RequestBody List<ApplyHoliDayApproveRequest> applyHoliDayApproveRequests) throws ApprovalNotPossibleException, NotFoundException {
		logger.debug("Request to save Holiday approval status");
		String result = holidaysService.aprroveMultipleHoliDay(applyHoliDayApproveRequests);
		ApiResponse apiResponse = null;
		HttpStatus httpStatus = null;
		if (result != null) {
			if (AppConstants.APPROVED.equals(result)) {
				apiResponse = new ApiResponse(AppConstants.SUCCES, "Holidays approved sucessfully");
			} else if (AppConstants.REJECTED.equals(result)) {
				apiResponse = new ApiResponse(AppConstants.SUCCES, "Holidays rejected sucessfully");
			} else {
				apiResponse = new ApiResponse(AppConstants.SUCCES, "Operation Already done");
			}
			httpStatus = HttpStatus.OK;
		} else {
			apiResponse = new ApiResponse(AppConstants.ERROR, "Operation failed");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		return new ResponseEntity<>(apiResponse, httpStatus);
	}

	// REST Method to approve or reject multiple leave simultaneously.
	@PostMapping("/approvemultipleleave")
	public ResponseEntity<ApiResponse> approveMultipleLeave(
			@RequestBody List<ApplyLeaveApproovalRequest> applyLeaveApproovalRequests) throws ApprovalNotPossibleException, NotFoundException {
		logger.debug("Request to save Holiday approval status");
		String result = holidaysService.aprroveMltipleLeave(applyLeaveApproovalRequests);
		ApiResponse apiResponse = null;
		HttpStatus httpStatus = null;
		if (result != null) {
			if (AppConstants.APPROVED.equals(result)) {
				apiResponse = new ApiResponse(AppConstants.SUCCES, "Leaves approved sucessfully");
			} else if (AppConstants.REJECTED.equals(result)) {
				apiResponse = new ApiResponse(AppConstants.SUCCES, "Leaves rejected sucessfully");
			} else {
				apiResponse = new ApiResponse(AppConstants.SUCCES, "Operation Already done");
			}
			httpStatus = HttpStatus.OK;
		} else {
			apiResponse = new ApiResponse(AppConstants.ERROR, "Operation failed");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		return new ResponseEntity<>(apiResponse, httpStatus);
	}

}
