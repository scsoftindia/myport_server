package com.scsoft.myport.rest;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.scsoft.myport.domain.ComplaintsConcernsMain;
import com.scsoft.myport.domain.ComplaintsConcernsType;
import com.scsoft.myport.error.ApprovalNotPossibleException;
import com.scsoft.myport.error.InvalidRequestException;
import com.scsoft.myport.error.NotFoundException;
import com.scsoft.myport.rest.request.ComplaintRequest;
import com.scsoft.myport.rest.request.ComplaintStatusUpdateRequest;
import com.scsoft.myport.rest.response.ApiResponse;
import com.scsoft.myport.service.CompliantsService;
import com.scsoft.myport.service.DTO.ComplaintDTO;
import com.scsoft.myport.service.util.AppConstants;

@RestController
@RequestMapping("/api")
public class ComplaintsConcernsResource {
	private final Logger logger = LoggerFactory.getLogger(ComplaintsConcernsResource.class);
	@Autowired
	private CompliantsService compliantsService;

	@GetMapping("/concerntypes")
	public List<ComplaintsConcernsType> getAllComplaintsConcernsType() {
		logger.debug("Request to retrieve all concern types");
		return compliantsService.getAllComplaintsConcernsType();
	}

	@RequestMapping(path = "/savecompliant", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ApiResponse> saveComplaint(@RequestParam(value = "data") String data,
			@RequestParam(value = "file", required = false) MultipartFile file)
			throws JsonParseException, JsonMappingException, IOException, NotFoundException, InvalidRequestException {
		logger.debug("request to save Compliant");
		logger.debug("received " + data);
		ObjectMapper mapper = new ObjectMapper();
		ComplaintRequest complaintRequest = mapper.readValue(data, ComplaintRequest.class);
		ComplaintsConcernsMain complaintsConcernsMain = compliantsService.saveComplaint(complaintRequest, file);
        ApiResponse apiResponse = null;
        HttpStatus httpStatus = null;
        if(complaintsConcernsMain.getId() != null) {
        	apiResponse = new ApiResponse(AppConstants.SUCCES,"Complaint/concern send successfully");
        	httpStatus = HttpStatus.OK;
        }else {
        	apiResponse = new ApiResponse(AppConstants.ERROR,"Error occured while sedning Complaint/concern");
        	httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<>(apiResponse,httpStatus);
	}
	
	@PostMapping("/updatecomplaintstatus")
	public ResponseEntity<ApiResponse> updateComplaintStatus(@RequestBody ComplaintStatusUpdateRequest complaintStatusUpdateRequest) throws ApprovalNotPossibleException, NotFoundException, InvalidRequestException{
		logger.debug("request to save Compliant status");
		ComplaintsConcernsMain complaintsConcernsMain = compliantsService.updateComplaintStatus(complaintStatusUpdateRequest);
		 ApiResponse apiResponse = null;
	      HttpStatus httpStatus = null;
		if(complaintsConcernsMain.getCc_resolved().equals(complaintStatusUpdateRequest.getStatus().toString())) {
			if("Forwarded".equals(complaintStatusUpdateRequest.getStatus().toString())) {
				apiResponse = new ApiResponse(AppConstants.SUCCES,"Complaint Forwarded successfully");
	        	httpStatus = HttpStatus.OK;
			}else if("Yes".equals(complaintStatusUpdateRequest.getStatus().toString())) {
				apiResponse = new ApiResponse(AppConstants.SUCCES,"Complaint resolved successfully");
	        	httpStatus = HttpStatus.OK;
			}else if("No".equals(complaintStatusUpdateRequest.getStatus().toString())) {
				apiResponse = new ApiResponse(AppConstants.SUCCES,"Complaint Rejected successfully");
	        	httpStatus = HttpStatus.OK;
			}
		}else {
			apiResponse = new ApiResponse(AppConstants.ERROR,"Error occured while updating Complaint/concern status");
        	httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		 return new ResponseEntity<>(apiResponse,httpStatus);
		
	}

	@GetMapping("/employeecomplaints/{id}")
	public List<ComplaintDTO> getComplaints(@PathVariable("id") Integer id) {
		logger.debug("Request to fetch all complaints by employee id");
		return compliantsService.getEmployeeComplaintsList(id);
	}
	@GetMapping("/employeependingcomplaints")
	public List<ComplaintDTO> getPendingComplaints() {
		logger.debug("Request to fetch all complaints by employee id");
		return compliantsService.getPendingComplaints();
	}

}
