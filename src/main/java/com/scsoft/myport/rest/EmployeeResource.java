package com.scsoft.myport.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.scsoft.myport.domain.UserMobileInfo;
import com.scsoft.myport.error.NotFoundException;
import com.scsoft.myport.error.OperationNotPossibleException;
import com.scsoft.myport.rest.request.EditProfileRequest;
import com.scsoft.myport.rest.request.UserMobileInfoRequest;
import com.scsoft.myport.rest.response.ApiResponse;
import com.scsoft.myport.service.SupervisorDTO;
import com.scsoft.myport.service.UserService;
import com.scsoft.myport.service.DTO.EmployeeDTO;
import com.scsoft.myport.service.util.AppConstants;

import io.jsonwebtoken.lang.Objects;

@RestController
@RequestMapping("/api")
public class EmployeeResource {
	private final Logger logger = LoggerFactory.getLogger(EmployeeResource.class);
	@Autowired
	private UserService userService;
	@GetMapping("/employess")
	public List<EmployeeDTO> getEmployees(){
		logger.debug("Request to fetch employess");
		return userService.findAllEmployess();
		
	}
	
	@GetMapping("/findallemployess")
	public List<EmployeeDTO> getAllEmployees(){
		logger.debug("Request to fetch employess");
		return userService.getAllEmployess();		
	}
	@GetMapping("/findallsupervisors")
	public List<SupervisorDTO> getAllSupervisors(){
		logger.debug("Request to fetch employess");
		return userService.getSupervisorList();		
	}
	
	@PostMapping("/savemobiledata")
	public ResponseEntity<ApiResponse> saveMobileData(@RequestBody UserMobileInfoRequest userMobileInfoRequest) throws NotFoundException{
		logger.debug("saving user mobile data");
		ApiResponse apiResponse = null;
		HttpStatus httpStatus = null;
		UserMobileInfo userMobileInfo = this.userService.saveUserInfo(userMobileInfoRequest);
        if(userMobileInfo != null && userMobileInfo.getId() != null) {
        	apiResponse = new ApiResponse(AppConstants.SUCCES,"User mobile data save successfully");
        	httpStatus = HttpStatus.OK;
        }else {
        	apiResponse = new ApiResponse(AppConstants.ERROR,"Error occured while saving mobile data");
        	httpStatus = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<ApiResponse>(apiResponse,httpStatus);
	}
	@PostMapping("/editprofile")
	public ResponseEntity<ApiResponse> saveUserProfile(@RequestBody EditProfileRequest editProfileRequest) throws NotFoundException, OperationNotPossibleException{
		logger.debug("REST request to save user profile");
		ApiResponse apiResponse = null;
		HttpStatus httpStatus = null;
		Boolean result = this.userService.editProfile(editProfileRequest);
		if(result) {
			apiResponse = new ApiResponse(AppConstants.SUCCES,"Profile edited successfully");
			httpStatus = httpStatus.OK;
		}else {
			apiResponse = new ApiResponse(AppConstants.SUCCES,"Error occured while editing profile");
			httpStatus = httpStatus.BAD_REQUEST;
		}
		  return new ResponseEntity<ApiResponse>(apiResponse,httpStatus);
	}
	

}
