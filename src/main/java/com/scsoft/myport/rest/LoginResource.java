package com.scsoft.myport.rest;

import java.io.IOException;
import java.util.List;

import org.apache.directory.api.ldap.model.exception.LdapException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.scsoft.myport.error.NotFoundException;
import com.scsoft.myport.rest.request.LoginRequest;
import com.scsoft.myport.rest.request.TrackRequest;
import com.scsoft.myport.rest.request.XyzRequest;
import com.scsoft.myport.service.PaymentLookupService;
import com.scsoft.myport.service.UserService;
import com.scsoft.myport.service.DTO.EmployeeDTO;

@RestController
@RequestMapping("/api")
public class LoginResource {
	@Autowired
	private UserService userService;
	@Autowired
	private PaymentLookupService paymentLookupService;

	@PostMapping("/login")
	public EmployeeDTO login(@RequestBody LoginRequest loginRequest) throws LdapException {
		String username = loginRequest.getUsername();
		String password = loginRequest.getPassword();
		return userService.getEmployeeDetails(username, password);
	}

	@PostMapping("/logout/{empId}/{macAdress}")
	public String logout(@PathVariable Integer empId, @PathVariable String macAdress) throws LdapException, NotFoundException {
		this.userService.updateUserMobileInfo(macAdress, empId);
		return paymentLookupService.deActivatePushRequest(empId);

	}


	@PostMapping("/tracksave")
	public String performTrackService(@RequestBody TrackRequest trackRequest) {
		return this.userService.performTrack(trackRequest);
	}
	@GetMapping("/isactive/{empId}")
	public Boolean isActive(@PathVariable Integer empId) {
		return this.userService.isActiveEmployee(empId);
	}
}
