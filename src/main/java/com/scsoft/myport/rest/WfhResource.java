package com.scsoft.myport.rest;

import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.scsoft.myport.error.ApprovalNotPossibleException;
import com.scsoft.myport.error.InvalidRequestException;
import com.scsoft.myport.error.NotFoundException;
import com.scsoft.myport.error.OperationNotPossibleException;
import com.scsoft.myport.rest.request.WfhApproveRequest;
import com.scsoft.myport.rest.request.WfhCancellRequest;
import com.scsoft.myport.rest.request.WfhRequest;
import com.scsoft.myport.rest.response.ApiResponse;
import com.scsoft.myport.service.ApplyWfhService;
import com.scsoft.myport.service.DTO.WfhDTO;
import com.scsoft.myport.service.DTO.WfhEmpDTO;
import com.scsoft.myport.service.util.AppConstants;

@RestController
@RequestMapping("/api")
public class WfhResource {
	private final Logger logger = LoggerFactory.getLogger(WfhResource.class);
	@Autowired
	private ApplyWfhService applyWfhService;

	@PostMapping("/applywfh")
	public ResponseEntity<ApiResponse> applyWfh(@RequestBody WfhRequest wfhRequest)
			throws OperationNotPossibleException, NotFoundException, InvalidRequestException, ParseException {
		logger.debug("Request to save wfh Information");

		ApiResponse apiResponse = null;
		HttpStatus httpStatus = null;
		Boolean result = this.applyWfhService.applyWfh(wfhRequest);
		if (!result) {
			apiResponse = new ApiResponse(AppConstants.ERROR, "Bad request");
			httpStatus = HttpStatus.BAD_REQUEST;
		} else {
			apiResponse = new ApiResponse(AppConstants.SUCCES, "Work from Home applied successfully");
			httpStatus = HttpStatus.OK;
		}
		return new ResponseEntity<ApiResponse>(apiResponse, httpStatus);
	}

	@PostMapping("/cancellwfh")
	public ResponseEntity<ApiResponse> cancellWfh(@RequestBody WfhCancellRequest wfhCancellRequest)
			throws OperationNotPossibleException, NotFoundException, InvalidRequestException {
		logger.debug("Request to save wfh Information");

		ApiResponse apiResponse = null;
		HttpStatus httpStatus = null;
		if (StringUtils.isEmpty(wfhCancellRequest.getCancelReason())) {
			apiResponse = new ApiResponse(AppConstants.ERROR, "Bad request");
			httpStatus = HttpStatus.BAD_REQUEST;
		} else {
			Boolean result = this.applyWfhService.cancellWfh(wfhCancellRequest);
			if (!result) {
				apiResponse = new ApiResponse(AppConstants.ERROR, "Bad request");
				httpStatus = HttpStatus.BAD_REQUEST;
			} else {
				apiResponse = new ApiResponse(AppConstants.SUCCES, "Work from Home Cancelled successfully");
				httpStatus = HttpStatus.OK;
			}
		}
		return new ResponseEntity<ApiResponse>(apiResponse, httpStatus);
	}

	@PostMapping("/approvewfh")
	public ResponseEntity<ApiResponse> approveWfh(@RequestBody WfhApproveRequest wfhApproveRequest)
			throws OperationNotPossibleException, NotFoundException, InvalidRequestException,
			ApprovalNotPossibleException {
		logger.debug("Request to save wfh Information");

		ApiResponse apiResponse = null;
		HttpStatus httpStatus = null;
		String resultStatus = wfhApproveRequest.getStatus().toString();
		Boolean result = this.applyWfhService.approveWfh(wfhApproveRequest);
		if (!result) {
			apiResponse = new ApiResponse(AppConstants.ERROR, "Bad request");
			httpStatus = HttpStatus.BAD_REQUEST;
		} else {
			apiResponse = new ApiResponse(AppConstants.SUCCES, "Work from Home " + resultStatus + " successfully");
			httpStatus = HttpStatus.OK;
		}
		return new ResponseEntity<ApiResponse>(apiResponse, httpStatus);
	}

	@GetMapping("/getpendingwfhrequests/{empId}")
	public ResponseEntity<List<WfhDTO>> getPendingRequests(@PathVariable Integer empId) {
		logger.debug("Request to fetch pending wfh requests");
		List<WfhDTO> wfhDTOs = this.applyWfhService.getPendingWfhRequests(empId);
		return new ResponseEntity(wfhDTOs, HttpStatus.OK);

	}

	@GetMapping("/getpendingwfhhistory/{empId}")
	public ResponseEntity<List<WfhEmpDTO>> getpendingWfhHistory(@PathVariable Integer empId) {
		logger.debug("Request to fetch  wfh Hisotry");
		List<WfhEmpDTO> wfhEmpDTOs = this.applyWfhService.getWfhHistory(empId);
		return new ResponseEntity(wfhEmpDTOs, HttpStatus.OK);

	}
}
