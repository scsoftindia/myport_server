package com.scsoft.myport.rest;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;

import com.scsoft.myport.error.ValidationException;
import com.scsoft.myport.rest.request.CcfRequest;
import com.scsoft.myport.rest.response.ApiResponse;
import com.scsoft.myport.rest.validator.CcfValidator;
import com.scsoft.myport.service.CcfService;

@RestController
@RequestMapping("/api")
public class CcfResource {
	private final Logger logger = LoggerFactory.getLogger(CcfResource.class);
	@Autowired
	private CcfService ccfService;
	@Autowired
	private CcfValidator ccfValidator;
	
	@PostMapping("/saveccf")
	public ResponseEntity<ApiResponse> createCcf(@RequestBody @Valid CcfRequest ccfRequest,BindingResult result) throws ValidationException{
		logger.debug("Request to save ccf");
		this.ccfValidator.validate(ccfRequest,result);
		if(result.hasFieldErrors()) {
			List<FieldError> fieldErrors = result.getFieldErrors();
			logger.debug("Validation error occured",fieldErrors);
			throw new ValidationException(fieldErrors);
			
		}
		return null;
		
	}
}
