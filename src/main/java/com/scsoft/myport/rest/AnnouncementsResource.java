package com.scsoft.myport.rest;

import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.scsoft.myport.domain.Announcements;
import com.scsoft.myport.rest.request.AnnouncementRequest;
import com.scsoft.myport.rest.response.ApiResponse;
import com.scsoft.myport.service.AnnouncementsService;
import com.scsoft.myport.service.DTO.AnnouncementDTO;
import com.scsoft.myport.service.util.AppConstants;

@RestController
@RequestMapping("/api")
public class AnnouncementsResource {
	@Autowired
	private AnnouncementsService announcementsService;
	private final Logger logger = LoggerFactory.getLogger(AnnouncementsResource.class);

	@GetMapping("/announcements")
	public List<AnnouncementDTO> getAllAnnouncements() {
		logger.debug("Request to fetch all Announcements");
		return announcementsService.findAllAnnouncements();
	}

	@GetMapping("/announcements/{id}")
	public Announcements getAnnouncements(@PathVariable Integer id) {
		logger.debug("Request to fetch  Announcements by" + id);
		return announcementsService.findAnnouncements(id);
	}
	@PostMapping("/saveannouncement")
	public ResponseEntity<ApiResponse> saveAnnounceMent(@RequestBody AnnouncementRequest announcementRequest) throws ParseException {
		logger.debug("Request to publish new announcement");
		ApiResponse apiResponse = null;
		HttpStatus httpStatus = null;
		if(StringUtils.isEmpty(announcementRequest.getAnnouncement()) || StringUtils.isEmpty(announcementRequest.getAnnouncementHeading()) || announcementRequest.getAnnouncementBy() == null) {
			apiResponse = new ApiResponse(AppConstants.SUCCES,"Bad request");
			httpStatus =HttpStatus.BAD_REQUEST;
		}else {
			Announcements announcements = this.announcementsService.save(announcementRequest);
			if(announcements != null && announcements.getId() != null) {
				apiResponse = new ApiResponse(AppConstants.SUCCES,"Announcement send successfully");
				httpStatus =HttpStatus.OK;
			}else {
				apiResponse = new ApiResponse(AppConstants.ERROR,"Error occurred while sending Announcement");
				httpStatus = HttpStatus.BAD_REQUEST;
			}
		}
		
		return new ResponseEntity(apiResponse,httpStatus);
		
	}

}
