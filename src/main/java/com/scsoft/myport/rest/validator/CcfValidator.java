package com.scsoft.myport.rest.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.scsoft.myport.rest.request.CcfRequest;

@Component
public class CcfValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		
		return CcfRequest.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		CcfRequest ccfRequest = (CcfRequest) obj;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title_of_change", " Title of Change Not Found");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "description_of_change", " Description Not Found");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "change_requester_id", " Change requester Not Found");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "team", " team Not Found");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "service_owner", " Service owner Not Found");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "group_team", " Group team Not Found");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "service_affected", " Service affected Not Found");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cis_affected", " Cis affected Not Found");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "sd_tool_ticket_number", "SD tool ticket number Not Found");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "comments_details", "comments details Not Found");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "names_of_affected_systems", "Names of affected systems Not Found");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "number_of_affected_systems", "Number of affected systems Not Found");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "impact_on_service", "Impact on service Not Found");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "impact_description", "Impact description Not Found");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "consequence", "Consequence Not Found");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "probability_of_failuire", "Probability of failuire Not Found");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "impact_of_the_failure", "Impact of the failure Not Found");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "overall_subjective_risk", "Impact of the failure Not Found");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "implemetation_step", "Implemetation step Not Found");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "backout_plan_step", "Backout plan step Not Found");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "verfication_plan_steps", "Verfication plan steps Not Found");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "change_type", "Change type Not Found");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "scheduled_start_date", "Scheduled start date Not Found");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "scheduled_end_date", "Scheduled end date Not Found");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "change_expires_on", "Change expires on Not Found");

	}

}
