package com.scsoft.myport.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.scsoft.myport.domain.BestPerformer;
import com.scsoft.myport.service.BestPerformerService;
import com.scsoft.myport.service.DTO.BestPerformerDTO;

@RestController
@RequestMapping("/api")
public class BestPerformerResource {
	private final Logger logger = LoggerFactory.getLogger(BestPerformerResource.class);
	@Autowired
	private BestPerformerService bestPerformerService;
	@GetMapping("/bestperformerList")
	public List<BestPerformerDTO> getBestPerformers(){
		logger.debug("Request to fetch Best performers for current quarter");
		return bestPerformerService.getBestPerformers();
		
	}

}
