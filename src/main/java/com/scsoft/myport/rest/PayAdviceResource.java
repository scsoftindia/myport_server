package com.scsoft.myport.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.scsoft.myport.domain.PayadviceCategories;
import com.scsoft.myport.domain.PayadviceSubCategories;
import com.scsoft.myport.domain.PaymentAdvice;
import com.scsoft.myport.domain.PaymentAdviceDocuments;
import com.scsoft.myport.domain.Vendors;
import com.scsoft.myport.error.ApprovalNotPossibleException;
import com.scsoft.myport.error.InvalidRequestException;
import com.scsoft.myport.error.NotFoundException;
import com.scsoft.myport.rest.request.PayadviceCategoriesAproveRequest;
import com.scsoft.myport.rest.request.PayadviceCategoriesRequest;
import com.scsoft.myport.rest.request.PayadviceSubCategoriesRequest;
import com.scsoft.myport.rest.request.PaymentAdviceApproovalRequest;
import com.scsoft.myport.rest.request.PaymentAdviceRequest;
import com.scsoft.myport.rest.request.VendorsRequest;
import com.scsoft.myport.rest.response.ApiResponse;
import com.scsoft.myport.service.PayAdviceService;
import com.scsoft.myport.service.DTO.PaymentAdviceDTO;
import com.scsoft.myport.service.util.AppConstants;
import com.scsoft.myport.service.util.Encryptor;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
@RequestMapping("/api")
public class PayAdviceResource {
	private final Logger logger = LoggerFactory.getLogger(PayAdviceResource.class);
	@Autowired
	private PayAdviceService payAdviceService;
	@Value("${advice.file.path}")
	private String adviceFilePath;

	@PostMapping("/savepayadvicecategory")
	public ResponseEntity<String> savePayadviceCategory(
			@RequestBody PayadviceCategoriesRequest payadviceCategoriesRequest) {
		logger.debug("Request to save PayadviceCategories");
		PayadviceCategories payadviceCategories = payAdviceService.savePayadviceCategories(payadviceCategoriesRequest);
		String result = null;
		HttpStatus httpStatus = null;
		if (payadviceCategories.getId() == null) {
			result = AppConstants.ERROR;
			httpStatus = HttpStatus.BAD_REQUEST;
		} else {
			result = AppConstants.SUCCES;
			httpStatus = HttpStatus.OK;
		}
		return new ResponseEntity<String>(result, httpStatus);
	}

	@PostMapping("/updatepayadvicecategory")
	public ResponseEntity<String> updatePayadviceCategory(
			@RequestBody PayadviceCategoriesRequest payadviceCategoriesRequest) {
		logger.debug("Request to Update PayadviceCategories");
		if (payadviceCategoriesRequest.getId() == null) {
			return new ResponseEntity<String>(AppConstants.ERROR, HttpStatus.BAD_REQUEST);
		}
		PayadviceCategories payadviceCategories = payAdviceService.savePayadviceCategories(payadviceCategoriesRequest);
		String result = null;
		HttpStatus httpStatus = null;
		if (payadviceCategories.getId() == null) {
			result = AppConstants.ERROR;
			httpStatus = HttpStatus.BAD_REQUEST;
		} else {
			result = AppConstants.SUCCES;
			httpStatus = HttpStatus.OK;
		}
		return new ResponseEntity<String>(result, httpStatus);
	}

	@PostMapping("/updatepayadvicecategorystatus")
	public ResponseEntity<ApiResponse> updatePayadviceCategoryStatus(
			@RequestBody PayadviceCategoriesAproveRequest payadviceCategoriesAproveRequest) {
		logger.debug("Request to Update PayadviceCategories Status");
		if (payadviceCategoriesAproveRequest.getId() == null) {
			ApiResponse apiResponse = new ApiResponse(AppConstants.ERROR, "Object with given ID not found");
			return new ResponseEntity<ApiResponse>(apiResponse, HttpStatus.BAD_REQUEST);
		}
		PayadviceCategories payadviceCategories = payAdviceService
				.savePayadviceCategoriesStatus(payadviceCategoriesAproveRequest);
		ApiResponse apiResponse = null;
		HttpStatus httpStatus = HttpStatus.OK;
		if (payadviceCategories.getIs_approved()) {
			apiResponse = new ApiResponse(AppConstants.SUCCES, "Request approved");
		} else if (!payadviceCategories.getIs_approved()) {
			apiResponse = new ApiResponse(AppConstants.SUCCES, "Request Rejected");
		}
		return new ResponseEntity<ApiResponse>(apiResponse, httpStatus);
	}
	
	@GetMapping("/approvecategorybyemail/{status}/{id}/{empId}")
	public ResponseEntity<ApiResponse> approveCategoryByemail(@PathVariable String status,@PathVariable String id,@PathVariable String empId) throws NotFoundException, InvalidRequestException, ApprovalNotPossibleException {
		logger.debug("Request to Update PayadviceCategories Status");
		PayadviceCategories payadviceCategories = payAdviceService
				.savePayadviceCategoriesStatusByEmail(Integer.valueOf(Encryptor.decode(empId)), Integer.valueOf(Encryptor.decode(id)), Encryptor.decode(status));
		ApiResponse apiResponse = null;
		HttpStatus httpStatus = HttpStatus.OK;
		if (payadviceCategories.getIs_approved()) {
			apiResponse = new ApiResponse(AppConstants.SUCCES, "Request approved");
		} else if (!payadviceCategories.getIs_approved()) {
			apiResponse = new ApiResponse(AppConstants.SUCCES, "Request Rejected");
		}
		return new ResponseEntity<ApiResponse>(apiResponse, httpStatus);
	}

	@PostMapping("/savepayadvicesubcategory")
	public ResponseEntity<String> savePayadviceSubCategory(
			@RequestBody PayadviceSubCategoriesRequest payadviceSubCategoriesRequest) {
		logger.debug("Request to save PayadviceSubCategories");
		PayadviceSubCategories payadviceSubCategories = payAdviceService
				.savePayadviceSubCategories(payadviceSubCategoriesRequest);
		String result = null;
		HttpStatus httpStatus = null;
		if (payadviceSubCategories.getId() == null) {
			result = AppConstants.ERROR;
			httpStatus = HttpStatus.BAD_REQUEST;
		} else {
			result = AppConstants.SUCCES;
			httpStatus = HttpStatus.OK;
		}
		return new ResponseEntity<String>(result, httpStatus);
	}

	@PostMapping("/updatepayadvicesubcategory")
	public ResponseEntity<String> updatePayadviceSubCategory(
			@RequestBody PayadviceSubCategoriesRequest payadviceSubCategoriesRequest) {
		logger.debug("Request to Update PayadviceSubCategories");
		if (payadviceSubCategoriesRequest.getId() == null) {
			return new ResponseEntity<String>(AppConstants.ERROR, HttpStatus.BAD_REQUEST);
		}
		PayadviceSubCategories payadviceSubCategories = payAdviceService
				.savePayadviceSubCategories(payadviceSubCategoriesRequest);
		String result = null;
		HttpStatus httpStatus = null;
		if (payadviceSubCategories.getId() == null) {
			result = AppConstants.ERROR;
			httpStatus = HttpStatus.BAD_REQUEST;
		} else {
			result = AppConstants.SUCCES;
			httpStatus = HttpStatus.OK;
		}
		return new ResponseEntity<String>(result, httpStatus);
	}

	@PostMapping("/updatepayadvicesubcategorystatus")
	public ResponseEntity<ApiResponse> updatepayadvicesubcategoryStatus(
			@RequestBody PayadviceCategoriesAproveRequest payadviceCategoriesAproveRequest) {
		logger.debug("Request to Update PayadviceSubCategories Status");
		if (payadviceCategoriesAproveRequest.getId() == null) {
			ApiResponse apiResponse = new ApiResponse(AppConstants.ERROR, "Object with given ID not found");
			return new ResponseEntity<ApiResponse>(apiResponse, HttpStatus.BAD_REQUEST);
		}
		PayadviceSubCategories payadviceSubCategories = payAdviceService
				.savePayadviceSubCategoriesStatus(payadviceCategoriesAproveRequest);
		ApiResponse apiResponse = null;
		HttpStatus httpStatus = HttpStatus.OK;
		if (payadviceSubCategories.getIs_approved()) {
			apiResponse = new ApiResponse(AppConstants.SUCCES, "Request approved");
		} else if (!payadviceSubCategories.getIs_approved()) {
			apiResponse = new ApiResponse(AppConstants.SUCCES, "Request Rejected ");
		}
		return new ResponseEntity<ApiResponse>(apiResponse, httpStatus);
	}
	
	@GetMapping("/approvesubcategorybyemail/{status}/{id}/{empId}")
	public ResponseEntity<ApiResponse> approveSubCategorybyemail(@PathVariable String status,@PathVariable String id,@PathVariable String empId) throws NotFoundException, InvalidRequestException, ApprovalNotPossibleException {
		logger.debug("Request to Update PayadviceSubCategories Status");
		PayadviceSubCategories payadviceCategories = payAdviceService
				.savePayadviceSubCategoriesStatusByEmail(Integer.valueOf(Encryptor.decode(empId)), Integer.valueOf(Encryptor.decode(id)), Encryptor.decode(status));
		ApiResponse apiResponse = null;
		HttpStatus httpStatus = HttpStatus.OK;
		if (payadviceCategories.getIs_approved()) {
			apiResponse = new ApiResponse(AppConstants.SUCCES, "Request approved");
		} else if (!payadviceCategories.getIs_approved()) {
			apiResponse = new ApiResponse(AppConstants.SUCCES, "Request Rejected");
		}
		return new ResponseEntity<ApiResponse>(apiResponse, httpStatus);
	}

	@PostMapping("/savevendors")
	public ResponseEntity<String> savevendors(@RequestBody VendorsRequest vendorsRequest) {
		logger.debug("Request to save PayadviceSubCategories");
		Vendors vendors = payAdviceService.saveVendors(vendorsRequest);
		String result = null;
		HttpStatus httpStatus = null;
		if (vendors.getId() == null) {
			result = AppConstants.ERROR;
			httpStatus = HttpStatus.BAD_REQUEST;
		} else {
			result = AppConstants.SUCCES;
			httpStatus = HttpStatus.OK;
		}
		return new ResponseEntity<String>(result, httpStatus);
	}

	@PostMapping("/updatevendors")
	public ResponseEntity<String> updateVendors(@RequestBody VendorsRequest vendorsRequest) {
		logger.debug("Request to save PayadviceSubCategories");
		if (vendorsRequest.getId() == null) {
			return new ResponseEntity<String>(AppConstants.ERROR, HttpStatus.BAD_REQUEST);
		}
		Vendors vendors = payAdviceService.saveVendors(vendorsRequest);
		String result = null;
		HttpStatus httpStatus = null;
		if (vendors.getId() == null) {
			result = AppConstants.ERROR;
			httpStatus = HttpStatus.BAD_REQUEST;
		} else {
			result = AppConstants.SUCCES;
			httpStatus = HttpStatus.OK;
		}
		return new ResponseEntity<String>(result, httpStatus);
	}

	@PostMapping("/updatevendorsstatus")
	public ResponseEntity<ApiResponse> updatevendorsStatus(
			@RequestBody PayadviceCategoriesAproveRequest payadviceCategoriesAproveRequest) {
		logger.debug("Request to Update PayadviceSubCategories Status");
		if (payadviceCategoriesAproveRequest.getId() == null) {
			ApiResponse apiResponse = new ApiResponse(AppConstants.ERROR, "Object with given ID not found");
			return new ResponseEntity<ApiResponse>(apiResponse, HttpStatus.BAD_REQUEST);
		}
		Vendors vendors = payAdviceService.saveVendorsStatus(payadviceCategoriesAproveRequest);
		ApiResponse apiResponse = null;
		HttpStatus httpStatus = HttpStatus.OK;
		if (vendors.isStatus()) {
			apiResponse = new ApiResponse(AppConstants.SUCCES, "Request approved");
		} else if (!vendors.isStatus()) {
			apiResponse = new ApiResponse(AppConstants.SUCCES, "Request Rejected");
		}
		return new ResponseEntity<ApiResponse>(apiResponse, httpStatus);
	}
	
	@GetMapping("/approvevendorbyemail/{status}/{id}/{empId}")
	public ResponseEntity<ApiResponse> approveVendorbyemail(@PathVariable String status,@PathVariable String id,@PathVariable String empId) throws NotFoundException, InvalidRequestException, ApprovalNotPossibleException {
		logger.debug("Request to Update PayadviceSubCategories Status");
		Vendors vendors = payAdviceService
				.saveVendorsStatusByEmail(Integer.valueOf(Encryptor.decode(empId)), Integer.valueOf(Encryptor.decode(id)), Encryptor.decode(status));
		ApiResponse apiResponse = null;
		HttpStatus httpStatus = HttpStatus.OK;
		if (vendors.isStatus()) {
			apiResponse = new ApiResponse(AppConstants.SUCCES, "Request approved");
		} else if (!vendors.isStatus()) {
			apiResponse = new ApiResponse(AppConstants.SUCCES, "Request Rejected");
		}
		return new ResponseEntity<ApiResponse>(apiResponse, httpStatus);
	}

	@PostMapping("/savepaymentadvice/{empId}")
	public ResponseEntity<ApiResponse> savePaymentAdvice(@PathVariable Integer empId,
			@RequestParam(value = "data") String data,
			@RequestParam(value = "files", required = false) MultipartFile[] files)
			throws JsonParseException, JsonMappingException, IOException {
		logger.debug("Request to save Payment advice requests");
		ApiResponse apiResponse = null;
		HttpStatus httpStatus = null;
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		List<PaymentAdviceRequest> paymentAdviceRequests = mapper.readValue(data,
				new TypeReference<List<PaymentAdviceRequest>>() {
				});
		Boolean result = this.payAdviceService.savePaymentAdviceList(paymentAdviceRequests, files, empId);
		if (result) {
			apiResponse = new ApiResponse(AppConstants.SUCCES, "Payment advices added succesfully");
			httpStatus = HttpStatus.OK;
		} else {
			apiResponse = new ApiResponse(AppConstants.ERROR, "Invalid input found in the request");
			httpStatus = HttpStatus.BAD_REQUEST;
		}
		return new ResponseEntity<ApiResponse>(apiResponse, httpStatus);
	}

	@GetMapping("/paymentadvicelist")
	public ResponseEntity<List<PaymentAdviceDTO>> getAllPaymentAdvice(@RequestParam("size") Integer size,
			@RequestParam("pagenumber") Integer pagenumber, @RequestParam("sortdirection") String sortdirection,
			@RequestParam("sortProperty") String sortProperty,@RequestParam("isApproval") Boolean isApproval,
			@RequestParam("approvalType") String approvalType,@RequestParam("empId") Integer empId,
	        @RequestParam(name="categoryId",required=false) Integer categoryId,
	        @RequestParam(name="sub_categoryId",required=false) Integer SubCategoryId,
	        @RequestParam(name="vendor",required=false) Integer vendorId,
	        @RequestParam(name="year",required=false) Integer year,
	        @RequestParam(name="month",required=false) Integer month,
	        @RequestParam(name="checkNumber",required=false) String checkNumber){
		logger.debug("Request to fetch all Payment advice");
		Pageable pageable = new PageRequest(pagenumber, size,
				new Sort(Sort.Direction.fromString(sortdirection), sortProperty));
		HttpHeaders responseHeaders = new HttpHeaders();
		Page<PaymentAdviceDTO> paymentAdvices = this.payAdviceService.getAllPaymentAdvices(pageable,isApproval,approvalType,categoryId,SubCategoryId,vendorId,year,month,checkNumber);
		List<PaymentAdviceDTO> content = paymentAdvices.getContent();
		List<PaymentAdviceDTO> values = new ArrayList();
		Integer forwardedCount = 0;
		if(isApproval && approvalType != null && "admin".equals(approvalType)) {
			Integer newAdviceId = 1;
			Integer forwardedAdviceId = 6;
			
		 for(PaymentAdviceDTO paymentAdviceDTO:content) {
			 if(paymentAdviceDTO.getAprrovalStatus().getId().equals(newAdviceId)) {
				 values.add(paymentAdviceDTO);
			 }
			 else if(paymentAdviceDTO.getAprrovalStatus().getId().equals(forwardedAdviceId) && !paymentAdviceDTO.getPrimary_aproover_emp_Id().equals(empId) ) {
				 values.add(paymentAdviceDTO);
			 }
			 else if(paymentAdviceDTO.getAprrovalStatus().getId().equals(forwardedAdviceId) && paymentAdviceDTO.getPrimary_aproover_emp_Id().equals(empId) ) {
				 forwardedCount++;
			 }
		 }
		}else {
			values = content;
		}
		Long totalelements = (Long)paymentAdvices.getTotalElements();
		totalelements = totalelements - forwardedCount;
		Long totalpages = totalelements/size;
		if(totalpages*size == 0) {
			totalpages = 1L;
		}else if(totalpages*size < totalelements ) {
			totalpages = totalpages+ 1L;
		}
		responseHeaders.set("totalelements", String.valueOf(totalelements));
		responseHeaders.set("totalpages", String.valueOf(totalpages));
		return new ResponseEntity<>(values,responseHeaders, HttpStatus.OK);
	}
	@GetMapping("/getpaymentadviceyears")
	public ResponseEntity<List<Integer>> getPaymentAdviceYears(){
		logger.debug("Request to fetch all the paymentAdvice years");
		List<Integer> years = this.payAdviceService.getAdviceYears();
		return new ResponseEntity(years,HttpStatus.OK);
	}
	@GetMapping("/getpaymentadvicechecknumbers")
	public ResponseEntity<List<String>> getPaymentAdviceCheckNumbers(){
		logger.debug("Request to fetch all the paymentAdvice check numbers");
		List<String> checkNumberList = this.payAdviceService.getCheckNumberList();
		return new ResponseEntity(checkNumberList,HttpStatus.OK);
	}
	
	@GetMapping("/getpaymentadvicedetail/{id}")
	public ResponseEntity<PaymentAdviceDTO> getPaymentAdvice(@PathVariable Integer id){
		logger.debug("Request to fetch payment advice details by id");
		PaymentAdviceDTO paymentAdviceDTO = this.payAdviceService.getPaymentAdvice(id);
		return new ResponseEntity<PaymentAdviceDTO>(paymentAdviceDTO,HttpStatus.OK);
		
	}
	@PutMapping("/updatePaymentAdvice/{empId}")
	public ResponseEntity<ApiResponse> updatePaymentAdvice(@PathVariable Integer empId, @RequestBody PaymentAdviceRequest paymentAdviceRequest ){
		logger.debug("Request to update the payment advice");
		ApiResponse apiResponse = null;
	    HttpStatus httpStatus = null;
	    if(paymentAdviceRequest.getId() == null || paymentAdviceRequest.getId() == 0 || !this.payAdviceService.isAdviceExists(paymentAdviceRequest.getId()) ) {
	    	apiResponse = new ApiResponse(AppConstants.ERROR, "Payment advice with the given id not found");
	    	httpStatus = HttpStatus.BAD_REQUEST;
	    }else {
			PaymentAdvice paymentAdvice = this.payAdviceService.updatePaymentAdvice(paymentAdviceRequest, empId);
             if(paymentAdvice != null) {
            	 apiResponse = new ApiResponse(AppConstants.SUCCES, "Payment advice updated successfully");
     	    	httpStatus = HttpStatus.OK;
             }else {
            	 apiResponse = new ApiResponse(AppConstants.ERROR, "Error occured while updating payment advice");
     	    	httpStatus = HttpStatus.BAD_REQUEST;
             }
	    }
	    return new ResponseEntity(apiResponse, httpStatus);
	    
	}
	@GetMapping("/approvepaymentadvicebyemail/{status}/{empId}/{id}")
	public ResponseEntity<ApiResponse> updatePaymentAdviceByEmail(@PathVariable String status,@PathVariable String empId,@PathVariable String id ) throws NotFoundException, InvalidRequestException, ApprovalNotPossibleException{
		logger.debug("Request to update the payment advice");
		ApiResponse apiResponse = null;
	    HttpStatus httpStatus = null;
	    PaymentAdvice paymentAdvice = this.payAdviceService.updatePaymentAdviceStatusById(Integer.valueOf(Encryptor.decode(id)), Encryptor.decode(status), Integer.valueOf(Encryptor.decode(empId)));
	    if(paymentAdvice == null) {
	    	apiResponse = new ApiResponse(AppConstants.ERROR, "Error occured while updating payment advice");
 	    	httpStatus = HttpStatus.BAD_REQUEST;
	    }else {
	    	httpStatus = HttpStatus.OK;
	    	if (paymentAdvice.getAprrovalStatus().getId() == 2) {
				apiResponse = new ApiResponse(AppConstants.SUCCES, "Request approved");
			} else if (paymentAdvice.getAprrovalStatus().getId() == 3) {
				apiResponse = new ApiResponse(AppConstants.SUCCES, "Request rejected");
			} else if (paymentAdvice.getAprrovalStatus().getId() == 4) {
				apiResponse = new ApiResponse(AppConstants.SUCCES, "Payment processed by finance");
			} else if (paymentAdvice.getAprrovalStatus().getId() == 5) {
				apiResponse = new ApiResponse(AppConstants.SUCCES, "Payment rejected by finance");
			}
			else if (paymentAdvice.getAprrovalStatus().getId() == 6) {
				apiResponse = new ApiResponse(AppConstants.SUCCES, "Payment advice approved and forwarded");
			}
			else if (paymentAdvice.getAprrovalStatus().getId() == 7) {
				apiResponse = new ApiResponse(AppConstants.SUCCES, "Payment advice forward request approved ");
			}else if (paymentAdvice.getAprrovalStatus().getId() == 8) {
				apiResponse = new ApiResponse(AppConstants.SUCCES, "Payment advice forward request rejected");
			}
	    }
	    return new ResponseEntity(apiResponse, httpStatus);
	    
	}

	@PostMapping("/updatepaymentadvicestatus")
	public ResponseEntity<ApiResponse> updatePaymentadviceStatus(
			@RequestBody PaymentAdviceApproovalRequest paymentAdviceApproovalRequest) throws ApprovalNotPossibleException {
		logger.debug("Request to update payment advice status");
		ApiResponse apiResponse = null;
		HttpStatus httpStatus = HttpStatus.OK;
		if (paymentAdviceApproovalRequest.getAdviceId() == null || paymentAdviceApproovalRequest.getEmpId() == null
				|| paymentAdviceApproovalRequest.getAprrovalStatus() == null) {
			apiResponse = new ApiResponse(AppConstants.ERROR, "Invalid request");
			httpStatus = HttpStatus.BAD_REQUEST;
			return new ResponseEntity<ApiResponse>(apiResponse, httpStatus);
		}
		PaymentAdvice paymentAdvice = this.payAdviceService.updatePaymentAdviceStatus(paymentAdviceApproovalRequest);
		if (paymentAdvice.getAprrovalStatus().getId() == 2) {
			apiResponse = new ApiResponse(AppConstants.SUCCES, "Request approved");
		} else if (paymentAdvice.getAprrovalStatus().getId() == 3) {
			apiResponse = new ApiResponse(AppConstants.SUCCES, "Request rejected");
		} else if (paymentAdvice.getAprrovalStatus().getId() == 4) {
			apiResponse = new ApiResponse(AppConstants.SUCCES, "Payment processed by finance");
		} else if (paymentAdvice.getAprrovalStatus().getId() == 5) {
			apiResponse = new ApiResponse(AppConstants.SUCCES, "Payment rejected by finance");
		}
		else if (paymentAdvice.getAprrovalStatus().getId() == 6) {
			apiResponse = new ApiResponse(AppConstants.SUCCES, "Payment advice approved and forwarded");
		}
		else if (paymentAdvice.getAprrovalStatus().getId() == 7) {
			apiResponse = new ApiResponse(AppConstants.SUCCES, "Payment advice forward request approved ");
		}else if (paymentAdvice.getAprrovalStatus().getId() == 8) {
			apiResponse = new ApiResponse(AppConstants.SUCCES, "Payment advice forward request rejected");
		}
		return new ResponseEntity<ApiResponse>(apiResponse, httpStatus);
	}
	@CrossOrigin(origins = "*")
	 @GetMapping("/download-files/{id}")
	    public ResponseEntity getOrderFormFile(@PathVariable("id") Integer id, HttpServletRequest request) throws IOException {
	        logger.debug("REST request to download file : {}", id);
	        PaymentAdviceDocuments document = payAdviceService.getDocument(id);
	        ResponseEntity response;
	        HttpHeaders headers = new HttpHeaders();
	        String pdfFile =  document.getDocumentpath();
	        Path path = Paths.get(adviceFilePath+pdfFile);
	        byte[] contents = Files.readAllBytes(path);
	        headers.add("file", document.getDocumentpath());
	        headers.setContentType(MediaType.parseMediaType("application/octet-stream"));
	        headers.setContentDispositionFormData(document.getDocumentpath(), document.getDocumentpath());
	        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
	        response = new ResponseEntity(contents, headers, HttpStatus.OK);
	        return response;
	    }
	
	

}
