package com.scsoft.myport.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.scsoft.myport.error.NotFoundException;
import com.scsoft.myport.rest.response.ApiResponse;
import com.scsoft.myport.service.LookupService;
import com.scsoft.myport.service.DTO.DashboardCountDTO;

@RestController
@RequestMapping("/api")
public class LookupResource {
	private final Logger logger = LoggerFactory.getLogger(LookupResource.class);
	@Autowired
	private LookupService lookupService;
	
	@GetMapping("/dashboardcounts/{empId}")
	public ResponseEntity<DashboardCountDTO> getDashboardCounts(@PathVariable Integer empId ) throws NotFoundException{
		logger.debug("Fetching dashboard informations");
		DashboardCountDTO dashboardCountDTO = this.lookupService.getAllDashBoardCounts(empId);
		return new ResponseEntity(dashboardCountDTO,HttpStatus.OK);
	}

}
