package com.scsoft.myport.rest.request;

public class AniversaryWishRequest {
	private Integer for_emp;
	private Integer by_emp;
	private String wish;
	private Integer anniversary_year;

	public Integer getFor_emp() {
		return for_emp;
	}

	public void setFor_emp(Integer for_emp) {
		this.for_emp = for_emp;
	}

	public Integer getBy_emp() {
		return by_emp;
	}

	public void setBy_emp(Integer by_emp) {
		this.by_emp = by_emp;
	}

	public String getWish() {
		return wish;
	}

	public void setWish(String wish) {
		this.wish = wish;
	}

	public Integer getAnniversary_year() {
		return anniversary_year;
	}

	public void setAnniversary_year(Integer anniversary_year) {
		this.anniversary_year = anniversary_year;
	}

}
