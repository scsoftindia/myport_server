package com.scsoft.myport.rest.request;

public class FcmDetailRequest {
	private String fcm_id;
	private Integer emp_id;
	
	public String getFcm_id() {
		return fcm_id;
	}
	public void setFcm_id(String fcm_id) {
		this.fcm_id = fcm_id;
	}
	public Integer getEmp_id() {
		return emp_id;
	}
	public void setEmp_id(Integer emp_id) {
		this.emp_id = emp_id;
	}
	
}
