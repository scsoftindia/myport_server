package com.scsoft.myport.rest.request;

public class ApplyLeaveApproovalRequest {
	private Integer leaveRequestId;
	private Integer approverId;
	private String cancelReason;
	private Boolean isReject;
	private Boolean isRejectCancelLeave;
	private Boolean isApproveCancelLeave;
	private String canCelLeaveComment;

	public Integer getLeaveRequestId() {
		return leaveRequestId;
	}

	public void setLeaveRequestId(Integer leaveRequestId) {
		this.leaveRequestId = leaveRequestId;
	}

	public Integer getApproverId() {
		return approverId;
	}

	public void setApproverId(Integer approverId) {
		this.approverId = approverId;
	}

	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public Boolean getIsReject() {
		if(this.isReject == null) {
			this.isReject = false;
		}
		return isReject;
	}

	public void setIsReject(Boolean isReject) {
		if(this.isReject == null) {
			this.isReject = false;
		}
		this.isReject = isReject;
	}

	public Boolean getIsRejectCancelLeave() {
		if(this.isRejectCancelLeave == null) {
			this.isRejectCancelLeave = false;
		}
		return isRejectCancelLeave;
	}

	public void setIsRejectCancelLeave(Boolean isRejectCancelLeave) {
		this.isRejectCancelLeave = isRejectCancelLeave;
	}

	public Boolean getIsApproveCancelLeave() {
		if(this.isApproveCancelLeave == null) {
			this.isApproveCancelLeave = false;
		}
		return isApproveCancelLeave;
	}

	public void setIsApproveCancelLeave(Boolean isApproveCancelLeave) {
		this.isApproveCancelLeave = isApproveCancelLeave;
	}

	public String getCanCelLeaveComment() {
		return canCelLeaveComment;
	}

	public void setCanCelLeaveComment(String canCelLeaveComment) {
		this.canCelLeaveComment = canCelLeaveComment;
	}
	
	
	

}
