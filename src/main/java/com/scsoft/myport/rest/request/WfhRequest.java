package com.scsoft.myport.rest.request;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WfhRequest {
	@JsonProperty("reason")
	private String wfh_reason;
	@JsonProperty("empId")
	private Integer empId;
	@JsonProperty("addSupId")
	private Integer add_sup_id;
	private String fromDate;
	@JsonProperty("noOfDays")
	private Double no_of_days;
	private String toDate;

	public String getWfh_reason() {
		return wfh_reason;
	}

	public void setWfh_reason(String reason) {
		this.wfh_reason = reason;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public Integer getAdd_sup_id() {
		return add_sup_id;
	}

	public void setAdd_sup_id(Integer addSupId) {
		this.add_sup_id = addSupId;
	}

	public Double getNo_of_days() {
		return no_of_days;
	}

	public void setNo_of_days(Double no_of_days) {
		this.no_of_days = no_of_days;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

}
