package com.scsoft.myport.rest.request;

public class EditProfileRequest {
	private String temparotyAddress;
	private String permanantAdresss;
	private String phone;
	private Integer empId;

	public String getTemparotyAddress() {
		return temparotyAddress;
	}

	public void setTemparotyAddress(String temparotyAddress) {
		this.temparotyAddress = temparotyAddress;
	}

	public String getPermanantAdresss() {
		return permanantAdresss;
	}

	public void setPermanantAdresss(String permanantAdresss) {
		this.permanantAdresss = permanantAdresss;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	
	

}
