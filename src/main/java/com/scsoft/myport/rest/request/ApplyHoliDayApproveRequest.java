package com.scsoft.myport.rest.request;

public class ApplyHoliDayApproveRequest {
	private Integer holidayId;
	private Integer approverId;
	private String cancelReason;
	private Boolean isReject;
	private Boolean isRejectCancelLHoliday;
	private Boolean isApproveCancelHoliday;
	private String canCelHolidayComment;

	public Integer getHolidayId() {
		return holidayId;
	}

	public void setHolidayId(Integer holidayId) {
		this.holidayId = holidayId;
	}

	public Integer getApproverId() {
		return approverId;
	}

	public void setApproverId(Integer approverId) {
		this.approverId = approverId;
	}

	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public Boolean getIsReject() {
		if(this.isReject == null) {
			this.isReject = false;
		}
		return isReject;
	}

	public void setIsReject(Boolean isReject) {
		this.isReject = isReject;
	}

	public Boolean getIsRejectCancelLHoliday() {
		if(this.isRejectCancelLHoliday == null) {
			this.isRejectCancelLHoliday = false;
		}
		return isRejectCancelLHoliday;
	}

	public void setIsRejectCancelLHoliday(Boolean isRejectCancelLHoliday) {
		this.isRejectCancelLHoliday = isRejectCancelLHoliday;
	}

	public Boolean getIsApproveCancelHoliday() {
		if(this.isApproveCancelHoliday == null) {
			this.isApproveCancelHoliday = false;
		}
		return isApproveCancelHoliday;
	}

	public void setIsApproveCancelHoliday(Boolean isApproveCancelHoliday) {
		this.isApproveCancelHoliday = isApproveCancelHoliday;
	}

	public String getCanCelHolidayComment() {
		return canCelHolidayComment;
	}

	public void setCanCelHolidayComment(String canCelHolidayComment) {
		this.canCelHolidayComment = canCelHolidayComment;
	}
	

}
