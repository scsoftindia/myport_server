package com.scsoft.myport.rest.request;

import java.util.List;

public class HoliDayRequest {
	private Integer id;//null in case of fresh holiday request
	private Integer empId;
	private Integer holidayId;
	private Integer superVisorId;
    private Boolean isCancelHoliday;//true if holiday cancellation and false for new request
    private String cancelReaseon;//null if new holiday request
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getEmpId() {
		return empId;
	}
	public void setEmpId(Integer empId) {
		this.empId = empId;
	}
	

	public Integer getHolidayId() {
		return holidayId;
	}
	public void setHolidayId(Integer holidayId) {
		this.holidayId = holidayId;
	}
	public Integer getSuperVisorId() {
		return superVisorId;
	}
	public void setSuperVisorId(Integer superVisorId) {
		this.superVisorId = superVisorId;
	}
	public Boolean getIsCancelHoliday() {
		if(this.isCancelHoliday == null) {
			this.isCancelHoliday = false;
		}
		return isCancelHoliday;
	}
	public void setIsCancelHoliday(Boolean isCancelHoliday) {
		this.isCancelHoliday = isCancelHoliday;
	}
	public String getCancelReaseon() {
		return cancelReaseon;
	}
	public void setCancelReaseon(String cancelReaseon) {
		this.cancelReaseon = cancelReaseon;
	}

    
    
    

}
