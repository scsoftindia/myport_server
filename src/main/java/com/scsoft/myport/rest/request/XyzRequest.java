package com.scsoft.myport.rest.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class XyzRequest {
	@JsonProperty("latitude")
	private String a;
	@JsonProperty("longitude")
	private String b;
	@JsonProperty("empId")
	private Integer d;
	public String getA() {
		return a;
	}
	public void setA(String a) {
		this.a = a;
	}
	public String getB() {
		return b;
	}
	public void setB(String b) {
		this.b = b;
	}
	public Integer getD() {
		return d;
	}
	public void setD(Integer d) {
		this.d = d;
	}
	
	
}
