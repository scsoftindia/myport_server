package com.scsoft.myport.rest.request;

public class PaymentAdviceApproovalRequest {
	private Integer empId;
	private Integer adviceId;
	private AprrovalStatus aprrovalStatus; 
	private String rejectReason;
	private String aprrovalNote;
	private String releaseNote;
	private String releaseRejectNote;
	private String forward_approve_comment;
	
	public Integer getEmpId() {
		return empId;
	}


	public void setEmpId(Integer empId) {
		this.empId = empId;
	}


	public Integer getAdviceId() {
		return adviceId;
	}


	public void setAdviceId(Integer adviceId) {
		this.adviceId = adviceId;
	}


	public AprrovalStatus getAprrovalStatus() {
		return aprrovalStatus;
	}


	public void setAprrovalStatus(AprrovalStatus aprrovalStatus) {
		this.aprrovalStatus = aprrovalStatus;
	}
	
	
	public String getRejectReason() {
		return rejectReason;
	}


	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}


	public String getAprrovalNote() {
		return aprrovalNote;
	}


	public void setAprrovalNote(String aprrovalNote) {
		this.aprrovalNote = aprrovalNote;
	}




	public String getReleaseNote() {
		return releaseNote;
	}


	public void setReleaseNote(String releaseNote) {
		this.releaseNote = releaseNote;
	}


	public String getReleaseRejectNote() {
		return releaseRejectNote;
	}


	public void setReleaseRejectNote(String releaseRejectNote) {
		this.releaseRejectNote = releaseRejectNote;
	}


	


	public String getForward_approve_comment() {
		return forward_approve_comment;
	}


	public void setForward_approve_comment(String forward_approve_comment) {
		this.forward_approve_comment = forward_approve_comment;
	}





	public enum AprrovalStatus {
		APRROVED,
		REJECT,
		PROCESSED_BY_FINANCE,
		REJECTED_BY_FINANCE,
		APPRROVE_AND_FORWARD,
		FORWARD_REJECT,
		FORWARD_APRROVED,
		NON_APRROVAL;
		
	}

}
