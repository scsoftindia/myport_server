package com.scsoft.myport.rest.request;

public class WfhApproveRequest {
	private Integer id;
	private Integer approverId;
	private Type type;
	private Status status;
	private String comment;
	public enum Type{
		NEW,
		CANCELL
	}
	public enum Status{
		Approved,
		Rejected
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getApproverId() {
		return approverId;
	}
	public void setApproverId(Integer approverId) {
		this.approverId = approverId;
	}
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	
	

}
