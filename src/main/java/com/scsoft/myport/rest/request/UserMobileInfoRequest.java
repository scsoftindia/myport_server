package com.scsoft.myport.rest.request;


public class UserMobileInfoRequest {

		private String model;
		private String device;
		private String manufacturer;
		private String brand;
		private String serial_number;
		private String macAdress;
		private String sim_one_imei;
		private String sim_two_imei;
		private Integer emp_id;
		private String carrier_name;
		private String carrier_operator;
		
		public String getModel() {
			return model;
		}
		public void setModel(String model) {
			this.model = model;
		}
		public String getDevice() {
			return device;
		}
		public void setDevice(String device) {
			this.device = device;
		}
		public String getManufacturer() {
			return manufacturer;
		}
		public void setManufacturer(String manufacturer) {
			this.manufacturer = manufacturer;
		}
		public String getBrand() {
			return brand;
		}
		public void setBrand(String brand) {
			this.brand = brand;
		}
		public String getSerial_number() {
			return serial_number;
		}
		public void setSerial_number(String serial) {
			this.serial_number = serial;
		}
		public String getMacAdress() {
			return macAdress;
		}
		public void setMacAdress(String mac_adress) {
			this.macAdress = mac_adress;
		}
		public String getSim_one_imei() {
			return sim_one_imei;
		}
		public void setSim_one_imei(String sim_one_imei) {
			this.sim_one_imei = sim_one_imei;
		}
		public String getSim_two_imei() {
			return sim_two_imei;
		}
		public void setSim_two_imei(String sim_two_imei) {
			this.sim_two_imei = sim_two_imei;
		}
		public Integer getEmp_id() {
			return emp_id;
		}
		public void setEmp_id(Integer emp_id) {
			this.emp_id = emp_id;
		}
		public String getCarrier_name() {
			return carrier_name;
		}
		public void setCarrier_name(String carrier_name) {
			this.carrier_name = carrier_name;
		}
		public String getCarrier_operator() {
			return carrier_operator;
		}
		public void setCarrier_operator(String carrier_operator) {
			this.carrier_operator = carrier_operator;
		}
		
		
		

		
}
