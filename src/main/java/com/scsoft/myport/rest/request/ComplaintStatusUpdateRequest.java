package com.scsoft.myport.rest.request;

public class ComplaintStatusUpdateRequest {
	private Integer complaintId;
	private Integer empId;
	private String comment;
	private Status status;
	
	public enum Status{
		Forwarded,
		Yes,
		No
	}

	public Integer getComplaintId() {
		return complaintId;
	}

	public void setComplaintId(Integer complaintId) {
		this.complaintId = complaintId;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	

}
