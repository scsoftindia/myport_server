package com.scsoft.myport.rest.request;

public class PayadviceCategoriesAproveRequest {
	private Integer id;
	private Status status;
	private Integer empId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public enum Status {
		APPROVE, REJECT
	}

}
