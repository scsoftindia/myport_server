package com.scsoft.myport.rest.request;

import java.util.Date;


public class AnnouncementRequest {

	private String announcementHeading;
	private String announcement;
	private Integer announcementBy;
	private String startDate;
	private String announcementDate;
	public String getAnnouncementHeading() {
		return announcementHeading;
	}
	public void setAnnouncementHeading(String announcementHeading) {
		this.announcementHeading = announcementHeading;
	}
	public String getAnnouncement() {
		return announcement;
	}
	public void setAnnouncement(String announcement) {
		this.announcement = announcement;
	}
	public Integer getAnnouncementBy() {
		return announcementBy;
	}
	public void setAnnouncementBy(Integer announcementBy) {
		this.announcementBy = announcementBy;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getAnnouncementDate() {
		return announcementDate;
	}
	public void setAnnouncementDate(String announcementDate) {
		this.announcementDate = announcementDate;
	}
	
	

}
