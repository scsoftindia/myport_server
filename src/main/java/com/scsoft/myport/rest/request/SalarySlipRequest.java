package com.scsoft.myport.rest.request;

import java.util.List;

public class SalarySlipRequest {
	private Integer id;
	private Integer requested_by;
	private Integer request_year;
	private List<Integer> request_months;
	private String request_reason;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getRequested_by() {
		return requested_by;
	}
	public void setRequested_by(Integer requested_by) {
		this.requested_by = requested_by;
	}
	public Integer getRequest_year() {
		return request_year;
	}
	public void setRequest_year(Integer request_year) {
		this.request_year = request_year;
	}
	public List<Integer> getRequest_months() {
		return request_months;
	}
	public void setRequest_months(List<Integer> request_months) {
		this.request_months = request_months;
	}
	public String getRequest_reason() {
		return request_reason;
	}
	public void setRequest_reason(String request_reason) {
		this.request_reason = request_reason;
	}
	
	
}
