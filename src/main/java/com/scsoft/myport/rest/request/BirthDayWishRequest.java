package com.scsoft.myport.rest.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BirthDayWishRequest {
	@JsonProperty("bday_year")
	private int bday_year;
	@JsonProperty("for_emp")
	private int for_emp;
	@JsonProperty("by_emp")
	private int by_emp;
	@JsonProperty("wish")
	private String wish;

	public int getBday_year() {
		return bday_year;
	}

	public void setBday_year(int bday_year) {
		this.bday_year = bday_year;
	}

	public int getFor_emp() {
		return for_emp;
	}

	public void setFor_emp(int for_emp) {
		this.for_emp = for_emp;
	}

	public int getBy_emp() {
		return by_emp;
	}

	public void setBy_emp(int by_emp) {
		this.by_emp = by_emp;
	}

	public String getWish() {
		return wish;
	}

	public void setWish(String wish) {
		this.wish = wish;
	}
}
