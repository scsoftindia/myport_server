package com.scsoft.myport.rest.request;

import com.scsoft.myport.domain.PayadviceCategories;

public class PayadviceSubCategoriesRequest {
	private Integer id;
	private Integer pay_advice_cat_id;
	private String pay_subcategory_name;
	private String pay_subcategory_description;
	private Integer emp_id;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getPay_advice_cat_id() {
		return pay_advice_cat_id;
	}

	public void setPay_advice_cat_id(Integer pay_advice_cat_id) {
		this.pay_advice_cat_id = pay_advice_cat_id;
	}

	public String getPay_subcategory_name() {
		return pay_subcategory_name;
	}

	public void setPay_subcategory_name(String pay_subcategory_name) {
		this.pay_subcategory_name = pay_subcategory_name;
	}

	public String getPay_subcategory_description() {
		return pay_subcategory_description;
	}

	public void setPay_subcategory_description(String pay_subcategory_description) {
		this.pay_subcategory_description = pay_subcategory_description;
	}

	public Integer getEmp_id() {
		return emp_id;
	}

	public void setEmp_id(Integer emp_id) {
		this.emp_id = emp_id;
	}
	

}
