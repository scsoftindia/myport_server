package com.scsoft.myport.rest.request;

import java.io.File;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.scsoft.myport.domain.CurrencyType;
import com.scsoft.myport.domain.EmpEmployment;
import com.scsoft.myport.domain.PayadviceCategories;
import com.scsoft.myport.domain.PayadviceSubCategories;
import com.scsoft.myport.domain.PaymentBank;
import com.scsoft.myport.domain.PaymentModes;
import com.scsoft.myport.domain.Vendors;

public class PaymentAdviceRequest {
	private Integer id;
	private Integer appliedEmpId;
	private PayadviceCategories payadviceCategories;
	private Integer categoryId;
	private PayadviceSubCategories payadviceSubCategories;
	private Integer subCategoryId;
	private Vendors vendors;
	private Integer vendorId;
	private Date bill_date;
	private Date due_date;
	private String bill_number;
	private double bill_amount;
	private PaymentModes paymentModes;
	private Integer paymentModeId;
	@JsonInclude(Include.NON_NULL)
	private EmpEmployment paymentReleaseBy;
	private String payment_cash_billno;
	private String payment_cheque_number;
	private Date payment_cheque_date;
	private Date payment_online_date;
	private String rejected_reason;
	private Date bill_period_from;
	private Date bill_peroid_to;
	private String description;
	private CurrencyType currencyType;
	private Integer currencyTypeId;
	private Integer index;
	@JsonInclude(Include.NON_NULL)
	private Integer primaryAprooverId;
	private Integer paymentBankId;
	private String paymentRemark;
	private String releaseNote;
	@JsonInclude(Include.NON_NULL)
	private PaymentAdviceApproovalRequest.AprrovalStatus aprrovalStatus;
	private Boolean isUpdateByFinance;
	private Boolean isUpdateProcess;
	private PaymentBank paymentBank;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getAppliedEmpId() {
		return appliedEmpId;
	}
	public void setAppliedEmpId(Integer appliedEmpId) {
		this.appliedEmpId = appliedEmpId;
	}
	public PayadviceCategories getPayadviceCategories() {
		return payadviceCategories;
	}
	public void setPayadviceCategories(PayadviceCategories payadviceCategories) {
		this.payadviceCategories = payadviceCategories;
	}
	public PayadviceSubCategories getPayadviceSubCategories() {
		return payadviceSubCategories;
	}
	public void setPayadviceSubCategories(PayadviceSubCategories payadviceSubCategories) {
		this.payadviceSubCategories = payadviceSubCategories;
	}
	public Vendors getVendors() {
		return vendors;
	}
	public void setVendors(Vendors vendors) {
		this.vendors = vendors;
	}
	public Date getBill_date() {
		return bill_date;
	}
	public void setBill_date(Date bill_date) {
		this.bill_date = bill_date;
	}
	
	public Date getDue_date() {
		return due_date;
	}
	public void setDue_date(Date due_date) {
		this.due_date = due_date;
	}
	public String getBill_number() {
		return bill_number;
	}
	public void setBill_number(String bill_number) {
		this.bill_number = bill_number;
	}
	public double getBill_amount() {
		return bill_amount;
	}
	public void setBill_amount(double bill_amount) {
		this.bill_amount = bill_amount;
	}
	public PaymentModes getPaymentModes() {
		return paymentModes;
	}
	public void setPaymentModes(PaymentModes paymentModes) {
		this.paymentModes = paymentModes;
	}
	public EmpEmployment getPaymentReleaseBy() {
		return paymentReleaseBy;
	}
	public void setPaymentReleaseBy(EmpEmployment paymentReleaseBy) {
		this.paymentReleaseBy = paymentReleaseBy;
	}
	public String getPayment_cash_billno() {
		return payment_cash_billno;
	}
	public void setPayment_cash_billno(String payment_cash_billno) {
		this.payment_cash_billno = payment_cash_billno;
	}
	public String getPayment_cheque_number() {
		return payment_cheque_number;
	}
	public void setPayment_cheque_number(String payment_cheque_number) {
		this.payment_cheque_number = payment_cheque_number;
	}
	public Date getPayment_cheque_date() {
		return payment_cheque_date;
	}
	public void setPayment_cheque_date(Date payment_cheque_date) {
		this.payment_cheque_date = payment_cheque_date;
	}
	public Date getPayment_online_date() {
		return payment_online_date;
	}
	public void setPayment_online_date(Date payment_online_date) {
		this.payment_online_date = payment_online_date;
	}
	public String getRejected_reason() {
		return rejected_reason;
	}
	public void setRejected_reason(String rejected_reason) {
		this.rejected_reason = rejected_reason;
	}
	public Date getBill_period_from() {
		return bill_period_from;
	}
	public void setBill_period_from(Date bill_period_from) {
		this.bill_period_from = bill_period_from;
	}
	public Date getBill_peroid_to() {
		return bill_peroid_to;
	}
	public void setBill_peroid_to(Date bill_peroid_to) {
		this.bill_peroid_to = bill_peroid_to;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	public Integer getSubCategoryId() {
		return subCategoryId;
	}
	public void setSubCategoryId(Integer subCategoryId) {
		this.subCategoryId = subCategoryId;
	}
	public Integer getVendorId() {
		return vendorId;
	}
	public void setVendorId(Integer vendorId) {
		this.vendorId = vendorId;
	}
	public Integer getPaymentModeId() {
		return paymentModeId;
	}
	public void setPaymentModeId(Integer paymentModeId) {
		this.paymentModeId = paymentModeId;
	}
	public CurrencyType getCurrencyType() {
		return currencyType;
	}
	public void setCurrencyType(CurrencyType currencyType) {
		this.currencyType = currencyType;
	}
	public Integer getCurrencyTypeId() {
		return currencyTypeId;
	}
	public void setCurrencyTypeId(Integer currencyTypeId) {
		this.currencyTypeId = currencyTypeId;
	}
	public Integer getIndex() {
		return index;
	}
	public void setIndex(Integer index) {
		this.index = index;
	}
	public Integer getPrimaryAprooverId() {
		return primaryAprooverId;
	}
	public void setPrimaryAprooverId(Integer primaryAprooverId) {
		this.primaryAprooverId = primaryAprooverId;
	}
	public Integer getPaymentBankId() {
		return paymentBankId;
	}
	public void setPaymentBankId(Integer paymentBankId) {
		this.paymentBankId = paymentBankId;
	}
	public String getPaymentRemark() {
		return paymentRemark;
	}
	public void setPaymentRemark(String paymentRemark) {
		this.paymentRemark = paymentRemark;
	}
	public String getReleaseNote() {
		return releaseNote;
	}
	public void setReleaseNote(String releaseNote) {
		this.releaseNote = releaseNote;
	}
	public PaymentAdviceApproovalRequest.AprrovalStatus getAprrovalStatus() {
		return aprrovalStatus;
	}
	public void setAprrovalStatus(PaymentAdviceApproovalRequest.AprrovalStatus aprrovalStatus) {
		this.aprrovalStatus = aprrovalStatus;
	}
	public Boolean getIsUpdateByFinance() {
		if(this.isUpdateByFinance == null) {
			this.isUpdateByFinance = false;
		}
		return isUpdateByFinance;
	}
	public void setIsUpdateByFinance(Boolean isUpdateByFinance) {
		this.isUpdateByFinance = isUpdateByFinance;
	}
	public Boolean getIsUpdateProcess() {
		if(isUpdateProcess == null) {
			isUpdateProcess = false;
		}
		return isUpdateProcess;
	}
	public void setIsUpdateProcess(Boolean isUpdateProcess) {
		this.isUpdateProcess = isUpdateProcess;
	}
	public PaymentBank getPaymentBank() {
		return paymentBank;
	}
	public void setPaymentBank(PaymentBank paymentBank) {
		this.paymentBank = paymentBank;
	}
	
	
	
	
	
	
	
	
	
	

	
}
