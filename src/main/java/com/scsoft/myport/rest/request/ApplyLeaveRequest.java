package com.scsoft.myport.rest.request;

import java.math.BigDecimal;
import java.util.Date;

import com.scsoft.myport.domain.LeaveTypes;

public class ApplyLeaveRequest {
	private Integer id;
	private LeaveTypes leaveTypes;
	private Date fomDate;
	private Date toDate;
	private BigDecimal noOfDays;
	private String reason;
	private String acceptLop;
	private int addSupId;
	private Integer supervisorId;
	private Integer empId;
	private Boolean isHalfDay;
	private Boolean isLeaveCancel;
	private String cancelReason;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LeaveTypes getLeaveTypes() {
		return leaveTypes;
	}

	public void setLeaveTypes(LeaveTypes leaveTypes) {
		this.leaveTypes = leaveTypes;
	}
	
	public Date getFomDate() {
		return fomDate;
	}

	public void setFomDate(Date fomDate) {
		this.fomDate = fomDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public BigDecimal getNoOfDays() {
		return noOfDays;
	}

	public void setNoOfDays(BigDecimal noOfDays) {
		this.noOfDays = noOfDays;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getAcceptLop() {
		return acceptLop;
	}

	public void setAcceptLop(String acceptLop) {
		this.acceptLop = acceptLop;
	}

	public int getAddSupId() {
		return addSupId;
	}

	public void setAddSupId(int addSupId) {
		this.addSupId = addSupId;
	}

	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public Boolean getIsLeaveCancel() {
		if(this.isLeaveCancel == null) {
			this.isLeaveCancel = false;
		}
		return isLeaveCancel;
	}

	public void setIsLeaveCancel(Boolean isLeaveCancel) {
		this.isLeaveCancel = isLeaveCancel;
	}

	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public Integer getSupervisorId() {
		return supervisorId;
	}

	public void setSupervisorId(Integer supervisorId) {
		this.supervisorId = supervisorId;
	}

	public Boolean getIsHalfDay() {
		if(this.isHalfDay == null) {
			this.isHalfDay = false;
		}
		return isHalfDay;
	}

	public void setIsHalfDay(Boolean isHalfDay) {
		this.isHalfDay = isHalfDay;
	}
	
	
}
