package com.scsoft.myport.rest.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PayadviceCategoriesRequest {
	@JsonProperty("id")
	private Integer id;
	@JsonProperty("cat_name")
	private String cat_name;
	@JsonProperty("cat_description")
	private String cat_description;
	@JsonProperty("emp_id")
	private Integer emp_id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCat_name() {
		return cat_name;
	}

	public void setCat_name(String cat_name) {
		this.cat_name = cat_name;
	}

	public String getCat_description() {
		return cat_description;
	}

	public void setCat_description(String cat_description) {
		this.cat_description = cat_description;
	}

	public Integer getEmp_id() {
		return emp_id;
	}

	public void setEmp_id(Integer emp_id) {
		this.emp_id = emp_id;
	}
	

}
