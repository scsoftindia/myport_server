package com.scsoft.myport.rest.request;

public class WfhCancellRequest {
	private String cancelReason;
	private Integer id;

	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
