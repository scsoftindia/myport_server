package com.scsoft.myport.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.scsoft.myport.domain.AlbumImages;
import com.scsoft.myport.domain.Albums;
import com.scsoft.myport.service.GallaryService;

@RestController
@RequestMapping("/api")
public class GallaryResource {
	private final Logger logger = LoggerFactory.getLogger(GallaryResource.class);

	@Autowired
	private GallaryService gallaryService;

	@GetMapping("/getalbums")
	public ResponseEntity<List<Albums>> getAllAlbums() {
		logger.debug("Request to fetch all albums");
		List<Albums> albums = this.gallaryService.getAllAlbums();
		return new ResponseEntity(albums, HttpStatus.OK);
	}
	
	@GetMapping("/getalbumimages/{albumId}")
	public ResponseEntity<List<AlbumImages>> getAllAbumImages(@PathVariable Integer albumId) {
		logger.debug("Request to fetch all images from album");
		List<AlbumImages> albumImages = this.gallaryService.getAlbumImages(albumId);
		return new ResponseEntity(albumImages, HttpStatus.OK);
	}

}
