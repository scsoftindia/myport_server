package com.scsoft.myport.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.scsoft.myport.domain.BirthdayWishes;
import com.scsoft.myport.error.InvalidRequestException;
import com.scsoft.myport.error.NotFoundException;
import com.scsoft.myport.error.OperationNotPossibleException;
import com.scsoft.myport.rest.request.BirthDayWishRequest;
import com.scsoft.myport.rest.response.ApiResponse;
import com.scsoft.myport.service.BirthDaysService;
import com.scsoft.myport.service.DTO.BirthDaysDTO;
import com.scsoft.myport.service.util.AppConstants;

@RestController
@RequestMapping("/api")
public class BirthDaysResource {
	private final Logger logger = LoggerFactory.getLogger(BirthDaysResource.class);
	@Autowired
	private BirthDaysService birthDaysService;

	@GetMapping("/birthdays")
	public List<BirthDaysDTO> getAllAnnouncements() {
		logger.debug("Request to fetch all BirthDays");
		return birthDaysService.getUpcomingBirthDays();
	}

	@PostMapping("/sendbdaywishes")
	public ResponseEntity<ApiResponse> sendBirthDayWishes(@RequestBody BirthDayWishRequest birthDayWishRequest) throws NotFoundException, InvalidRequestException, OperationNotPossibleException {
		logger.debug("Request to Save BirthDay wish");
		BirthdayWishes birthdayWishes = birthDaysService.saveBirthdayWishes(birthDayWishRequest);
		HttpStatus httpStatus = null;
		ApiResponse apiResponse = null;
		if (birthdayWishes.getWish_id() != null) {
			apiResponse = new ApiResponse(AppConstants.SUCCES, "Birthday wish send successfully");
			httpStatus = HttpStatus.OK;
		} else {
			apiResponse = new ApiResponse(AppConstants.ERROR, "Error occurred  while sending birthday wish");
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		return new ResponseEntity(apiResponse, httpStatus);
	}
}
