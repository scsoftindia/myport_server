package com.scsoft.myport.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.scsoft.myport.domain.CurrencyType;
import com.scsoft.myport.domain.PayadviceCategories;
import com.scsoft.myport.domain.PayadviceSubCategories;
import com.scsoft.myport.domain.PaymentModes;
import com.scsoft.myport.domain.Vendors;
import com.scsoft.myport.rest.request.FcmDetailRequest;
import com.scsoft.myport.rest.response.ApiResponse;
import com.scsoft.myport.service.PaymentLookupService;
import com.scsoft.myport.service.DTO.ApprovalCountDTO;
import com.scsoft.myport.service.DTO.AprooverDTO;
import com.scsoft.myport.service.DTO.PayadviceCategoriesDTO;
import com.scsoft.myport.service.DTO.PaymentBankDTO;
import com.scsoft.myport.service.util.AppConstants;

@RestController
@RequestMapping("/api")
public class PaymentLookupResource {
	private final Logger logger = LoggerFactory.getLogger(PaymentLookupResource.class);
	@Autowired
	private PaymentLookupService paymentLookupService;
	
//	@CrossOrigin
	@GetMapping("/paymentmodes")
	public ResponseEntity<List<PaymentModes>> getALlPaymentModes() {
		logger.debug("Request to fetch all  the payment modes");
		List<PaymentModes> paymentModes = paymentLookupService.getAllPayments();
		return new ResponseEntity<>(paymentModes, HttpStatus.OK);
	}

	@GetMapping("/payadvicecategories")
	public ResponseEntity<List<PayadviceCategories>> getAllPayAdviceCategories() {
		logger.debug("Request to fetch all the Pay advice Categories");
		List<PayadviceCategories> payadviceCategories = paymentLookupService.getAllCategories();
		return new ResponseEntity<List<PayadviceCategories>>(payadviceCategories, HttpStatus.OK);
	}
	@GetMapping("/getcategory/{id}")
	public ResponseEntity<PayadviceCategories> getCategoryById(@PathVariable("id") Integer id) {
		logger.debug("Request to fetch  Pay advice Categories by id");
		PayadviceCategories payadviceCategories = paymentLookupService.getCategoryById(id);
		return new ResponseEntity<PayadviceCategories>(payadviceCategories, HttpStatus.OK);
	}
	@GetMapping("/getsubcategory/{id}")
	public ResponseEntity<PayadviceSubCategories> getSubCategoryById(@PathVariable("id") Integer id) {
		logger.debug("Request to fetch  Pay advice sub Categories by id");
		PayadviceSubCategories payadviceSubCategories = paymentLookupService.getSubCategoryById(id);
		return new ResponseEntity<PayadviceSubCategories>(payadviceSubCategories, HttpStatus.OK);
	}
	@GetMapping("/getvendor/{id}")
	public ResponseEntity<Vendors> getVendorById(@PathVariable("id") Integer id) {
		logger.debug("Request to fetch Pay advice Vendor by id");
		Vendors vendors = paymentLookupService.getVendorsById(id);
		return new ResponseEntity<Vendors>(vendors, HttpStatus.OK);
	}
	
	@GetMapping("/payadvicesubcategories/{catId}")
	public ResponseEntity<List<PayadviceSubCategories>> getAllPayAdviceSubCategories(@PathVariable("catId") Integer catId){
		logger.debug("Request to fetch all sub categories by category id");
		List<PayadviceSubCategories> payadviceSubCategories = paymentLookupService.getAllSubCategoriesByCategories(catId);
		return new ResponseEntity<List<PayadviceSubCategories>>(payadviceSubCategories, HttpStatus.OK);
		
	}
	@GetMapping("/vendors/{catId}")
	public ResponseEntity<List<Vendors>> getAllVendors(@PathVariable("catId") Integer catId){
		logger.debug("REquest to fetch All the vendors by category and sub category");
		List<Vendors> vendors = paymentLookupService.getAllVendors(catId);
		return new ResponseEntity<>(vendors,HttpStatus.OK);
	}
	@GetMapping("/currencytypes")
	public ResponseEntity<List<CurrencyType>> getALLCurrencyTypes(){
		logger.debug("Request to fetch All the vendors by category and sub category");
		List<CurrencyType> currencyTypes = paymentLookupService.getAllCurrencyTypes();
		return new ResponseEntity<>(currencyTypes,HttpStatus.OK);
	}
	@GetMapping("/getpayadvicecategories")
	public ResponseEntity<List<PayadviceCategoriesDTO>> getPayadviceCategories(@RequestParam("size") Integer size,
			@RequestParam("pagenumber") Integer pagenumber, @RequestParam("sortdirection") String sortdirection,
			@RequestParam("sortProperty") String sortProperty,@RequestParam("isApproval") Boolean isApproval, @RequestParam(required = false) String searchTerm){
		logger.debug("Request to fetch All the Categories");
		Pageable pageable = new PageRequest(pagenumber, size,
				new Sort(Sort.Direction.fromString(sortdirection), sortProperty));
		HttpHeaders responseHeaders = new HttpHeaders();
		Page<PayadviceCategoriesDTO> payadviceCategoriesDTOs = this.paymentLookupService.getCategories(pageable,isApproval);
		responseHeaders.set("totalelements", String.valueOf((Long)payadviceCategoriesDTOs.getTotalElements()));
		responseHeaders.set("totalpages", String.valueOf((Integer)payadviceCategoriesDTOs.getTotalPages()));
		return new ResponseEntity<>(payadviceCategoriesDTOs.getContent(),responseHeaders,HttpStatus.OK);
	}
	@GetMapping("/getpayadvicesubcategories")
	public ResponseEntity<List<PayadviceSubCategories>> getPayadviceSubCategories(@RequestParam("size") Integer size,
			@RequestParam("pagenumber") Integer pagenumber, @RequestParam("sortdirection") String sortdirection,
			@RequestParam("sortProperty") String sortProperty, @RequestParam(required = false) Integer catId,@RequestParam("isApproval") Boolean isApproval){
		logger.debug("Request to fetch All the sub Categories");
		if("created_emp_full_name".equals(sortProperty)) {
			sortProperty = "createdBy.id";
		}else if("modified_emp_fullname".equals(sortProperty)) {
			sortProperty = "lastModifiedBy.id";
		}else if("category_name".equals(sortProperty)) {
			sortProperty = "payadviceCategories.cat_name";
		}
		Pageable pageable = new PageRequest(pagenumber, size,
				new Sort(Sort.Direction.fromString(sortdirection), sortProperty));
		HttpHeaders responseHeaders = new HttpHeaders();
		Page<PayadviceSubCategories> payadviceSubCategories = this.paymentLookupService.getAllSubCategories(catId, pageable,isApproval);
		responseHeaders.set("totalelements", String.valueOf((Long)payadviceSubCategories.getTotalElements()));
		responseHeaders.set("totalpages", String.valueOf((Integer)payadviceSubCategories.getTotalPages()));
		return new ResponseEntity<>(payadviceSubCategories.getContent(),responseHeaders,HttpStatus.OK);
	}
	
	@GetMapping("/getvendors")
	public ResponseEntity<List<Vendors>> getVendors(@RequestParam("size") Integer size,
			@RequestParam("pagenumber") Integer pagenumber, @RequestParam("sortdirection") String sortdirection,
			@RequestParam("sortProperty") String sortProperty, @RequestParam(required = false) Integer catId,@RequestParam("isApproval") Boolean isApproval){
		logger.debug("Request to fetch All the sub Categories");
		if("created_emp_full_name".equals(sortProperty)) {
			sortProperty = "createdBy.id";
		}else if("modified_emp_fullname".equals(sortProperty)) {
			sortProperty = "lastModifiedBy.id";
		}else if("category_name".equals(sortProperty)) {
			sortProperty = "payadviceCategories.cat_name";
		}
		Pageable pageable = new PageRequest(pagenumber, size,
				new Sort(Sort.Direction.fromString(sortdirection), sortProperty));
		HttpHeaders responseHeaders = new HttpHeaders();
		Page<Vendors> vendors = this.paymentLookupService.getAllVendors(catId, pageable,isApproval);
		responseHeaders.set("totalelements", String.valueOf((Long)vendors.getTotalElements()));
		responseHeaders.set("totalpages", String.valueOf((Integer)vendors.getTotalPages()));
		return new ResponseEntity<>(vendors.getContent(),responseHeaders,HttpStatus.OK);
	}
	
	@GetMapping("/getaproovarlist")
	public ResponseEntity<List<AprooverDTO>> getAproovarList(){
		logger.debug("Request to fetch all the aprooval details ");
		List<AprooverDTO> aprooverDTOs = this.paymentLookupService.getApproverList();
		return new ResponseEntity<List<AprooverDTO>>(aprooverDTOs, HttpStatus.OK);
	}
	@GetMapping("/getaproovalcount/{empId}")
	public ResponseEntity<ApprovalCountDTO> getAproovalCount(@PathVariable Integer empId){
		logger.debug("Request to fetch all  new category,sub category and vendors counts ");
		ApprovalCountDTO aprooverDTO = this.paymentLookupService.getApprovalCounts(empId);
		return new ResponseEntity<ApprovalCountDTO>(aprooverDTO, HttpStatus.OK);
	}
	
	
	@PostMapping("/savepushrequest")
	public ResponseEntity<ApiResponse> savePushRequest(@RequestBody FcmDetailRequest fcmDetailRequest){
		logger.debug("Request to save push request details");
		ApiResponse apiResponse = null;
		String result = this.paymentLookupService.savePushRequestDetails(fcmDetailRequest);
        if(result == "activated") {
        	apiResponse = new ApiResponse(AppConstants.SUCCES, "Push request activated");
        }else {
        	apiResponse = new ApiResponse(AppConstants.SUCCES, "Push request updated");
        }
		return new ResponseEntity<ApiResponse>(apiResponse,HttpStatus.OK);
	}
	@PostMapping("/savepaymentbank")
	public ResponseEntity<ApiResponse> saveBankInfo(@RequestBody PaymentBankDTO paymentBankDTO){
		logger.debug("request to save payment bank info");
		ApiResponse apiResponse = null;
		HttpStatus httpStatus = null;
		paymentBankDTO = this.paymentLookupService.savePaymentBank(paymentBankDTO);
		if(paymentBankDTO.getId() != null) {
			apiResponse = new ApiResponse(AppConstants.SUCCES, "Bank information saved");
			httpStatus = HttpStatus.OK;
		}else {
			apiResponse = new ApiResponse(AppConstants.ERROR, "Error occured while saving");
			httpStatus = HttpStatus.BAD_REQUEST;
		}
		return new ResponseEntity<ApiResponse>(apiResponse,httpStatus);
	}
	@GetMapping("/getpaymentbanks")
	public ResponseEntity<List<PaymentBankDTO>> getAllPaymentBanks(){
		logger.debug("Request to fetch all the payment banks");
		List<PaymentBankDTO> paymentBankDTOs = this.paymentLookupService.getAllPaymentBanks();
		return new ResponseEntity(paymentBankDTOs,HttpStatus.OK);
	}
	
	
	

}
