package com.scsoft.myport.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/api/**").allowedOrigins("*")
				.allowedMethods("PUT", "POST", "GET", "PATCH", "OPTIONS", "DELETE").allowCredentials(false)
				.exposedHeaders("totalelements", "totalpages", "file");
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {

		registry.addResourceHandler("/api/leavefiles/**")
				.addResourceLocations("file:/var/www/myport/assets/images/leave/");
		registry.addResourceHandler("/api/complaintfiles/**")
				.addResourceLocations("file:/var/www/myport/assets/images/complaints/");
		registry.addResourceHandler("/api/advicefiles/**")
				.addResourceLocations("file:/var/www/myport/assets/images/paymentadvice/");
		registry.addResourceHandler("/payment/**").addResourceLocations("classpath:/static/");
		
		
		/*
		 * registry.addResourceHandler("/api/leavefiles/**") .addResourceLocations(
		 * "file:/home/summitme/public_html/myport/assets/images/leave/");
		 * registry.addResourceHandler("/api/complaintfiles/**") .addResourceLocations(
		 * "file:/home/summitme/public_html/myport/assets/images/complaints/");
		 * registry.addResourceHandler("/api/advicefiles/**") .addResourceLocations(
		 * "file:/home/summitme/public_html/myport/assets/images/paymentadvice/");
		 * registry.addResourceHandler("/payment/**").addResourceLocations(
		 * "classpath:/static/");
		 */

		/*
		 * registry.addResourceHandler("/api/leavefiles/**").addResourceLocations(
		 * "file:E:/myport_files/");
		 * registry.addResourceHandler("/api/complaintfiles/**").addResourceLocations(
		 * "file:E:/myport_files/");
		 * registry.addResourceHandler("/api/advicefiles/**").addResourceLocations(
		 * "file:E:/myport_files/");
		 * registry.addResourceHandler("/payment/**").addResourceLocations(
		 * "classpath:/static/");
		 */
	}

//	 @Bean
//	    public ViewResolver getViewResolver() {
//	        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
//	        resolver.setPrefix("/");
//	        resolver.setSuffix(".html");
//	        return resolver;
//	    }
	// add a mapping for redirection to index when / requested
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/").setViewName("index");
		registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
	}

}
