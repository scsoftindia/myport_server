package com.scsoft.myport.configuration;


import java.util.Date;
import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EnableJpaRepositories("com.scsoft.myport.repository")
@EnableTransactionManagement
public class DatabaseConfiguration {
	
	 @PostConstruct
	    public void started(){
		 TimeZone zone = TimeZone.getTimeZone("Asia/Kolkata");
	        TimeZone.setDefault(zone);   // It will set Asia/Kolkata timezone
	        System.out.println("Spring boot application running in Indian timezone :"+new Date());   // It will print Asia/Kolkata timezone
	    }
	

}
