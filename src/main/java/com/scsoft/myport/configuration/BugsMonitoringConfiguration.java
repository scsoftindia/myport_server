package com.scsoft.myport.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.bugsnag.Bugsnag;

@Configuration
public class BugsMonitoringConfiguration {
	@Bean
    public Bugsnag bugsnag() {
        return new Bugsnag("8beafd943488387e9e7fa93e6b0b126a");
    }
}
