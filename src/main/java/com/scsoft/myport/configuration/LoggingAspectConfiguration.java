package com.scsoft.myport.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.scsoft.myport.apo.logging.LoggingAspect;


@Configuration
public class LoggingAspectConfiguration {
	  @Bean
	    public LoggingAspect loggingAspect() {
	        return new LoggingAspect();
	    }

}
